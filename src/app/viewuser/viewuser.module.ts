//import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//import { DashboardComponent } from './dashboard.component';
import { RouterModule } from '@angular/router';
import { ViewuserRoutes } from './viewuser.routing';
import { ViewuserComponent } from '../viewuser/viewuser.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { HttpModule } from '@angular/http';
import { DataTablesModule } from 'angular-datatables';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MembersComponent } from '../members/members.component';
import { AdminService } from '../services/admin.service';
import { FormsModule, FormBuilder, FormGroup, Validators, FormControl, ReactiveFormsModule } from '@angular/forms';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';

@NgModule({
  imports: [
    RouterModule.forChild(ViewuserRoutes),
    DataTablesModule,
    HttpClientModule,
    CommonModule,

    HttpModule,

    NgbModule,
    FormsModule, ReactiveFormsModule,
    //BrowserModule
  ],
  declarations: [

    ViewuserComponent

  ],

  providers: [AdminService, AuthGuard, NotAuthGuard],
  bootstrap: [ViewuserComponent]
})
export class ViewuserModule { }






