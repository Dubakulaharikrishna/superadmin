import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';

import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { Router, ActivatedRoute } from '@angular/router';
import { AdminService } from '../services/admin.service';
import { Members } from '../Models/Members';
import { retry } from 'rxjs/operator/retry';
import { DatePipe } from '@angular/common';
import { Status } from '../Models/Status';

@Component({
  selector: 'app-viewuser',
  templateUrl: './viewuser.component.html',
  styleUrls: ['./viewuser.component.css'],
  providers: [DatePipe]
})
export class ViewuserComponent implements OnInit {
  dataLoaded: any;
  imageurl: any;
  createdby: any;
  dobDate: any;
  public viewmembersUpdateFormGroup: FormGroup;
  public ChangePassword: FormGroup;
  public Sendnotificationsform: FormGroup;
  viewmembers: Members;
  id: any;
  memberImage: any;
  isSubmitted: boolean = false;
  isSubmittedpwd: boolean = false;
  memId: any;
  status: Status;
  Email: any;
  constructor(private adminservice: AdminService, private router: Router,
    public route: ActivatedRoute, public ele: ElementRef, private http: HttpClient, public fb: FormBuilder, public datepipe: DatePipe) {
    this.viewmembers = new Members();
    this.isSubmittedpwd = false;
  }

  ngOnInit() {
    this.Membersform();
    this.route.params.subscribe(params => {
      this.memId = params['memId'];

    });
    this.Changepassword();
    this.onGetMembersById();
    this.Notificationsform();
    this.createdby = localStorage.getItem('MemberName');

  }

  NotificationsSubmit() {
    const notify = {
      memberIdsList: [this.memId],
      activityType: this.Sendnotificationsform.controls["ActivityType"].value,
      message: this.Sendnotificationsform.controls["Message"].value,
      meaasgeBody: this.Sendnotificationsform.controls["MessageBody"].value,
      notificationChanelType: this.Sendnotificationsform.controls["NotificationChanelType"].value,
    }

    if (this.Sendnotificationsform.valid) {
        this.isSubmitted = false;

     // alert(JSON.stringify(notify));
      this.adminservice.OnPostNotifications(notify).subscribe(res => {
          console.log(res);
          //this.viewmembers = res
        });
      alert("Hi Notification Acccepted Successfully");
      }
    else {
        this.isSubmitted = true;
        //alert(JSON.stringify(license));
     
    }
  }
  readURL(event) {
    let files = event.target.files;
    this.memberImage = files[0];
    console.log("imageeee", this.memberImage);
  }

  onUpdateMembers(viewmembers) {
    console.log("updatemember", viewmembers);
    let formData = new FormData();

   
    formData.append('imageFile', this.memberImage, this.memberImage.name);
    
    let date = new Date();

    const members = {
      _id: viewmembers._id,
      UserName: this.viewmembersUpdateFormGroup.controls["viewmembers.UserName"].value,
     // Password: this.viewmembersUpdateFormGroup.controls["viewmembers.Password"].value,
     // ConfirmPassword: this.viewmembersUpdateFormGroup.controls["viewmembers.ConfirmPassword"].value,
      MobileNumber: this.viewmembersUpdateFormGroup.controls["viewmembers.MobileNumber"].value,
      Email: this.viewmembersUpdateFormGroup.controls["viewmembers.Email"].value,
      FirstName: this.viewmembersUpdateFormGroup.controls["viewmembers.FirstName"].value,
      LastName: this.viewmembersUpdateFormGroup.controls["viewmembers.LastName"].value,
      MiddleName: this.viewmembersUpdateFormGroup.controls["viewmembers.MiddleName"].value,
      DOB: this.viewmembersUpdateFormGroup.controls['viewmembers.DOB'].value,
      Gender: this.viewmembersUpdateFormGroup.controls['viewmembers.Gender'].value,
      Industry: this.viewmembersUpdateFormGroup.controls['viewmembers.Industry'].value,
      Profession: this.viewmembersUpdateFormGroup.controls['viewmembers.Profession'].value,
      AddressLine1: this.viewmembersUpdateFormGroup.controls['viewmembers.AddressLine1'].value,
      AddressLine2: this.viewmembersUpdateFormGroup.controls["viewmembers.AddressLine2"].value,
      City: this.viewmembersUpdateFormGroup.controls["viewmembers.City"].value,
      State: this.viewmembersUpdateFormGroup.controls["viewmembers.State"].value,
      ZipCode: this.viewmembersUpdateFormGroup.controls["viewmembers.ZipCode"].value,
      Country: this.viewmembersUpdateFormGroup.controls["viewmembers.Country"].value,
      CompanyName: this.viewmembersUpdateFormGroup.controls["viewmembers.CompanyName"].value,
      CompanyAddressLine1: this.viewmembersUpdateFormGroup.controls["viewmembers.CompanyAddressLine1"].value,
      CompanyAddressLine2: this.viewmembersUpdateFormGroup.controls["viewmembers.CompanyAddressLine2"].value,
      CompanyCity: this.viewmembersUpdateFormGroup.controls['viewmembers.CompanyCity'].value,
      CompanyCountry: this.viewmembersUpdateFormGroup.controls['viewmembers.CompanyCountry'].value,
      CompanyEmail: this.viewmembersUpdateFormGroup.controls['viewmembers.CompanyEmail'].value,
      AlternatePhoneNumber: this.viewmembersUpdateFormGroup.controls['viewmembers.AlternatePhoneNumber'].value,
      BloodGroup: this.viewmembersUpdateFormGroup.controls['viewmembers.BloodGroup'].value,
      FamilyType: this.viewmembersUpdateFormGroup.controls["viewmembers.FamilyType"].value,
      Origin: this.viewmembersUpdateFormGroup.controls["viewmembers.Origin"].value,
      TimeZone: this.viewmembersUpdateFormGroup.controls["viewmembers.TimeZone"].value,
      Hobbies: this.viewmembersUpdateFormGroup.controls['viewmembers.Hobbies'].value,
      MaritalStatus: this.viewmembersUpdateFormGroup.controls['viewmembers.MaritalStatus'].value,
      MemberStatus: this.viewmembersUpdateFormGroup.controls['viewmembers.MemberStatus'].value,
      IsMobileVerified: this.viewmembersUpdateFormGroup.controls['viewmembers.IsMobileVerified'].value,
      IsEmailVerified: this.viewmembersUpdateFormGroup.controls['viewmembers.IsEmailVerified'].value,
      ImageUrl: this.memberImage.name,
      UpdatedBy: this.createdby,
     // UpdatedDate: this.datepipe.transform(date, "yyyy-MM-dd"),
      CreatedBy: this.viewmembers.CreatedBy,
      //CreatedDate: this.viewmembers.CreatedDate,
    }
   // alert(JSON.stringify(member));

    console.log("finalmember", members);

    if (this.viewmembersUpdateFormGroup.valid) {
      this.isSubmitted = false;
      alert(JSON.stringify(members));
      this.adminservice.onUpdateMembers(members, formData).subscribe(data => {
        console.log(data);
       // this.viewmembers = res
        this.router.navigate(['/members/']);
      });
      alert("Successfully Updated");
    
    }
    else {
      this.isSubmitted = true;
      //alert(JSON.stringify(license));
    }

  }

  onchangepasswordSubmit() {


    const pas = {
      Id: this.status.Data.Id,
      Password: this.ChangePassword.controls["Password"].value,
      ConfirmPassword: this.ChangePassword.controls["ConfirmPassword"].value,
     
    }



    if (this.ChangePassword.valid) {
      this.isSubmittedpwd = false;

      // alert(JSON.stringify(viewmembers));
      this.adminservice.onChangePassword(pas).subscribe(res => {
        console.log(res);
        
      });
      alert("Successfully Updated");
    }
    else {
      this.isSubmittedpwd = true;
      //alert(JSON.stringify(license));
    }

  }

  openCpopup() {
    //this.Email = this.viewmembers.Email;
    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "block";
    cmask.style.display = "block";
  }
  openCpopup1() {
    this.Email = this.viewmembers.Email;
    let cpoper = document.getElementById("crpop1");
    let cmask = document.getElementById("crmask1");
    cpoper.style.display = "block";
    cmask.style.display = "block";
  }
  closeCpopup() {
    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "none";
    cmask.style.display = "none";
  }

  closeCpopup1() {
    let cpoper = document.getElementById("crpop1");
    let cmask = document.getElementById("crmask1");
    cpoper.style.display = "none";
    cmask.style.display = "none";
  }


  Notificationsform() {
    this.Sendnotificationsform = this.fb.group({
     // Title: ['', [Validators.required]],
      ActivityType: ['', [Validators.required]],
      Message: ['', [Validators.required]],
      MessageBody: ['', [Validators.required]],
      NotificationChanelType: ['', [Validators.required]]
    });
  }



  Membersform() {
    this.viewmembersUpdateFormGroup = this.fb.group({
      'viewmembers.UserName': ['', [Validators.required]],
    //  'viewmembers.Password': ['', [Validators.required]],
      //'viewmembers.ConfirmPassword': ['', [Validators.required]],
      'viewmembers.MobileNumber': ['', [Validators.required]],
      'viewmembers.Email': ['', [Validators.required]],
      'viewmembers.FirstName': ['', [Validators.required]],
      'viewmembers.LastName': ['', [Validators.required]],
      'viewmembers.MiddleName': ['', [Validators.required]],
      'viewmembers.DOB': ['', [Validators.required]],
      'viewmembers.Gender': ['', [Validators.required]],
      'viewmembers.Industry': ['', [Validators.required]],
      'viewmembers.Profession': ['', [Validators.required]],
      'viewmembers.AddressLine1': ['', [Validators.required]],
      'viewmembers.AddressLine2': ['', [Validators.required]],
      'viewmembers.City': ['', [Validators.required]],
      'viewmembers.State': ['', [Validators.required]],
      'viewmembers.ZipCode': ['', [Validators.required]],
      'viewmembers.Country': ['', [Validators.required]],
      'viewmembers.CompanyName': ['', [Validators.required]],
      'viewmembers.CompanyAddressLine1': ['', [Validators.required]],
      'viewmembers.CompanyAddressLine2': ['', [Validators.required]],
      'viewmembers.CompanyCity': ['', [Validators.required]],
      'viewmembers.CompanyCountry': ['', [Validators.required]],
      'viewmembers.CompanyEmail': ['', [Validators.required]],
      'viewmembers.AlternatePhoneNumber': ['', [Validators.required]],
      'viewmembers.BloodGroup': ['', [Validators.required]],
      'viewmembers.FamilyType': ['', [Validators.required]],
      'viewmembers.Origin': ['', [Validators.required]],
      'viewmembers.TimeZone': ['', [Validators.required]],
      'viewmembers.Hobbies': ['', [Validators.required]],
      'viewmembers.MaritalStatus': ['', [Validators.required]],
      'viewmembers.MemberStatus': ['', [Validators.required]],
      'viewmembers.IsMobileVerified': ['', [Validators.required]],
      'viewmembers.IsEmailVerified': ['', [Validators.required]],


    });
  }
  Changepassword() {
    this.ChangePassword = this.fb.group({
      Email: [''],
        Password: ['', [Validators.required]],
      ConfirmPassword: ['', [Validators.required]]
    });
  }

  onGetMembersById() {
    //alert(id);
    this.adminservice.onGetMembersById(this.memId).subscribe(result => {
      this.status = result;
      if (result.success == 1) {
        
        this.viewmembers = result.data;
      }
      else {
        alert(result.data);
      }

      console.log(this.viewmembers);

      if (this.viewmembers.DOB != null && this.viewmembers.DOB != "" && this.viewmembers.DOB != undefined) {
        this.dobDate = this.datepipe.transform(this.viewmembers.DOB, 'yyyy-MM-dd');
        this.viewmembers.DOB = this.dobDate;
      }
     

    });
    return this.viewmembers;
  }


}
