import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';

import { ViewuserComponent } from './viewuser.component';

export const ViewuserRoutes: Routes = [

  {
    path: '',
    component: ViewuserComponent,
    canActivate: [AuthGuard],
    data: {
      heading: 'Viewcontent works!'
    }    
  },

  
];

