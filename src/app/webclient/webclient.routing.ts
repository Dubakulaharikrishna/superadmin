import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';

import { WebclientComponent } from './webclient.component';

export const WebclientRoutes: Routes = [

  {
    path: '',
    component: WebclientComponent,
    canActivate: [AuthGuard],
    data: {
      heading: 'Login'
    }
  },


];

