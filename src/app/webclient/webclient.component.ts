import { Component, OnInit } from '@angular/core';
//changes
import { DatePipe } from '@angular/common'
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';
import { Webclient } from '../Models/Webclient';
import { AdminService } from '../services/admin.service';
import { Status } from '../Models/Status';
import { Ng2SmartTableModule, LocalDataSource } from 'ng2-smart-table';
import { License } from '../Models/license';
import { InstanceManager } from '../Models/InstanceManager';
import { Capabilities } from '../Models/capabilities';
import { ExcelService } from '../services/excel.service';
import { Subject } from 'rxjs/Subject';
import { MatSnackBar } from '@angular/material';


@Component({
  selector: 'app-webclient',
  templateUrl: './webclient.component.html',
  styleUrls: ['./webclient.component.css'],
  providers: [ExcelService, DatePipe]
})

export class WebclientComponent implements OnInit {
  weId: any;
  count: number;
  success: boolean = false;
  message: any;
  dtTrigger: Subject<any> = new Subject();
  public _success = new Subject<string>();
  staticAlertClosed = false;
  createdby: any;
  Instance_Id: any;
  LicenseInfo: any;
  isSubmitted: boolean = false;
  doBCheck: boolean = false;
  public data: Object;
  public temp_var: Object = false;
  public form: FormGroup;
  webclient: Webclient;
  public WebclientForm: FormGroup;
  webclientslist: Webclient[];
  id: any;
  instid: any;
  pagename: any;
  status: Status;
  listofLicense: License[];
  source: LocalDataSource;
  selectedValue: any = 50;
  page: any;
  listofinstances: InstanceManager[];
  listofcapabilities: Capabilities[];
  CompanyEstablishedDate: any;
  constructor(public router: Router, public snackBar: MatSnackBar, public datepipe: DatePipe, public route: ActivatedRoute, public http: HttpClient, private excelService: ExcelService, public fb: FormBuilder, private adminservice: AdminService) {
    this.webclient = new Webclient();
  }



 
  
 




  openCpopup() {

    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "block";
    cmask.style.display = "block";
  }

  closeCpopup() {
    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "none";
    cmask.style.display = "none";
    this.form.reset();
    this.isSubmitted = false;
  }




  onGetAllWebClientList() {
    this.adminservice.onGetAllWebClientList().subscribe(result => {

      if (result.success == 1) {
      
        this.webclientslist = result.data;
        this.count = this.webclientslist.length;
        this.source = new LocalDataSource(this.webclientslist);
       
        return this.webclientslist
      }

    });
  }
  onGetAllCapabilitiesList(id) {
    //let id = sessionStorage.getItem('AccountId');
    this.adminservice.onGetAllCapabilitiesList(id).subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;


        this.listofcapabilities = this.status.Data;
        this.source = new LocalDataSource(this.listofcapabilities);
        return this.listofcapabilities
      }

    });
  }

  license(id) {
    console.log(this.LicenseInfo);
   // alert(this.LicenseInfo);
  }



  test(id) {
    //  alert("Hi");
    //console.log(id);
    //alert(id);
    console.log(this.Instance_Id);
   // alert(this.Instance_Id);
  }
  onEdit(event: any) {
    
    this.router.navigate(['/viewwebclient/', { weId: event.data._id }]);
  }
  ngOnInit() {
 
    this.route.params.subscribe(params => {
      this.success = true;

      this.message = params['message'];
      setTimeout(() => this.staticAlertClosed = true, 20000);
      this._success.subscribe((message) => this.message = message);
      this._success.debounceTime(7000).subscribe(() => this.message = null);

    });
    this.pagename = "Client master";
    localStorage.setItem('loginSessId', this.pagename);
    this.createdby = localStorage.getItem('MemberName');
    this.instid = localStorage.getItem('InstanceId');
    //setTimeout(() => this.staticAlertClosed = true, 20000);
    //this._success.subscribe((message) => this.message = message);
    //this._success.debounceTime(7000).subscribe(() => this.message = null);
    this.WebClientForm();
    this.onGetAllWebClientList();
  //  this.onGetAllLicenseList();
   // this.onGetAllInstanceManagerList();
  }
  onCreateclient() {
    let date = new Date();
    if (this.form.valid) {
      this.isSubmitted = false;
      // alert(JSON.stringify(license));
      const client = {
        InstanceManagerId: this.instid,
        CompanyName: this.form.get('CompanyName').value,
        LegalName: this.form.get("LegalName").value,
        CompanyAddressLine1: this.form.get('CompanyAddressLine1').value,
        CompanyAddressLine2: this.form.get('CompanyAddressLine2').value,
        CompanyRegistrationNumber: this.form.get('CompanyRegistrationNumber').value,
        CompanyEmailID: this.form.get('CompanyEmailID').value,
       // CompanyAdminEmailID: this.form.get('CompanyAdminEmailID').value,
        CompanyType: this.form.get('CompanyType').value,
        CompanyEstablishedDate: this.form.get('CompanyEstablishedDate').value,
       // Instance_Id: this.form.get('Instance_Id').value,
        //LicenseInfo: this.form.get('LicenseInfo').value,
        DisplayNameOfCompany: this.form.get('DisplayNameOfCompany').value,
        LogoUrl: this.form.get('LogoUrl').value,
        AdminUserName: this.form.get('AdminUserName').value,
        PrimaryAdminEmailID: this.form.get('PrimaryAdminEmailID').value,
        CompanyPersonName: this.form.get('CompanyPersonName').value,
        CompanyPersonEmailID: this.form.get('CompanyPersonEmailID').value,
        AdminPhoneNumber: this.form.get('AdminPhoneNumber').value,
        CompanyPhone: this.form.get("CompanyPhone").value,
        CompanyPersonPhoneNumber: this.form.get('CompanyPersonPhoneNumber').value,
        CreatedBy: this.createdby,
       // ClientCreatedDate: this.datepipe.transform(date, "yyyy-MM-dd"),
        AdminPersonDesignation: this.form.get("AdminPersonDesignation").value,
      }
      // alert(JSON.stringify(client));
      this.adminservice.onCreateWebClient(client).subscribe(result => {
        console.log(result);
        if (result.success == 1) {
          this.success = true;
          this.message = result.Message;
          this._success.next(`Record Saved Successfully`);

          this.onGetAllWebClientList();
          this.closeCpopup();
          this.form.reset();
          //this.isSubmitted = false;
          console.log(this.snackBar, "this.snackBar")
          this.snackBar.open(`Record Saved Successfully`,'', {
            duration:20000,
          });

        }
        else {
          this.success = false;
          this.message = "Failed to create Client";
        }
      });
      
    
    }
    else {
      this.isSubmitted = true;
      //alert(JSON.stringify(license));
    }
    //alert(JSON.stringify(client));
  }


  onCheckDOB() {
    let date = new Date();
    let date1 = new Date(date),
      mnth = ("0" + (date.getMonth() + 1)).slice(-2),
      day = ("0" + date.getDate()).slice(-2);
    let todaydate = [date.getFullYear(), mnth, day].join("-");
    if (this.CompanyEstablishedDate <= todaydate) {
      this.doBCheck = false;
    }
    else {
      this.doBCheck = true;
    }
  }
 
  WebClientForm() {
    this.form = this.fb.group({
      CompanyName: ['', [Validators.required]],
      LegalName: ['', [Validators.required]],
      CompanyAddressLine1: ['', [Validators.required]],
      CompanyAddressLine2: ['', [Validators.required]],
      CompanyRegistrationNumber: ['', [Validators.required]],
      CompanyEmailID: ['', [Validators.required]],
      CompanyPhone: ['', [Validators.required]],
      //CompanyAdminEmailID: ['', [Validators.required]],
      CompanyType: ['', [Validators.required]],
      CompanyEstablishedDate: ['', [Validators.required]],
     // Instance_Id: ['', [Validators.required]],
     // LicenseInfo: ['', [Validators.required]],
      DisplayNameOfCompany: ['', [Validators.required]],
      LogoUrl: ['', [Validators.required]],
      AdminUserName: ['', [Validators.required]],
      PrimaryAdminEmailID: ['', [Validators.required]],
      CompanyPersonName: ['', [Validators.required]],
      CompanyPersonEmailID: ['', [Validators.required]],
      AdminPhoneNumber: ['', [Validators.required]],
      CompanyPersonPhoneNumber: ['', [Validators.required]],
      AdminPersonDesignation: ['', [Validators.required]]
    });
  }




  settings = {

    columns: {
      CompanyName: {
        title: 'Company Name',
        filter: false
      },

      AdminUserName: {
        title: 'Admin User Name',
        filter: false
      },

      PrimaryAdminEmailID:{
        title: 'Primary Admin Email ID',
        filter: false
      },
      CompanyEmailID: {
        title: 'Company Email ID',
        filter: false
      },
      CompanyEstablishedDate: {
        title: 'Company Established Date',
        filter: false,
        valuePrepareFunction: (CompanyEstablishedDate) => {
          var raws = new Date(CompanyEstablishedDate);
          var formatted = this.datepipe.transform(raws, 'dd-MM-yyyy');
          return formatted;
        }
      },

      //LicenseExpiryDate: {
      //  title: 'License ExpiryDate',
      //  filter: false,
      //  valuePrepareFunction: (LicenseExpiryDate) => {
      //    var raw = new Date(LicenseExpiryDate);
      //    var formatted = this.datepipe.transform(raw, 'dd-MM-yyyy HH:mm');
      //    return formatted;
      //  }
      //},
    },
    attr: { class: 'table table-bordered' },
    actions: {
      edit: false, //as an example
      delete: false, //as an example
      add: false,
      position: 'right',
      custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button" (click)="onEdit(rec._id)">Edit</button></span>` }]
    },
    pager: { display: true, perPage: 50 },
  };

  //route(event) {
  //  alert('hi');
  //}

  onSearch(query: string = '') {
    if (query != '') {
      this.source.setFilter([
        // fields we want to include in the search
        {
          field: 'CompanyName',
          search: query
        },
        {
          field: 'CompanyEmailID',
          search: query
        }
      ], false);
    }
    else {
      this.source = new LocalDataSource(this.webclientslist);
    }

    // second parameter specifying whether to perform 'AND' or 'OR' search 
    // (meaning all columns should contain search query or at least one)
    // 'AND' by default, so changing to 'OR' by setting false here
  }

  onSelectedFilter(event) {
    this.page = event.target.value;
    this.source = new LocalDataSource(this.webclientslist);
    this.settings = {

      columns: {
        CompanyName: {
          title: 'Company Name',
          filter: false
        },
        AdminUserName: {
          title: 'Admin User Name',
          filter: false
        },
        PrimaryAdminEmailID: {
          title: 'Primary Admin Email ID',
          filter: false
        },
        CompanyEmailID: {
          title: 'Company Email ID',
          filter: false
        },
      
        CompanyEstablishedDate: {
          title: 'Company Established Date',
          filter: false,
          valuePrepareFunction: (CompanyEstablishedDate) => {
            var raws = new Date(CompanyEstablishedDate);
            var formatted = this.datepipe.transform(raws, 'dd-MM-yyyy');
            return formatted;
          }
        },
        //LicenseExpiryDate: {
        //  title: 'License ExpiryDate',
        //  filter: false,
        //  valuePrepareFunction: (LicenseExpiryDate) => {
        //    var raw = new Date(LicenseExpiryDate);
        //    var formatted = this.datepipe.transform(raw, 'dd-MM-yyyy HH:mm');
        //    return formatted;
        //  }
        //},
      },
      attr: { class: 'table table-bordered' },
      actions: {
        edit: false, //as an example
        delete: false, //as an example
        add: false,
        position: 'right',
        custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button">Edit</button></span>` }]
      },
      pager: { display: true, perPage: this.page },
    };

  }

  onExport() {
    this.excelService.exportAsExcelFile(this.webclientslist, 'WebClient');
  }

}
