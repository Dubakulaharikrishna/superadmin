import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebclientComponent } from './webclient.component';

describe('WebclientComponent', () => {
  let component: WebclientComponent;
  let fixture: ComponentFixture<WebclientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebclientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebclientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
