//import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//import { DashboardComponent } from './dashboard.component';
import { RouterModule } from '@angular/router';
import { ViewliveRoutes } from './viewlive.routing';
import { ViewliveComponent } from '../viewlive/viewlive.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { HttpModule } from '@angular/http';
import { DataTablesModule } from 'angular-datatables';
import {HttpClientModule} from '@angular/common/http';
import { CommonModule } from '@angular/common'; 
import { BrowserModule } from '@angular/platform-browser';




@NgModule({
  imports: [
    RouterModule.forChild(ViewliveRoutes),
    DataTablesModule,
    HttpClientModule,
    CommonModule,
    
 HttpModule
    //BrowserModule
  ],
  declarations: [
   
    ViewliveComponent
    
  ],
 
  providers: [],
  bootstrap: [ViewliveComponent]
})
export class ViewliveModule { }






