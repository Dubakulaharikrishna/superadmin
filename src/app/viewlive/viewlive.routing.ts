import { Routes } from '@angular/router';

import { ViewliveComponent } from './viewlive.component';

export const ViewliveRoutes: Routes = [

  {
    path: '',
    component: ViewliveComponent,
    data: {
      heading: 'Viewlive works!'
    }    
  },

  
];

