import { Routes } from '@angular/router';

import { SurveymanagementComponent } from './surveymanagement.component';

export const SurveyManagementRoutes: Routes = [

  {
    path: '',
    component: SurveymanagementComponent,
    data: {
      heading: 'Surveymanagement'
    }    
  },

  
];

