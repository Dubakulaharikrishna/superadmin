//import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//import { DashboardComponent } from './dashboard.component';
import { RouterModule } from '@angular/router';
import { SurveyManagementRoutes } from './surveymanagement.routing';
import { SurveymanagementComponent } from '../surveymanagement/surveymanagement.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { HttpModule } from '@angular/http';
import { DataTablesModule } from 'angular-datatables';
import {HttpClientModule} from '@angular/common/http';
import { CommonModule } from '@angular/common'; 
import { BrowserModule } from '@angular/platform-browser';



@NgModule({
  imports: [DataTablesModule,
    HttpClientModule,
    CommonModule, HttpModule,
    RouterModule.forChild(SurveyManagementRoutes),
    //BrowserModule
  ],
  declarations: [
   
    SurveymanagementComponent
    
  ],
 
  providers: [],
  bootstrap: [SurveymanagementComponent]
})
export class SurveymanagementModule { }
