import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';

import { EventregistrationComponent } from './eventregistration.component';

export const EventregistrationRoutes: Routes = [

  {
    path: '',
    component: EventregistrationComponent,
    canActivate: [AuthGuard],

    data: {
      heading: 'Login'
    }
  },


];

