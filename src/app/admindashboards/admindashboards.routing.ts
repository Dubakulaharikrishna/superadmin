import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';

import { AdmindashboardsComponent } from './admindashboards.component';

export const AdmindashboardsRoutes: Routes = [

  {
    path: '',
    component: AdmindashboardsComponent,
    canActivate: [AuthGuard],

    data: {
      heading: 'Admindashboard'
    }    
  },

  
];

