import { Component, OnInit, AfterViewInit  } from '@angular/core';
import * as Chart from 'chart.js'


@Component({
  selector: 'app-admindashboards',
  templateUrl: './admindashboards.component.html',
  styleUrls: ['./admindashboards.component.css']
})
export class AdmindashboardsComponent implements AfterViewInit  {

    canvas: any;
    ctx: any;
  pagename: any;
  constructor() { }

  ngOnInit() {
   // sessionStorage.clear();
    //localStorage.removeItem('loginSessId');
  //  localStorage.clear();
   // localStorage.setTimeout = 1;
    //sessionStorage.removeItem('loginSessId');
 //   localStorage.removeItem('loginSessId');
    this.pagename = "Admin Dashboard";
    localStorage.setItem('loginSessId', this.pagename);
    
  }

  ngAfterViewInit() {
   // this.canvas = document.getElementById('myChart');
    //this.ctx = this.canvas.getContext('2d');

    let ctx = document.getElementById("doughnut-chart");

    let doughnut = new Chart(ctx, {
      type: 'doughnut',
      data: {
          labels: ["Asia", "Australia", "Africa", "Europe", "Latin America", "North America"],
          datasets: [{
              label: 'App Downloads',
              data: [2478,5267,734,784,433, 97],
              backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850", "#FC3"],
              borderWidth: 1
          }]
      },
      options: {
        responsive: true,
        title : {
            display : true,
            text : 'App Downloads across the globe in 2018'
        }
        
      }
    });


    let ctx2 = document.getElementById("linechart");

    let linechart= new Chart(ctx2, {
      type: 'line',
      
      data: {
          labels: ["Jan", "Feb", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
          datasets: [{
              label: 'Earnings',

              data: [10,8,6,5,12,8,16,17,6,7,6,10],
              color: [
                  'rgba(255, 99, 132, 1)',
                  'rgba(54, 162, 235, 1)',
                  'rgba(255, 206, 86, 1)'
              ],
              borderWidth: 1,
              borderColor : '#03dac7'
          }]
      },
      options: {
        responsive: true,
        display:true,
        title : {
            display : true,
            text : 'Revenue for the year of 2018'
        }
      }
    });

    //3rd chart for  popular cources
    let ctx3 = document.getElementById("bar-chart");
    let mybar = new Chart(ctx3, {
        type : 'bar',

        data: {
            labels: ["Javascript", "Phython", "Java", "C#", "C++", "C"],
            datasets: [
              {
                label: "Courses",
                backgroundColor: ["#ff0000", "#8e5ea2","#3cba9f","#e8c3b9","#c45850","#000", ""],
                data: [6,5,4,3,2,1,0]
              }
            ]
          },
          options: {
            legend: { display: true },
            title: {
              display: true,
              text: 'Popular Courses for the year of 2018'
            }
          }

    });


  }
 //chart ends here

}
