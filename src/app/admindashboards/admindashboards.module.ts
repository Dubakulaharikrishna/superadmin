//import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//import { DashboardComponent } from './dashboard.component';
import { RouterModule } from '@angular/router';
import { AdmindashboardsRoutes } from './admindashboards.routing';
import { AdmindashboardsComponent } from '../admindashboards/admindashboards.component';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';
import { AdminService } from '../services/admin.service';

@NgModule({
  imports: [
    RouterModule.forChild(AdmindashboardsRoutes),
    //BrowserModule
  ],
  declarations: [
   
    AdmindashboardsComponent
    
  ],
 
  providers: [AdminService, AuthGuard, NotAuthGuard],
  bootstrap: [AdmindashboardsComponent]
})
export class AdmindashboardModule { }






