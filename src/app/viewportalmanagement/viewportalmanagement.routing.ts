import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';

import { ViewportalmanagementComponent } from './viewportalmanagement.component';

export const ViewportalmanagementRoutes: Routes = [

  {
    path: '',
    component: ViewportalmanagementComponent,
    canActivate: [AuthGuard],
    data: {
      heading: 'Viewportalmanagement'
    }    
  },

  
];

