import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewportalmanagementComponent } from './viewportalmanagement.component';

describe('ViewportalmanagementComponent', () => {
  let component: ViewportalmanagementComponent;
  let fixture: ComponentFixture<ViewportalmanagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewportalmanagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewportalmanagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
