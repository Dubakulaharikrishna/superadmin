import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';
import { Subject } from 'rxjs/Subject';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { Router, ActivatedRoute } from '@angular/router';
import { AdminService } from '../services/admin.service';
import { InstanceManager } from '../Models/InstanceManager';
import { retry } from 'rxjs/operator/retry';
import { DatePipe } from '@angular/common';
import { Status } from '../Models/Status';
import { License } from '../Models/license';
@Component({
  selector: 'app-viewportalmanagement',
  templateUrl: './viewportalmanagement.component.html',
  styleUrls: ['./viewportalmanagement.component.css'],
  providers: [DatePipe]
})
export class ViewportalmanagementComponent implements OnInit {
  expDate: any;
  setDate: any;
  isSubmitted: boolean = false;
  Instanceform: FormGroup;
  public InstanceFormGroup: FormGroup;
  instancemanager: InstanceManager;
  //public InstancemanagerForm: FormGroup;
  listofinstances: InstanceManager[];
  id: any;
  success: boolean = false;
  message: any;
  dtTrigger: Subject<any> = new Subject();
  public _success = new Subject<string>();
  staticAlertClosed = false;
  createdby: any;
  instId: any;
  instid: any;
  status: Status;
  listofLicense: License[];
  constructor(private adminservice: AdminService, private router: Router, public route: ActivatedRoute, private http: HttpClient, public fb: FormBuilder, public datepipe: DatePipe) {
    //this.accountmanagement = new AccountManagement();
    this.instancemanager = new InstanceManager();
  }

  ngOnInit() {
    this.Instancemanagerform();
    this.route.params.subscribe(params => {
      this.instId = params['instId'];
     
    });
    this.onGetInstanceManagerById();
    this.onGetAllLicenseList();
    this.createdby = localStorage.getItem('MemberName');
    this.onGetAllLicenseList();
    setTimeout(() => this.staticAlertClosed = true, 20000);
    this._success.subscribe((message) => this.message = message);
    this._success.debounceTime(7000).subscribe(() => this.message = null);
    this.instid = localStorage.getItem('InstanceId');
  }

  onGetAllLicenseList() {
    this.adminservice.onGetAllLicenseList().subscribe(result => {

      if (result.success == 1) {

        this.listofLicense = result.data;
        // alert(JSON.stringify(this.Programstructures));
        return this.listofLicense
      }

    });
  }

  license(id) {
    console.log(this.instancemanager.AssociatedLicenseInfo);
   // alert(this.instancemanager.AssociatedLicenseInfo);
  }
  onUpdateInstance(instancemanager) {
    let date = new Date();


    const inst = {
      _id: instancemanager._id,
      InstanceManagerId: this.instid,
     // Id: this.instancemanager.Id,
      InstanceName: this.InstanceFormGroup.controls["instancemanager.InstanceName"].value,
      ServerName: this.InstanceFormGroup.controls["instancemanager.ServerName"].value,
      OSDetails: this.InstanceFormGroup.controls["instancemanager.OSDetails"].value,
      InstanceAdmin: this.InstanceFormGroup.controls["instancemanager.InstanceAdmin"].value,
      SetupDateTime: this.InstanceFormGroup.controls["instancemanager.SetupDateTime"].value,
      //AssociatedLicenseInfo: this.InstanceFormGroup.controls["instancemanager.AssociatedLicenseInfo"].value,
     // LicenseExpiryDate: this.InstanceFormGroup.controls["instancemanager.LicenseExpiryDate"].value,
     // LicenseType: this.InstanceFormGroup.controls["instancemanager.LicenseType"].value,
      NumberofClients: this.InstanceFormGroup.controls['instancemanager.NumberofClients'].value,
      NumberOfAccounts: this.InstanceFormGroup.controls['instancemanager.NumberOfAccounts'].value,
      InstancePublicIPAddress: this.InstanceFormGroup.controls['instancemanager.InstancePublicIPAddress'].value,
      InstancePrivateIPAddress: this.InstanceFormGroup.controls['instancemanager.InstancePrivateIPAddress'].value,
      CreatedBy: this.instancemanager.CreatedBy,
     // CreatedDate: this.instancemanager.CreatedDate,
      UpdatedBy: this.createdby,
     // UpdatedDate: this.datepipe.transform(date, "yyyy-MM-dd"),
    }




    if (this.InstanceFormGroup.valid) {
      this.isSubmitted = false;
     
    //  alert(JSON.stringify(instancemanager));
      this.adminservice.onUpdateInstanceManager(inst).subscribe(res => {
        console.log(res);
        this.instancemanager = res
        this.success = true;
        this.message = res.Message;
        this._success.next(`Record Updated Successfully`);
        this.router.navigate(['/instancemanager/', { upinstid: this.message}]);
      });
      alert("Successfully Updated");
     
    }
    else {
      this.isSubmitted = true;
      //alert(JSON.stringify(license));
    }

    // alert(JSON.stringify(lic));
  }
 


  Instancemanagerform() {
    this.InstanceFormGroup = this.fb.group({
      'instancemanager.InstanceName': ['', [Validators.required]],
      'instancemanager.ServerName': ['', [Validators.required]],
      'instancemanager.OSDetails': ['', [Validators.required]],
      'instancemanager.InstanceAdmin': ['', [Validators.required]],
      'instancemanager.SetupDateTime': ['', [Validators.required]],
     // 'instancemanager.AssociatedLicenseInfo': ['', [Validators.required]],
    //  'instancemanager.LicenseExpiryDate': ['', [Validators.required]],
      //'instancemanager.LicenseType': ['', [Validators.required]],
      'instancemanager.NumberofClients': ['', [Validators.required]],
      'instancemanager.NumberOfAccounts': ['', [Validators.required]],
      'instancemanager.InstancePublicIPAddress': ['', [Validators.required]],
      'instancemanager.InstancePrivateIPAddress': ['', [Validators.required]],

    });
  }
  onGetInstanceManagerById() {
    this.adminservice.onGetInstanceManagerById(this.instId).subscribe(result => {
      //  this.status = result;
      //alert(JSON.stringify(result));
      if (result.success == 1) {


        this.instancemanager = result.data;
      }
      else {
        alert(result.data);
      }
    
      console.log(this.instancemanager);

      if (this.instancemanager.SetupDateTime != null && this.instancemanager.SetupDateTime != "" && this.instancemanager.SetupDateTime != undefined) {
        this.setDate = this.datepipe.transform(this.instancemanager.SetupDateTime, 'yyyy-MM-dd');
        this.instancemanager.SetupDateTime = this.setDate;
      }
      if (this.instancemanager.LicenseExpiryDate != null && this.instancemanager.LicenseExpiryDate != "" && this.instancemanager.LicenseExpiryDate != undefined) {
        this.expDate = this.datepipe.transform(this.instancemanager.LicenseExpiryDate, 'yyyy-MM-dd');
        this.instancemanager.LicenseExpiryDate = this.expDate;
      }

    });
    return this.instancemanager;
  }
}
