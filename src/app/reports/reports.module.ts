//import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//import { DashboardComponent } from './dashboard.component';
import { RouterModule } from '@angular/router';
import { ReportsRoutes } from './reports.routing';
import { ReportsComponent } from '../reports/reports.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { HttpModule } from '@angular/http';
import { DataTablesModule } from 'angular-datatables';
import {HttpClientModule} from '@angular/common/http';
import { CommonModule } from '@angular/common'; 
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  imports: [DataTablesModule,
    HttpClientModule,
    CommonModule,
    
 HttpModule,
    RouterModule.forChild(ReportsRoutes),
    //BrowserModule
  ],
  declarations: [
   
    ReportsComponent
    
  ],
 
  providers: [],
  bootstrap: [ReportsComponent]
})
export class ReportsModule { }
