import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MemberprogrampurchaseComponent } from './memberprogrampurchase.component';

describe('MemberprogrampurchaseComponent', () => {
  let component: MemberprogrampurchaseComponent;
  let fixture: ComponentFixture<MemberprogrampurchaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MemberprogrampurchaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MemberprogrampurchaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
