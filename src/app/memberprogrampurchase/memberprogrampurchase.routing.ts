import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';

import { MemberprogrampurchaseComponent } from './memberprogrampurchase.component';

export const MemberprogrampurchaseRoutes: Routes = [

  {
    path: '',
    component: MemberprogrampurchaseComponent,
    canActivate: [AuthGuard],

    data: {
      heading: 'Login'
    }
  },


];

