import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewmemberprogrampurchaseComponent } from './viewmemberprogrampurchase.component';

describe('ViewmemberprogrampurchaseComponent', () => {
  let component: ViewmemberprogrampurchaseComponent;
  let fixture: ComponentFixture<ViewmemberprogrampurchaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewmemberprogrampurchaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewmemberprogrampurchaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
