import { Component, OnInit, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';

import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { Router, ActivatedRoute } from '@angular/router';
import { AdminService } from '../services/admin.service';
import { DatePipe } from '@angular/common';
import { retry } from 'rxjs/operator/retry';

import { Status } from '../Models/Status';
import { MemberProgramPurchase } from '../Models/MemberProgramPurchase';
import { Members } from '../Models/Members';


@Component({
  selector: 'app-viewmemberprogrampurchase',
  templateUrl: './viewmemberprogrampurchase.component.html',
  styleUrls: ['./viewmemberprogrampurchase.component.css'],
  providers: [DatePipe]
})
export class ViewmemberprogrampurchaseComponent implements OnInit {
  mempId: any;
  status: Status;
  startDate: any;
  validDate: any;
  renewalDate: any;
  listofMembers: Members[];
  memberprogrampurchase: MemberProgramPurchase;
  userName: any;
  constructor(private adminservice: AdminService, public datepipe: DatePipe, public ele: ElementRef, private router: Router, public route: ActivatedRoute, private http: HttpClient, public fb: FormBuilder) {

    this.memberprogrampurchase = new MemberProgramPurchase();

  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.mempId = params['mempId'];

    });
    this.onGetMemberProgramPurchaseById();
  }

  onGetAllMembersList() {
    let id = localStorage.getItem('AccountId');
    this.adminservice.onGetAllMembersList(id).subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofMembers = this.status.Data;
        console.log(this.listofMembers);
        console.log(data);
        // alert(JSON.stringify(this.Programstructures));
        return this.listofMembers
      }
    });
  }

  onGetMemberProgramPurchaseById() {
    //alert(id);
    let id = localStorage.getItem('AccountId');
    this.adminservice.onGetAllMembersList(id).subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofMembers = this.status.Data;
        console.log(this.listofMembers);
        console.log(data);
        this.adminservice.onGetMemberProgramPurchaseById(this.mempId).subscribe(result => {
          this.status = result;
          if (this.status.StatusCode == "1") {
            this.memberprogrampurchase = this.status.Data;
            // alert(JSON.stringify(this.eventmaster));
            this.userName = this.listofMembers.filter(m => m.Id == this.memberprogrampurchase.Member_Id)[0].UserName;
          }
          else {
            alert(this.status.Data);
          }
          console.log(this.memberprogrampurchase);


          if (this.memberprogrampurchase.ProgramStartDate != null && this.memberprogrampurchase.ProgramStartDate != "" && this.memberprogrampurchase.ProgramStartDate != undefined) {
            this.startDate = this.datepipe.transform(this.memberprogrampurchase.ProgramStartDate, 'yyyy-MM-dd');
            this.memberprogrampurchase.ProgramStartDate = this.startDate;
          }



          if (this.memberprogrampurchase.ProgramValidTillDate != null && this.memberprogrampurchase.ProgramValidTillDate != "" && this.memberprogrampurchase.ProgramValidTillDate != undefined) {
            this.validDate = this.datepipe.transform(this.memberprogrampurchase.ProgramValidTillDate, 'yyyy-MM-dd');
            this.memberprogrampurchase.ProgramValidTillDate = this.validDate;
          }

          if (this.memberprogrampurchase.NextRenewalDate != null && this.memberprogrampurchase.NextRenewalDate != "" && this.memberprogrampurchase.NextRenewalDate != undefined) {
            this.renewalDate = this.datepipe.transform(this.memberprogrampurchase.NextRenewalDate, 'yyyy-MM-dd');
            this.memberprogrampurchase.NextRenewalDate = this.renewalDate;
          }
          //if (this.eventregistration.EventEndTime != null && this.eventregistration.EventEndTime != "" && this.eventregistration.EventEndTime != undefined) {
          //  this.endTime = this.datepipe.transform(this.eventregistration.EventEndTime, 'yyyy-MM-dd');
          //  this.eventregistration.EventEndTime = this.startDate;
          //}

        });

        return this.memberprogrampurchase;
      }
    });
   
  }

}
