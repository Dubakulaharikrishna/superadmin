import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';

import { ViewmemberprogrampurchaseComponent } from './viewmemberprogrampurchase.component';

export const ViewmemberprogrampurchaseRoutes: Routes = [

  {
    path: '',
    component: ViewmemberprogrampurchaseComponent,
    canActivate: [AuthGuard],
    data: {
      heading: 'Viewaccount works!'
    }    
  },

  
];

