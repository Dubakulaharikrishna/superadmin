
import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';
import { ViewmembershipComponent } from '../viewmembership/viewmembership.component';

export const ViewmembershipRoutes: Routes = [

  {
    path: '',
    component: ViewmembershipComponent,
    canActivate: [AuthGuard],
    data: {
      heading: 'ViewAccountMembership'
    }
  },


];
