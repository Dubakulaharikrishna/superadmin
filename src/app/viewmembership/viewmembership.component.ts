import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';
//import { Programmanagement } from '../programmanagement/programmanagement.module';
import { CommonModule } from '@angular/common';
//import { ProgramManagementModule } from './programmanagement.module';
import { DatePipe } from '@angular/common'
import { AccountMembership } from '../Models/accountmembership';
import { AdminService } from '../services/admin.service';
import { CustomValidators } from 'ng2-validation';
import 'rxjs/add/operator/debounceTime';
import { Subject } from 'rxjs/Subject';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { retry } from 'rxjs/Operator/retry';
import { Status } from '../Models/Status';
import { AccountManagement } from '../Models/AccountManagement';
import { Members } from '../Models/Members';




@Component({
  selector: 'app-viewmembership',
  templateUrl: './viewmembership.component.html',
  styleUrls: ['./viewmembership.component.css'],
  providers: [DatePipe]
})
export class ViewmembershipComponent implements OnInit {
  dataLoaded: any;
  coverPic: any;
  createdby: any;
  loginDate: any;
  listofMembers: Members[];
  listofAccounts: AccountManagement[];
  public accountmembershipUpdateFormGroup: FormGroup;
  public form: FormGroup;
  accountmembership: AccountMembership;
  id: any;
  isSubmitted: boolean = false;
  accId: any;
  status: Status;
  constructor(private fb: FormBuilder, public ele: ElementRef, private adminservice: AdminService, private router: Router, public route: ActivatedRoute, private http: HttpClient, public datepipe: DatePipe) {

    this.accountmembership = new AccountMembership();



  }

  ngOnInit() {
    this.ViewAccountMemberShipform();
    this.route.params.subscribe(params => {
      this.accId = params['accId'];

    });
    this.onGetAccountMemberShipById();
    this.onGetAllAccountsList();
    this.onGetAllMembersList();
    this.createdby = localStorage.getItem('MemberName');
  }
  test(id) {
    //  alert("Hi");
    //console.log(id);
    //alert(id);
    console.log(this.accountmembership.MemberAccount_Id);
 //   alert(this.accountmembership.MemberAccount_Id);
  }
  member(id) {
    //  alert("Hi");
    //console.log(id);
    //alert(id);
    console.log(this.accountmembership.Member_Id);
  //  alert(this.accountmembership.Member_Id);
  }
  onGetAllAccountsList() {
    this.adminservice.onGetAllAccountsList().subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofAccounts = this.status.Data;

        //alert(JSON.stringify(this.listofAccounts));
        return this.listofAccounts
      }

    });
  }
  onGetAllMembersList() {
    let id = localStorage.getItem('AccountId');

    this.adminservice.onGetAllMembersList(id).subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofMembers = this.status.Data;
       
        // alert(JSON.stringify(this.Programstructures));
        return this.listofMembers
      }



    });
  }


  onUpdateAccountMembership(accountmembership) {
    let files = this.ele.nativeElement.querySelector('#ACM_ImageUrl').files;
    if (files.length > 0 && files.count != 0 && files != null) {

      let formData = new FormData();
      let file = files[0];
      this.coverPic = file.name;
      formData.append('ACM_ImageUrl', file, file.name);
      console.log(formData);
      console.log(files);
      this.adminservice.onfileupload(formData).subscribe(res => this.dataLoaded = res);
    }
    let date = new Date();
    const account = {
      Id: this.accountmembership.Id,
      Member_Id: this.accountmembershipUpdateFormGroup.controls["accountmembership.Member_Id"].value,
      MemberAccount_Id: this.accountmembershipUpdateFormGroup.controls["accountmembership.MemberAccount_Id"].value,
      ACM_UserName: this.accountmembershipUpdateFormGroup.controls["accountmembership.ACM_UserName"].value,
      ACM_Email: this.accountmembershipUpdateFormGroup.controls["accountmembership.ACM_Email"].value,
      ACM_Password: this.accountmembershipUpdateFormGroup.controls["accountmembership.ACM_Password"].value,
      ACM_LastLogin: this.accountmembershipUpdateFormGroup.controls["accountmembership.ACM_LastLogin"].value,
      ACM_Role_Id: this.accountmembershipUpdateFormGroup.controls["accountmembership.ACM_Role_Id"].value,
      ACM_LoginStatus: this.accountmembershipUpdateFormGroup.controls["accountmembership.ACM_LoginStatus"].value,
      ACM_ImageUrl: "Tcsimage/acmimages/" + this.coverPic,
      ACM_CreatedBy: this.accountmembership.ACM_CreatedBy,
      ACM_CreatedDate: this.accountmembership.ACM_CreatedDate,
      ACM_UpdatedBy: this.createdby,
      ACM_UpdatedDate: this.datepipe.transform(date, "yyyy-MM-dd")
    }

    if (this.accountmembershipUpdateFormGroup.valid) {
      //  alert(JSON.stringify(accountmanagement));
      this.isSubmitted = false;
      // alert(JSON.stringify(license));
   
    //  alert(JSON.stringify(accountmembership));
      this.adminservice.onUpdateAccountMembership(account).subscribe(res => {
        console.log(res);
        this.accountmembership = res
      });
      alert("Successfully Updated");
      this.router.navigate(['/accountmembership/']);
    }
    else {
      this.isSubmitted = true;
      //alert(JSON.stringify(license));
    }
    //alert(JSON.stringify(account));
  }

  ViewAccountMemberShipform() {
    this.accountmembershipUpdateFormGroup = this.fb.group({
      'accountmembership.Member_Id': ['', [Validators.required]],
      'accountmembership.MemberAccount_Id': ['', [Validators.required]],

      'accountmembership.ACM_UserName': ['', [Validators.required]],

      'accountmembership.ACM_Email': ['', [Validators.required]],
      'accountmembership.ACM_Password': ['', [Validators.required]],
      'accountmembership.ACM_LastLogin': ['', [Validators.required]],
      'accountmembership.ACM_Role_Id': ['', [Validators.required]],
      'accountmembership.ACM_LoginStatus': ['', [Validators.required]],



    });
  }
  onGetAccountMemberShipById() {

    this.adminservice.onGetAccountMemberShipById(this.accId).subscribe(result => {
      this.status = result;
      if (this.status.StatusCode == "1") {


        this.accountmembership = this.status.Data;
      }
      else {
        alert(this.status.Data);
      }
      console.log(this.accountmembership);

      if (this.accountmembership.ACM_LastLogin != null && this.accountmembership.ACM_LastLogin != "" && this.accountmembership.ACM_LastLogin != undefined) {
        this.loginDate = this.datepipe.transform(this.accountmembership.ACM_LastLogin, 'yyyy-MM-dd');
        this.accountmembership.ACM_LastLogin = this.loginDate;
      }



    });
    return this.accountmembership;
  }

}
