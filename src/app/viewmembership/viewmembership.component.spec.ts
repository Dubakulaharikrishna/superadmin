import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewmembershipComponent } from './viewmembership.component';

describe('ViewmembershipComponent', () => {
  let component: ViewmembershipComponent;
  let fixture: ComponentFixture<ViewmembershipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewmembershipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewmembershipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
