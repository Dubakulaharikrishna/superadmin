import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';

import { ForumtopicComponent } from './forumtopic.component';

export const ForumtopicRoutes: Routes = [

  {
    path: '',
    component: ForumtopicComponent,
    canActivate: [AuthGuard],

    data: {
      heading: 'Login'
    }
  },


];

