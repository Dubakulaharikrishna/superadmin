import { Component, OnInit, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl, } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import { AdminService } from '../services/admin.service';
import { CustomValidators } from 'ng2-validation';
import { DatePipe } from '@angular/common'
import 'rxjs/add/operator/debounceTime';
import { Subject } from 'rxjs/Subject';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/share';
import { CommonModule } from '@angular/common';
import { Members } from '../Models/Members';
import { Status } from '../Models/Status';
import { ForumTopic } from '../Models/ForumTopic';
import { Forum } from '../Models/Forum';
import { LocalDataSource } from 'ng2-smart-table';
import { AccountManagement } from '../Models/AccountManagement';
import { ExcelService } from '../services/excel.service';

@Component({
  selector: 'app-forumtopic',
  templateUrl: './forumtopic.component.html',
  styleUrls: ['./forumtopic.component.css'],
  providers: [ExcelService, DatePipe]
})
export class ForumtopicComponent implements OnInit {
  MasterAccount_Id: any;

  public ForumTopicUpdateFormGroup: FormGroup;
  public ForumTopicForm: FormGroup;
  public form: FormGroup;
  forumtopic: ForumTopic;
  public isSubmitted: boolean = false;
  id: any;
  createdby: any;
  message: any;
  dtTrigger: Subject<any> = new Subject();
  public _success = new Subject<string>();
  staticAlertClosed = false;
  ForumTopics_Id: any;
  Member_Id: any;
  Forum_Id: any;
  success: boolean = false;
  dataLoaded: any;
  media: any;
  Dfile1: any;
  Dfile2: any;
  Dfile3: any;
  listofForums: Forum[];
  listofMembers: Members[];
  listofforumtopic: ForumTopic[];
  status: Status;
  pagename: any;
  accountId: any;
  source: LocalDataSource;
  selectedValue: any = 50;
  page: any;
  roleId: any;
  listofAccounts: AccountManagement[];
  constructor(private fb: FormBuilder, public datepipe: DatePipe, public ele: ElementRef, private excelService: ExcelService, private adminservice: AdminService, private router: Router, private http: HttpClient) {

    this.forumtopic = new ForumTopic();
  }


  onGetAllForumList() {
    let id = localStorage.getItem('AccountId');
   // alert(id);
    this.adminservice.onGetAllForumList(id).subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofForums = this.status.Data;
        // alert(JSON.stringify(this.Programstructures));
        return this.listofForums;
      }


    });
  }
  onGetAllForumTopicsList(id) {
   // let id = sessionStorage.getItem('AccountId');
   // alert(id);
    this.adminservice.onGetAllForumTopicsList(id).subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofforumtopic = this.status.Data;
        this.source = new LocalDataSource(this.listofforumtopic);
        // alert(JSON.stringify(this.Programstructures));
        return this.listofforumtopic;
      }


    });
  }

  onGetAllAccountsList() {
    this.adminservice.onGetAllAccountsList().subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofAccounts = this.status.Data;

        //alert(JSON.stringify(this.listofAccounts));
        return this.listofAccounts
      }

    });
  }

  onChange(event) {
    console.log(event);
    this.onGetAllForumTopicsList(event.target.value);
  }



  onGetAllMembersList() {
    let id = localStorage.getItem('AccountId');
    //alert(id);
    this.adminservice.onGetAllMembersList(id).subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofMembers = this.status.Data;
        // alert(JSON.stringify(this.Programstructures));
        return this.listofMembers
      }


    });
  }

  //test1(id) {
  //  //  alert("Hi");
  //  //console.log(id);
  //  //alert(id);
  //  console.log(this.ForumTopics_Id);
  //  alert(this.ForumTopics_Id);
  //}


  test(id) {
    //  alert("Hi");
    //console.log(id);
    //alert(id);
    console.log(this.Forum_Id);
    //alert(this.Forum_Id);
  }

  test3(id) {
    //  alert("Hi");
    //console.log(id);
    //alert(id);
    console.log(this.Member_Id);
   // alert(this.Member_Id);
  }


  reset() {
    this.onGetAllForumTopicsList(this.accountId);
    this.MasterAccount_Id = undefined;

  }
  openCpopup() {

    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "block";
    cmask.style.display = "block";
  }

  closeCpopup() {
    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "none";
    cmask.style.display = "none";
  }

  onEdit(event: any) {


    this.router.navigate(['/viewforumtopic/', { forumtId: event.data.Id }]);
    
  }



  ngOnInit() {
   // localStorage.removeItem('loginSessId');
 //   localStorage.clear();
   /// localStorage.setTimeout = 1;
   // sessionStorage.removeItem('loginSessId');
   // localStorage.removeItem('loginSessId');
    this.pagename = "Forum Topic";
    localStorage.setItem('loginSessId', this.pagename);
    this.accountId = localStorage.getItem('AccountId');
    this.ForumTopicform();
    this.onGetAllForumTopicsList(this.accountId);
    this.onGetAllForumList();
    this.onGetAllMembersList();
    this.onGetAllAccountsList();
    this.roleId = localStorage.getItem('RoleId');
    setTimeout(() => this.staticAlertClosed = true, 20000);
    this._success.subscribe((message) => this.message = message);
    this._success.debounceTime(7000).subscribe(() => this.message = null);
    this.createdby = localStorage.getItem('MemberName');


  }
  onCreateForumTopic() {

    let files1 = this.ele.nativeElement.querySelector('#ForumTopicFile1').files;
    let files2 = this.ele.nativeElement.querySelector('#ForumTopicFile2').files;
    let files3 = this.ele.nativeElement.querySelector('#ForumTopicFile3').files;
    //let files4 = this.ele.nativeElement.querySelector('#ForumDiscussionFile4').files;
    if (files1.length > 0 && files1.count != 0 && files1 != null) {

      let formData = new FormData();
      let file = files1[0];
      this.Dfile1 = file.name;
      formData.append('ForumTopicFile1', file, file.name);
      console.log(formData);
      console.log(files1);
      this.adminservice.onfileupload(formData).subscribe(res => this.dataLoaded = res);
    }
    if (files2.length > 0 && files2.count != 0 && files2 != null) {

      let formData = new FormData();
      let file = files2[0];
      this.Dfile2 = file.name;
      formData.append('ForumTopicFile2', file, file.name);
      console.log(formData);
      console.log(files2);
      this.adminservice.onfileupload(formData).subscribe(res => this.dataLoaded = res);
    }
    if (files3.length > 0 && files3.count != 0 && files3 != null) {

      let formData = new FormData();
      let file = files3[0];
      this.Dfile3 = file.name;
      formData.append('ForumTopicFile3', file, file.name);
      console.log(formData);
      console.log(files3);
      this.adminservice.onfileupload(formData).subscribe(res => this.dataLoaded = res);
    }
    let date = new Date();
    const ForumTopic = {
      Forum_Id: this.form.controls["Forum_Id"].value,
      Member_Id: this.form.controls["Member_Id"].value,
      ForumTopicTitle: this.form.controls["ForumTopicTitle"].value,
      ForumTopicContent: this.form.controls["ForumTopicContent"].value,
      ForumTopicType: this.form.controls["ForumTopicType"].value,
      ForumTopicFile1: "Tcsimages/forumtopicimages/" + this.Dfile1,
      ForumTopicFile2: "Tcsimages/forumtopicimages/" + this.Dfile2,
      ForumTopicFile3: "Tcsimages/forumtopicimages/" + this.Dfile3,
      ForumTopicIsModerated: this.form.controls["ForumTopicIsModerated"].value,
      ForumTopicStatus: this.form.controls["ForumTopicStatus"].value,
      ForumTopicCreatedBy: this.createdby,
      ForumTopicCreatedDate: this.datepipe.transform(date, "yyyy-MM-dd")
    }

    if (this.form.valid) {
      this.isSubmitted = false;
    // alert(JSON.stringify(ForumTopic));
     
      this.adminservice.onCreateForumTopic(ForumTopic).subscribe(data => {
        console.log(data);
        if (data.StatusCode == "1") {
          this.success = true;
          this.message = data.Message;
          this._success.next(`Record Saved Successfully`);

          this.onGetAllForumTopicsList(this.accountId);
          this.closeCpopup();
        }
        else {
          this.success = false;
          this.message = "Failed to create account master";
        }
      });
      //alert("Successfully Submitted");
   


    }
    else {
      this.isSubmitted = true;
    }

  }


  ForumTopicform() {
    this.form = this.fb.group({

      'Forum_Id': ['', [Validators.required]],
      'Member_Id': ['', [Validators.required]],
      'ForumTopicTitle': ['', [Validators.required]],
      'ForumTopicContent': ['', [Validators.required]],
      'ForumTopicType': ['', [Validators.required]],
     
      'ForumTopicIsModerated': [''],
      'ForumTopicStatus': ['', [Validators.required]]
    });
  }


  /*Smart table*/
  settings = {

    columns: {
      ForumTopicTitle: {
        title: 'ForumTopicTitle',
        filter: false
      },
      ForumTopicStatus: {
        title: 'ForumTopic Status',
        filter: false
      },

    },
    attr: { class: 'table table-bordered' },
    actions: {
      edit: false, //as an example
      delete: false, //as an example
      add: false,
      position: 'right',
      custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button" (click)="onEdit(rec.Id)">Edit</button></span>` }]
    },
    pager: { display: true, perPage: 50 },
  };

  //route(event) {
  //  alert('hi');
  //}

  onSearch(query: string = '') {
    if (query != '') {
      this.source.setFilter([
        // fields we want to include in the search
        {
          field: 'ForumTopicTitle',
          search: query
        },
        {
          field: 'ForumTopicStatus',
          search: query
        }
      ], false);
    }
    else {
      this.source = new LocalDataSource(this.listofforumtopic);
    }

    // second parameter specifying whether to perform 'AND' or 'OR' search 
    // (meaning all columns should contain search query or at least one)
    // 'AND' by default, so changing to 'OR' by setting false here
  }

  onSelectedFilter(event) {
    this.page = event.target.value;
    this.source = new LocalDataSource(this.listofforumtopic);
    this.settings = {

      columns: {
        ForumTopicTitle: {
          title: 'ForumTopicTitle',
          filter: false
        },
        ForumTopicStatus: {
          title: 'ForumTopic Status',
          filter: false
        },

      },
      attr: { class: 'table table-bordered' },
      actions: {
        edit: false, //as an example
        delete: false, //as an example
        add: false,
        position: 'right',
        custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button">Edit</button></span>` }]
      },
      pager: { display: true, perPage: this.page },
    };

  }




  onExport() {
    this.excelService.exportAsExcelFile(this.listofforumtopic, 'ForumTopic');
  }


}
