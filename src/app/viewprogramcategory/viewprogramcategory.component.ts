import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';

import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { Router, ActivatedRoute } from '@angular/router';
import { AdminService } from '../services/admin.service';
import { DatePipe } from '@angular/common'
import { retry } from 'rxjs/operator/retry';
import { programcategory } from '../Models/programcategory';
import { Status } from '../Models/Status';
import { AccountManagement } from '../Models/AccountManagement';

@Component({
  selector: 'app-viewprogramcategory',
  templateUrl: './viewprogramcategory.component.html',
  styleUrls: ['./viewprogramcategory.component.css'],
  providers: [DatePipe]
})
export class ViewprogramcategoryComponent implements OnInit {
  dataLoaded: any;
  coverPic: any;
  public ViewProgramCategoryform: FormGroup;
  programcategory: programcategory;
  id: any;
  createdby: any;
  listofAccounts: AccountManagement[];
  isSubmitted: boolean = false;
  pcId: any;
  status: Status;
  constructor(private adminservice: AdminService, public datepipe: DatePipe, public ele: ElementRef, private router: Router, public route: ActivatedRoute, private http: HttpClient, public fb: FormBuilder) {
    this.programcategory = new programcategory();
  }

  ngOnInit() {
    this.ProgramCategoryform();
    this.route.params.subscribe(params => {
      this.pcId = params['pcId'];

    });
    this.onGetProgramCategoryById();
    this.onGetAllAccountsList();

    this.createdby = localStorage.getItem('MemberName');
  }


  onGetAllAccountsList() {
    this.adminservice.onGetAllAccountsList().subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofAccounts = this.status.Data;

        //alert(JSON.stringify(this.listofAccounts));
        return this.listofAccounts
      }

    });
  }
  test(id) {
    //  alert("Hi");
    //console.log(id);
    //alert(id);
    console.log(this.programcategory.MasterAccount_Id);
  //  alert(this.programcategory.MasterAccount_Id);
  }

  onUpdateProgramCategory(programcategory) {
    let files = this.ele.nativeElement.querySelector('#ProgramCategoryImgUrl').files;
    if (files.length > 0 && files.count != 0 && files != null) {

      let formData = new FormData();
      let file = files[0];
      this.coverPic = file.name;
      formData.append('ProgramCategoryImgUrl', file, file.name);
      console.log(formData);
      console.log(files);
      this.adminservice.onfileupload(formData).subscribe(res => this.dataLoaded = res);
    }
    let date = new Date();

    const prcategory = {
      Id: this.programcategory.Id,
      MasterAccount_Id: this.ViewProgramCategoryform.controls["programcategory.MasterAccount_Id"].value,
      ProgramCategoryName: this.ViewProgramCategoryform.controls["programcategory.ProgramCategoryName"].value,
      ProgramCategoryDescription: this.ViewProgramCategoryform.controls["programcategory.ProgramCategoryDescription"].value,
      ProgramCategoryStatus: this.ViewProgramCategoryform.controls["programcategory.ProgramCategoryStatus"].value,
      ProgramCategoryImgUrl: "Tcsimages/prgcategoryimages/" + this.coverPic,
      ProgramCategoryCreatedBy: this.programcategory.ProgramCategoryCreatedBy,
      ProgramCategoryCreatedDate: this.programcategory.ProgramCategoryCreatedDate,
      ProgramCategoryUpdatedBy: this.createdby,
      ProgramCategoryUpdatedDate: this.datepipe.transform(date, "yyyy-MM-dd")
    }
    if (this.ViewProgramCategoryform.valid) {
      this.isSubmitted = false;
      // alert(JSON.stringify(license));
      
   //   alert(JSON.stringify(programcategory));
      this.adminservice.onUpdateProgramCategory(prcategory).subscribe(res => this.programcategory = res);
      alert("Successfully Updated");
      this.router.navigate(['/programcategory/']);
    }
    else {
      this.isSubmitted = true;
      //alert(JSON.stringify(license));
    }
    //alert(JSON.stringify(account));
  }


  ProgramCategoryform() {
    this.ViewProgramCategoryform = this.fb.group({
      // AccountID: ['', [Validators.required]],
      'programcategory.MasterAccount_Id': ['', [Validators.required]],
      'programcategory.ProgramCategoryName': ['', [Validators.required]],
      'programcategory.ProgramCategoryDescription': ['', [Validators.required]],
      'programcategory.ProgramCategoryStatus': ['', [Validators.required]],
     

    });
  }

  //onUpdateAccount(account) {
  //  this.adminservice.onUpdateAccount(account).subscribe(res => this.accountmanagement = res);

  //}


  onGetProgramCategoryById() {
    //alert(id);
    this.adminservice.onGetProgramCategoryById(this.pcId).subscribe(result => {
      this.status = result;
      if (this.status.StatusCode == "1") {


        this.programcategory = this.status.Data;
      }
      else {
        alert(this.status.Data);
      }
      console.log(this.programcategory);
    });
    return this.programcategory;
  }
}
