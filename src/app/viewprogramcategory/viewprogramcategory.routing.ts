import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';

import { ViewprogramcategoryComponent } from './viewprogramcategory.component';

export const ViewprogramcategoryRoutes: Routes = [

  {
    path: '',
    component: ViewprogramcategoryComponent,
    canActivate: [AuthGuard],
    data: {
      heading: 'Viewaccount works!'
    }    
  },

  
];

