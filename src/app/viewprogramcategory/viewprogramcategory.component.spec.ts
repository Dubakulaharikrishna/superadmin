import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewprogramcategoryComponent } from './viewprogramcategory.component';

describe('ViewprogramcategoryComponent', () => {
  let component: ViewprogramcategoryComponent;
  let fixture: ComponentFixture<ViewprogramcategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewprogramcategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewprogramcategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
