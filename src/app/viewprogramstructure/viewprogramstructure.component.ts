//import { Component, OnInit } from '@angular/core';

//@Component({
//  selector: 'app-viewprogramstructure',
//  templateUrl: './viewprogramstructure.component.html',
//  styleUrls: ['./viewprogramstructure.component.css']
//})
//export class ViewprogramstructureComponent implements OnInit {

//  constructor() { }

//  ngOnInit() {
//  }

//}





import { AdminService } from '../services/admin.service';
import { CustomValidators } from 'ng2-validation';
import 'rxjs/add/operator/debounceTime';
import { Subject } from 'rxjs/Subject';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/share';
import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl, } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { CustomFormsModule } from 'ng2-validation';
import { BrowserModule } from '@angular/platform-browser';

import { retry } from 'rxjs/Operator/retry';
import 'rxjs/Rx';
import { DatePipe } from '@angular/common'
import { Programstructure } from '../Models/Programstructure';

import { Status } from '../Models/Status';
import { forEach } from '@angular/router/src/utils/collection';
import { Capabilities } from '../Models/capabilities';
import { Modules } from '../Models/modules';
import { Exercise } from '../Models/Exercise';
import { programcategory } from '../Models/programcategory';




@Component({
  selector: 'app-viewprogramstructure',
  templateUrl: './viewprogramstructure.component.html',
  styleUrls: ['./viewprogramstructure.component.css'],
  providers: [DatePipe]
})
export class ViewprogramstructureComponent implements OnInit {
  dataLoaded: any;
  media: any;

  public viewprogramstructureform: FormGroup;

  coverpic: any;
  programstructure: Programstructure;
  id: any;
  isSubmitted: boolean = false;
  prmId: any;
  expanded: boolean = false;
  secret: boolean = false;
  secretprogram: boolean = false;
  secretmodule: boolean = false;
  secretexercise: boolean = false;
  selectedcapabilities: any = [];
  selectedmodules: any = [];
  selectedexercise: any = [];
  createdby: any;
  listofprogramcategory: programcategory[];
  listofexercise: Exercise[];
  listofcapabilities: Capabilities[];
  listofModules: Modules[];
  selectedcatagorys: any = [];
  status: Status;
  public tt: any = [];


  constructor(private fb: FormBuilder,
    private adminservice: AdminService,
    private router: Router,
    public route: ActivatedRoute, public datepipe: DatePipe,
    private http: HttpClient, public ele: ElementRef
  ) {

    //this.programmanagement = new ProgramManagement();
    this.programstructure = new Programstructure();
    this.status = new Status();

  }


  onGetAllProgramManagementList() {
    let id = localStorage.getItem('AccountId');
    this.adminservice.onGetAllCapabilitiesList(id).subscribe(data => {
      // alert(data);

      if (data.StatusCode == "1") {

        this.listofcapabilities = data.Data;
        //  alert(JSON.stringify(this.listofcapabilities));
        return this.listofcapabilities;

      }
      // this.listofcapabilities = data;
      //// alert(this.listofprogrammanagement);
      // console.log(this.listofcapabilities);
    });
  }


  onGetAllProgramsList() {
    let id = localStorage.getItem('AccountId');
    this.adminservice.onGetAllModulesList(id).subscribe(data => {
      if (data.StatusCode == "1") {

        this.listofModules = data.Data;
        //alert(JSON.stringify(this.listofModules));
        return this.listofModules;

      }

    });
  }

  onGetAllExerciseList() {
    let id = localStorage.getItem('AccountId');
    // alert(id);
    this.adminservice.onGetAllExerciseList(id).subscribe(data => {
      if (data.StatusCode == "1") {

        this.listofexercise = data.Data;
        // alert(JSON.stringify(this.listofexercise));
        return this.listofexercise;

      }


    });
  }





  ngOnInit() {
    this.Programstructureform();
    this.route.params.subscribe(params => {
      this.prmId = params['prmId'];

    });
    this.onGetProgramstructureById();
    this.onGetAllProgramManagementList();
    this.onGetAllExerciseList();
    this.onGetAllProgramsList();
    this.createdby = localStorage.getItem('MemberName');
    this.onGetAllProgramCategoryList();
  }

  onGetAllProgramCategoryList() {
    let id = localStorage.getItem('AccountId');
    //  alert(id);
    this.adminservice.onGetAllProgramCategory().subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofprogramcategory = this.status.Data;
        // alert(JSON.stringify(this.listofprogramcategory));
        return this.listofprogramcategory;

      }
    });
  }

  capabilitiestest() {
    this.secret = true;
    var checkboxes = document.getElementById("ProgramStrCapabilities");
    //  alert(this.expanded);
    if (!this.expanded) {
      checkboxes.style.display = "block";
      this.expanded = true;
    } else {
      checkboxes.style.display = "none";
      this.expanded = false;
    }


    //   alert("hi");
  }

  capabilities(id) {

    var removedId;

    this.selectedcapabilities.forEach((existingId) => {

      if (existingId == id) {
        removedId = existingId;
        //  this.selectedcapabilities.pop(id);
      }

    });
    if (removedId != null) {
      var ob = this.selectedcapabilities.indexOf(removedId)
      this.selectedcapabilities.splice(ob, 1);
    }

    if (removedId != id) {
      this.selectedcapabilities.push(id);
    }


    //this.selectedcapabilities.push(id);
    //console.log(this.selectedcapabilities);
    //alert(this.selectedcapabilities);

  }





  modulestest() {
    this.secretmodule = true;
    var checkboxes = document.getElementById("ProgramStrModules");
    //  alert(this.expanded);
    if (!this.expanded) {
      checkboxes.style.display = "block";
      this.expanded = true;
    } else {
      checkboxes.style.display = "none";
      this.expanded = false;
    }


    //   alert("hi");
  }



  modules(id) {


    var remId;

    this.selectedmodules.forEach((existingId) => {

      if (existingId == id) {
        remId = existingId;
        //  this.selectedcapabilities.pop(id);
      }

    });

    if (remId != null) {
      var ob = this.selectedmodules.indexOf(remId)
      this.selectedmodules.splice(ob, 1);
    }

    if (remId != id) {
      this.selectedmodules.push(id);
    }

    //this.selectedmodules.push(id);
    //console.log(this.selectedmodules);


  }

  exercisetest() {
    this.secretexercise = true;
    var checkboxes = document.getElementById("ProgramStrExercise");
    //  alert(this.expanded);
    if (!this.expanded) {
      checkboxes.style.display = "block";
      this.expanded = true;
    } else {
      checkboxes.style.display = "none";
      this.expanded = false;
    }


    //   alert("hi");
  }

  exercise(id) {


    var removeId;
    this.selectedexercise.forEach((existingId) => {

      if (existingId == id) {
        removeId = existingId;
      }
    });
    if (removeId != null) {
      var ob = this.selectedexercise.indexOf(removeId)
      this.selectedexercise.splice(ob, 1);
    }

    if (removeId != id) {
      this.selectedexercise.push(id);
    }


    //this.selectedexercise.push(id);
    //console.log(this.selectedmodules);


  }

  ProgramCategorytest() {
    this.secretprogram = true;
    var checkboxes = document.getElementById("ProgramCategory");
    //  alert(this.expanded);
    if (!this.expanded) {
      checkboxes.style.display = "block";
      this.expanded = true;
    } else {
      checkboxes.style.display = "none";
      this.expanded = false;
    }


    //   alert("hi");
  }

  Programcategory(id) {

    var removedId;

    this.selectedcatagorys.forEach((existingId) => {

      if (existingId == id) {
        removedId = existingId;
        //  this.selectedcapabilities.pop(id);
      }

    });
    if (removedId != null) {
      var ob = this.selectedcatagorys.indexOf(removedId)
      this.selectedcatagorys.splice(ob, 1);
    }

    if (removedId != id) {
      this.selectedcatagorys.push(id);
    }


  }

  onUpdateProgramstructure() {
    //alert(this.selectedcapabilities.length);

    let files = this.ele.nativeElement.querySelector('#ProgramStrCoverPicture').files;
    if (files.length > 0 && files.count != 0 && files != null) {

      let formData = new FormData();
      let file = files[0];
      this.coverpic = file.name;
      formData.append('ProgramStrCoverPicture', file, file.name);
      console.log(formData);
      console.log(files);
      this.adminservice.onfileupload(formData).subscribe(res => this.dataLoaded = res);
    }
    let date = new Date();

    const programStructure = {
      Id: this.programstructure.Id,
      ProgramStrName: this.viewprogramstructureform.controls["programstructure.ProgramStrName"].value,
      //NoOfCapabilities: this.form.controls["NoOfCapabilities"].value,
      //NoOfModules: this.form.controls["NoOfModules"].value,
      ProgramStrType: this.viewprogramstructureform.controls["programstructure.ProgramStrType"].value,
      ProgramStrDependency: this.viewprogramstructureform.controls["programstructure.ProgramStrDependency"].value,
      // ProgramStrTitle: (<HTMLInputElement>document.getElementById("ProgramStrTitle")).value,
      //  ProgramStrTitle: this.form.controls["ProgramStrTitle"].value,
      // ProgramCategory: this.form.controls["ProgramCategory"].value,
      ProgramStrCoverPicture: "Tcsimages/programstrimages/" + this.coverpic,
      ProgramCategory: this.selectedcatagorys,
      ProgramStrCapabilities: this.selectedcapabilities,
      ProgramStrModules: this.selectedmodules,
      ProgramStrExercise: this.selectedexercise,
      ProgramStrDescription: this.viewprogramstructureform.controls["programstructure.ProgramStrDescription"].value,
      ProgramStrAccessType: this.viewprogramstructureform.controls["programstructure.ProgramStrAccessType"].value,
      ProgramStrStatus: this.viewprogramstructureform.controls["programstructure.ProgramStrStatus"].value,
      ProgramStrDiscountPrice: this.viewprogramstructureform.controls["programstructure.ProgramStrDiscountPrice"].value,
      ProgramStrListedPrice: this.viewprogramstructureform.controls["programstructure.ProgramStrListedPrice"].value,
      ProgramStrIsFree: this.viewprogramstructureform.controls["programstructure.ProgramStrIsFree"].value,
      ProgramStrCreatedBy: this.programstructure.ProgramStrCreatedBy,
      ProgramStrCreatedDate: this.programstructure.ProgramStrCreatedDate,
      ProgramStrUpdatedBy: this.createdby,
      ProgramStrUpdatedDate: this.datepipe.transform(date, "yyyy-MM-dd")
    }
   


    if (this.viewprogramstructureform.valid) {
      //  alert(JSON.stringify(accountmanagement));
      this.isSubmitted = false;
     // alert(JSON.stringify(programStructure));
      this.adminservice.onUpdateProgramStructure(programStructure).subscribe(res => {
       
        console.log(res);
        // this.programstructure = res
        this.status.Data = res

      });
      alert("Successfully Updated");
    }
    else {
      this.isSubmitted = true;
      //alert(JSON.stringify(license));
    }
    //alert(JSON.stringify(account));
  }



  Programstructureform() {
    this.viewprogramstructureform = this.fb.group({
      'programstructure.ProgramStrName': ['', [Validators.required]],
      'programstructure.ProgramCategory': [''],
      'programstructure.ProgramStrDescription': ['', [Validators.required]],
      'programstructure.ProgramStrType': ['', [Validators.required]],
      'programstructure.ProgramStrDependency': ['', [Validators.required]],
      'programstructure.ProgramStrCapabilities': [''],
      'programstructure.ProgramStrModules': [''],
      'programstructure.ProgramStrExercise': [''],
      'programstructure.ProgramStrAccessType': ['', [Validators.required]],
      'programstructure.ProgramStrStatus': ['', [Validators.required]],
      'programstructure.ProgramStrListedPrice': ['', [Validators.required]],
      'programstructure.ProgramStrDiscountPrice': ['', [Validators.required]],
      'programstructure.ProgramStrIsFree': ['']

    });
  }



  onGetProgramstructureById() {

    this.adminservice.onGetProgramStructureById(this.prmId).subscribe(result => {
      this.status = result;
      //  alert(JSON.stringify(this.status));
      if (this.status.StatusCode == "1") {


        this.programstructure = this.status.Data;
      }
      else {
        alert(this.status.Data);
      }
      console.log(this.programstructure);
      // this.status = result;
      //alert(this.status);

      //for (let tt of this.status.Data.ProgramStrCapabilities) {
      //  this.selectedcapabilities.push(tt);
      //}
      this.status.Data.ProgramStrCapabilities.forEach((tes) => {
        this.selectedcapabilities.push(tes);
      });
      // alert(this.selectedcapabilities.length);

      this.status.Data.ProgramStrModules.forEach((te) => {
        this.selectedmodules.push(te);
      });



      this.status.Data.ProgramStrExercise.forEach((tez) => {
        this.selectedexercise.push(tez);
      });

      this.status.Data.ProgramCategory_Id.forEach((tes) => {
        this.selectedcatagorys.push(tes);
      });
      //  alert(JSON.stringify(this.status.Data.Id));
      //alert(JSON.stringify(this.status.Data.ProgramStrCapabilities));
      // this.tt = this.status.Data.ProgramStrCapabilities;

      // alert(this.selectedcapabilities.length);
    });
    //   alert(JSON.stringify(this.status));
    return this.programstructure;
  }


}




