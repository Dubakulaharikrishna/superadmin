import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';

import { ViewprogramstructureComponent } from './viewprogramstructure.component';

export const ViewProgramstructureRoutes: Routes = [

  {
    path: '',
    component: ViewprogramstructureComponent,
    canActivate: [AuthGuard],
    data: {
      heading: 'viewprogrammanagement'
    }
  },


];
