import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewprogramstructureComponent } from './viewprogramstructure.component';

describe('ViewprogramstructureComponent', () => {
  let component: ViewprogramstructureComponent;
  let fixture: ComponentFixture<ViewprogramstructureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewprogramstructureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewprogramstructureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
