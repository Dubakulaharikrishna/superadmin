//import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//import { DashboardComponent } from './dashboard.component';
import { RouterModule } from '@angular/router';
import { ViewProgramstructureRoutes } from './viewprogramstructure.routing';
import { ViewprogramstructureComponent } from '../viewprogramstructure/viewprogramstructure.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { HttpModule } from '@angular/http';
import { DataTablesModule } from 'angular-datatables';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { NgbProgressbarModule, NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { NgbAccordionModule } from '@ng-bootstrap/ng-bootstrap';
import { CustomFormsModule } from 'ng2-validation';
import { FormsModule, FormBuilder, FormGroup, Validators, FormControl, ReactiveFormsModule } from '@angular/forms';
import { AdminService } from '../services/admin.service';

import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';




@NgModule({
  imports: [
    RouterModule.forChild(ViewProgramstructureRoutes),
    DataTablesModule,
    HttpClientModule,
    CommonModule,
    FormsModule,
    CustomFormsModule,
    NgbModule,
    ReactiveFormsModule,

    HttpModule
    //BrowserModule
  ],
  declarations: [

    ViewprogramstructureComponent

  ],

  providers: [AdminService, AuthGuard, NotAuthGuard],
  bootstrap: [ViewprogramstructureComponent]
})
export class ViewprogramstructureModule { }
