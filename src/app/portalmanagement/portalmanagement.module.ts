//import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//import { DashboardComponent } from './dashboard.component';
import { RouterModule } from '@angular/router';
import { PortalManagementRoutes } from './portalmanagement.routing';
import { PortalmanagementComponent } from '../portalmanagement/portalmanagement.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { HttpModule } from '@angular/http';
import { DataTablesModule } from 'angular-datatables';
import {HttpClientModule} from '@angular/common/http';
import { CommonModule } from '@angular/common'; 
import { BrowserModule } from '@angular/platform-browser';




@NgModule({
  
imports: [ 
      DataTablesModule,
      HttpClientModule,
      CommonModule,
      RouterModule.forChild(PortalManagementRoutes),
   HttpModule
  ],
  declarations: [
   
    PortalmanagementComponent
    
  ],
 
  providers: [],
  bootstrap: [PortalmanagementComponent]
})
export class PortalManagementModule { }



 



