import { Routes } from '@angular/router';

import { PortalmanagementComponent } from './portalmanagement.component';

export const PortalManagementRoutes: Routes = [

  {
    path: '',
    component: PortalmanagementComponent,
    data: {
      heading: 'Portalmanagement'
    }    
  },

  
];

