import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PortalmanagementComponent } from './portalmanagement.component';

describe('PortalmanagementComponent', () => {
  let component: PortalmanagementComponent;
  let fixture: ComponentFixture<PortalmanagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PortalmanagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PortalmanagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
