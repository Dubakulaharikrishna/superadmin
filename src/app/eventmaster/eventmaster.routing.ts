import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';

import { EventmasterComponent } from './eventmaster.component';

export const EventmasterRoutes: Routes = [

  {
    path: '',
    component: EventmasterComponent,
    canActivate: [AuthGuard],

    data: {
      heading: 'Login'
    }
  },


];

