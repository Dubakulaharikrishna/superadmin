import { Component, OnInit, ElementRef, ChangeDetectionStrategy } from '@angular/core';
//changes
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { Subject } from 'rxjs/Subject';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';
import { ExcelService } from '../services/excel.service';
import { AdminService } from '../services/admin.service';
import { Status } from '../Models/Status';
import { Ng2SmartTableModule, LocalDataSource } from 'ng2-smart-table';
import { Eventmaster } from '../Models/Eventmaster';
import { Eventtype } from '../Models/Eventtype';
import { programcategory } from '../Models/programcategory';
import { AccountManagement } from '../Models/AccountManagement';
import { DatePipe } from '@angular/common'
import { WindowRef } from '../Models/WindowRef';

import { CalendarModule } from 'primeng/calendar';
import { Members } from '../Models/Members';

@Component({
  selector: 'app-eventmaster',
  templateUrl: './eventmaster.component.html',
  styleUrls: ['./eventmaster.component.css'],
  providers: [ExcelService, DatePipe],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EventmasterComponent implements OnInit {
  webinar: any;
  presenterImage: any;
  MasterAccount_Id: any;
  EventType_Id: any;
  ProgramCategory_Id: any;
  name: any;
  listofMembers: Members[];
  source: LocalDataSource;
  selectedValue: any = 50;
  page: any;
  success: boolean = false;
  message: any;
  dtTrigger: Subject<any> = new Subject();
  public _success = new Subject<string>();
  staticAlertClosed = false;
  

  dataLoaded: any;
  media: any;
  coverPic: any;
  accountId: any;
  isSubmitted: boolean = false;
  public data: Object;
  public temp_var: Object = false;
  Instanceform: FormGroup;
  public form: FormGroup;
  eventmaster: Eventmaster;
  listofeventtypes: Eventtype[];
  listofeventmaster: Eventmaster[];
  listofprogramcategory: programcategory[];
  id: any;
  memberImage: any;
  createdby: any;
  pagename: any;
  status: Status;
  Member_Id: any;
  roleId: any;
  listofAccounts: AccountManagement[];
  constructor(public router: Router, private winRef: WindowRef, public datepipe: DatePipe, public ele: ElementRef, private excelService: ExcelService, public http: HttpClient, public fb: FormBuilder, private adminservice: AdminService) {

    this.eventmaster = new Eventmaster();
     console.log('Native window obj', winRef.nativeWindow);
  }

  ngOnInit() {
  //  this.onGetWebinarTocken();
    this.onNavigate();
   // localStorage.removeItem('loginSessId');
    //localStorage.clear();
  //  localStorage.setTimeout = 1;
   // sessionStorage.removeItem('loginSessId');
   // localStorage.removeItem('loginSessId');
    this.accountId = localStorage.getItem('AccountId');
    this.pagename = "Event Master";
    localStorage.setItem('loginSessId', this.pagename);
    this.onGetAllAccountsList();
    this.Eventmasterform();
    this.setWebinarSession();
    this.onGetAllEventmaster(this.accountId);
    this.onGetAllEventtypes();
    this.onGetAllMembersList();
    this.onGetAllProgramCategoryList();
    this.roleId = localStorage.getItem('RoleId');
    
    setTimeout(() => this.staticAlertClosed = true, 20000);
    this._success.subscribe((message) => this.message = message);
    this._success.debounceTime(7000).subscribe(() => this.message = null);
    this.createdby = localStorage.getItem('MemberName');


  }

  member(id) {

    console.log(this.Member_Id);
  
  }
  test(id) {
  
    console.log(this.EventType_Id);


   
  }
  test1(id) {
   
    console.log(this.ProgramCategory_Id);
    
  }

  reset() {
    this.onGetAllEventmaster(this.accountId);
    this.MasterAccount_Id = undefined;
  }
  openCpopup() {
    this.onGetWebinarTocken();
    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "block";
    cmask.style.display = "block";
  }

  closeCpopup() {
    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "none";
    cmask.style.display = "none";
  }

  onGetWebinarTocken() {
    this.adminservice.onCreateWebinarpath().subscribe(result => {
     // alert(JSON.stringify(result));
      this.webinar = result.data;
      console.log("hi", result);
    });
  }
  onGetAllAccountsList() {
    this.adminservice.onGetAllAccountsList().subscribe(result => {
      if (result.success == 1) {
        this.listofAccounts = result.data;
        //alert(JSON.stringify(this.listofAccounts));
        return this.listofAccounts
      }
    });
  }

  onChange(event) {
    console.log(event);
    this.onGetAllEventmaster(event.target.value);
  }

  onChangePrice(event) {
    (<HTMLInputElement>document.getElementById("EventPrice")).value == "0";
  }
  onEdit(event: any) {
    this.router.navigate(['/vieweventmaster/', { evemId: event.data._id }]);
  }
  onGetAllEventmaster(id) {
    //let id = sessionStorage.getItem('AccountId');
   // alert(id);
    if (id == "") {
      id = null;
    }
    this.adminservice.onGetAllEventMaster(id).subscribe(result => {
      if (result.success == 1) {
        this.listofeventmaster = result.eventsList;
        this.source = new LocalDataSource(this.listofeventmaster);
        alert(JSON.stringify(this.listofeventmaster));
        return this.listofeventmaster
      }
    });
  }
  setWebinarSession() {
    this.adminservice.onsetWebinarSession().subscribe(result => {
      console.log(result);
    });
    
  }

  onGetAllEventtypes() {
    let id = localStorage.getItem('AccountId');
    this.adminservice.onGetAllEventtypes(id).subscribe(result => {
      if (result.success == 1) {
        this.listofeventtypes = result.data;
        return this.listofeventtypes
      }
    });
  }

  onGetAllProgramCategoryList() {
   // let id = localStorage.getItem('AccountId');
   // alert(id);
    this.adminservice.onGetAllProgramCategory().subscribe(result => {
      if (result.success == 1) {
        this.listofprogramcategory = result.data;
       // this.source = new LocalDataSource(this.listofprogramcategory);
        return this.listofprogramcategory;
      }
    });
  }

  onGetAllMembersList() {
    let id = localStorage.getItem('AccountId');
    this.adminservice.onGetAllMembersList(id).subscribe(result => {
      if (result.success == 1) {
        this.listofMembers = result.data;
        return this.listofMembers
      }
    });
  }


  readURL(event) {
    let files = event.target.files;
    this.memberImage = files[0];
  }
  web: any;
  onNavigate() {

    window.open("https://api.getgo.com/oauth/v2/authorize?response_type=code&client_id=HAg4YueS1nAVGjzVxO5KArpiFZZDMNFk", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=400,height=400");
    console.log(window.localStorage);
    // localStorage.getItem('webinarSessionData' )
  }

   setData(data) {
     alert(data);
  console.log(data);
}
  readURLPresenter(event) {
    let files = event.target.files;
    this.presenterImage = files[0];

  }
  onCreateEventmaster() {
   
    let formData = new FormData();

    formData.append('eventFile', this.memberImage, this.memberImage.name);

    formData.append('eventPresenterFile', this.presenterImage, this.presenterImage.name);

    let date = new Date();

    //this.socialelements.MetaTitle = (<HTMLInputElement>document.getElementById("MetaTitle")).value;
    //this.socialelements.MetaKeywords = (<HTMLInputElement>document.getElementById("MetaKeywords")).value;
    //this.socialelements.MetaDescrition = (<HTMLInputElement>document.getElementById("MetaDescrition")).value;

    //  alert(JSON.stringify( this.socialelements));
    //alert(JSON.stringify(program));
    let gl: any = {};
    gl = { GuidelineName: this.form.controls["GuidelineName"].value, GuidelineDescription: this.form.controls["GuidelineDescription"].value};
    const EventMaster = {
      access_token: this.webinar.access_token,
      organizer_key: this.webinar.organizer_key,


      EventType_Id: this.form.controls["EventType_Id"].value,
      ProgramCategory_Id: this.form.controls["ProgramCategory_Id"].value,
      EventTitle: this.form.controls["EventTitle"].value,
      Member_Id: this.form.controls["Member_Id"].value,
      EventName: this.form.get('EventName').value,
     
      EventImageUrl: this.memberImage.name,
      EventDescription: this.form.controls["EventDescription"].value,
      EventScheduledStartDate: this.form.controls["EventScheduledStartDate"].value,
      EventScheduledStartTime: this.form.controls["EventScheduledStartTime"].value,
      EventScheduledEndDate: this.form.controls["EventScheduledEndDate"].value,
      EventScheduledEndTime: this.form.controls["EventScheduledEndTime"].value,
      EventAccessUrl: this.form.controls["EventAccessUrl"].value,
      EventRegistrationUrl: this.form.controls["EventRegistrationUrl"].value,
      EventPresenterImageUrl: this.presenterImage.name,
      EventCompletionTemplate: this.form.controls["EventCompletionTemplate"].value,
      EventReminderTemplate: this.form.controls["EventReminderTemplate"].value,
      EventIsRemindersEnabled: this.form.controls["EventIsRemindersEnabled"].value,
      EventIsNotificationsEnabled: this.form.controls["EventIsNotificationsEnabled"].value,
      EventIsFree: this.form.controls["EventIsFree"].value,
      EventResultsUrl: this.form.controls["EventResultsUrl"].value,
      EventSurveyUrl: this.form.controls["EventSurveyUrl"].value,
      EventExerciseUrl: this.form.controls["EventExerciseUrl"].value,
      EventAssignmentUrl: this.form.controls["EventAssignmentUrl"].value,
      EventFeedbackUrl: this.form.controls["EventFeedbackUrl"].value,
      EventParticipationCount: this.form.controls["EventParticipationCount"].value,
      EventStatus: this.form.controls["EventStatus"].value,
      EventLocation: this.form.controls["EventLocation"].value,
      EventPrice: this.form.controls["EventPrice"].value,
      EventCategoryName: this.form.controls["EventCategoryName"].value,
     //GuidelineName: this.form.controls["GuidelineName"].value,
     // GuidelineDescription: this.form.controls["GuidelineDescription"].value,
      //TemplateName: this.form.controls["TemplateName"].value,
      //Attributes: this.form.controls["Attributes"].value,
      EventCreatedBy: this.createdby,
      EventGuidelines: {
        GuidelineName: this.form.controls["GuidelineName"].value, GuidelineDescription: this.form.controls["GuidelineDescription"].value
      },
      EventRegistrationTemplate: { TemplateName: this.form.controls["TemplateName"].value, Attributes: this.form.controls["Attributes"].value},

    //  EventCreatedDate: this.datepipe.transform(date, "yyyy-MM-dd")
    }
    // alert(JSON.stringify(programManagement));
    if (this.form.valid) {
      this.isSubmitted = false;
     // alert(JSON.stringify(EventMaster));

      this.adminservice.onCreateEventmaster(EventMaster, formData).subscribe(result => {
        console.log(result);
        if (result.success == 1) {
          this.success = true;
          this.message = result.Message;
          this._success.next(`Record Saved Successfully`);

          this.onGetAllEventmaster(this.accountId);
          this.closeCpopup();

        }
        else {
          this.success = false;
          this.message = "Failed to create account master";
        }
      });
    }

    else {
      this.isSubmitted = true;
    }


  }



  Eventmasterform() {
    this.form = this.fb.group({
      'EventType_Id': ['', [Validators.required]],
      'ProgramCategory_Id': [''],
      'Member_Id': [''],
      'EventTitle': ['', [Validators.required]],
      'EventIsFree':[''],
      'EventName': ['', [Validators.required]],
      'EventDescription': ['', [Validators.required]],
      'EventScheduledStartDate': ['', [Validators.required]],
      'EventScheduledStartTime': ['', [Validators.required]],
      'EventScheduledEndDate': ['', [Validators.required]],
      'EventScheduledEndTime': ['', [Validators.required]],
      'EventAccessUrl': ['', [Validators.required]],
      'EventRegistrationUrl': ['', [Validators.required]],
      'EventCompletionTemplate': ['', [Validators.required]],
      'EventReminderTemplate': ['', [Validators.required]],
      'EventResultsUrl': ['', [Validators.required]],
      'EventSurveyUrl': ['', [Validators.required]],
      'EventExerciseUrl': ['', [Validators.required]],
      'EventAssignmentUrl': ['', [Validators.required]],
      'EventFeedbackUrl': ['', [Validators.required]],
      'EventParticipationCount': ['', [Validators.required]],
      //// RegistrationNumber': ['', [Validators.required]],

      'EventStatus': ['', [Validators.required]],
      'EventLocation': ['', [Validators.required]],
      'EventPrice': ['', [Validators.required]],
      'EventCategoryName': ['', [Validators.required]],
     
      'GuidelineName': ['', [Validators.required]],
     
      'GuidelineDescription': ['', [Validators.required]],
     
      'TemplateName': ['', [Validators.required]],
      'Attributes': ['', [Validators.required]],
      'EventIsRemindersEnabled': ['', [Validators.required]],
      'EventIsNotificationsEnabled': ['', [Validators.required]]

     

    });
  }







  /*Smart table*/
  settings = {

    columns: {
      EventName: {
        title: 'EventName',
        filter: false
      },
      EventStatus: {
        title: 'Event Status',
        filter: false
      },

    },
    attr: { class: 'table table-bordered' },
    actions: {
      edit: false, //as an example
      delete: false, //as an example
      add: false,
      position: 'right',
      custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button" (click)="onEdit(rec._id)">Edit</button></span>` }]
    },
    pager: { display: true, perPage: 50 },
  };

  //route(event) {
  //  alert('hi');
  //}

  onSearch(query: string = '') {
    if (query != '') {
      this.source.setFilter([
        // fields we want to include in the search
        {
          field: 'EventName',
          search: query
        },
        {
          field: 'Event Status',
          search: query
        }
      ], false);
    }
    else {
      this.source = new LocalDataSource(this.listofeventmaster);
    }

    // second parameter specifying whether to perform 'AND' or 'OR' search 
    // (meaning all columns should contain search query or at least one)
    // 'AND' by default, so changing to 'OR' by setting false here
  }

  onSelectedFilter(event) {
    this.page = event.target.value;
    this.source = new LocalDataSource(this.listofeventmaster);
    this.settings = {

      columns: {
        EventName: {
          title: 'Event Name',
          filter: false
        },
        EventStatus: {
          title: 'Event Status',
          filter: false
        },

      },
      attr: { class: 'table table-bordered' },
      actions: {
        edit: false, //as an example
        delete: false, //as an example
        add: false,
        position: 'right',
        custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button">Edit</button></span>` }]
      },
      pager: { display: true, perPage: this.page },
    };

  }



  onExport() {
    this.excelService.exportAsExcelFile(this.listofeventmaster, 'EventMaster');
  }

}
