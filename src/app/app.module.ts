import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, FormBuilder, FormGroup, Validators, FormControl, ReactiveFormsModule } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';

import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgbProgressbarModule, NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { NgbAccordionModule } from '@ng-bootstrap/ng-bootstrap';
import { AdminService } from './services/admin.service';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
//import { AccountsComponent } from './accounts/accounts.component';
import { AppRoutes } from './app.routing';
import { HeaderComponent } from './header/header.component';
//import { AboutComponent } from './about/about.component';
import { ChartsModule, Color } from 'ng2-charts';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { MatSnackBarModule } from '@angular/material';
import { WindowRef } from './Models/WindowRef';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

//import { ViewservicetypeComponent } from './viewservicetype/viewservicetype.component';
//import { ServicetypeComponent } from './servicetype/servicetype.component';
//import { ViewmemberprogrampurchaseComponent } from './viewmemberprogrampurchase/viewmemberprogrampurchase.component';
//import { MemberprogrampurchaseComponent } from './memberprogrampurchase/memberprogrampurchase.component';
//import { ViewservicepurchasesComponent } from './viewservicepurchases/viewservicepurchases.component';
//import { ServicepurchasesComponent } from './servicepurchases/servicepurchases.component';
//import { ServiceComponent } from './service/service.component';
//import { ViewmembereventpurchaseComponent } from './viewmembereventpurchase/viewmembereventpurchase.component';
//import { EventregistrationComponent } from './eventregistration/eventregistration.component';
//import { ViewforumtopicComponent } from './viewforumtopic/viewforumtopic.component';
//import { ForumtopicComponent } from './forumtopic/forumtopic.component';
//import { ForumtopicComponent } from './forumtopic/forumtopic.component';
//import { ViewforumComponent } from './viewforum/viewforum.component';
//import { ForumComponent } from './forum/forum.component';
//import { ViewforumdiscussionComponent } from './viewforumdiscussion/viewforumdiscussion.component';
//import { ForumdiscussionComponent } from './forumdiscussion/forumdiscussion.component';
//import { VieweventmasterComponent } from './vieweventmaster/vieweventmaster.component';
//import { EventmasterComponent } from './eventmaster/eventmaster.component';
//import { VieweventtypeComponent } from './vieweventtype/vieweventtype.component';
//import { EventtypeComponent } from './eventtype/eventtype.component';







@NgModule({
  declarations: [

    AppComponent,
    LoginComponent, 
    HeaderComponent, 
    
  ],

  imports: [
    BrowserModule,
    RouterModule.forRoot(AppRoutes),
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    NoopAnimationsModule,
    MatSnackBarModule,
    NgbModule.forRoot(),
    RouterModule.forRoot(AppRoutes),
  ],
  providers: [AdminService, WindowRef ],
  bootstrap: [AppComponent]
})
export class AppModule { }
