import { Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HeaderComponent } from './header/header.component';


export const AppRoutes: Routes = [

  {
    path: '',
    component: LoginComponent,
    children: [{
      path: 'login',
      loadChildren: './login/login.module#LoginModule'
    }],
    
  },
  {
    path: '',
    component: HeaderComponent,
    children: [{
      path: 'dashboard', 
      loadChildren: './dashboard/dashboard.module#DashboardModule'
    },

    {
      path: 'home',
      loadChildren: './home/home.module#HomeModule'
      },

      {
        
        path: 'portalmanagement', 
         loadChildren: './portalmanagement/portalmanagement.module#PortalManagementModule'
      },


      {
        path: 'contentmanagement', 
        loadChildren: './contentmanagement/contentmanagement.module#ContentmanagementModule'
      },

      {
        path: 'accountmanagement', 
        loadChildren: './accountmanagement/accountmanagement.module#AccountmanagementModule'
      },

      {
        path: 'usermanagement', 
        loadChildren: './usermanagement/usermanagement.module#UsermanagementModule'
      },

      //{
      //  path: 'coachmanagement',
      //  loadChildren: './coachmanagement/coachmanagement.module#CoachmanagementModule'
      //},
      {
        path: 'webclient',
        loadChildren: './webclient/webclient.module#WebclientModule'
      },

      {
        path: 'servicemanagement', 
        loadChildren: './servicemanagement/servicemanagement.module#ServicemanagementModule'
      },

      {
        path: 'surveymanagement',
        loadChildren: './surveymanagement/surveymanagement.module#SurveymanagementModule'
      },
      {
        path: 'eventmanagement', 
        loadChildren: './eventmanagement/eventmanagement.module#EventmanagementModule'
      },


      {
        path: 'capabilities',
        loadChildren: './capabilities/capabilities.module#CapabilitiesModule'
      },

      {
        path: 'reports', 
        loadChildren: './reports/reports.module#ReportsModule'
      },

      {
        path: 'admindashboards',
        loadChildren: './admindashboards/admindashboards.module#AdmindashboardModule'
      },


      //{
      //  path: 'livetracking',
      //  loadChildren: './livetracking/livetracking.module#LivetrackingModule'
      //},
      {
        path: 'viewportalmanagement', 
        loadChildren: './viewportalmanagement/viewportalmanagement.module#ViewportalmanagementModule'
      },
      {
        path: 'viewmodules',
        loadChildren: './viewmodules/viewmodules.module#ViewModulesModule'
      },
      {
        path: 'viewaccount', 
        loadChildren: './viewaccount/viewaccount.module#ViewaccountModule'
      },

      {
        path: 'viewuser', 
        loadChildren: './viewuser/viewuser.module#ViewuserModule'
      },
      {
        path: 'viewcoach', 
        loadChildren: './viewcoach/viewcoach.module#ViewcoachModule'
      },
      {
        path: 'viewservice',
        loadChildren: './viewservice/viewservice.module#ViewServiceModule'
      },
      {
        path: 'viewsurvey', 
        loadChildren: './viewsurvey/viewsurvey.module#ViewsurveyModule'
      },

      {
        path: 'viewevent', 
        loadChildren: './viewevent/viewevent.module#VieweventModule'
      },
      {
        path: 'viewprogram', 
        loadChildren: './viewprogram/viewprogram.module#ViewprogramModule'
      },
      {
        path: 'viewreport', 
        loadChildren: './viewreport/viewreport.module#ViewreportModule'
      },
      {
        path: 'viewlive', 
        loadChildren: './viewlive/viewlive.module#ViewliveModule'
      },
      {
        path: 'instancemanager',
        loadChildren: './instancemanager/instancemanager.module#InstancemanagerModule'
      },
      {
        path: 'members', 
        loadChildren: './members/members.module#MembersModule'
      },
      {
        path: 'modules',
        loadChildren: './modules/modules.module#ModulesModule'
      },
      {
        path: 'accountprograms',
        loadChildren: './accountprograms/accountprograms.module#AccountProgramsModule'
      },
      {
        path: 'license', 
        loadChildren: './license/license.module#LicenseModule'
      },
      { 
        path: 'accountmembership', 
        loadChildren: './accountmembership/accountmembership.module#AccountMembershipModule'
      },

      {
        path: 'viewmembership', 
        loadChildren: './viewmembership/viewmembership.module#ViewmembershipModule'
      },
      {
        path: 'viewwebclient', 
        loadChildren: './viewwebclient/viewwebclient.module#ViewwebclientModule'
      },
      {
        path: 'viewlicense', 
        loadChildren: './viewlicense/viewlicense.module#ViewlicenseModule'
      },


      {
        path: 'viewcapabilities',
        loadChildren: './viewcapabilities/viewcapabilities.module#ViewCapabilitiesModule'
      },
      {
        path: 'contentcategory', 
        loadChildren: './contentcategory/contentcategory.module#ContentcategoryModule'
      },
      {
        path: 'viewcontentcategory', 
        loadChildren: './viewcontentcategory/viewcontentcategory.module#ViewcontentcategoryModule'
      },
      {
        path: 'programcategory', 
        loadChildren: './programcategory/programcategory.module#ProgramcategoryModule'
      },

      {
        path: 'viewprogramcategory', 
        loadChildren: './viewprogramcategory/viewprogramcategory.module#ViewprogramcategoryModule'
      },
      
      {
        path: 'generatelicense', 
        loadChildren: './generatelicense/generatelicense.module#GeneratelicenseModule'
      },

      {
        path: 'exercisequestion', 
        loadChildren: './exercisequestion/exercisequestion.module#ExerciseQuestionModule'
      },

      {
        path: 'viewexercisequestion',
        loadChildren: './viewexercisequestion/viewexercisequestion.module#ViewExerciseQuestionModule'
      },

      {
        path: 'exerciseanswer', 
        loadChildren: './exerciseanswer/exerciseanswer.module#ExerciseAnswerModule'
      },

      {
        path: 'viewexerciseanswer',
        loadChildren: './viewexerciseanswer/viewexerciseanswer.module#ViewExerciseAnswerModule'
      },

      {
        path: 'viewlicenseinfo', 
        loadChildren: './viewlicenseinfo/viewlicenseinfo.module#ViewlicenseinfoModule'
      },

      {
        path: 'themestudio', 
        loadChildren: './themestudio/themestudio.module#ThemeStudioModule'
      },

      {
        path: 'viewthemestudio',
        loadChildren: './viewthemestudio/viewthemestudio.module#ViewThemeStudioModule'
      },

      {
        path: 'exercise', 
        loadChildren: './exercise/exercise.module#ExerciseModule'
      },
      {
        path: 'viewexercise', 
        loadChildren: './viewexercise/viewexercise.module#ViewExerciseModule'
      },

      {
        path: 'exercisetype', 
        loadChildren: './exercisetype/exercisetype.module#ExerciseTypeModule'
      },

      {
        path: 'viewexercisetype', 
        loadChildren: './viewexercisetype/viewexercisetype.module#ViewExerciseTypeModule'
      },



      {
        path: 'programstructure', 
        loadChildren: './programstructure/programstructure.module#ProgramStructureModule'
      },
      {
        path: 'viewprogramstructure', 
        loadChildren: './viewprogramstructure/viewprogramstructure.module#ViewprogramstructureModule'
      },
      {
        path: 'eventtype',
        loadChildren: './eventtype/eventtype.module#EventtypeModule'
      },
      {
        path: 'vieweventtype',
        loadChildren: './vieweventtype/vieweventtype.module#ViewEventtypeModule'
      },


      {
        path: 'eventmaster',
        loadChildren: './eventmaster/eventmaster.module#EventmasterModule'
      },


      {
        path: 'vieweventmaster',
        loadChildren: './vieweventmaster/vieweventmaster.module#ViewEventmasterModule'
      },

      {
        path: 'forumdiscussion',
        loadChildren: './forumdiscussion/forumdiscussion.module#ForumdiscussionModule'
      },

      {
        path: 'viewforumdiscussion',
        loadChildren: './viewforumdiscussion/viewforumdiscussion.module#ViewforumdiscussionModule'
      },
      {
        path: 'forum',
        loadChildren: './forum/forum.module#ForumModule'
      },
      {
        path: 'viewforum',
        loadChildren: './viewforum/viewforum.module#ViewForumModule'
      },
      {
        path: 'forumtopic',
        loadChildren: './forumtopic/forumtopic.module#ForumtopicModule'
      },
      {
        path: 'viewforumtopic',
        loadChildren: './viewforumtopic/viewforumtopic.module#ViewforumtopicModule'
      },

      {
        path: 'eventregistration',
        loadChildren: './eventregistration/eventregistration.module#EventRegistrationModule'
      },
      {
        path: 'vieweventregistration',
        loadChildren: './vieweventregistration/vieweventregistration.module#ViewEventRegistrationModule'
      },

      {
        path: 'membereventpurchase',
        loadChildren: './membereventpurchase/membereventpurchase.module#MemberEventPurchaseModule'
      },
      {
        path: 'viewmembereventpurchase',
        loadChildren: './viewmembereventpurchase/viewmembereventpurchase.module#ViewMemberEventPurchaseModule'
      },


      {
        path: 'service',
        loadChildren: './service/service.module#ServiceModule'
      },
      {
        path: 'servicepurchases',
        loadChildren: './servicepurchases/servicepurchases.module#ServicePurchaseModule'
      },

      {
        path: 'viewservicepurchases',
        loadChildren: './viewservicepurchases/viewservicepurchases.module#ViewServicePurchaseModule'
      },

      {
        path: 'memberprogrampurchase',
        loadChildren: './memberprogrampurchase/memberprogrampurchase.module#MemberProgramPurchaseModule'
      },
      {
        path: 'viewmemberprogrampurchase',
        loadChildren: './viewmemberprogrampurchase/viewmemberprogrampurchase.module#ViewMemberProgramPurchaseModule'
      },


      {
        path: 'servicetype',
        loadChildren: './servicetype/servicetype.module#ServiceTypeModule'
      },
      {
        path: 'viewservicetype',
        loadChildren: './viewservicetype/viewservicetype.module#ViewServiceTypeModule'
      },
      {
        path: 'webinar',
        loadChildren: './webinar/webinar.module#WebinarModule'
      },
    ]
  }

];

