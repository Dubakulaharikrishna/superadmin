import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';

import { ExerciseQuestionComponent } from './exercisequestion.component';

export const ExerciseQuestionRoutes: Routes = [{
  path: '',
  component: ExerciseQuestionComponent,
  canActivate: [AuthGuard],

  data: {
    heading: 'exercisequestion'
  }
}];
