
import { Component, OnInit } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';
import { ExerciseQuestion } from '../Models/ExerciseQuestion';
import { AdminService } from '../services/admin.service';
import { ExcelService } from '../services/excel.service';
import { Subject } from 'rxjs/Subject';
import { CommonModule } from '@angular/common';
import { Status } from '../Models/Status';
import { Ng2SmartTableModule, LocalDataSource } from 'ng2-smart-table';
import { AccountManagement } from '../Models/AccountManagement';
import { Exercise } from '../Models/Exercise';
import { DatePipe } from '@angular/common'

@Component({
  selector: 'app-exercisequestion',
  templateUrl: './exercisequestion.component.html',
  styleUrls: ['./exercisequestion.component.css'],
  providers: [ExcelService, DatePipe]
})
export class ExerciseQuestionComponent implements OnInit {
  MasterAccount_Id: any;
  success: boolean = false;
  message: any;
  dtTrigger: Subject<any> = new Subject();
  public _success = new Subject<string>();
  staticAlertClosed = false;
  source: LocalDataSource;
  selectedValue: any = 50;
  page: any;
  Exercise_Id: any;
  result: any[];
  title = 'Simple Datatable Example using Angular 4';
  public data: Object;
  public temp_var: Object = false;
  public form: FormGroup;
  exercisequestion: ExerciseQuestion;
  public ExerciseQuestionForm: FormGroup;
  isSubmitted: boolean = false;
  listofAccounts: AccountManagement[];
  listofExerciseQuestion: ExerciseQuestion[];
  id: any;
  this: any;
  pagename: any;
  roleId: any;
  status: Status;
  accountId: any;
  createdby: any;
  listofexercise: Exercise[];
  constructor(private adminservice: AdminService, private excelService: ExcelService, public datepipe: DatePipe, private router: Router, private http: HttpClient, public fb: FormBuilder) {
    this.exercisequestion = new ExerciseQuestion();

  }
  onGetAllAccountsList() {
    this.adminservice.onGetAllAccountsList().subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofAccounts = this.status.Data;

        //alert(JSON.stringify(this.listofAccounts));
        return this.listofAccounts
      }

    });
  }

  onChange(event) {
    console.log(event);
    this.onGetAllExerciseQuestionList(event.target.value);
  }
  reset() {
    this.onGetAllExerciseQuestionList(this.accountId);
    this.MasterAccount_Id = undefined;

  }


  openCpopup() {

    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "block";
    cmask.style.display = "block";
  }

  closeCpopup() {
    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "none";
    cmask.style.display = "none";
  }
  onEdit(event: any) {
    this.router.navigate(['/viewexercisequestion/', { exqId: event.data.Id}]);
  }

  ngOnInit() {

   // sessionStorage.clear();
 //   localStorage.removeItem('loginSessId');
   // localStorage.clear();
    //localStorage.setTimeout = 1;
   // sessionStorage.removeItem('loginSessId');
   // localStorage.removeItem('loginSessId');
    this.pagename = "Exercise question";
    localStorage.setItem('loginSessId', this.pagename);
    this.accountId = localStorage.getItem('AccountId');
    this.onGetAllAccountsList();
    this.roleId = localStorage.getItem('RoleId');

    setTimeout(() => this.staticAlertClosed = true, 20000);
    this._success.subscribe((message) => this.message = message);
    this._success.debounceTime(7000).subscribe(() => this.message = null);
    this.ExerciseQuestionform();
    this.onGetAllExerciseQuestionList(this.accountId);
    this.onGetAllExerciseList();
    this.createdby = localStorage.getItem('MemberName');
  }
  onGetAllExerciseList() {
    let id = localStorage.getItem('AccountId');
    //  alert(id);
    this.adminservice.onGetAllExerciseList(id).subscribe(data => {
      // alert(JSON.stringify(data));

      if (data.StatusCode == "1") {
        this.status = data;
        this.listofexercise = this.status.Data;
        this.source = new LocalDataSource(this.listofexercise);
        return this.listofexercise
      }


    });
  }
  test(id) {
    //  alert("Hi");
    //console.log(id);
    //alert(id);
    console.log(this.Exercise_Id);
 //   alert(this.Exercise_Id);
  }
  onGetAllExerciseQuestionList(id) {
  //  let id = sessionStorage.getItem('AccountId');
   // alert(id);
    this.adminservice.onGetAllExerciseQuestionList(id).subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofExerciseQuestion = this.status.Data;
        // alert(JSON.stringify(this.listofExerciseQuestion));
        this.source = new LocalDataSource(this.listofExerciseQuestion);
        return this.listofExerciseQuestion;
      }
      
    });
  }


  onCreateExerciseQuestion() {
    let date = new Date();
    const exeq = {
      Exercise_Id: this.form.controls["Exercise_Id"].value,
      ExerciseQuestion: this.form.controls["ExerciseQuestion"].value,
      ExerciseQuestionSummary: this.form.controls["ExerciseQuestionSummary"].value,
      ExerciseQuestionDescription: this.form.controls["ExerciseQuestionDescription"].value,
      ExerciseQuestionAnswers: this.form.controls["ExerciseQuestionAnswers"].value,
      ExerciseQuestionAnswerOptions: this.form.controls["ExerciseQuestionAnswerOptions"].value,
      ExerciseQuestionAnswerHints: this.form.controls["ExerciseQuestionAnswerHints"].value,
      ExerciseQuestionAnswerDisplayType: this.form.controls["ExerciseQuestionAnswerDisplayType"].value,
      ExerciseQuestionAnswerExclusionGroups: this.form.controls["ExerciseQuestionAnswerExclusionGroups"].value,
      ExerciseQuestionIsMandatory: this.form.controls["ExerciseQuestionIsMandatory"].value.toString(),
      ExerciseQuestionIsProficient: this.form.controls["ExerciseQuestionIsProficient"].value.toString(),
      ExerciseQuestionCreatedBy: this.createdby,
      ExerciseQuestionCreatedDate: this.datepipe.transform(date, "yyyy-MM-dd"),

      ExerciseQuestionStatus: this.form.controls["ExerciseQuestionStatus"].value,

    }
    if (this.form.valid) {
      this.isSubmitted = false;
      //alert(JSON.stringify(exeq));
      this.adminservice.onCreateExerciseQuestion(exeq).subscribe(data => {
        console.log(data);
        if (data.StatusCode == "1") {
          this.success = true;
          this.message = data.Message;
          this._success.next(`Record Saved Successfully`);

          this.onGetAllExerciseQuestionList(this.accountId);
          this.closeCpopup();

        }
        else {
          this.success = false;
          this.message = "Failed to create account master";
        }
      });
      //alert("Submitted Successfully");
    
    }
    else {
      this.isSubmitted = true;
    }

  }


  ExerciseQuestionform() {
    this.form = this.fb.group({
      'Exercise_Id': ['', [Validators.required]],
      'ExerciseQuestion': ['', [Validators.required]],

      'ExerciseQuestionSummary': ['', [Validators.required]],

      'ExerciseQuestionDescription': ['', [Validators.required]],
      'ExerciseQuestionAnswers': ['', [Validators.required]],
      'ExerciseQuestionAnswerOptions': ['', [Validators.required]],
      'ExerciseQuestionAnswerHints': ['', [Validators.required]],
      'ExerciseQuestionAnswerDisplayType': ['', [Validators.required]],
      'ExerciseQuestionAnswerExclusionGroups': ['', [Validators.required]],
      'ExerciseQuestionIsMandatory': ['', [Validators.required]],
      'ExerciseQuestionIsProficient': ['', [Validators.required]],
    //  'ExerciseQuestionCreatedDate': ['', [Validators.required]],
    //  'ExerciseQuestionCreatedBy': ['', [Validators.required]],
    //  'ExerciseQuestionUpdatedDate': ['', [Validators.required]],
    //  'ExerciseQuestionUpdatedBy': ['', [Validators.required]],
      'ExerciseQuestionStatus': ['', [Validators.required]],

    });
  }
  /*Smart table*/
  settings = {

    columns: {
      ExerciseQuestion: {
        title: 'ExerciseQuestion',
        filter: false
      },
      ExerciseQuestionSummary: {
        title: 'ExerciseQuestionSummary',
        filter: false
      },

    },
    attr: { class: 'table table-bordered' },
    actions: {
      edit: false, //as an example
      delete: false, //as an example
      add: false,
      position: 'right',
      custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button" (click)="onEdit(rec.Id)">Edit</button></span>` }]
    },
    pager: { display: true, perPage: 50 },
  };

  //route(event) {
  //  alert('hi');
  //}

  onSearch(query: string = '') {
    if (query != '') {
      this.source.setFilter([
        // fields we want to include in the search
        {
          field: 'ExerciseQuestion',
          search: query
        },
        {
          field: 'ExerciseQuestionSummary',
          search: query
        }
      ], false);
    }
    else {
      this.source = new LocalDataSource(this.listofExerciseQuestion);
    }

    // second parameter specifying whether to perform 'AND' or 'OR' search 
    // (meaning all columns should contain search query or at least one)
    // 'AND' by default, so changing to 'OR' by setting false here
  }

  onSelectedFilter(event) {
    this.page = event.target.value;
    this.source = new LocalDataSource(this.listofExerciseQuestion);
    this.settings = {

      columns: {
        ExerciseQuestion: {
          title: 'ExerciseQuestion',
          filter: false
        },
        ExerciseQuestionSummary: {
          title: 'ExerciseQuestionSummary',
          filter: false
        },

      },
      attr: { class: 'table table-bordered' },
      actions: {
        edit: false, //as an example
        delete: false, //as an example
        add: false,
        position: 'right',
        custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button">Edit</button></span>` }]
      },
      pager: { display: true, perPage: this.page },
    };

  }


  onExport() {
    this.excelService.exportAsExcelFile(this.listofExerciseQuestion, 'ExerciseQuestion');
  }

}


