import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExercisequestionComponent } from './exercisequestion.component';

describe('ExercisequestionComponent', () => {
  let component: ExercisequestionComponent;
  let fixture: ComponentFixture<ExercisequestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExercisequestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExercisequestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
