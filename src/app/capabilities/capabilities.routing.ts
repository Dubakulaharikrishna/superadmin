import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';

import { CapabilitiesComponent } from './capabilities.component';
export const CapabilitiesRoutes: Routes = [

  {
    path: '',
    component: CapabilitiesComponent,
    canActivate: [AuthGuard],

    data: {
      heading: 'Capabilities'
    }
  },


];
