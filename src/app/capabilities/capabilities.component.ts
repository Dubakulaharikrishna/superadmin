import { Component, OnInit, ElementRef } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { Subject } from 'rxjs/Subject';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';
//import { Programmanagement } from '../programmanagement/programmanagement.module';
import { CommonModule } from '@angular/common';
//import { ProgramManagementModule } from './programmanagement.module';
import { Capabilities } from '../Models/capabilities';
import { AdminService } from '../services/admin.service';
import { DatePipe } from '@angular/common'
import { Status } from '../Models/Status';
import { programcategory } from '../Models/programcategory';
import { Programstructure } from '../Models/Programstructure';
import { Ng2SmartTableModule, LocalDataSource } from 'ng2-smart-table';
import { ExcelService } from '../services/excel.service';
@Component({
  selector: 'app-capabilities',
  templateUrl: './capabilities.component.html',
  styleUrls: ['./capabilities.component.css'],
  providers: [ExcelService, DatePipe]
})
export class CapabilitiesComponent implements OnInit {
  MasterAccount_Id: any;
  dataLoaded: any;
  media: any;
  success: boolean = false;
  message: any;
  dtTrigger: Subject<any> = new Subject();
  public _success = new Subject<string>();
  staticAlertClosed = false;
  source: LocalDataSource;
  selectedValue: any = 50;
  page: any;
  coverPic: any;
  accountId: any;
  //form: any;
  public data: Object;
  public temp_var: Object = false;
  result: any[];
  title = 'Simple Datatable Example using Angular 4';
  //public data: Object;
  //public temp_var: Object = false;
  public form: FormGroup;
  public CapabilitiesUpdateFormGroup: FormGroup;
  capabilities: Capabilities;
  listofAccounts: any;
  public CapabilitiestForm: FormGroup;
  isSubmitted: boolean = false;
  listofcapabilities: Capabilities[];
  listofprogramcategory: programcategory[];
  listofprogramstructure: Programstructure[];
  id: any;
  status: Status;
  pagename: any;
  expanded: boolean = false;
  secret: boolean = false;
  selectedcatagorys: any = [];
  socialelements: any = {};
  name: any;
  roleId: any;
  createdby: any;
  constructor(public router: Router, private excelService: ExcelService, public datepipe: DatePipe, public http: HttpClient, public fb: FormBuilder, private adminservice: AdminService, public ele: ElementRef) {

    this.capabilities = new Capabilities();
    //this.isSubmitted = false;
  }


  onGetAllCapabilitiesList(id) {
    //let id = sessionStorage.getItem('AccountId');
    this.adminservice.onGetAllCapabilitiesList(id).subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
       

        this.listofcapabilities  = this.status.Data;
        this.source = new LocalDataSource(this.listofcapabilities);
        return this.listofcapabilities 
      }
    
    });
  }
  onGetAllProgramCategoryList() {
    let id = localStorage.getItem('AccountId');
  //  alert(id);
    this.adminservice.onGetAllProgramCategory().subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofprogramcategory = this.status.Data;
        return this.listofprogramcategory;

      }
    });
  }
  onGetAllProgramStructureList() {
    let id = localStorage.getItem('AccountId');
    this.adminservice.onGetAllProgramStructureList(id).subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofprogramstructure = this.status.Data;
        return this.listofprogramstructure;

      }
      console.log(this.listofprogramstructure);
    });

  }
  onGetAllAccountsList() {
    this.adminservice.onGetAllAccountsList().subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofAccounts = this.status.Data;

        //alert(JSON.stringify(this.listofAccounts));
        return this.listofAccounts
      }

    });
  }

  test(id) {
    //  alert("Hi");
    //console.log(id);
    //alert(id);
    console.log(this.name);
   // alert(this.name);
  }
  reset() {
    this.onGetAllCapabilitiesList(this.accountId);
    this.MasterAccount_Id = undefined;
  }
  onChange(event) {
    console.log(event);
    this.onGetAllCapabilitiesList(event.target.value);
  }
  ProgramCategorytest() {
    this.secret = true;
    var checkboxes = document.getElementById("ProgramCategory");
    //  alert(this.expanded);
    if (!this.expanded) {
      checkboxes.style.display = "block";
      this.expanded = true;
    } else {
      checkboxes.style.display = "none";
      this.expanded = false;
    }


    //   alert("hi");
  }

  Programcategory(id) {

    var removedId;

    this.selectedcatagorys.forEach((existingId) => {

      if (existingId == id) {
        removedId = existingId;
        //  this.selectedcapabilities.pop(id);
      }

    });
    if (removedId != null) {
      var ob = this.selectedcatagorys.indexOf(removedId)
      this.selectedcatagorys.splice(ob, 1);
    }

    if (removedId != id) {
      this.selectedcatagorys.push(id);
    }


  }

  openCpopup() {

    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "block";
    cmask.style.display = "block";
  }

  closeCpopup() {
    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "none";
    cmask.style.display = "none";
  }
  onEdit(event: any) {
    this.router.navigate(['/viewcapabilities/', { ccId: event.data.Id}]);
  }

  ngOnInit() {

   // sessionStorage.clear();
   // localStorage.removeItem('loginSessId');
  //  localStorage.clear();
   // localStorage.setTimeout = 1;
   // sessionStorage.removeItem('loginSessId');
   // localStorage.removeItem('loginSessId');
    this.pagename = "Capabilities";
    localStorage.setItem('loginSessId', this.pagename);
    this.accountId = localStorage.getItem('AccountId');
    setTimeout(() => this.staticAlertClosed = true, 20000);
    this._success.subscribe((message) => this.message = message);
    this._success.debounceTime(7000).subscribe(() => this.message = null);
    this.onGetAllAccountsList();
    this.roleId = localStorage.getItem('RoleId');
    this.Capabilityform();
    this.onGetAllCapabilitiesList(this.accountId);
    this.onGetAllProgramCategoryList();
    this.onGetAllProgramStructureList();
    this.createdby = localStorage.getItem('MemberName');
  }



  onCreateCapabilities() {


    let files = this.ele.nativeElement.querySelector('#CapabilityCoverPicture').files;
    let filesMedia = this.ele.nativeElement.querySelector('#CapabilityCoverMediaFile').files;
    if (files.length > 0 && files.count != 0 && files != null) {

      let formData = new FormData();
      let file = files[0];
      this.coverPic = file.name;
      formData.append('CapabilityCoverPicture', file, file.name);
      console.log(formData);
      console.log(files);
      this.adminservice.onfileupload(formData).subscribe(res => this.dataLoaded = res);
    }
    if (filesMedia.length > 0 && filesMedia.count != 0 && filesMedia != null) {

      let formData = new FormData();
      let fileM = filesMedia[0];
      this.media = fileM.name;
      formData.append('CapabilityCoverMediaFile', fileM, fileM.name);
      console.log(formData);
      console.log(files);
      this.adminservice.onfileupload(formData).subscribe(res => this.dataLoaded = res);
    }



    //this.socialelements.MetaTitle = (<HTMLInputElement>document.getElementById("MetaTitle")).value;
    //this.socialelements.MetaKeywords = (<HTMLInputElement>document.getElementById("MetaKeywords")).value;
    //this.socialelements.MetaDescrition = (<HTMLInputElement>document.getElementById("MetaDescrition")).value;

    //  alert(JSON.stringify( this.socialelements));
    //alert(JSON.stringify(program));
    let date = new Date();
    const Capabilities = {
      CapabilityName: this.form.controls["CapabilityName"].value,
      CapabilityDescription: this.form.controls["CapabilityDescription"].value,
      ProgramCategory_Id: this.selectedcatagorys,
      ProgramStr_Id: this.form.get('ProgramStr_Id').value,
      CapabilitySynopsis: this.form.controls["CapabilitySynopsis"].value,
      CapabilityCoverPicture: "Tcsimages/prgcoverimages/" + this.coverPic,
      CapabilityType: this.form.controls["CapabilityType"].value,
      CapabilityCategory: this.form.controls["CapabilityCategory"].value,
      CapabilityTags: this.form.controls["CapabilityTags"].value,
      CapabilityStartDate: this.form.controls["CapabilityStartDate"].value,
      CapabilityValidTill: this.form.controls["CapabilityValidTill"].value,
    //  CapabilityActualPrice: this.form.controls["CapabilityActualPrice"].value,
    //  CapabilityDiscountPrice: this.form.controls["CapabilityDiscountPrice"].value,
      CapabilityAccessType: this.form.controls["CapabilityAccessType"].value,
    
      CapabilityPlatforms: this.form.controls["CapabilityPlatforms"].value,
      CapabilityCoverMediaFile: "Tcsimages/prgcoverimages/" + this.media,
      CapabilityIsFree: this.form.controls["CapabilityIsFree"].value,
      CapabilityListedPrice: this.form.controls["CapabilityListedPrice"].value,
      CapabilityDiscountPrice: this.form.controls["CapabilityDiscountPrice"].value,
      CapabilityStatus: this.form.controls["CapabilityStatus"].value,
      MetaTitle: this.form.controls["MetaTitle"].value,
      MetaKeywords: this.form.controls["MetaKeywords"].value,
      CapabilityIsTracking: this.form.controls["CapabilityIsTracking"].value,
      MetaDescrition: this.form.controls["MetaDescrition"].value,
      CapabilityDependency: this.form.controls["CapabilityDependency"].value,
      CapabilityCreatedBy: this.createdby,
      CapabilityCreatedDate: this.datepipe.transform(date, "yyyy-MM-dd")
      // SocialElements: this.socialelements,
    }
    // alert(JSON.stringify(programManagement));
    if (this.form.valid) {
      this.isSubmitted = false;
      //  alert(JSON.stringify(programManagement));
      this.adminservice.onCreateCapabilities(Capabilities).subscribe(data => {
        console.log(data);
        if (data.StatusCode == "1") {
          this.success = true;
          this.message = data.Message;
          this._success.next(`Record Saved Successfully`);

          this.onGetAllCapabilitiesList(this.accountId);
          this.closeCpopup();

        }
        else {
          this.success = false;
          this.message = "Failed to create account master";
        }
      });
    //  alert("Successfully Submitted");

    }

    else {
      this.isSubmitted = true;
    }


  }




  //onUpdateInstanceManager(program) {
  //  this.adminservice.onUpdateProgramManagement(program).subscribe(res => this.program = res);

  //}



  Capabilityform() {
    this.form = this.fb.group({
      'CapabilityName': ['', [Validators.required]],
      'CapabilityDescription': ['', [Validators.required]],
      'ProgramStr_Id': ['', [Validators.required]],
      'CapabilitySynopsis': ['', [Validators.required]],
      'ProgramCategory_Id': [''],
      //  'ProgramCoverPicture': ['', [Validators.required]],
      'CapabilityType': ['', [Validators.required]],
      'CapabilityCategory': ['', [Validators.required]],
      'CapabilityTags': ['', [Validators.required]],
      'CapabilityStartDate': ['', [Validators.required]],
      'CapabilityValidTill': ['', [Validators.required]],
      //'CapabilityActualPrice': ['', [Validators.required]],
      //'CapabilityDiscountPrice': ['', [Validators.required]],
      'CapabilityAccessType': ['', [Validators.required]],
      'CapabilityIsTracking': ['', [Validators.required]],
      'CapabilityPlatforms': ['', [Validators.required]],
      'CapabilityIsFree':['',[Validators.required]],
      //  'ProgramCoverMediaFile': ['', [Validators.required]],
     
      'CapabilityStatus': ['', [Validators.required]],
      'MetaTitle': ['', [Validators.required]],
      'MetaKeywords': ['', [Validators.required]],
      'MetaDescrition': ['', [Validators.required]],
      'CapabilityDiscountPrice': ['', [Validators.required]],
      'CapabilityListedPrice': ['', [Validators.required]],
      'CapabilityDependency': ['', [Validators.required]]


    });
  }
  /*Smart table*/
  settings = {

    columns: {
      CapabilityName: {
        title: 'Capability Name',
        filter: false
      },
      CapabilityStatus: {
        title: 'Capability Status',
        filter: false
      },

    },
    attr: { class: 'table table-bordered' },
    actions: {
      edit: false, //as an example
      delete: false, //as an example
      add: false,
      position: 'right',
      custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button" (click)="onEdit(rec.Id)">Edit</button></span>` }]
    },
    pager: { display: true, perPage: 50 },
  };

  //route(event) {
  //  alert('hi');
  //}

  onSearch(query: string = '') {
    if (query != '') {
      this.source.setFilter([
        // fields we want to include in the search
        {
          field: 'CapabilityName',
          search: query
        },
        {
          field: 'CapabilityStatus',
          search: query
        }
      ], false);
    }
    else {
      this.source = new LocalDataSource(this.listofcapabilities);
    }

    // second parameter specifying whether to perform 'AND' or 'OR' search 
    // (meaning all columns should contain search query or at least one)
    // 'AND' by default, so changing to 'OR' by setting false here
  }

  onSelectedFilter(event) {
    this.page = event.target.value;
    this.source = new LocalDataSource(this.listofcapabilities);
    this.settings = {

      columns: {
        CapabilityName: {
          title: 'Capability Name',
          filter: false
        },
        CapabilityStatus: {
          title: 'Capability Status',
          filter: false
        },

      },
      attr: { class: 'table table-bordered' },
      actions: {
        edit: false, //as an example
        delete: false, //as an example
        add: false,
        position: 'right',
        custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button">Edit</button></span>` }]
      },
      pager: { display: true, perPage: this.page },
    };

  }


  onExport() {
    this.excelService.exportAsExcelFile(this.listofcapabilities, 'Capabilities');
  }



}
