import { Component, OnInit, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AdminService } from '../services/admin.service';
import { Status } from '../Models/Status';
import { login } from '../Models/login';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray, NG_VALIDATORS, Validator, AbstractControl, ValidatorFn } from '@angular/forms';
import { Directive, forwardRef, Attribute, OnChanges, SimpleChanges, Input } from '@angular/core';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DOCUMENT } from "@angular/platform-browser";
import { retry } from 'rxjs/operator/retry';

//import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  isSubmitted: boolean = false;
  successMessage: string;
  status: any;
  isError: boolean = false;


  constructor(private router: Router, private frm: FormBuilder, private adminservice: AdminService) {


    this.status = {};
    this.isError = false;
  }

  ngOnInit() {
    this.creatLoginForm();
  }

  submit(event: any) {
    if (event.keyCode == 13) {
      let htmlForm: HTMLElement = document.getElementById('btnLogin') as HTMLElement;
      htmlForm.click(); 
    }
  }



  creatLoginForm() {
    this.loginForm = this.frm.group({
      Email: ['', Validators.required],
      Password: ['', Validators.required]
    });
  }



  onLogin() {
    var Email = this.loginForm.get("Email").value;
    var Password = this.loginForm.get("Password").value;
    // alert(Email);
    // alert(Password);
    this.onCheckUser(Email, Password);
    //if (this.status.StatusCode == "1") {
    //  this.router.navigate(['/admindashboards/']);
    //}
    //else {
    //  alert("Invalid UserName/Password");
    //}
  }



  onCheckUser(Email, Password) {
    this.adminservice.onLogin(Email, Password).subscribe(res => {
      console.log(res)
      this.status = res;
      this.isSubmitted = true;
      this.isError = false;
      if (this.status.success == 1) {
        this.isSubmitted = false;
        localStorage.setItem('InstanceId', this.status.data._id);
        //localStorage.setItem('AccountId', this.status.data.MemberAccount_Id == "" ? null : this.status.data.MemberAccount_Id);
        localStorage.setItem('MemberName', this.status.data.UserName);
        ////localStorage.setItem('AccountId', this.status.Data.m_Item2[0].MemberAccount_Id);
        //localStorage.setItem('MemberId', this.status.data.Member_Id);
        //this.adminservice.storeUserData(this.status.response.token, this.status.data.ACM_Email);
        ////alert(sessionStorage.getItem('AccountId'));
        this.router.navigate(['/admindashboards/']);
        console.log(localStorage.getItem('InstanceId'));
      }
      else {
        if (this.status.message != "UserName/Email and Password are Mandatory") {
          this.isError = true;
          this.successMessage = "Your email address or password is not a valid";
        }
        //alert("Invalid UserName/Password");
      }
   //   alert(JSON.stringify(this.status));
    });
    return this.status;
  }


}
