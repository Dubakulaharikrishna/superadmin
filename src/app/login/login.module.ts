import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { LoginComponent } from './login.component';
import { RouterModule } from '@angular/router';
import { LoginRoutes } from '../login/login.routing';
import { AdminService } from '../services/admin.service';


@NgModule({
  imports: [
    RouterModule.forChild(LoginRoutes),
    HttpModule,
 
  ],
  declarations: [

    LoginComponent

  ],

  providers: [AdminService],
  bootstrap: [LoginComponent]
})
export class LoginModule { }
