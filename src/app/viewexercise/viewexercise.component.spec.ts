import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewexerciseComponent } from './viewexercise.component';

describe('ViewexerciseComponent', () => {
  let component: ViewexerciseComponent;
  let fixture: ComponentFixture<ViewexerciseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewexerciseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewexerciseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
