import { NgModule } from '@angular/core';
//import { DashboardComponent } from './dashboard.component';
import { RouterModule } from '@angular/router';
import { ViewExerciseComponent } from '../viewexercise/viewexercise.component';
import { BrowserModule } from '@angular/platform-browser';


import { ViewExerciseRoutes } from './viewexercise.routing';
//import { programmanagement } from './programmanagement';
//import { ProgramManagementComponent } './programmanagement/programmanagement.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { HttpModule } from '@angular/http';
import { DataTablesModule } from 'angular-datatables';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';

import { FormsModule, FormBuilder, FormGroup, Validators, FormControl, ReactiveFormsModule } from '@angular/forms';

import { NgbAccordionModule } from '@ng-bootstrap/ng-bootstrap';
import { CustomFormsModule } from 'ng2-validation';
import { NgbProgressbarModule, NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AdminService } from '../services/admin.service';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';


@NgModule({
  imports: [
    DataTablesModule,
    HttpClientModule,
    CommonModule,
    NgbModule,


    CustomFormsModule,

    ReactiveFormsModule,
    FormsModule,



    HttpModule,
    RouterModule.forChild(ViewExerciseRoutes),
    //BrowserModule
  ],
  declarations: [

    ViewExerciseComponent

  ],

  providers: [AdminService, AuthGuard, NotAuthGuard],
  bootstrap: [ViewExerciseComponent]
})
export class ViewExerciseModule { }
