
import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';

import { ViewExerciseComponent } from '../viewexercise/viewexercise.component';

export const ViewExerciseRoutes: Routes = [
  {
    path: '',
    component: ViewExerciseComponent,
    canActivate: [AuthGuard],
    data: {
      heading: 'ViewExercise'
    }
  },


];
