
import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import 'rxjs/add/operator/share';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';
//import { Programmanagement } from '../programmanagement/programmanagement.module';
import { CommonModule } from '@angular/common';
//import { ProgramManagementModule } from './programmanagement.module';
import { DatePipe } from '@angular/common'

import { AdminService } from '../services/admin.service';
import { CustomValidators } from 'ng2-validation';
import 'rxjs/add/operator/debounceTime';
import { Subject } from 'rxjs/Subject';
import 'rxjs/Rx';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { retry } from 'rxjs/Operator/retry';
import { Exercise } from '../Models/Exercise';
import { Status } from '../Models/Status';
import { ExerciseType } from '../Models/ExerciseType ';



@Component({
  selector: 'app-viewexercise',
  templateUrl: './viewexercise.component.html',
  styleUrls: ['./viewexercise.component.css'],
   providers: [DatePipe]
})
export class ViewExerciseComponent implements OnInit {


  listofexercisetype: ExerciseType[];
  public exerciseUpdateFormGroup: FormGroup;
  public form: FormGroup;
  exercise: Exercise;
  id: any;
  createdby: any;
  isSubmitted: boolean = false;
  exId: any;
  status: Status;
  constructor(private fb: FormBuilder, public datepipe: DatePipe, private adminservice: AdminService, private router: Router, public route: ActivatedRoute, private http: HttpClient) {

    this.exercise = new Exercise();



  }

  ngOnInit() {
    this.UpdateExerciseform();
    this.route.params.subscribe(params => {
      this.exId = params['exId'];

    });
    this.onGetExerciseById();
    this.onGetAllExerciseTypeList();
    this.createdby = localStorage.getItem('MemberName');

  }

  onGetAllExerciseTypeList() {
    let id = localStorage.getItem('AccountId');
    //alert(id);
    this.adminservice.onGetAllExerciseTypeList(id).subscribe(data => {

      if (data.StatusCode == "1") {
        this.status = data;
        this.listofexercisetype = this.status.Data;
        // alert(JSON.stringify(this.Programstructures));


        return this.listofexercisetype;
      }




    });
  }
  onUpdateExercise(exercise) {
    let date = new Date();
    const Exercise = {
      Id: this.exercise.Id,
      ExerciseTitle: this.exerciseUpdateFormGroup.controls["exercise.ExerciseTitle"].value,
      ExerciseType_Id: this.exerciseUpdateFormGroup.controls["exercise.ExerciseType_Id"].value,
      NoofQuestions: this.exerciseUpdateFormGroup.controls["exercise.NoofQuestions"].value,
      ExerciseDescription: this.exerciseUpdateFormGroup.controls["exercise.ExerciseDescription"].value,
      ExerciseSynopsis: this.exerciseUpdateFormGroup.controls["exercise.ExerciseSynopsis"].value,
      ExerciseObjectives: this.exerciseUpdateFormGroup.controls["exercise.ExerciseObjectives"].value,
      ExerciseAccessType: this.exerciseUpdateFormGroup.controls["exercise.ExerciseAccessType"].value,
      IsExerciseModerated: this.exerciseUpdateFormGroup.controls["exercise.IsExerciseModerated"].value,
      ExerciseStatus: this.exerciseUpdateFormGroup.controls["exercise.ExerciseStatus"].value,
      ExerciseDependency: this.exerciseUpdateFormGroup.controls["exercise.ExerciseDependency"].value,
      ExerciseCreatedBy: this.exercise.ExerciseCreatedBy,
      ExerciseCreatedDate: this.exercise.ExerciseCreatedDate,
      ExerciseUpdatedBy: this.createdby,
      ExerciseUpdatedDate: this.datepipe.transform(date, "yyyy-MM-dd")
    }
    if (this.exerciseUpdateFormGroup.valid) {
      //  alert(JSON.stringify(accountmanagement));
      this.isSubmitted = false;
      // alert(JSON.stringify(license));

    
      this.adminservice.onUpdateExercise(Exercise).subscribe(res => {
        console.log(res);
        this.exercise = res
      });
      alert("Updated Successfully");
      this.router.navigate(['/exercise/']);

    }
    else {
      this.isSubmitted = true;
      //alert(JSON.stringify(license));
    }
    //alert(JSON.stringify(account));
  }
  test(id) {
    //  alert("Hi");
    //console.log(id);
    //alert(id);
    console.log(this.exercise.ExerciseType_Id);
  //  alert(this.exercise.ExerciseType_Id);
  }
  UpdateExerciseform() {
    this.exerciseUpdateFormGroup = this.fb.group({
      // 'themestudio.ThemeStudio_Id': ['', [Validators.required]],

      'exercise.ExerciseTitle': ['', [Validators.required]],
     // 'exercise.Exercise_Type_Id': ['', [Validators.required]],
      'exercise.NoofQuestions': ['', [Validators.required]],
      'exercise.ExerciseDescription': ['', [Validators.required]],
      'exercise.ExerciseSynopsis': ['', [Validators.required]],
      'exercise.ExerciseObjectives': ['', [Validators.required]],
      'exercise.ExerciseAccessType': ['', [Validators.required]],

      'exercise.ExerciseStatus': ['', [Validators.required]],
      'exercise.ExerciseType_Id': ['', [Validators.required]],
      'exercise.ExerciseDependency': ['', [Validators.required]],
      'exercise.IsExerciseModerated': ['']
      //'exercise.Exercise_Type_Id': ['', [Validators.required]],
    });
  }
  onGetExerciseById() {

    this.adminservice.onGetExerciseById(this.exId).subscribe(result => {

      this.status = result;
      if (this.status.StatusCode == "1") {


        this.exercise = this.status.Data;
      }
      else {
        alert(this.status.Data);
      }


      console.log(this.exercise);
    });
    return this.exercise;
  }

}
