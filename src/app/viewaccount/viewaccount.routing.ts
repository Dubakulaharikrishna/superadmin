import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';


import { ViewaccountComponent } from './viewaccount.component';

export const ViewaccountRoutes: Routes = [

  {
    path: '',
    component: ViewaccountComponent,
    canActivate: [AuthGuard],

    data: {
      heading: 'Viewaccount works!'
    }    
  },

  
];

