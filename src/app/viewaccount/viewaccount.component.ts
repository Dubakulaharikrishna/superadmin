import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';

import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { Router, ActivatedRoute } from '@angular/router';
import { AdminService } from '../services/admin.service';
import { AccountManagement } from '../Models/AccountManagement';
import { retry } from 'rxjs/operator/retry';
import { Status } from '../Models/Status';
import { InstanceManager } from '../Models/InstanceManager';
import { Webclient } from '../Models/Webclient';
import { License } from '../Models/license';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-viewaccount',
  templateUrl: './viewaccount.component.html',
  styleUrls: ['./viewaccount.component.css'],
  providers: [DatePipe]
})
export class ViewaccountComponent implements OnInit {
  licensetype: any;
  lstartdate: any;
  licensekey: any;
  lgenerateddate: any;
  lgeneratedby: any;
  lenddate: any;
  decode: any;
  companyemail: any;
  compemail: any;
  regnum: any;
  companyname: any;
  clientid: any;
  webclient: any;
  instanceadmin: any;
  instancename: any;
  instancemanager: any;
  public accountUpdateFormGroup: FormGroup;
  accountmanagement: AccountManagement;
  id: any;
  isSubmitted: boolean = false;
  accId: any;
  listofinstances: InstanceManager[];
  webclientslist: Webclient[];
  listofLicense: License[];
  status: Status;
  createdby: any;
  instid: any;
  ClientInfo: any ;
  constructor(private adminservice: AdminService, public datepipe: DatePipe, private router: Router, public route: ActivatedRoute, private http: HttpClient, public fb: FormBuilder)
  {
    this.accountmanagement = new AccountManagement();
     //this.ClientInfo = {};
        this.accountmanagement.InstanceInfo = {};
        //this.accountmanagement.LicenseInfo = {};
  }

  ngOnInit() {
    this.Accountmanagementform();
    this.route.params.subscribe(params => {
      this.accId = params['accId'];
    
    });
    this.onGetAccountById();
    this.onGetAllInstanceManagerList();
    this.onGetAllWebClientList();
    this.onGetAllLicenseList();
    this.createdby = localStorage.getItem('MemberName');
    this.instid = localStorage.getItem('InstanceId');
  }

  onGetInstanceManagerById() {
    this.adminservice.onGetInstanceManagerById(this.accountmanagement.LicenseInfo).subscribe(result => {
      //  this.status = result;
      //    alert(JSON.stringify(result));
      if (result.success == 1) {


        this.instancemanager = result.data;
        this.instancename = result.data.InstanceName;
        this.instanceadmin = result.data.InstanceAdmin;
      }
      else {
        alert(result.data);
      }

      console.log(this.instancemanager);



    });
    return this.instancemanager;
  }
  onGetWebClientById() {
   // alert(this.accountmanagement.ClientInfo);
    this.adminservice.onGetWebClientById(this.accountmanagement.ClientInfo).subscribe(result => {
      //  alert(JSON.stringify(result));
      // console.log("hiii", result);
      //  this.status = result;
      if (result.success == 1) {


        this.webclient = result.data;
        this.clientid = result.data.Client_Id;
        this.companyname = result.data.CompanyName;
        this.regnum = result.data.CompanyRegistrationNumber;
        this.compemail = result.data.CompanyEmailID;
        this.companyemail = result.data.CompanyAdminEmailID;
      }
      else {
        alert(result.data);
      }
      console.log(this.webclient);




    });
    return this.webclient;
  }


  onGetLicenseById() {
    this.adminservice.onGetLicenseById(this.accountmanagement.LicenseInfo).subscribe(result => {
      // this.status = result;
      if (result.success == 1) {


        this.license = result.data;
        this.decode = result.data.DecodeScript;
        this.lenddate = result.data.LicenseEndDate;
        this.lgeneratedby = result.data.LicenseGeneratedBy;
        this.lgenerateddate = result.data.LicenseGeneratedDate;
        this.licensekey = result.data.LicenseKey;
        this.lstartdate = result.data.LicenseStartDate;
        this.licensetype = result.data.LicenseType;

      }
      else {
        alert(result.data);
      }
      //this.requestStatus = result.RequestStatus;
      //this.licenseStatus = result.LicenseStatus;
      console.log(this.license);

    });
    return true;
  }




  test(id) {
    //  alert("Hi");
    //console.log(id);
    //alert(id);
    console.log(this.accountmanagement.InstanceInfo);
    this.onGetInstanceManagerById();
 //   alert(this.accountmanagement.InstanceInfo);
  }

  client(id) {
    console.log(this.accountmanagement.ClientInfo);
    this.onGetWebClientById();
  //  alert(this.accountmanagement.ClientInfo);

  }
  license(id) {
    console.log(this.accountmanagement.LicenseInfo);
    this.onGetLicenseById();
   // alert(this.accountmanagement.LicenseInfo);
  }

  onGetAllInstanceManagerList() {
    this.adminservice.onGetAllInstanceManagerList().subscribe(result => {
      if (result.success == 1) {
        this.listofinstances = result.data;

        // alert(JSON.stringify(this.Programstructures));
        return this.listofinstances
      }

    });
  }
  onGetAllWebClientList() {
    this.adminservice.onGetAllWebClientList().subscribe(result => {

      if (result.success == 1) {

        this.webclientslist = result.data;

        // alert(JSON.stringify(this.Programstructures));
        return this.webclientslist
      }

    });
  }
  onGetAllLicenseList() {
    this.adminservice.onGetAllLicenseList().subscribe(result => {

      if (result.success == 1) {
        this.listofLicense = result.data;
        // alert(JSON.stringify(this.Programstructures));
        return this.listofLicense
      }

    });
  }

  onUpdateAccount(accountmanagement: any) {
    let date = new Date();
    const account = {
      _id: accountmanagement._id,
      InstanceManagerId: this.instid,
      AccountName: this.accountUpdateFormGroup.controls["accountmanagement.AccountName"].value,
      AccountWebUrl: this.accountUpdateFormGroup.controls["accountmanagement.AccountWebUrl"].value,
      AccountAdmin: this.accountUpdateFormGroup.controls["accountmanagement.AccountAdmin"].value,
      DevUrl: this.accountUpdateFormGroup.controls["accountmanagement.DevUrl"].value,
      ClientInfo: { Client_Id: this.clientid, CompanyName: this.companyname, CompanyRegistrationNumber: this.regnum, CompanyEmailID: this.compemail, CompanyAdminEmailID: this.companyemail },
      InstanceInfo: {
        InstanceName: this.instancename, InstanceAdmin: this.instanceadmin
      },
      LicenseInfo: { DecodeScript: this.decode, LicenseEndDate: this.lenddate, LicenseGeneratedBy: this.lgeneratedby, LicenseGeneratedDate: this.lgenerateddate, LicenseKey: this.licensekey, LicenseStartDate: this.lstartdate, LicenseType: this.licensetype },
      AppUrl: this.accountUpdateFormGroup.controls["accountmanagement.AppUrl"].value,
     // IsMobileEnabled: this.accountUpdateFormGroup.controls['accountmanagement.IsMobileEnabled'].value,
     // IsAndroidAppPublic: this.accountUpdateFormGroup.controls['accountmanagement.IsAndroidAppPublic'].value,
     // IsIOSAppPublic: this.accountUpdateFormGroup.controls['accountmanagement.IsIOSAppPublic'].value,
    //  IsHttpsEnabled: this.accountUpdateFormGroup.controls['accountmanagement.IsHttpsEnabled'].value,
    //  IsLiveStatus: this.accountUpdateFormGroup.controls['accountmanagement.IsLiveStatus'].value,
      CreatedBy: this.accountmanagement.AccountCreatedBy,
      
     
      UpdatedBy: this.createdby,
      AccountUpdatedDate: this.datepipe.transform(date, "yyyy-MM-dd"),
    }

    if (this.accountUpdateFormGroup.valid) {
    //   alert(JSON.stringify(accountmanagement));
      this.isSubmitted = false;
   

      this.adminservice.onUpdateAccount(account).subscribe(result => {
        console.log(result);
        //this.account = result
        this.router.navigate(['/accountmanagement/']);
      });
      alert("Successfully Updated");
      
    }
    else {
      this.isSubmitted = true;
      //alert(JSON.stringify(license));
    }
    //alert(JSON.stringify(account));
  }

  Accountmanagementform() {
    this.accountUpdateFormGroup = this.fb.group({
      // AccountID: ['', [Validators.required]],
     'accountmanagement.AccountName': ['', [Validators.required]],
      'accountmanagement.AccountWebUrl': ['', [Validators.required]],
     // 'accountmanagement.IsMobileEnabled': [''],
     // 'accountmanagement.IsAndroidAppPublic': [''],
     // 'accountmanagement.IsIOSAppPublic': [''],
      'accountmanagement.AccountAdmin': ['', [Validators.required]],
     // 'accountmanagement.AccountCreatedDate': ['', [Validators.required]],
     // 'accountmanagement.IsHttpsEnabled': [''],
      'accountmanagement.DevUrl': ['', [Validators.required]],
     // 'accountmanagement.IsLiveStatus': [''],
      'ClientInfo': ['', [Validators.required]],
      'accountmanagement.InstanceInfo.InstanceName': ['', [Validators.required]],
      'accountmanagement.LicenseInfo.CompanyName': ['', [Validators.required]],
      'accountmanagement.AppUrl': ['', [Validators.required]],

    });
  }

  //onUpdateAccount(account) {
  //  this.adminservice.onUpdateAccount(account).subscribe(res => this.accountmanagement = res);

  //}


  onGetAccountById() {
    //alert(id);
    this.adminservice.onGetAccountsById(this.accId).subscribe(result => {
     // alert(JSON.stringify(result));
      if (result.success == 1) {

        this.accountmanagement = result.data;
        //this.accountmanagement.ClientInfo = {};
        //this.accountmanagement.InstanceInfo = {};
        //this.accountmanagement.LicenseInfo = {};
        this.ClientInfo = result.data.ClientInfo.CompanyName;
       
        //this.accountmanagement.InstanceInfo.InstanceName = result.data.InstanceInfo.InstanceName;
       // this.accountmanagement.LicenseInfo.CompanyName = result.data.LicenseInfo.CompanyName;
      }
      else {
        alert(result.data);
      }


      console.log(this.accountmanagement);
    });
    return this.accountmanagement;
  }


}
