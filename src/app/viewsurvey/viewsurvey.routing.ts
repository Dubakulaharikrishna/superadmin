import { Routes } from '@angular/router';

import { ViewsurveyComponent } from './viewsurvey.component';

export const ViewsurveyRoutes: Routes = [

  {
    path: '',
    component: ViewsurveyComponent,
    data: {
      heading: 'Viewsurvey works!'
    }    
  },

  
];

