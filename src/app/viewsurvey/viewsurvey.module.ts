//import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//import { DashboardComponent } from './dashboard.component';
import { RouterModule } from '@angular/router';
import { ViewsurveyRoutes } from './viewsurvey.routing';
import { ViewsurveyComponent } from '../viewsurvey/viewsurvey.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { HttpModule } from '@angular/http';
import { DataTablesModule } from 'angular-datatables';
import {HttpClientModule} from '@angular/common/http';
import { CommonModule } from '@angular/common'; 
import { BrowserModule } from '@angular/platform-browser';




@NgModule({
  imports: [
    RouterModule.forChild(ViewsurveyRoutes),
    DataTablesModule,
    HttpClientModule,
    CommonModule,
    
 HttpModule
    //BrowserModule
  ],
  declarations: [
   
    ViewsurveyComponent
    
  ],
 
  providers: [],
  bootstrap: [ViewsurveyComponent]
})
export class ViewsurveyModule { }






