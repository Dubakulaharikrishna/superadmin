import { Routes } from '@angular/router';

import { LivetrackingComponent } from './livetracking.component';

export const LivetrackingRoutes: Routes = [

  {
    path: '',
    component: LivetrackingComponent,
    data: {
      heading: 'Livetracking'
    }    
  },

  
];

