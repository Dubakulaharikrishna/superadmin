import { Component, OnInit } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { Router } from '@angular/router';

@Component({
  selector: 'app-livetracking',
  templateUrl: './livetracking.component.html',
  styleUrls: ['./livetracking.component.css']
})
export class LivetrackingComponent implements OnInit {

  result: any[];
  title = 'Simple Datatable Example using Angular 4';
  public data: Object;
  public temp_var: Object = false;
constructor(private router: Router, private http: HttpClient) {
    //this.cpopup = false;
    //this.cmask = false;

  

  }

  openCpopup() {

    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "block";
    cmask.style.display = "block";
  }

  closeCpopup() {
    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "none";
    cmask.style.display = "none";
  }
  onEdit() {
    this.router.navigate(['/viewlive/']);
  }

     ngOnInit(): 
  void {
 this.http.get('https://jsonplaceholder.typicode.com/posts').subscribe((res: Response) => {
       this.data=res;
      this.temp_var=true;
    });

  }

}
