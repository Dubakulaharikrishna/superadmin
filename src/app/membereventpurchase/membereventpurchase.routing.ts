import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';

import { MembereventpurchaseComponent } from './membereventpurchase.component';

export const MembereventpurchaseRoutes: Routes = [

  {
    path: '',
    component: MembereventpurchaseComponent,
    canActivate: [AuthGuard],

    data: {
      heading: 'Login'
    }
  },


];

