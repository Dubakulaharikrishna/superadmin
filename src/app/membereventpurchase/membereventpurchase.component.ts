import { Component, OnInit, ElementRef } from '@angular/core';
//changes
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';
import { ExcelService } from '../services/excel.service';
import { AdminService } from '../services/admin.service';
import { Status } from '../Models/Status';
import { Ng2SmartTableModule, LocalDataSource } from 'ng2-smart-table';
import { Eventmaster } from '../Models/Eventmaster';
import { Eventtype } from '../Models/Eventtype';
import { programcategory } from '../Models/programcategory';
import { AccountManagement } from '../Models/AccountManagement';
import { MemberEventPurchase } from '../Models/MemberEventPurchase';

@Component({
  selector: 'app-membereventpurchase',
  templateUrl: './membereventpurchase.component.html',
  styleUrls: ['./membereventpurchase.component.css'],
  providers: [ExcelService]
})
export class MembereventpurchaseComponent implements OnInit {
  MasterAccount_Id: any;
  accountId: any;
  pagename: any;
  status: Status;
  listofAccounts: AccountManagement[];
  listofmembereventpurchase: MemberEventPurchase[];
  source: LocalDataSource;
  selectedValue: any = 50;
  page: any;
  roleId: any;
  constructor(public router: Router, public ele: ElementRef, private excelService: ExcelService, public http: HttpClient, public fb: FormBuilder, private adminservice: AdminService) { }

  ngOnInit() {
   // localStorage.removeItem('loginSessId');
    //localStorage.clear();
   // localStorage.setTimeout = 1;
    //sessionStorage.removeItem('loginSessId');
    //localStorage.removeItem('loginSessId');
    this.accountId = localStorage.getItem('AccountId');
    this.pagename = "Member EventPurchase";
    localStorage.setItem('loginSessId', this.pagename);
    this.onGetAllAccountsList();
    this.OnGetAllMemberEventPurchases(this.accountId);
    this.roleId = localStorage.getItem('RoleId');

  }

  onGetAllAccountsList() {
    this.adminservice.onGetAllAccountsList().subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofAccounts = this.status.Data;

        //alert(JSON.stringify(this.listofAccounts));
        return this.listofAccounts
      }

    });
  }


  OnGetAllMemberEventPurchases(id) {
    // let id = sessionStorage.getItem('AccountId');
    this.adminservice.OnGetAllMemberEventPurchases(id).subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofmembereventpurchase = this.status.Data;
        this.source = new LocalDataSource(this.listofmembereventpurchase);
        //alert(JSON.stringify(this.listofAccounts));
        return this.listofmembereventpurchase
      }

    });
  }
  onEdit(event: any) {
    this.router.navigate(['/viewmembereventpurchase/', { mevepId: event.data.Id }]);
  }
  onChange(event) {
    console.log(event);
    this.OnGetAllMemberEventPurchases(event.target.value);
  }
  reset() {
    this.OnGetAllMemberEventPurchases(this.accountId);
    this.MasterAccount_Id = undefined;
  }
  /*Smart table*/
  settings = {

    columns: {
      PaymentMode: {
        title: 'Payment Mode',
        filter: false
      },
      PaymentStatus: {
        title: 'Payment Status',
        filter: false
      },

    },
    attr: { class: 'table table-bordered' },
    actions: {
      edit: false, //as an example
      delete: false, //as an example
      add: false,
      position: 'right',
      custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button" (click)="onEdit(rec.Id)">View</button></span>` }]
    },
    pager: { display: true, perPage: 50 },
  };

  //route(event) {
  //  alert('hi');
  //}

  onSearch(query: string = '') {
    if (query != '') {
      this.source.setFilter([
        // fields we want to include in the search
        {
          field: 'PaymentMode',
          search: query
        },
        {
          field: 'PaymentStatus',
          search: query
        }
      ], false);
    }
    else {
      this.source = new LocalDataSource(this.listofmembereventpurchase);
    }

    // second parameter specifying whether to perform 'AND' or 'OR' search 
    // (meaning all columns should contain search query or at least one)
    // 'AND' by default, so changing to 'OR' by setting false here
  }

  onSelectedFilter(event) {
    this.page = event.target.value;
    this.source = new LocalDataSource(this.listofmembereventpurchase);
    this.settings = {

      columns: {
        PaymentMode: {
          title: 'Payment Mode',
          filter: false
        },
        PaymentStatus: {
          title: 'Payment Status',
          filter: false
        },

      },
      attr: { class: 'table table-bordered' },
      actions: {
        edit: false, //as an example
        delete: false, //as an example
        add: false,
        position: 'right',
        custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button">View</button></span>` }]
      },
      pager: { display: true, perPage: this.page },
    };

  }

  onExport() {
    this.excelService.exportAsExcelFile(this.listofmembereventpurchase, 'MemberEventPurchase');
  }


}
