import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MembereventpurchaseComponent } from './membereventpurchase.component';

describe('MembereventpurchaseComponent', () => {
  let component: MembereventpurchaseComponent;
  let fixture: ComponentFixture<MembereventpurchaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MembereventpurchaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MembereventpurchaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
