//import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//import { DashboardComponent } from './dashboard.component';
import { RouterModule } from '@angular/router';
import { VieweventRoutes } from './viewevent.routing';
import { VieweventComponent } from '../viewevent/viewevent.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { HttpModule } from '@angular/http';
import { DataTablesModule } from 'angular-datatables';
import {HttpClientModule} from '@angular/common/http';
import { CommonModule } from '@angular/common'; 
import { BrowserModule } from '@angular/platform-browser';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';
import { AdminService } from '../services/admin.service';




@NgModule({
  imports: [
    RouterModule.forChild(VieweventRoutes),
    DataTablesModule,
    HttpClientModule,
    CommonModule,
    
 HttpModule
    //BrowserModule
  ],
  declarations: [
   
    VieweventComponent
    
  ],
 
  providers: [AdminService, AuthGuard, NotAuthGuard],
  bootstrap: [VieweventComponent]
})
export class VieweventModule { }






