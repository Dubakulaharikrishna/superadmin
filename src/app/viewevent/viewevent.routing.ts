import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';

import { VieweventComponent } from './viewevent.component';

export const VieweventRoutes: Routes = [

  {
    path: '',
    component: VieweventComponent,
    canActivate: [AuthGuard],
    data: {
      heading: 'Viewevent works!'
    }    
  },

  
];

