import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';

import { AccountmembershipComponent } from './accountmembership.component';
//import { ProgramManagementComponent } './programmanagement/programmanagement.component'
export const AccountMembershipRoutes: Routes = [

  {
    path: '',
    component: AccountmembershipComponent,
    canActivate: [AuthGuard],
    data: {
      heading: 'AccountMembership'
    }
  },


];
