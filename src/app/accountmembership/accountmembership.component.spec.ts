import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountmembershipComponent } from './accountmembership.component';

describe('AccountmembershipComponent', () => {
  let component: AccountmembershipComponent;
  let fixture: ComponentFixture<AccountmembershipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountmembershipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountmembershipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
