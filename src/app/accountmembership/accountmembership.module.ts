
//import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//import { DashboardComponent } from './dashboard.component';
import { RouterModule } from '@angular/router';
import { AccountmembershipComponent } from '../accountmembership/accountmembership.component';
import { AccountMembershipRoutes } from './accountmembership.routing';
//import { programmanagement } from './programmanagement';
//import { ProgramManagementComponent } './programmanagement/programmanagement.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { HttpModule } from '@angular/http';
import { DataTablesModule } from 'angular-datatables';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
//import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, FormBuilder, FormGroup, Validators, FormControl, ReactiveFormsModule } from '@angular/forms';

import { NgbAccordionModule } from '@ng-bootstrap/ng-bootstrap';
import { CustomFormsModule } from 'ng2-validation';
import { NgbProgressbarModule, NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AdminService } from '../services/admin.service';

import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';
import { Ng2SmartTableModule } from 'ng2-smart-table';


@NgModule({
  imports: [DataTablesModule,
    RouterModule,
    HttpClientModule,
    CommonModule,
    NgbModule,
    NgbProgressbarModule,
    NgbTabsetModule,
    CustomFormsModule,
    NgbAccordionModule,
    ReactiveFormsModule,
    FormsModule,
    //BrowserModule,
    Ng2SmartTableModule,
    HttpModule,
    RouterModule.forChild(AccountMembershipRoutes),
    //BrowserModule
  ],
  declarations: [

    AccountmembershipComponent,

  ],

  providers: [AdminService, AuthGuard, NotAuthGuard],
  bootstrap: [AccountmembershipComponent]
})
export class AccountMembershipModule { }
