import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
//import { BrowserModule } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import 'rxjs/Rx';
import { Subject } from 'rxjs/Subject';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';
//import { Programmanagement } from '../programmanagement/programmanagement.module';
//import { CommonModule } from '@angular/common';
//import { ProgramManagementModule } from './programmanagement.module';
import { CommonModule } from '@angular/common';
import { AccountMembership } from '../Models/accountmembership';
import { AdminService } from '../services/admin.service';
import { Status } from '../Models/Status';
import { Ng2SmartTableModule, LocalDataSource } from 'ng2-smart-table';
import { AccountManagement } from '../Models/AccountManagement';
import { Members } from '../Models/Members';
import { ExcelService } from '../services/excel.service';
import { DatePipe } from '@angular/common'

@Component({
  selector: 'app-accountmembership',
  templateUrl: './accountmembership.component.html',
  styleUrls: ['./accountmembership.component.css'],
  providers: [ExcelService, DatePipe]
})
export class AccountmembershipComponent implements OnInit {
  dataLoaded: any;
  coverPic: any;
  Member_Id: any;
  MemberAccount_Id: any;
  MasterAccount_Id: any;
  success: boolean = false;
  message: any;
  source: LocalDataSource;
  selectedValue: any = 50;
  page: any;
  result: any[];
  title = 'Simple Datatable Example using Angular 4';
  public data: Object;
  public temp_var: Object = false;
  public form: FormGroup;
  account: AccountMembership;
  public AccountMemberShipForm: FormGroup;
  isSubmitted: boolean = false;
  listofaccountmembership: AccountMembership[];
  listofMembers: Members[];
  id: any;
  createdby: any;
  this: any;
  roleId: any;
  pagename: any;
  listofAccounts: AccountManagement[];
  status: Status;
  accountId: any;
  dtTrigger: Subject<any> = new Subject();
  public _success = new Subject<string>();
  staticAlertClosed = false;
  constructor(private router: Router, public ele: ElementRef, public datepipe: DatePipe, private excelService: ExcelService, private http: HttpClient, public fb: FormBuilder, public adminservice: AdminService) {
    this.account = new AccountMembership();
    this.isSubmitted = false;
  }




  ngOnInit() {

    // sessionStorage.clear();
    //localStorage.removeItem('loginSessId');
    //localStorage.clear();
    //  localStorage.setTimeout = 1;
    // sessionStorage.removeItem('loginSessId');
    // localStorage.removeItem('loginSessId');
    this.pagename = "Account member ship";
    localStorage.setItem('loginSessId', this.pagename);
    this.accountId = localStorage.getItem('AccountId');
    this.roleId = localStorage.getItem('RoleId');
    setTimeout(() => this.staticAlertClosed = true, 20000);
    this._success.subscribe((message) => this.message = message);
    this._success.debounceTime(7000).subscribe(() => this.message = null);
    this.createdby = localStorage.getItem('MemberName');
    this.AccountMemberShipform();
    this.onGetAllAccountMemberShipList(this.accountId);
    this.onGetAllAccountsList();
    this.onGetAllMembersList();
  }

  openCpopup() {

    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "block";
    cmask.style.display = "block";
  }

  closeCpopup() {
    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "none";
    cmask.style.display = "none";
  }
  onEdit(event: any) {
    this.router.navigate(['/viewmembership/', { accId: event.data.Id }]);
  }
  reset() {
    this.onGetAllAccountMemberShipList(this.accountId);
    this.MasterAccount_Id = undefined;
  }


  test(id) {
   
    console.log(this.MemberAccount_Id);
   // alert(this.MemberAccount_Id);
  }
  member(id) {
    //  alert("Hi");
    //console.log(id);
    //alert(id);
    console.log(this.Member_Id);
   // alert(this.Member_Id);
  }
  onGetAllAccountsList() {
    this.adminservice.onGetAllAccountsList().subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofAccounts = this.status.Data;

        //alert(JSON.stringify(this.listofAccounts));
        return this.listofAccounts
      }

    });
  }
  onGetAllMembersList() {
    let id = localStorage.getItem('AccountId');

    this.adminservice.onGetAllMembersList(id).subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofMembers = this.status.Data;
       
        // alert(JSON.stringify(this.Programstructures));
        return this.listofMembers
      }



    });
  }

  onGetAllAccountMemberShipList(id) {
    
   // alert(id);
    this.adminservice.onGetAllAccountMemberShipList(id).subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofaccountmembership = this.status.Data;
        this.source = new LocalDataSource(this.listofaccountmembership);
        // alert(JSON.stringify(this.Programstructures));
        return this.listofaccountmembership
      }
     
    });
  }
  onChange(event) {
    console.log(event);
    this.onGetAllAccountMemberShipList(event.target.value);
  }


  onCreateAccountMembership() {
    let files = this.ele.nativeElement.querySelector('#ACM_ImageUrl').files;
    if (files.length > 0 && files.count != 0 && files != null) {

      let formData = new FormData();
      let file = files[0];
      this.coverPic = file.name;
      formData.append('ACM_ImageUrl', file, file.name);
      console.log(formData);
      console.log(files);
      this.adminservice.onfileupload(formData).subscribe(res => this.dataLoaded = res);
    }
    //alert(JSON.stringify(program));
    let date = new Date();
    const accountMembership = {
      Member_Id: this.form.controls["Member_Id"].value,
      MemberAccount_Id: this.form.controls["MemberAccount_Id"].value,
      ACM_UserName: this.form.controls["ACM_UserName"].value,
      ACM_Email: this.form.controls["ACM_Email"].value,
      ACM_Password: this.form.controls["ACM_Password"].value,
      ACM_LastLogin: this.form.controls["ACM_LastLogin"].value,
      ACM_Role_Id: this.form.controls["ACM_Role_Id"].value,
      ACM_LoginStatus: this.form.controls["ACM_LoginStatus"].value,
      ACM_ImageUrl: "Tcsimage/acmimages/" + this.coverPic,
      ACM_CreatedBy: this.createdby,
      ACM_CreatedDate: this.datepipe.transform(date, "yyyy-MM-dd")
    }
    if (this.form.valid) {
      this.isSubmitted = false;
      alert(JSON.stringify(accountMembership));
      this.adminservice.onCreateAccountMemberShip(accountMembership).subscribe(data => {
        console.log(data);
        if (data.StatusCode == "1") {
          this.success = true;
          this.message = data.Message;
          this._success.next(`Record Saved Successfully`);
          this.onGetAllAccountMemberShipList(this.accountId);
          this.closeCpopup();

        }
        else {
          this.success = false;
          this.message = "Failed to create account master";
        }
      });
    }
    else {
      this.isSubmitted = true;
    }
  
  }


  AccountMemberShipform() {
    this.form = this.fb.group({
      'Member_Id': ['', [Validators.required]],
      'MemberAccount_Id': ['', [Validators.required]],

      'ACM_UserName': ['', [Validators.required]],

      'ACM_Email': ['', [Validators.required]],
      'ACM_Password': ['', [Validators.required]],
      'ACM_LastLogin': ['', [Validators.required]],
      'ACM_Role_Id': ['', [Validators.required]],
      'ACM_LoginStatus': ['', [Validators.required]],



    });
  }


  /*Smart table*/
  settings = {

    columns: {
      ACM_UserName: {
        title: 'User Name',
        filter: false
      },
      ACM_Email: {
        title: 'Email',
        filter: false
      },
    
    },
    attr: { class: 'table table-bordered' },
    actions: {
      edit: false, //as an example
      delete: false, //as an example
      add: false,
      position: 'right',
      custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button" (click)="onEdit(rec.Id)">Edit</button></span>` }]
    },
    pager: { display: true, perPage: 50 },
  };

  //route(event) {
  //  alert('hi');
  //}

  onSearch(query: string = '') {
    if (query != '') {
      this.source.setFilter([
        // fields we want to include in the search
        {
          field: 'ACM_UserName',
          search: query
        },
        {
          field: 'ACM_Email',
          search: query
        }
      ], false);
    }
    else {
      this.source = new LocalDataSource(this.listofaccountmembership);
    }

    // second parameter specifying whether to perform 'AND' or 'OR' search 
    // (meaning all columns should contain search query or at least one)
    // 'AND' by default, so changing to 'OR' by setting false here
  }

  onSelectedFilter(event) {
    this.page = event.target.value;
    this.source = new LocalDataSource(this.listofaccountmembership);
    this.settings = {

      columns: {
        ACM_UserName: {
          title: 'User Name',
          filter: false
        },
        ACM_Email: {
          title: 'Email',
          filter: false
        },
     
      },
      attr: { class: 'table table-bordered' },
      actions: {
        edit: false, //as an example
        delete: false, //as an example
        add: false,
        position: 'right',
        custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button">Edit</button></span>` }]
      },
      pager: { display: true, perPage: this.page },
    };

  }


  onExport() {
    this.excelService.exportAsExcelFile(this.listofaccountmembership, 'AccountMembership');
  }

}
