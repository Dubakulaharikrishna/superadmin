
import { Component, OnInit, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';

import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { Router, ActivatedRoute } from '@angular/router';
import { AdminService } from '../services/admin.service';

import { retry } from 'rxjs/operator/retry';

import { Status } from '../Models/Status';
import { Eventtype } from '../Models/Eventtype';
import { Eventmaster } from '../Models/Eventmaster';
import { programcategory } from '../Models/programcategory';

import { MemberEventPurchase } from '../Models/MemberEventPurchase';
import { Members } from '../Models/Members';

@Component({
  selector: 'app-viewmembereventpurchase',
  templateUrl: './viewmembereventpurchase.component.html',
  styleUrls: ['./viewmembereventpurchase.component.css']
})
export class ViewmembereventpurchaseComponent implements OnInit {
  status: Status;
  mevepId: any;
  membereventpurchase: MemberEventPurchase;
  listofMembers: Members[];
  userName: any;
  constructor(private adminservice: AdminService,  public ele: ElementRef, private router: Router, public route: ActivatedRoute, private http: HttpClient, public fb: FormBuilder) {

    this.membereventpurchase = new MemberEventPurchase();

  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.mevepId = params['mevepId'];

    });
    this.onGetMemberEventPurchaseById();
  }
  onGetMemberEventPurchaseById() {

    let id = localStorage.getItem('AccountId');
    this.adminservice.onGetAllMembersList(id).subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofMembers = this.status.Data;
        console.log(this.listofMembers);
        console.log(data);
        //alert(id);
        this.adminservice.onGetMemberEventPurchaseById(this.mevepId).subscribe(result => {

          this.status = result;
          if (this.status.StatusCode == "1") {


            this.membereventpurchase = this.status.Data;
            // alert(JSON.stringify(this.eventmaster));
            this.userName = this.listofMembers.filter(m => m.Id == this.membereventpurchase.Member_Id)[0].UserName;
          }
          else {
            alert(this.status.Data);
          }
          console.log(this.membereventpurchase);


          //if (this.eventregistration.EventStartDate != null && this.eventregistration.EventStartDate != "" && this.eventregistration.EventStartDate != undefined) {
          //  this.startDate = this.datepipe.transform(this.eventregistration.EventStartDate, 'yyyy-MM-dd');
          //  this.eventregistration.EventStartDate = this.startDate;
          //}



          //if (this.eventregistration.EventEndDate != null && this.eventregistration.EventEndDate != "" && this.eventregistration.EventEndDate != undefined) {
          //  this.endDate = this.datepipe.transform(this.eventregistration.EventEndDate, 'yyyy-MM-dd');
          //  this.eventregistration.EventEndDate = this.endDate;
          //}

          //if (this.eventregistration.EventStartTime != null && this.eventregistration.EventStartTime != "" && this.eventregistration.EventStartTime != undefined) {
          //  this.startTime = this.datepipe.transform(this.eventregistration.EventStartTime, 'yyyy-MM-dd');
          //  this.eventregistration.EventStartTime = this.startDate;
          //}
          //if (this.eventregistration.EventEndTime != null && this.eventregistration.EventEndTime != "" && this.eventregistration.EventEndTime != undefined) {
          //  this.endTime = this.datepipe.transform(this.eventregistration.EventEndTime, 'yyyy-MM-dd');
          //  this.eventregistration.EventEndTime = this.startDate;
          //}





        });

        return this.membereventpurchase;
      }
    });
  }

}
