import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewmembereventpurchaseComponent } from './viewmembereventpurchase.component';

describe('ViewmembereventpurchaseComponent', () => {
  let component: ViewmembereventpurchaseComponent;
  let fixture: ComponentFixture<ViewmembereventpurchaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewmembereventpurchaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewmembereventpurchaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
