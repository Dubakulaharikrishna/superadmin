import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';

import { ViewmembereventpurchaseComponent } from './viewmembereventpurchase.component';

export const ViewmembereventpurchaseRoutes: Routes = [

  {
    path: '',
    component: ViewmembereventpurchaseComponent,
    canActivate: [AuthGuard],
    data: {
      heading: 'Viewaccount works!'
    }    
  },

  
];

