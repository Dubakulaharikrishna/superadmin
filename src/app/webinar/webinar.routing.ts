import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';

import { WebinarComponent } from './webinar.component';

export const WebinarRoutes: Routes = [

  {
    path: '',
    component: WebinarComponent,
    canActivate: [AuthGuard],
    data: {
      heading: 'Webinar works'
    }
  },


];

