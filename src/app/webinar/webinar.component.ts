import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { Router } from '@angular/router';
//changes
import { Subject } from 'rxjs/Subject';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';

import { AdminService } from '../services/admin.service';

import { Status } from '../Models/Status';
import { Ng2SmartTableModule, LocalDataSource } from 'ng2-smart-table';



@Component({
  selector: 'app-webinar',
  templateUrl: './webinar.component.html',
  styleUrls: ['./webinar.component.css']
})
export class WebinarComponent implements OnInit {
  webinar: any;
  MasterAccount: any;
  dataLoaded: any;
  listofupcomingwebinars: any[];
  listofhistoricalwebinars: any[];
  success: boolean = false;
  message: any;
  dtTrigger: Subject<any> = new Subject();
  public _success = new Subject<string>();
  staticAlertClosed = false;
  MasterAccount_Id: any;
  public isSubmitted: boolean = false;
  result: any[];
  title = 'Simple Datatable Example using Angular 4';
  source: LocalDataSource;
  source1: LocalDataSource;
  selectedValue: any = 50;
  page: any;
  public form: FormGroup;
 
  pagename: any;
  id: any;

  Id: any;
  roleId: any;
  status: Status;
  accountId: any;

  constructor(private adminservice: AdminService,  public ele: ElementRef, private router: Router, private http: HttpClient, public fb: FormBuilder) { }

  ngOnInit() {
    this.onNavigate();
    this.onGetWebinarTocken();
    
    this.pagename = "Webinar";
    localStorage.setItem('loginSessId', this.pagename);
    this.accountId = localStorage.getItem('AccountId');
    this.roleId = localStorage.getItem('RoleId');
    setTimeout(() => this.staticAlertClosed = true, 20000);
    this._success.subscribe((message) => this.message = message);
    this._success.debounceTime(7000).subscribe(() => this.message = null);
    
   
    
  }
  onNavigate() {

    window.open("https://api.getgo.com/oauth/v2/authorize?response_type=code&client_id=HAg4YueS1nAVGjzVxO5KArpiFZZDMNFk", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=400,height=400");
    console.log(window.localStorage);
    // localStorage.getItem('webinarSessionData' )
  }

  onGetWebinarTocken() {
    this.adminservice.onCreateWebinarpath().subscribe(result => {
      // alert(JSON.stringify(result));
      this.webinar = result.data;
      this.onGetAllUpComingWebinars(this.webinar);
      this.onGetAllHistoricalWebinars(this.webinar);
      console.log("hi", result);
    });
  }

  onGetAllUpComingWebinars(webinar) {
    //let id = localStorage.getItem('AccountId');
    
    this.adminservice.onGetAllUpcomingWebinars(webinar).subscribe(result => {
      
      if (result.success == 1) {
        this.listofupcomingwebinars = result.data;
        this.source = new LocalDataSource(this.listofupcomingwebinars);
        return this.listofupcomingwebinars;
      }
    });
  }

  onGetAllHistoricalWebinars(webinar) {
    //let id = localStorage.getItem('AccountId');

    this.adminservice.onGetAllHistoricalWebinars(webinar).subscribe(result => {
      console.log("web", result);
      if (result.success == 1) {
        this.listofhistoricalwebinars = result.data;
        this.source1 = new LocalDataSource(this.listofhistoricalwebinars);
        return this.listofhistoricalwebinars;
      }
    });
  }
  /*Smart table*/
  settings = {

    columns: {
      EventName: {
        title: 'Event Name',
        filter: false
      },
      EventDescription: {
        title: 'Event Description',
        filter: false
      },

    },
    attr: { class: 'table table-bordered' },
    actions: {
      edit: false, //as an example
      delete: false, //as an example
      add: false,
      position: 'right',
      custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button" (click)="onEdit(rec.Id)">Edit</button></span>` }]
    },
    pager: { display: true, perPage: 50 },
  };

  //route(event) {
  //  alert('hi');
  //}

  onSearch(query: string = '') {
    if (query != '') {
      this.source.setFilter([
        // fields we want to include in the search
        {
          field: 'EventName',
          search: query
        },
        {
          field: 'EventDescription',
          search: query
        }
      ], false);
    }
    else {
      this.source = new LocalDataSource(this.listofupcomingwebinars);
    }

    // second parameter specifying whether to perform 'AND' or 'OR' search 
    // (meaning all columns should contain search query or at least one)
    // 'AND' by default, so changing to 'OR' by setting false here
  }

  onSelectedFilter(event) {
    this.page = event.target.value;
    this.source = new LocalDataSource(this.listofupcomingwebinars);
    this.settings = {

      columns: {
        EventName: {
          title: 'EventName',
          filter: false
        },
        EventDescription: {
          title: 'EventDescription',
          filter: false
        },

      },
      attr: { class: 'table table-bordered' },
      actions: {
        edit: false, //as an example
        delete: false, //as an example
        add: false,
        position: 'right',
        custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button">Edit</button></span>` }]
      },
      pager: { display: true, perPage: this.page },
    };

  }


  settings1 = {

    columns: {
      EventName: {
        title: 'EventName',
        filter: false
      },
      EventDescription: {
        title: 'Event Description',
        filter: false
      },

    },
    attr: { class: 'table table-bordered' },
    actions: {
      edit: false, //as an example
      delete: false, //as an example
      add: false,
      position: 'right',
      custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button" (click)="onEdit(rec.Id)">Edit</button></span>` }]
    },
    pager: { display: true, perPage: 50 },
  };

  //route(event) {
  //  alert('hi');
  //}

  onSearch1(query: string = '') {
    if (query != '') {
      this.source1.setFilter([
        // fields we want to include in the search
        {
          field: 'EventName',
          search: query
        },
        {
          field: 'Event Description',
          search: query
        }
      ], false);
    }
    else {
      this.source1 = new LocalDataSource(this.listofhistoricalwebinars);
    }

    // second parameter specifying whether to perform 'AND' or 'OR' search 
    // (meaning all columns should contain search query or at least one)
    // 'AND' by default, so changing to 'OR' by setting false here
  }

  onSelectedFilter1(event) {
    this.page = event.target.value;
    this.source1 = new LocalDataSource(this.listofhistoricalwebinars);
    this.settings1 = {

      columns: {
        EventName: {
          title: 'Event Name',
          filter: false
        },
        EventDescription: {
          title: 'Event Description',
          filter: false
        },

      },
      attr: { class: 'table table-bordered' },
      actions: {
        edit: false, //as an example
        delete: false, //as an example
        add: false,
        position: 'right',
        custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button">Edit</button></span>` }]
      },
      pager: { display: true, perPage: this.page },
    };

  }

  //onExport() {
  //  this.excelService.exportAsExcelFile(this.listofprogramcategory, 'ProgramCategory');
  //}
}
