import { Routes } from '@angular/router';

import { CoachmanagementComponent } from './coachmanagement.component';

export const CoachmanagementRoutes: Routes = [

  {
    path: '',
    component: CoachmanagementComponent,
    data: {
      heading: 'Coachmanagement'
    }    
  },

  
];

