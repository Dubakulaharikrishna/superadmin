import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoachmanagementComponent } from './coachmanagement.component';

describe('CoachmanagementComponent', () => {
  let component: CoachmanagementComponent;
  let fixture: ComponentFixture<CoachmanagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoachmanagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoachmanagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
