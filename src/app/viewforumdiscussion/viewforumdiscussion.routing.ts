import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';

import { ViewforumdiscussionComponent } from './viewforumdiscussion.component';

export const ViewforumdiscussionRoutes: Routes = [

  {
    path: '',
    component: ViewforumdiscussionComponent,
    canActivate: [AuthGuard],

    data: {
      heading: 'Login'
    }
  },


];

