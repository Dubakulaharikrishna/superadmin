import { Component, OnInit, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';

import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { Router, ActivatedRoute } from '@angular/router';
import { AdminService } from '../services/admin.service';

import { retry } from 'rxjs/operator/retry';
import { DatePipe } from '@angular/common'
import { Status } from '../Models/Status';
import { Forumdiscussion } from '../Models/forumdiscussion';
import { Forum } from '../Models/Forum';
import { Members } from '../Models/Members';
import { ForumTopic } from '../Models/ForumTopic';


@Component({
  selector: 'app-viewforumdiscussion',
  templateUrl: './viewforumdiscussion.component.html',
  styleUrls: ['./viewforumdiscussion.component.css'],
  providers: [DatePipe]
})
export class ViewforumdiscussionComponent implements OnInit {
  Dfile4: any;
  Dfile3: any;
  Dfile2: any;
  Member_Id: any;
  ForumTopics_Id: any;
  Forum_Id: any;
  name: any;
 
  selectedValue: any = 50;
  page: any;

  listofMembers: Members[];
  dataLoaded: any;
  media: any;
  Dfile1: any;

  isSubmitted: boolean = false;
  public data: Object;
  public temp_var: Object = false;

  public Viewforumdiscussionform: FormGroup;
  forumdiscussion: Forumdiscussion;
  listofForumdiscussions: Forumdiscussion[];
  listofForums: Forum[];
  listofForumtopics: ForumTopic[];
  id: any;
  pagename: any;
  status: Status;
  forumId: any;
  createdby: any;
  constructor(private adminservice: AdminService, public datepipe: DatePipe, public ele: ElementRef, private router: Router, public route: ActivatedRoute, private http: HttpClient, public fb: FormBuilder) {
    this.forumdiscussion = new Forumdiscussion();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.forumId = params['forumId'];

    });

    this.onGetAllForumList();
    this.onGetAllForumTopicsList();
    this.onGetAllMembersList();
    this.onGetForumdiscussionById();
    this.ViewForumdiscussionform();
    this.createdby = localStorage.getItem('MemberName');

  }



  test(id) {
    //  alert("Hi");
    //console.log(id);
    //alert(id);
    console.log(this.Forum_Id);
    //alert(this.forumdiscussion.Forum_Id);
  }
  test1(id) {
    //  alert("Hi");
    //console.log(id);
    //alert(id);
    console.log(this.ForumTopics_Id);
   // alert(this.forumdiscussion.ForumTopics_Id);
  }


  test2(id) {
    //  alert("Hi");
    //console.log(id);
    //alert(id);
    console.log(this.Member_Id);
   // alert(this.forumdiscussion.Member_Id);
  }

  onGetAllForumList() {
    let id = localStorage.getItem('AccountId');
   // alert(id);
    this.adminservice.onGetAllForumList(id).subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofForums = this.status.Data;
        // alert(JSON.stringify(this.Programstructures));
        return this.listofForums;
      }


    });
  }
  onGetAllForumTopicsList() {
    let id = localStorage.getItem('AccountId');
   // alert(id);
    this.adminservice.onGetAllForumTopicsList(id).subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofForumtopics = this.status.Data;
        // alert(JSON.stringify(this.Programstructures));
        return this.listofForumtopics;
      }


    });
  }





  onGetAllMembersList() {
    let id = localStorage.getItem('AccountId');
    //alert(id);
    this.adminservice.onGetAllMembersList(id).subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofMembers = this.status.Data;
        // alert(JSON.stringify(this.Programstructures));
        return this.listofMembers
      }


    });
  }



  onGetForumdiscussionById() {
    //alert(id);
    this.adminservice.onGetForumdiscussionById(this.forumId).subscribe(result => {

      this.status = result;
      if (this.status.StatusCode == "1") {


        this.forumdiscussion = this.status.Data;
        // alert(JSON.stringify(this.eventmaster));
      }
      else {
        alert(this.status.Data);
      }
      console.log(this.forumdiscussion);

    });

    return this.forumdiscussion;
  }

  onUpdateForumdiscussion(forumdiscussion) {


    let files1 = this.ele.nativeElement.querySelector('#ForumDiscussionFile1').files;
    let files2 = this.ele.nativeElement.querySelector('#ForumDiscussionFile2').files;
    let files3 = this.ele.nativeElement.querySelector('#ForumDiscussionFile3').files;
    //let files4 = this.ele.nativeElement.querySelector('#ForumDiscussionFile4').files;
    if (files1.length > 0 && files1.count != 0 && files1 != null) {

      let formData = new FormData();
      let file = files1[0];
      this.Dfile1 = file.name;
      formData.append('ForumDiscussionFile1', file, file.name);
      console.log(formData);
      console.log(files1);
      this.adminservice.onfileupload(formData).subscribe(res => this.dataLoaded = res);
    }
    if (files2.length > 0 && files2.count != 0 && files2 != null) {

      let formData = new FormData();
      let file = files2[0];
      this.Dfile2 = file.name;
      formData.append('ForumDiscussionFile2', file, file.name);
      console.log(formData);
      console.log(files2);
      this.adminservice.onfileupload(formData).subscribe(res => this.dataLoaded = res);
    }
    if (files3.length > 0 && files3.count != 0 && files3 != null) {

      let formData = new FormData();
      let file = files3[0];
      this.Dfile3 = file.name;
      formData.append('ForumDiscussionFile3', file, file.name);
      console.log(formData);
      console.log(files3);
      this.adminservice.onfileupload(formData).subscribe(res => this.dataLoaded = res);
    }
    //if (files4.length > 0 && files4.count != 0 && files4 != null) {

    //  let formData = new FormData();
    //  let file = files4[0];
    //  this.Dfile4 = file.name;
    //  formData.append('EventImageUrl', file, file.name);
    //  console.log(formData);
    //  console.log(files3);
    //  this.adminservice.onfileupload(formData).subscribe(res => this.dataLoaded = res);
    //}



    let date = new Date();


    const Forumdiscussion = {
      Id: this.forumdiscussion.Id,
      Forum_Id: this.Viewforumdiscussionform.controls["forumdiscussion.Forum_Id"].value,
      ForumTopics_Id: this.Viewforumdiscussionform.controls["forumdiscussion.ForumTopics_Id"].value,
      Member_Id: this.Viewforumdiscussionform.controls["forumdiscussion.Member_Id"].value,

      ForumDiscussionComments: this.Viewforumdiscussionform.controls['forumdiscussion.ForumDiscussionComments'].value,

      ForumDiscussionFile1: "Tcsimages/forumdiscussionimages/" + this.Dfile1,
      ForumDiscussionFile2: "Tcsimages/forumdiscussionimages/" + this.Dfile2,
      ForumDiscussionFile3: "Tcsimages/forumdiscussionimages/" + this.Dfile3,
      //ForumDiscussionFile4: "Tcsimages/prgcoverimages/" + this.Dfile4,
     // ForumDiscussionIsModerated: this.Viewforumdiscussionform.controls["forumdiscussion.ForumDiscussionIsModerated"].value,
     // ForumDiscussionPositiveCounters: this.Viewforumdiscussionform.controls["forumdiscussion.ForumDiscussionPositiveCounters"].value,
     // ForumDiscussionNegativeCounters: this.Viewforumdiscussionform.controls["forumdiscussion.ForumDiscussionNegativeCounters"].value,
      ForumDiscussionStatus: this.Viewforumdiscussionform.controls["forumdiscussion.ForumDiscussionStatus"].value,
      ForumDiscussionCreatedBy: this.forumdiscussion.ForumDiscussionCreatedBy,
      ForumDiscussionCreatedDate: this.forumdiscussion.ForumDiscussionCreatedDate,
      ForumDiscussionUpdatedBy: this.createdby,
      ForumDiscussionUpdatedDate: this.datepipe.transform(date, "yyyy-MM-dd")
    }
    // alert(JSON.stringify(programManagement));
    if (this.Viewforumdiscussionform.valid) {
      this.isSubmitted = false;
      //  alert(JSON.stringify(programManagement));
      this.adminservice.onUpdateForumdiscussion(Forumdiscussion).subscribe(data => {

      });
      alert("Successfully Updated");
      this.router.navigate(['/forumdiscussion/']);
    }

    else {
      this.isSubmitted = true;
    }


  }

  ViewForumdiscussionform() {

    this.Viewforumdiscussionform = this.fb.group({
      'forumdiscussion.Forum_Id': ['', [Validators.required]],
      'forumdiscussion.ForumTopics_Id': ['', [Validators.required]],

      'forumdiscussion.Member_Id': ['', [Validators.required]],

      'forumdiscussion.ForumDiscussionComments': ['', [Validators.required]],
     // 'forumdiscussion.ForumDiscussionIsModerated': ['', [Validators.required]],
     // 'forumdiscussion.ForumDiscussionPositiveCounters': ['', [Validators.required]],
     // 'forumdiscussion.ForumDiscussionNegativeCounters': ['', [Validators.required]],
      'forumdiscussion.ForumDiscussionStatus': ['', [Validators.required]]




    });
  }



}
