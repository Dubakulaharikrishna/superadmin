import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewforumdiscussionComponent } from './viewforumdiscussion.component';

describe('ViewforumdiscussionComponent', () => {
  let component: ViewforumdiscussionComponent;
  let fixture: ComponentFixture<ViewforumdiscussionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewforumdiscussionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewforumdiscussionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
