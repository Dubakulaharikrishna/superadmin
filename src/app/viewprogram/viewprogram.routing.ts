import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';

import { ViewprogramComponent } from './viewprogram.component';

export const ViewprogramRoutes: Routes = [

  {
    path: '',
    component: ViewprogramComponent,
    canActivate: [AuthGuard],
    data: {
      heading: 'Viewprogram works!'
    }    
  },

  
];

