import { Component, OnInit, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { Router, ActivatedRoute } from '@angular/router';
import { AdminService } from '../services/admin.service';
import { accountprograms } from '../Models/accountprograms';
import { retry } from 'rxjs/operator/retry';
import { DatePipe } from '@angular/common';
import { Status } from '../Models/Status';
import { Capabilities } from '../Models/capabilities';
import { Webclient } from '../Models/Webclient';
import { AccountManagement } from '../Models/AccountManagement';


@Component({
  selector: 'app-viewprogram',
  templateUrl: './viewprogram.component.html',
  styleUrls: ['./viewprogram.component.css'],
  providers: [DatePipe]
})
export class ViewprogramComponent implements OnInit {
  endDate: any;
  createdby: any;
  coverPic: any;
  dataLoaded: any;
  public viewaccountprogramsUpdateFormGroup: FormGroup;
  viewaccountprograms: accountprograms;
  id: any;
  isSubmitted: boolean = false;
  accId: any;
  startDate: any;
  status: Status;
  listofcapabilities: Capabilities[];
  webclientslist: Webclient[];
  listofAccounts: AccountManagement[];
  constructor(private adminservice: AdminService, private router: Router,
    public route: ActivatedRoute, private http: HttpClient, public fb: FormBuilder, public ele: ElementRef, public datepipe: DatePipe) {
    this.viewaccountprograms = new accountprograms();
  }

  ngOnInit() {
    this.Accountprogramsform();
    this.route.params.subscribe(params => {
      this.accId = params['accId'];
    });
    this.onGetAccountprogramsById();
    this.onGetAllWebClientList();
    this.onGetAllCapabilitiesList();
    this.onGetAllAccountsList();
    this.createdby = localStorage.getItem('MemberName');
  }
 
  onGetAllCapabilitiesList() {
    let id = localStorage.getItem('AccountId');
    this.adminservice.onGetAllCapabilitiesList(id).subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofcapabilities = this.status.Data;
        return this.listofcapabilities
      }
    });
  }




  onGetAllWebClientList() {
    this.adminservice.onGetAllWebClientList().subscribe(data => {

      if (data.StatusCode == "1") {
        this.status = data;
        this.webclientslist = this.status.Data;
        // alert(JSON.stringify(this.Programstructures));
        return this.webclientslist
      }

    });
  }

  onGetAllAccountsList() {
    this.adminservice.onGetAllAccountsList().subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofAccounts = this.status.Data;

        //alert(JSON.stringify(this.listofAccounts));
        return this.listofAccounts
      }

    });
  }


  client(id) {

    console.log(this.viewaccountprograms.Client_Id);
   // alert(this.viewaccountprograms.Client_Id);
  }
  account(id) {

    console.log(this.viewaccountprograms.MasterAccount_Id);
  //  alert(this.viewaccountprograms.MasterAccount_Id);
  }
  capability(id) {

    console.log(this.viewaccountprograms.Capability_Id);
   // alert(this.viewaccountprograms.Capability_Id);
  }


  onUpdateaccountprograms(viewaccountprograms) {

    let files = this.ele.nativeElement.querySelector('#ProgramCoverPicture').files;
    if (files.length > 0 && files.count != 0 && files != null) {

      let formData = new FormData();
      let file = files[0];
      this.coverPic = file.name;
      formData.append('ProgramCoverPicture', file, file.name);
      console.log(formData);
      console.log(files);
      this.adminservice.onfileupload(formData).subscribe(res => this.dataLoaded = res);
    } 
    let date = new Date();
    const account = {
      Id: this.viewaccountprograms.Id,
      Client_Id: this.viewaccountprogramsUpdateFormGroup.controls["viewaccountprograms.Client_Id"].value,
      Capability_Id: this.viewaccountprogramsUpdateFormGroup.controls["viewaccountprograms.Capability_Id"].value,
      MasterAccount_Id: this.viewaccountprogramsUpdateFormGroup.controls["viewaccountprograms.MasterAccount_Id"].value,
      AccountProgramStartDate: this.viewaccountprogramsUpdateFormGroup.controls["viewaccountprograms.AccountProgramStartDate"].value,
      AccountProgramEndDate: this.viewaccountprogramsUpdateFormGroup.controls["viewaccountprograms.AccountProgramEndDate"].value,
      AccountProgramAccessType: this.viewaccountprogramsUpdateFormGroup.controls["viewaccountprograms.AccountProgramAccessType"].value,
      AccountProgramBusinessRule: this.viewaccountprogramsUpdateFormGroup.controls["viewaccountprograms.AccountProgramBusinessRule"].value,
      AccountProgramStatus: this.viewaccountprogramsUpdateFormGroup.controls["viewaccountprograms.AccountProgramStatus"].value,
      ProgramCoverPicture: "Tcsimages/prgcoverimages/" + this.coverPic,
      ProgramDescription: this.viewaccountprogramsUpdateFormGroup.controls['viewaccountprograms.ProgramInfo.ProgramDescription'].value,
      ProgramName: this.viewaccountprogramsUpdateFormGroup.controls['viewaccountprograms.ProgramInfo.ProgramName'].value,
      AccountProgramCreatedBy: this.viewaccountprograms.AccountProgramCreatedBy,
      AccountProgramCreatedDate: this.viewaccountprograms.AccountProgramCreatedDate,
      AccountProgramUpdatedBy: this.createdby,
      AccountProgramUpdatedDate: this.datepipe.transform(date, "yyyy-MM-dd")
    }

    if (this.viewaccountprogramsUpdateFormGroup.valid) {
     // alert(JSON.stringify(account));
      this.isSubmitted = false;
     // alert(JSON.stringify(account));
      this.adminservice.onUpdateAccountPrograms(account).subscribe(res => {
        console.log(res);

   this.viewaccountprograms = res
      });
      alert("Successfully Updated");
      this.router.navigate(['/accountprograms/']);
    }
    else {
      this.isSubmitted = true;
      //alert(JSON.stringify(license));
    }
    //alert(JSON.stringify(account));
  }



  Accountprogramsform() {
    this.viewaccountprogramsUpdateFormGroup = this.fb.group({

      'viewaccountprograms.Client_Id': ['', [Validators.required]],
      'viewaccountprograms.Capability_Id': ['', [Validators.required]],
      'viewaccountprograms.MasterAccount_Id': ['', [Validators.required]],
      'viewaccountprograms.AccountProgramStartDate': ['', [Validators.required]],
      'viewaccountprograms.AccountProgramEndDate': ['', [Validators.required]],
      'viewaccountprograms.AccountProgramAccessType': ['', [Validators.required]],
      'viewaccountprograms.AccountProgramBusinessRule': ['', [Validators.required]],
      'viewaccountprograms.AccountProgramStatus': ['', [Validators.required]],
      'viewaccountprograms.ProgramInfo.ProgramName': ['', [Validators.required]],
      'viewaccountprograms.ProgramInfo.ProgramDescription': ['', [Validators.required]]

    });
  }
  
  onGetAccountprogramsById() {
    //alert(id);
    this.adminservice.onGetAccountsProgramsById(this.accId).subscribe(result => {
      this.status = result;
      if (this.status.StatusCode == "1") {

        this.viewaccountprograms = this.status.Data;
      }
      else {
        alert(this.status.Data);
      }
      console.log(this.viewaccountprograms);

      if (this.viewaccountprograms.AccountProgramStartDate != null && this.viewaccountprograms.AccountProgramStartDate != "" && this.viewaccountprograms.AccountProgramStartDate != undefined) {
        this.startDate = this.datepipe.transform(this.viewaccountprograms.AccountProgramStartDate, 'yyyy-MM-dd');
        this.viewaccountprograms.AccountProgramStartDate = this.startDate;
      }

      if (this.viewaccountprograms.AccountProgramEndDate != null && this.viewaccountprograms.AccountProgramEndDate != "" && this.viewaccountprograms.AccountProgramEndDate != undefined) {
        this.endDate = this.datepipe.transform(this.viewaccountprograms.AccountProgramEndDate, 'yyyy-MM-dd');
        this.viewaccountprograms.AccountProgramEndDate = this.endDate;
      }
      this.viewaccountprograms.ProgramInfo = JSON.parse(this.viewaccountprograms.ProgramInfo);


    });
    return this.viewaccountprograms;
  }

}

