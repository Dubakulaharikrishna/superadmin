//import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, FormBuilder, FormGroup, Validators, FormControl, ReactiveFormsModule } from '@angular/forms';
//import { CustomFormsModule } from 'ng2-validation';
//import { DashboardComponent } from './dashboard.component';
import { RouterModule } from '@angular/router';
import { ViewprogramRoutes } from './viewprogram.routing';
import { ViewprogramComponent } from '../viewprogram/viewprogram.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { HttpModule } from '@angular/http';
import { DataTablesModule } from 'angular-datatables';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AccountprogramsComponent } from '../accountprograms/accountprograms.component';
import { AdminService } from '../services/admin.service';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';




@NgModule({
  imports: [
    RouterModule.forChild(ViewprogramRoutes),
    DataTablesModule,
    HttpClientModule,
    CommonModule,

    HttpModule
    //BrowserModule
    , NgbModule, FormsModule, ReactiveFormsModule,


  ],
  declarations: [

    ViewprogramComponent

  ],

  providers: [AdminService, AuthGuard, NotAuthGuard],
  bootstrap: [ViewprogramComponent]
})
export class ViewprogramModule { }






