import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';
import { ExerciseAnswerComponent } from './exerciseanswer.component';

export const ExerciseAnswerRoutes: Routes = [

  {
    path: '',
    component: ExerciseAnswerComponent,
    canActivate: [AuthGuard],

    data: {
      heading: 'License'
    }
  },


];
