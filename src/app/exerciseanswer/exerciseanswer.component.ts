
import { Component, OnInit } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';
import { ExcelService } from '../services/excel.service';

import { AdminService } from '../services/admin.service';

import { CommonModule } from '@angular/common';
import { ExerciseAnswer } from '../Models/ExerciseAnswer ';
import { Status } from '../Models/Status';
import { Ng2SmartTableModule, LocalDataSource } from 'ng2-smart-table';
import { AccountManagement } from '../Models/AccountManagement';

@Component({
  selector: 'app-exerciseanswer',
  templateUrl: './exerciseanswer.component.html',
  styleUrls: ['./exerciseanswer.component.css'],
  providers: [ExcelService]
})
export class ExerciseAnswerComponent implements OnInit {
  MasterAccount_Id: any;
  //form: any;
  result: any[];
  source: LocalDataSource;
  selectedValue: any = 50;
  page: any;
  title = 'Simple Datatable Example using Angular 4';
  public data: Object;
  public temp_var: Object = false;
  public form: FormGroup;
  exerciseanswer: ExerciseAnswer;
  public ExerciseAnswerForm: FormGroup;
  isSubmitted: boolean = false;
  pagename: any;
  listofExerciseAnswer: ExerciseAnswer[];
  id: any;
  this: any;
  status: Status;
  accountId: any;
  roleId: any;
  listofAccounts: AccountManagement[];
  constructor(private adminservice: AdminService, private router: Router, private excelService: ExcelService, private http: HttpClient, public fb: FormBuilder) {
    this.exerciseanswer = new ExerciseAnswer();
    //this.isSubmitted = false;
  }


  onGetAllAccountsList() {
    this.adminservice.onGetAllAccountsList().subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofAccounts = this.status.Data;

        //alert(JSON.stringify(this.listofAccounts));
        return this.listofAccounts
      }

    });
  }

  onChange(event) {
    console.log(event);
    this.onGetAllExerciseAnswerList(event.target.value);
  }

  reset() {
    this.onGetAllExerciseAnswerList(this.accountId);
    this.MasterAccount_Id = undefined;
  }

  openCpopup() {

    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "block";
    cmask.style.display = "block";
  }

  closeCpopup() {
    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "none";
    cmask.style.display = "none";
  }



  onEdit(event: any) {
    this.router.navigate(['/viewexerciseanswer/', { exaId: event.data.Id}]);
  }

  ngOnInit() {

   // sessionStorage.clear();
   // localStorage.removeItem('loginSessId');
 //   localStorage.clear();
   // localStorage.setTimeout = 1;
   // sessionStorage.removeItem('loginSessId');
  //  localStorage.removeItem('loginSessId');
    this.pagename = "Exercise answer";
    localStorage.setItem('loginSessId', this.pagename);
    this.onGetAllAccountsList();
    this.accountId = localStorage.getItem('AccountId');

    this.roleId = localStorage.getItem('RoleId');
    this.onGetAllAccountsList();
    this.ExerciseAnswerform();
    this.onGetAllExerciseAnswerList(this.accountId);

  
  }

  onGetAllExerciseAnswerList(id) {
    //let id = sessionStorage.getItem('AccountId');
   // alert(id);
    this.adminservice.onGetAllExerciseAnswerList(id).subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofExerciseAnswer = this.status.Data;
        // alert(JSON.stringify(this.Programstructures));
        this.source = new LocalDataSource(this.listofExerciseAnswer);
        return this.listofExerciseAnswer;
      }
     
    });
  }

 

  onCreateExerciseAnswer() {

    const exea = {
      ExerciseAnswer: this.form.controls["ExerciseAnswer"].value,
      Submission_Id: this.form.controls["Submission_Id"].value,
      Exercise_Question_Id: this.form.controls["Exercise_Question_Id"].value,
      Exercise_Id: this.form.controls["Exercise_Id"].value,
      Member_Id: this.form.controls["Member_Id"].value,
      ExAnsSelectedAnswerOptions: this.form.controls["ExAnsSelectedAnswerOptions"].value,
      ExerciseAnswerDetails: this.form.controls["ExerciseAnswerDetails"].value,
      ExerciseAnswerRemarks: this.form.controls["ExerciseAnswerRemarks"].value,
      ExerciseAnswerSubmissionDateTime: this.form.controls["ExerciseAnswerSubmissionDateTime"].value,
      ExerciseAnswerValidityStatus: this.form.controls["ExerciseAnswerValidityStatus"].value,
     // ExerciseAnswerCreatedDate: this.form.controls["ExerciseAnswerCreatedDate"].value,
    //  ExerciseAnswerCreatedBy: this.form.controls["ExerciseAnswerCreatedBy"].value,
     // ExerciseAnswerUpdatedDate: this.form.controls["ExerciseAnswerUpdatedDate"].value,
      //ExerciseAnswerUpdatedBy: this.form.controls["ExerciseAnswerUpdatedBy"].value,
      ExerciseAnswerStatus: this.form.controls["ExerciseAnswerStatus"].value,


    }
    if (this.form.valid) {
      this.isSubmitted = false;
     // alert(JSON.stringify(exea));
      this.adminservice.onCreateExerciseAnswer(exea).subscribe(data => {
      });
      alert("Submitted Successfully");
    }
    else {
      this.isSubmitted = true;
    }
    
  }


  ExerciseAnswerform() {
    this.form = this.fb.group({
      'ExerciseAnswer': ['', [Validators.required]],
      'Submission_Id': ['', [Validators.required]],

      'Exercise_Question_Id': ['', [Validators.required]],

      'Exercise_Id': ['', [Validators.required]],
      'Member_Id': ['', [Validators.required]],
      'ExAnsSelectedAnswerOptions': ['', [Validators.required]],
      'ExerciseAnswerDetails': ['', [Validators.required]],
      'ExerciseAnswerRemarks': ['', [Validators.required]],
      'ExerciseAnswerSubmissionDateTime': ['', [Validators.required]],
      'ExerciseAnswerValidityStatus': ['', [Validators.required]],
     // 'ExerciseAnswerCreatedDate': ['', [Validators.required]],
     // 'ExerciseAnswerCreatedBy': ['', [Validators.required]],
     // 'ExerciseAnswerUpdatedDate': ['', [Validators.required]],
      //'ExerciseAnswerUpdatedBy': ['', [Validators.required]],
      'ExerciseAnswerStatus': ['', [Validators.required]],



    });
  }
  /*Smart table*/
  settings = {

    columns: {
      ExerciseAnswer: {
        title: 'ExerciseAnswer',
        filter: false
      },
      ExAnsSelectedAnswerOptions: {
        title: 'ExAnsSelectedAnswerOptions',
        filter: false
      },

    },
    attr: { class: 'table table-bordered' },
    actions: {
      edit: false, //as an example
      delete: false, //as an example
      add: false,
      position: 'right',
      custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button" (click)="onEdit(rec.Id)">Edit</button></span>` }]
    },
    pager: { display: true, perPage: 50 },
  };

  //route(event) {
  //  alert('hi');
  //}

  onSearch(query: string = '') {
    if (query != '') {
      this.source.setFilter([
        // fields we want to include in the search
        {
          field: 'ExerciseAnswer',
          search: query
        },
        {
          field: 'ExAnsSelectedAnswerOptions',
          search: query
        }
      ], false);
    }
    else {
      this.source = new LocalDataSource(this.listofExerciseAnswer);
    }

    // second parameter specifying whether to perform 'AND' or 'OR' search 
    // (meaning all columns should contain search query or at least one)
    // 'AND' by default, so changing to 'OR' by setting false here
  }

  onSelectedFilter(event) {
    this.page = event.target.value;
    this.source = new LocalDataSource(this.listofExerciseAnswer);
    this.settings = {

      columns: {
        ExerciseAnswer: {
          title: 'ExerciseAnswer',
          filter: false
        },
        ExAnsSelectedAnswerOptions: {
          title: 'ExAnsSelectedAnswerOptions',
          filter: false
        },

      },
      attr: { class: 'table table-bordered' },
      actions: {
        edit: false, //as an example
        delete: false, //as an example
        add: false,
        position: 'right',
        custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button">Edit</button></span>` }]
      },
      pager: { display: true, perPage: this.page },
    };

  }
  onExport() {
    this.excelService.exportAsExcelFile(this.listofExerciseAnswer, 'ExcerciseAnswers');
  }



}

