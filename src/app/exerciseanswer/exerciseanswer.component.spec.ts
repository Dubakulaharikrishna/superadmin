import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExerciseanswerComponent } from './exerciseanswer.component';

describe('ExerciseanswerComponent', () => {
  let component: ExerciseanswerComponent;
  let fixture: ComponentFixture<ExerciseanswerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExerciseanswerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExerciseanswerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
