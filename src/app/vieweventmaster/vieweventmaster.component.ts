import { Component, OnInit, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';

import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { Router, ActivatedRoute } from '@angular/router';
import { AdminService } from '../services/admin.service';

import { retry } from 'rxjs/operator/retry';

import { Status } from '../Models/Status';
import { Eventtype } from '../Models/Eventtype';
import { Eventmaster } from '../Models/Eventmaster';
import { programcategory } from '../Models/programcategory';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-vieweventmaster',
  templateUrl: './vieweventmaster.component.html',
  styleUrls: ['./vieweventmaster.component.css'],
  providers: [DatePipe]
})
export class VieweventmasterComponent implements OnInit {
  endDate: any;
  startDate: any;
  public ViewEventmasterform: FormGroup;
  eventmaster: Eventmaster;
  id: any;
  createdby: any;
  isSubmitted: boolean = false;
  evemId: any;
  status: Status;
  EventType_Id: any;
  ProgramCategory_Id: any;
  listofeventtypes: Eventtype[];
  listofprogramcategory: programcategory[];

  dataLoaded: any;
  media: any;
  coverPic: any;
  constructor(private adminservice: AdminService, public datepipe: DatePipe, public ele: ElementRef, private router: Router, public route: ActivatedRoute, private http: HttpClient, public fb: FormBuilder) {
    this.eventmaster = new Eventmaster();
  }

  ngOnInit() {
    this.Eventmasterform();
    this.route.params.subscribe(params => {
      this.evemId = params['evemId'];

    });
    this.onGetEventmasterById();
    this.onGetAllEventtypes();
    this.onGetAllProgramCategoryList();
    this.createdby = localStorage.getItem('MemberName');

  }



  test(id) {
    //  alert("Hi");
    //console.log(id);
    //alert(id);
    console.log(this.EventType_Id);
   // alert(this.eventmaster.EventType_Id);
  }
  test1(id) {
    //  alert("Hi");
    //console.log(id);
    //alert(id);
    console.log(this.ProgramCategory_Id);
   // alert(this.eventmaster.ProgramCategory_Id);
  }

  onGetAllEventtypes() {
    let id = localStorage.getItem('AccountId');
  //  alert(id);
    this.adminservice.onGetAllEventtypes(id).subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofeventtypes = this.status.Data;

        // alert(JSON.stringify(this.Programstructures));
        return this.listofeventtypes
      }

    });
  }

  onGetAllProgramCategoryList() {
    let id = localStorage.getItem('AccountId');
    //alert(id);
    this.adminservice.onGetAllProgramCategory().subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofprogramcategory = this.status.Data;
        
        // alert(JSON.stringify(this.Programstructures));
        return this.listofprogramcategory;
      }

    });
  }




  onGetEventmasterById() {
    //alert(id);
    this.adminservice.onGetEventMasterById(this.evemId).subscribe(result => {
     
      this.status = result;
      if (this.status.StatusCode == "1") {


        this.eventmaster = this.status.Data;
       // alert(JSON.stringify(this.eventmaster));
      }
      else {
        alert(this.status.Data);
      }
      console.log(this.eventmaster);


      if (this.eventmaster.EventScheduledStartDate != null && this.eventmaster.EventScheduledStartDate != "" && this.eventmaster.EventScheduledStartDate != undefined) {
        this.startDate = this.datepipe.transform(this.eventmaster.EventScheduledStartDate, 'yyyy-MM-dd');
        this.eventmaster.EventScheduledStartDate = this.startDate;
      }



      if (this.eventmaster.EventScheduledEndDate != null && this.eventmaster.EventScheduledEndDate != "" && this.eventmaster.EventScheduledEndDate != undefined) {
        this.endDate = this.datepipe.transform(this.eventmaster.EventScheduledEndDate, 'yyyy-MM-dd');
        this.eventmaster.EventScheduledEndDate = this.endDate;
      }





      this.eventmaster.EventRegistrationTemplate = JSON.parse(this.eventmaster.EventRegistrationTemplate);
      this.eventmaster.EventGuidelines = JSON.parse(this.eventmaster.EventGuidelines);
    });
   
    return this.eventmaster;
  }


  onUpdateEventmaster(eventmaster) {

    let files = this.ele.nativeElement.querySelector('#EventImageUrl').files;
    let filesMedia = this.ele.nativeElement.querySelector('#EventPresenterImageUrl').files;
    if (files.length > 0 && files.count != 0 && files != null) {

      let formData = new FormData();
      let file = files[0];
      this.coverPic = file.name;
      formData.append('EventImageUrl', file, file.name);
      console.log(formData);
      console.log(files);
      this.adminservice.onfileupload(formData).subscribe(res => this.dataLoaded = res);
    }
    if (filesMedia.length > 0 && filesMedia.count != 0 && filesMedia != null) {

      let formData = new FormData();
      let fileM = filesMedia[0];
      this.media = fileM.name;
      formData.append('EventPresenterImageUrl', fileM, fileM.name);
      console.log(formData);
      console.log(files);
      this.adminservice.onfileupload(formData).subscribe(res => this.dataLoaded = res);
    }

    let date = new Date();


    //this.socialelements.MetaTitle = (<HTMLInputElement>document.getElementById("MetaTitle")).value;
    //this.socialelements.MetaKeywords = (<HTMLInputElement>document.getElementById("MetaKeywords")).value;
    //this.socialelements.MetaDescrition = (<HTMLInputElement>document.getElementById("MetaDescrition")).value;

    //  alert(JSON.stringify( this.socialelements));
    //alert(JSON.stringify(program));
    const EventMaster = {
      Id: this.eventmaster.Id,
      EventType_Id: this.ViewEventmasterform.controls["eventmaster.EventType_Id"].value,
      ProgramCategory_Id: this.ViewEventmasterform.controls["eventmaster.ProgramCategory_Id"].value,
      EventTitle: this.ViewEventmasterform.controls["eventmaster.EventTitle"].value,

      EventName: this.ViewEventmasterform.controls['eventmaster.EventName'].value,

      EventImageUrl: "Tcsimages/eventmasterimages/" + this.coverPic,
      EventDescription: this.ViewEventmasterform.controls["eventmaster.EventDescription"].value,
      EventScheduledStartDate: this.ViewEventmasterform.controls["eventmaster.EventScheduledStartDate"].value,
      EventScheduledStartTime: this.ViewEventmasterform.controls["eventmaster.EventScheduledStartTime"].value,
      EventScheduledEndDate: this.ViewEventmasterform.controls["eventmaster.EventScheduledEndDate"].value,
      EventScheduledEndTime: this.ViewEventmasterform.controls["eventmaster.EventScheduledEndTime"].value,
      //  CapabilityActualPrice: this.form.controls["CapabilityActualPrice"].value,
      //  CapabilityDiscountPrice: this.form.controls["CapabilityDiscountPrice"].value,
      EventAccessUrl: this.ViewEventmasterform.controls["eventmaster.EventAccessUrl"].value,

      EventRegistrationUrl: this.ViewEventmasterform.controls["eventmaster.EventRegistrationUrl"].value,
      EventPresenterImageUrl: "Tcsimages/eventmasterimages/" + this.media,
      EventCompletionTemplate: this.ViewEventmasterform.controls["eventmaster.EventCompletionTemplate"].value,
      EventReminderTemplate: this.ViewEventmasterform.controls["eventmaster.EventReminderTemplate"].value,
      EventIsRemindersEnabled: this.ViewEventmasterform.controls["eventmaster.EventIsRemindersEnabled"].value,
      EventIsNotificationsEnabled: this.ViewEventmasterform.controls["eventmaster.EventIsNotificationsEnabled"].value,
      EventResultsUrl: this.ViewEventmasterform.controls["eventmaster.EventResultsUrl"].value,
      EventSurveyUrl: this.ViewEventmasterform.controls["eventmaster.EventSurveyUrl"].value,
      EventExerciseUrl: this.ViewEventmasterform.controls["eventmaster.EventExerciseUrl"].value,
      EventAssignmentUrl: this.ViewEventmasterform.controls["eventmaster.EventAssignmentUrl"].value,
      EventFeedbackUrl: this.ViewEventmasterform.controls["eventmaster.EventFeedbackUrl"].value,
      EventParticipationCount: this.ViewEventmasterform.controls["eventmaster.EventParticipationCount"].value,
      EventStatus: this.ViewEventmasterform.controls["eventmaster.EventStatus"].value,
      EventLocation: this.ViewEventmasterform.controls["eventmaster.EventLocation"].value,
      EventPrice: this.ViewEventmasterform.controls["eventmaster.EventPrice"].value,
      EventCategoryName: this.ViewEventmasterform.controls["eventmaster.EventCategoryName"].value,
      GuidelineName: this.ViewEventmasterform.controls["eventmaster.EventGuidelines.GuidelineName"].value,
      GuidelineDescription: this.ViewEventmasterform.controls["eventmaster.EventGuidelines.GuidelineDescription"].value,
      TemplateName: this.ViewEventmasterform.controls["eventmaster.EventRegistrationTemplate.TemplateName"].value,
      Attributes: this.ViewEventmasterform.controls["eventmaster.EventRegistrationTemplate.Attributes"].value,
      EventCreatedBy: this.eventmaster.EventCreatedBy,
      EventCreatedDate: this.eventmaster.EventCreatedDate,
      EventUpdatedBy: this.createdby,
      EventUpdatedDate: this.datepipe.transform(date, "yyyy-MM-dd")
    }
    // alert(JSON.stringify(programManagement));
    if (this.ViewEventmasterform.valid) {
      this.isSubmitted = false;
    //  alert(JSON.stringify(EventMaster));
      this.adminservice.onUpdateEventmaster(EventMaster).subscribe(data => {

      });
      alert("Successfully Updated");

      this.router.navigate(['/eventmaster/']);
    }

    else {
      this.isSubmitted = true;
    }

   
  }

  Eventmasterform() {
    this.ViewEventmasterform = this.fb.group({
      'eventmaster.EventType_Id': ['', [Validators.required]],
      'eventmaster.ProgramCategory_Id': ['', [Validators.required]],

      'eventmaster.EventTitle': ['', [Validators.required]],

      'eventmaster.EventName': ['', [Validators.required]],
      'eventmaster.EventDescription': ['', [Validators.required]],
      'eventmaster.EventScheduledStartDate': ['', [Validators.required]],
      'eventmaster.EventScheduledStartTime': ['', [Validators.required]],
      'eventmaster.EventScheduledEndDate': ['', [Validators.required]],
      'eventmaster.EventScheduledEndTime': ['', [Validators.required]],
      'eventmaster.EventAccessUrl': ['', [Validators.required]],
      'eventmaster.EventRegistrationUrl': ['', [Validators.required]],
      'eventmaster.EventCompletionTemplate': ['', [Validators.required]],
      'eventmaster.EventReminderTemplate': ['', [Validators.required]],
      'eventmaster.EventResultsUrl': ['', [Validators.required]],
      'eventmaster.EventSurveyUrl': ['', [Validators.required]],
      'eventmaster.EventExerciseUrl': ['', [Validators.required]],
      'eventmaster.EventAssignmentUrl': ['', [Validators.required]],
      'eventmaster.EventFeedbackUrl': ['', [Validators.required]],
      'eventmaster.EventParticipationCount': ['', [Validators.required]],
      //// RegistrationNumber': ['', [Validators.required]],

      'eventmaster.EventStatus': ['', [Validators.required]],
      'eventmaster.EventLocation': ['', [Validators.required]],
      'eventmaster.EventPrice': ['', [Validators.required]],
      'eventmaster.EventCategoryName': ['', [Validators.required]],

      'eventmaster.EventGuidelines.GuidelineName': ['', [Validators.required]],

      'eventmaster.EventGuidelines.GuidelineDescription': ['', [Validators.required]],

      'eventmaster.EventRegistrationTemplate.TemplateName': ['', [Validators.required]],
      'eventmaster.EventRegistrationTemplate.Attributes': ['', [Validators.required]],
      'eventmaster.EventIsRemindersEnabled': ['', [Validators.required]],
      'eventmaster.EventIsNotificationsEnabled': ['', [Validators.required]]



    });
  }











}
