import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VieweventmasterComponent } from './vieweventmaster.component';

describe('VieweventmasterComponent', () => {
  let component: VieweventmasterComponent;
  let fixture: ComponentFixture<VieweventmasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VieweventmasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VieweventmasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
