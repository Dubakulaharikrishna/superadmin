import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';

import { VieweventmasterComponent } from './vieweventmaster.component';

export const VieweventmasterRoutes: Routes = [

  {
    path: '',
    component: VieweventmasterComponent,
    canActivate: [AuthGuard],
    data: {
      heading: 'Viewaccount works!'
    }    
  },

  
];

