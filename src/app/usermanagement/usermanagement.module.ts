//import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//import { DashboardComponent } from './dashboard.component';
import { RouterModule } from '@angular/router';
import { UsermanagementRoutes } from './usermanagement.routing';
import { UsermanagementComponent } from '../usermanagement/usermanagement.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { HttpModule } from '@angular/http';
import { DataTablesModule } from 'angular-datatables';
import {HttpClientModule} from '@angular/common/http';
import { CommonModule } from '@angular/common'; 
import { BrowserModule } from '@angular/platform-browser';




@NgModule({
  imports: [
    RouterModule.forChild(UsermanagementRoutes),
    DataTablesModule,
    HttpClientModule,
    CommonModule,
    
 HttpModule
    //BrowserModule
  ],
  declarations: [
   
    UsermanagementComponent
    
  ],
 
  providers: [],
  bootstrap: [UsermanagementComponent]
})
export class UsermanagementModule { }






