import { Component, OnInit, ElementRef } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';
import { Subject } from 'rxjs/Subject';
import { AdminService } from '../services/admin.service';
import { ExcelService } from '../services/excel.service';
import { DatePipe } from '@angular/common'
import { CommonModule } from '@angular/common';
import { ExerciseType } from '../Models/ExerciseType ';
import { Status } from '../Models/Status';
import { Ng2SmartTableModule, LocalDataSource } from 'ng2-smart-table';
import { AccountManagement } from '../Models/AccountManagement';
@Component({
  selector: 'app-exercisetype',
  templateUrl: './exercisetype.component.html',
  styleUrls: ['./exercisetype.component.css'],
  providers: [ExcelService, DatePipe]
})

export class ExerciseTypeComponent implements OnInit {
  MasterAccount: any;
  dataLoaded: any;
  exeicon: any;
  success: boolean = false;
  message: any;
  createdby: any;
  dtTrigger: Subject<any> = new Subject();
  public _success = new Subject<string>();
  staticAlertClosed = false;
  MasterAccount_Id: any;
  public data: Object;
  public temp_var: Object = false;
  public ExerciseTypeUpdateFormGroup: FormGroup;
  public ExerciseTypeForm: FormGroup;
  public form: FormGroup;
  exercisetype: ExerciseType;
  public isSubmitted: boolean = false;
  id: any;
  listofAccounts: AccountManagement[];
  listofexercisetype: ExerciseType[];
  pagename: any;
  status: Status;
  source: LocalDataSource;
  accountId: any;
  selectedValue: any = 50;
  page: any;
  roleId: any;
  constructor(private fb: FormBuilder, public datepipe: DatePipe, private adminservice: AdminService, public ele: ElementRef, private excelService: ExcelService, private router: Router, private http: HttpClient) {

    this.exercisetype = new ExerciseType();
  }


  onGetAllExerciseTypeList(id) {
   // let id = sessionStorage.getItem('AccountId');
    //alert(id);
    this.adminservice.onGetAllExerciseTypeList(id).subscribe(data => {

      if (data.StatusCode == "1") {
        this.status = data;
        this.listofexercisetype = this.status.Data;
        // alert(JSON.stringify(this.Programstructures));
        this.source = new LocalDataSource(this.listofexercisetype);

        return this.listofexercisetype;
      }


      

    });
  }
  onGetAllAccountsList() {
    this.adminservice.onGetAllAccountsList().subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofAccounts = this.status.Data;
       
         //alert(JSON.stringify(this.listofAccounts));
        return this.listofAccounts
      }

    });
  }
  test(id) {
    //  alert("Hi");
    //console.log(id);
    //alert(id);
    console.log(this.MasterAccount_Id);
   // alert(this.MasterAccount_Id);
  }
  openCpopup() {

    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "block";
    cmask.style.display = "block";
  }

  closeCpopup() {
    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "none";
    cmask.style.display = "none";
  }

  onEdit(event: any) {


    this.router.navigate(['/viewexercisetype/', { extId: event.data.Id }]);
  }

  reset() {
    this.onGetAllExerciseTypeList(this.accountId);
    this.MasterAccount = undefined;
  }

  ngOnInit() {
   // sessionStorage.clear();
   // localStorage.removeItem('loginSessId');
    //localStorage.clear();
   // localStorage.setTimeout = 1;
   // sessionStorage.removeItem('loginSessId');
  //  localStorage.removeItem('loginSessId');
    this.pagename = "Exercise Type";
    localStorage.setItem('loginSessId', this.pagename);
    this.accountId = localStorage.getItem('AccountId');
    this.ExerciseTypeform();
    this.onGetAllExerciseTypeList(this.accountId);
    this.onGetAllAccountsList();
    this.roleId = localStorage.getItem('RoleId');
    this.createdby = localStorage.getItem('MemberName');
    setTimeout(() => this.staticAlertClosed = true, 20000);
    this._success.subscribe((message) => this.message = message);
    this._success.debounceTime(7000).subscribe(() => this.message = null);

  }

  onChange(event) {
    console.log(event);
    this.onGetAllExerciseTypeList(event.target.value);
  }


  onCreateExerciseType() {
    let files = this.ele.nativeElement.querySelector('#ExerciseTypeIcon').files;
    if (files.length > 0 && files.count != 0 && files != null) {

      let formData = new FormData();
      let file = files[0];
      this.exeicon = file.name;
      formData.append('ExerciseTypeIcon', file, file.name);
      console.log(formData);
      console.log(files);
      this.adminservice.onfileupload(formData).subscribe(res => this.dataLoaded = res);
    }
    let date = new Date();
    const exercisetype = {
      MasterAccount_Id: this.form.controls["MasterAccount_Id"].value,
      ExerciseTypeName: this.form.controls["ExerciseTypeName"].value,
      ExerciseTypeDescription: this.form.controls["ExerciseTypeDescription"].value,
      ExerciseTypeIcon: "Tcsimages/exeiconimages/" + this.exeicon,
      ExerciseTypeStatus: this.form.controls["ExerciseTypeStatus"].value,
      ExerciseTypeCreatedBy: this.createdby,
      ExerciseTypeCreatedDate: this.datepipe.transform(date, "yyyy-MM-dd")



    }

    if (this.form.valid) {
      this.isSubmitted = false;
      //alert(JSON.stringify(ExerciseType));
      this.adminservice.onCreateExerciseType(exercisetype).subscribe(data => {
        console.log(data);
        if (data.StatusCode == "1") {
          this.success = true;
          this.message = data.Message;
          this._success.next(`Record Saved Successfully`);

          this.onGetAllExerciseTypeList(this.accountId);
          this.closeCpopup();

        }
        else {
          this.success = false;
          this.message = "Failed to create account master";
        }
      });
      //alert("Successfully Submitted");
     
    }
    else {
      this.isSubmitted = true;
    }



  }


  ExerciseTypeform() {
    this.form = this.fb.group({

      ExerciseTypeName: ['', [Validators.required]],
      ExerciseTypeDescription: ['', [Validators.required]],
      MasterAccount_Id: ['', [Validators.required]],
     
      ExerciseTypeStatus: ['', [Validators.required]]

    });
  }
  /*Smart table*/
  settings = {

    columns: {
      ExerciseTypeName: {
        title: 'ExerciseTypeName',
        filter: false
      },
      ExerciseTypeDescription: {
        title: 'ExerciseTypeDescription',
        filter: false
      },

    },
    attr: { class: 'table table-bordered' },
    actions: {
      edit: false, //as an example
      delete: false, //as an example
      add: false,
      position: 'right',
      custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button" (click)="onEdit(rec.Id)">Edit</button></span>` }]
    },
    pager: { display: true, perPage: 50 },
  };

  //route(event) {
  //  alert('hi');
  //}

  onSearch(query: string = '') {
    if (query != '') {
      this.source.setFilter([
        // fields we want to include in the search
        {
          field: 'ExerciseTypeName',
          search: query
        },
        {
          field: 'ExerciseTypeDescription',
          search: query
        }
      ], false);
    }
    else {
      this.source = new LocalDataSource(this.listofexercisetype);
    }

    // second parameter specifying whether to perform 'AND' or 'OR' search 
    // (meaning all columns should contain search query or at least one)
    // 'AND' by default, so changing to 'OR' by setting false here
  }

  onSelectedFilter(event) {
    this.page = event.target.value;
    this.source = new LocalDataSource(this.listofexercisetype);
    this.settings = {

      columns: {
        ExerciseTypeName: {
          title: 'ExerciseTypeName',
          filter: false
        },
        ExerciseTypeDescription: {
          title: 'ExerciseTypeDescription',
          filter: false
        },

      },
      attr: { class: 'table table-bordered' },
      actions: {
        edit: false, //as an example
        delete: false, //as an example
        add: false,
        position: 'right',
        custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button">Edit</button></span>` }]
      },
      pager: { display: true, perPage: this.page },
    };

  }

  onExport() {
    this.excelService.exportAsExcelFile(this.listofexercisetype, 'ExerciseTypes');
  }




}
