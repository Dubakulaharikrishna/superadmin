import { NgModule } from '@angular/core';

//import { DashboardComponent } from './dashboard.component';
import { RouterModule } from '@angular/router';
import { ExerciseTypeRoutes } from './exercisetype.routing';
import { ExerciseTypeComponent } from '../exercisetype/exercisetype.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { DataTablesModule } from 'angular-datatables';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
//import { BrowserModule } from '@angular/platform-browser';
//import { FileUploadModule } from 'ng2-file-upload/ng2-file-upload';

import { NgbProgressbarModule, NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { NgbAccordionModule } from '@ng-bootstrap/ng-bootstrap';
import { CustomFormsModule } from 'ng2-validation';
import { FormsModule, FormBuilder, FormGroup, Validators, FormControl, ReactiveFormsModule } from '@angular/forms';
import { AdminService } from '../services/admin.service';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';
import { Ng2SmartTableModule } from 'ng2-smart-table';

@NgModule({

  imports: [

    DataTablesModule, CommonModule,
    RouterModule,
    HttpClientModule,
    // BrowserModule,
    FormsModule,
    CustomFormsModule,
    Ng2SmartTableModule,
    NgbModule,
    ReactiveFormsModule,
    RouterModule.forChild(ExerciseTypeRoutes),
    HttpModule

  ],
  declarations: [

    ExerciseTypeComponent

  ],

  providers: [AdminService, AuthGuard, NotAuthGuard],
  bootstrap: [ExerciseTypeComponent]
})
export class ExerciseTypeModule { }
