import { Routes } from '@angular/router';
import { ExerciseTypeComponent } from './exercisetype.component';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';
export const ExerciseTypeRoutes: Routes = [

  {
    path: '',
    component: ExerciseTypeComponent,
    canActivate: [AuthGuard],

    data: {
      heading: 'ExerciseType'
    }
  },


];
