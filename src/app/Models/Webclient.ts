export class Webclient {
  id: any;
  Client_Id: any;
  InstanceManagerId: any;
  CompanyName: any;
  CompanyAddressLine1: any;
  CompanyAddressLine2: any;
  CompanyRegistrationNumber: any;
  CompanyEmailID: any;
  CompanyAdminEmailID: any;
  CompanyType: any;
  CompanyEstablishedDate: any;
  Instance_Id: any;
  LicenseInfo: any = {};
  DisplayNameOfCompany: any;
  LogoUrl: any;
  AdminUserName: any;
  PrimaryAdminEmailID: any;
  AdminPhoneNumber: any;
  ClientCreatedDate: any;
  ClientCreatedBy: any;
  ClientUpdatedDate: any;
  ClientUpdatedBy: any;
  CompanyPersonName: any;
  CopmpanyPersonPhoneNumber: any;
  CopmpanyPersonEmailID: any;

}
