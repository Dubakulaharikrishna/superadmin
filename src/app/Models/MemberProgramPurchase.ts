export class MemberProgramPurchase {
  Id: any;
  id: any;
  Member_Id: any;
  AccountProgramPricing_Id: any; 
  PaymentTransactionRef_Id: any;
  PaymentMode: any; 
  PaymentStatus: any; 
  PaymentGatewayResponseCode: any; 
  ProgramStartDate: any; 
  ProgramValidTillDate: any; 
  ProgramAccessDuration: any; 
  NextRenewalDate: any; 
  ServiceAccessDuration: any; 
  MPPCreatedBy: any; 
  MPPCreatedDate: any; 
  MPPUpdatedBy: any; 
  MPPUpdatedDate: any; 
  MPPStatus: any; 

}
