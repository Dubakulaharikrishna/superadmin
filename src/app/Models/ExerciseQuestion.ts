export class ExerciseQuestion {
  id: any;
  Id: any;
  Exercise_Id: any;
  ExerciseQuestion: any;
  ExerciseQuestionSummary: any;
  ExerciseQuestionDescription: any;
  ExerciseQuestionAnswers: any;
  ExerciseQuestionAnswerOptions: any[];
  ExerciseQuestionAnswerHints: any;
  ExerciseQuestionAnswerDisplayType: any;
  ExerciseQuestionAnswerExclusionGroups: any;
  ExerciseQuestionIsMandatory: any;
  ExerciseQuestionIsProficient: any;
  ExerciseQuestionCreatedDate: any;
  ExerciseQuestionCreatedBy: any;
  ExerciseQuestionUpdatedDate: any;
  ExerciseQuestionUpdatedBy: any;
  ExerciseQuestionStatus: any;


}
