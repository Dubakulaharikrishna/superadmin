export class MemberEventPurchase {
  Id: any
  Member_Id: any;
  MasterAccount_Id: any;
  AccountProgramPricing_Id: any;
  PaymentTransactionRef_Id: any;
  PaymentMode: any;
  PaymentStatus: any;
  PaymentGatewayResponseCode: any;
  MEPCreatedBy: any;
  MEPCreatedDate: any;
  MEPUpdatedBy: any;
  MEPUpdatedDate: any;
      
      
  MEPStatus: any;
}
