export class InstanceManager {
  id: any;
  Id: any;
  InstanceID: any;
  InstanceName: any;
  ServerName: any;
  OSDetails: any;
  InstallAdmin: any;
  SetupDateTime: any;
  AssociatedLicenseInfo: any;
  LicenseExpiryDate: any;
  LicenseType: any;
  NumberofClients: any;
  NumberOfAccounts: any;
  InstancePublicIPAddress: any;
  InstancePrivateIPAddress: any;
  CreatedBy: any;
  CreatedDate: any;
  UpdatedBy: any;
  UpdatedDate: any;

}
