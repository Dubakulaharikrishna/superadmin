export class Eventmaster {
  Id: any;
  id: any;
  EventMaster_Id: any;
  EventType_Id: any;
  EventTitle: any;
  EventName: any;
  EventImageUrl: any;
  EventPresenterImageUrl: any;
  EventDescription: any;
  EventScheduledStartDate: any;
  EventScheduledStartTime: any;
  EventScheduledEndDate: any;
  EventScheduledEndTime: any;
  //VCEnrollmentEndTime: any;
  EventAccessUrl: any;
  EventRegistrationUrl: any;
  EventRegistrationTemplate: any = {};
  EventCompletionTemplate: any;
  EventReminderTemplate: any;
  EventIsRemindersEnabled: any;
  EventIsNotificationsEnabled: any;
  EventResultsUrl: any;
  EventSurveyUrl: any;
  EventExerciseUrl: any;
  EventAssignmentUrl: any;
  EventFeedbackUrl: any;
  EventGuidelines: any = {};
  EventParticipationCount: any;
  EventCreatedBy: any;
  EventCreatedDate: any;
  EventUpdatedBy: any;
  EventUpdatedDate: any;
  EventStatus: any;
  ProgramCategory_Id: any;
  EventLocation: any;
  EventPrice: any;
  EventCategoryName: any;
  GuidelineName: any;
  GuidelineDescription: any;
  TemplateName: any;
  Attributes: any;
}
