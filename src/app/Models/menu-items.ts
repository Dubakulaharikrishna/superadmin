import { Injectable } from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  name: string;
  type?: string;
}

export interface Menu {
  state: string;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

const MENUITEMS = [

  //{
  //  state: 'dashboard',
  //  name: 'Dashboard',
  //  type: 'link',
  //  icon: 'basic-accelerator'
  //},

  {
    state: 'admindashboards',
    name: 'Dashboard',
    type: 'link',
    icon: 'basic-elaboration-calendar-flagged'
  },


  //{
  //  state: 'portalmanagement',
  //  name: 'Portal Management',
  //  type: 'link',
  //  icon: 'basic-accelerator'
  //}, 
  //{
  //  state: 'contentmanagement',
  //  name: 'Content Management',
  //  type: 'link',
  //  icon: 'basic-cards-diamonds'
  //},
  {
    state: 'license',
    name: 'License manager',
    type: 'link',
    icon: 'basic-elaboration-calendar-flagged'
  },
  {
    state: 'instancemanager',
    name: 'Instance manager',
    type: 'link',
    icon: 'basic-elaboration-calendar-flagged'
  },
  {
    state: 'webclient',
    name: 'Client master',
    type: 'link',
    icon: 'basic-cards-diamonds'
  },
  {
    state: 'accountmanagement',
    name: 'Account master',
    type: 'link',
    icon: 'basic-star'
  },
  {
    state: 'accountmembership',
    name: 'Account membership',
    type: 'link',
    icon: 'basic-elaboration-calendar-flagged'
  },
  //{
  //  state: 'usermanagement',
  //  name: 'User Management',
  //  type: 'link',
  //  icon: 'basic-notebook-pen'
  //},
  {
    state: 'members',
    name: 'Members',
    type: 'link',
    icon: 'basic-notebook-pen'
  },
    //{
  //  state: 'coachmanagement',
  //  name: 'Coach Management',
  //  type: 'link',
  //  icon: 'ecommerce-safe'
  //},

     {
    state: 'exercisequestion',
    name: 'Exercise question',
    type: 'link',
    icon: 'basic-elaboration-calendar-flagged'
  },
  {
    state: 'exerciseanswer',
    name: 'Exercise answer',
    type: 'link',
    icon: 'basic-elaboration-calendar-flagged'
  },
  
 
  {
    state: 'accountprograms',
    name: 'Account program',
    type: 'link',
    icon: 'basic-elaboration-calendar-flagged'
  },
  
  //{
  //  state: 'servicemanagement',
  //  name: 'Service Management',
  //  type: 'link',
  //  icon: 'basic-elaboration-calendar-flagged'
  //},

  //{
  //  state: 'surveymanagement',
  //  name: 'Survey Management',
  //  type: 'link',
  //  icon: 'basic-elaboration-calendar-flagged'
  //},
  //{
  //  state: 'eventmanagement',
  //  name: 'Event Management',
  //  type: 'link',
  //  icon: 'basic-elaboration-calendar-flagged'
  //},
  {
    state: 'programmanagement',
    name: 'Capabilities',
    type: 'link',
    icon: 'basic-elaboration-calendar-flagged'
  },

  {
    state: 'programcontent',
    name: 'Modules',
    type: 'link',
    icon: 'basic-elaboration-calendar-flagged'
  },

  {
    state: 'contentcategory',
    name: 'Content category',
    type: 'link',
    icon: 'basic-elaboration-calendar-flagged'
  },

  {
    state: 'programcategory',
    name: 'Program category',
    type: 'link',
    icon: 'basic-elaboration-calendar-flagged'
  },
  

  // {
  //  state: 'reports',
  //  name: 'Reports&Analytics',
  //  type: 'link',
  //  icon: 'basic-elaboration-calendar-flagged'
  //},

  //{
  //  state: 'admindashboards',
  //  name: 'Admin Dashboard',
  //  type: 'link',
  //  icon: 'basic-elaboration-calendar-flagged'
  //},
  //{
  //  state: 'livetracking',
  //  name: 'Live Tracking',
  //  type: 'link',
  //  icon: 'basic-elaboration-calendar-flagged'
  //},

 

];

@Injectable()
export class MenuItems {
  getAll(): Menu[] {
    return MENUITEMS;
  }

  add(menu: Menu) {
    MENUITEMS.push(menu);
  }
}
