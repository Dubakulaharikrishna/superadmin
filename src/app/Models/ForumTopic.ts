export class ForumTopic {
  id: any;
  Id: any;
  ForumTopics_Id: any;
  Forum_Id: any;
  Member_Id: any;
  ForumTopicTitle: any;
  ForumTopicContent: any;
  ForumTopicType: any;
  ForumTopicFile1: any;
  ForumTopicFile2: any;
  ForumTopicFile3: any;
  ForumTopicFile4: any;
  ForumTopicFile5: any;
  ForumTopicIsModerated: any;
  ForumTopicCreatedDate: any;
  ForumTopicCreatedBy: any;
  ForumTopicUpdatedDate: any;
  ForumTopicUpdatedBy: any;
  ForumTopicStatus: any;
}
