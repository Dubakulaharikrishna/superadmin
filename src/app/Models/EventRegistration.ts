export class EventRegistration {
  Id: any;
  id: any;
  Member_Id: any;
  MasterAccount_Id: any;
  EventMaster_Id: any;
        
  EventName: any;
  EventStartDate: any;
  EventStartTime: any;
  EventEndDate: any;
  EventEndTime: any;
  EventCreatedDate: any;
  EventCreatedBy: any;
  EventUpdatedDate: any;
  EventUpdatedBy: any;
  EventStatus: any;
}
