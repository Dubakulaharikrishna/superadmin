export class Programstructure {
  Id: any;
 
  ProgramStructure_Id: any;
  ProgramStrName: any;
  ProgramStrDescription: any;
  NoOfCapabilities: any;
  NoOfModules: any;
  ProgramStrType: any;
  
  ProgramStrCoverPicture: any;
  ProgramStrDependency: any;
  ProgramStrCapabilities : any[];
  ProgramStrModules : any[];
  ProgramStrExercise : any[];
  ProgramStrAccessType: any;
  ProgramStrCreatedBy: any;
  ProgramStrCreatedDate: any;
  ProgramStrUpdatedBy: any;
  ProgramStrUpdatedDate: any;
  ProgramStrStatus: any;
  ProgramCategory: any;
  ProgramStrListedPrice: any;
  ProgramStrDiscountPrice: any;
  ProgramStrIsFree: any;
}
