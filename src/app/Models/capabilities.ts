export class Capabilities {
  id: any;
  Id: any;

  Capability_Id: any;
  ProgramCategory_Id: any[];
 // ProgramManagementId: any;
  ProgramStr_Id
  CapabilityDescription: any;
  CapabilitySynopsis: any;
  CapabilityCoverPicture: any;
  CapabilityType: any;
  CapabilityCategory: any;
  CapabilityDependency: any;
  CapabilityTags: any[];
  CapabilityStartDate: any;
  CapabilityValidTill: any;
 // CapabilityActualPrice: any;
 // CapabilityDiscountPrice: any;
  CapabilityAccessType: any;
  CapabilityIsTracking: any;
  CapabilityPlatforms: any[];
  CapabilityCoverMediaFile: any;
  CapabilityCreatedDate: any;
  CapabilityCreatedBy: any;
  CapabilityUpdatedDate: any;
  CapabilityUpdatedBy: any;
  CapabilityStatus: any;
  CapabilitySocialElements: any = {};
  CapabilityName: any;
  CapabilityListedPrice: any;
  CapabilityDiscountPrice: any;
  CapabilityIsFree: any;
  MetaTitle: any;
  MetaKeywords: any;
  MetaDescrition: any;


}
