export class Modules {
  _id: any;
  id: any;
  Id: any;
  Module_Id: any;
  Capability_Id: any;
  ProgramStr_Id: any;
  ProgramCategory_Id: any[];
  ModuleTitle: any;
  ModuleAccessType: any;
  ModuleArea: any;
  ModuleMedia: any[];
  ModuleMediaName: any;
  ModuleMediaType: any;
  ModuleMediaFilePath: any;
  ModuleType: any;
  IsModuleTracking: any;
  ModuleDependency: any;

 // ModulePrice: any;
  //ModuleDiscountPrice: any;
  ModuleDisplayDate: any;
  ModuleValidTill: any;
  ModuleCreatedDate: any;
  ModuleCreatedBy: any;
  ModuleUpdatedDate: any;
  ModuleUpdatedBy: any;
  ModuleStatus: any;
  ModuleDiscountPrice: any;
  ModuleListedPrice: any;
  ModuleIsFree: any;
  description: any;
  sources: any;
  subtitle: any;
  thumb: any;
  title: any;
  type: any;

}
