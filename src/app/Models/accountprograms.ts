export class accountprograms {
  id: any;
  Id: any;
  AccountProgram_Id: any;
  Client_Id: any;
  Capability_Id: any;
  MasterAccount_Id: any;
  AccountProgramStartDate: any;
  AccountProgramEndDate: any;
  AccountProgramCreatedBy: any;
  AccountProgramCreatedDate: any;
  AccountProgramUpdatedDate: any;
  AccountProgramUpdatedBy: any;
  AccountProgramAccessType: any;
  AccountProgramBusinessRule: any;
  AccountProgramStatus: any;
  ProgramInfo: any = {};
  ProgramName: any;
  ProgramDescription: any;
  ProgramCoverPicture: any;
}
