export class Exercise {
  id: any;
  Id: any;
 // Exercise_Type_Id: any;
  ExerciseType_Id: any;
  Exercise_Id: any;
  ExerciseTitle: any;
  NoofQuestions: any;
  ExerciseDescription: any;
  ExerciseSynopsis: any;
  ExerciseObjectives: any;
  ExerciseAccessType: any;
  ExerciseCreatedDate: any;
  ExerciseCreatedBy: any;
  ExerciseUpdatedDate: any;
  ExerciseUpdatedBy: any;
  ExerciseStatus: any;
  ExerciseDependency: any;
  IsExerciseModerated: any;
}
