export class Forum {
  Id: any;
  id: any;
  Forum_Id: any;
  MasterAccount_Id: any;
  ForumName: any;
  ForumIsModerated: any;
  ForumIsDeleted: any;
  ForumCreatedDate: any;
  ForumCreatedBy: any;
  ForumUpdatedDate: any;
  ForumUpdatedBy: any;
  ForumStatus: any;
  ForumIcon: any;
}
