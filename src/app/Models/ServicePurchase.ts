export class ServicePurchase {
  id: any;
  Id: any;
  Service_Id: any;
  ServiceValidStartDate: any;
  ServiceValidTillDate: any;
  ServiceUpdatedDate: any;
  Member_Id: any;
  AccountProgramPricing_Id: any;
  PaymentTransactionRef_Id: any;
  PaymentMode: any;
  PaymentStatus: any;
  PaymentGatewayResponseCode: any;
  NextRenewalDate: any;
  ServiceAccessDuration: any;
  ServiceCreatedBy: any;
  ServiceCreatedDate: any;
  ServiceUpdatedBy: any;
  ServiceStatus: any;
}
