export class Service {


  id: any;
  Id: any;
  Service_Id: any;
  ServiceType_Id: any;
  ServiceTitle: any;
  ServiceDescription: any;
  ServiceContributor: any;
  ServiceCoverPicture: any;
  ServiceTag: any[];
  IsFreeService: any;
  ServiceDiscountedPrice: any;
  ServiceValidStartDate: any;
  ServiceValidEndDate: any;
  ServiceListedPrice: any;
  ServiceStatus: any;
  Service_Icon: any;
  ServiceLink: any[];
  ServiceCreatedBy: any;
  ServiceCreatedDate: any;
  ServiceUpdatedBy: any;
  ServiceUpdatedDate: any;

}
