export class Forumdiscussion {

  Id: any;
  id: any;
  ForumDiscussion_Id: any;
  Forum_Id: any;
  ForumTopics_Id: any;
  Member_Id: any;
  ForumDiscussionComments: any;
  ForumDiscussionIsModerated: any;
  ForumDiscussionPositiveCounters: any;
  ForumDiscussionNegativeCounters: any;
  ForumDiscussionFile1: any;
  ForumDiscussionFile2: any;
  ForumDiscussionFile3: any;
  ForumDiscussionCreatedDate: any;
  ForumDiscussionCreatedBy: any;
  ForumDiscussionUpdatedDate: any;
  ForumDiscussionUpdatedBy: any;
  ForumDiscussionStatus: any;
}
