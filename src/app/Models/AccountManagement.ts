export class AccountManagement {
  _id: any;
  Id: any;
  Account_Id: any;
  AccountName: any;
  AccountWebUrl: any;
  IsMobileEnabled: any;
  IsAndroidAppPublic: any;
  IsIOSAppPublic: any;
  AccountAdmin: any;
  IsHttpsEnabled: any;
  DevUrl: any;
  IsLiveStatus: any;
  ClientInfo: any = {};
  InstanceInfo: any = {};
  LicenseInfo: any = {};
  AppUrl: any;
  AccountCreatedDate: any;
  AccountCreatedBy: any;
  AccountUpdatedBy: any;
  AccountUpdatedDate: any;
}
