export class ExerciseAnswer {
  id: any;
  ExerciseAnswer: any;
  Submission_Id: any;
  Exercise_Question_Id: any;
  Exercise_Id: any;
  Member_Id: any;
  ExAnsSelectedAnswerOptions: any;
  ExerciseAnswerDetails: any;
  ExerciseAnswerRemarks: any;
  ExerciseAnswerSubmissionDateTime: any;
  ExerciseAnswerValidityStatus: any;
  ExerciseAnswerCreatedDate: any;
  ExerciseAnswerCreatedBy: any;
  ExerciseAnswerUpdatedDate: any;
  ExerciseAnswerUpdatedBy: any;
  ExerciseAnswerStatus: any;
}
