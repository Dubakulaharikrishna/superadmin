import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewforumtopicComponent } from './viewforumtopic.component';

describe('ViewforumtopicComponent', () => {
  let component: ViewforumtopicComponent;
  let fixture: ComponentFixture<ViewforumtopicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewforumtopicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewforumtopicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
