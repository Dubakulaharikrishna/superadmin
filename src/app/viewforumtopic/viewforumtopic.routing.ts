import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';

import { ViewforumtopicComponent } from './viewforumtopic.component';

export const ViewforumtopicRoutes: Routes = [

  {
    path: '',
    component: ViewforumtopicComponent,
    canActivate: [AuthGuard],

    data: {
      heading: 'Login'
    }
  },


];

