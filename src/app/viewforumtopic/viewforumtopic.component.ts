import { Component, OnInit, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';

import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { Router, ActivatedRoute } from '@angular/router';
import { AdminService } from '../services/admin.service';
import { DatePipe } from '@angular/common'
import { retry } from 'rxjs/operator/retry';

import { Status } from '../Models/Status';
import { Forumdiscussion } from '../Models/forumdiscussion';
import { Forum } from '../Models/Forum';
import { Members } from '../Models/Members';
import { ForumTopic } from '../Models/ForumTopic';
@Component({
  selector: 'app-viewforumtopic',
  templateUrl: './viewforumtopic.component.html',
  styleUrls: ['./viewforumtopic.component.css'],
  providers: [DatePipe]
})
export class ViewforumtopicComponent implements OnInit {

  public Viewforumtopicform: FormGroup;
  public ForumTopicForm: FormGroup;
  public form: FormGroup;
  forumtopic: ForumTopic;
  public isSubmitted: boolean = false;
  id: any;

  ForumTopics_Id: any;
  Member_Id: any;
  Forum_Id: any;
  createdby: any;
  dataLoaded: any;
  media: any;
  Dfile1: any;
  Dfile2: any;
  Dfile3: any;
  listofForums: Forum[];
  listofMembers: Members[];
  listofforumtopic: ForumTopic[];
  status: Status;
  forumtId: any;
  constructor(private adminservice: AdminService, public datepipe: DatePipe, public ele: ElementRef, private router: Router, public route: ActivatedRoute, private http: HttpClient, public fb: FormBuilder) {
    this.forumtopic = new ForumTopic();
  }
  ngOnInit() {

    this.route.params.subscribe(params => {
      this.forumtId = params['forumtId'];

    });
    this.ViewForumTopicform();
    this.onGetAllForumList();
    this.onGetAllMembersList();
    this.onGetForumTopicById();
    this.createdby = localStorage.getItem('MemberName');


  }



  onGetAllMembersList() {
    let id = localStorage.getItem('AccountId');
    //alert(id);
    this.adminservice.onGetAllMembersList(id).subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofMembers = this.status.Data;
        // alert(JSON.stringify(this.Programstructures));
        return this.listofMembers
      }


    });
  }



  onGetAllForumList() {
    let id = localStorage.getItem('AccountId');
  //  alert(id);
    this.adminservice.onGetAllForumList(id).subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofForums = this.status.Data;
        // alert(JSON.stringify(this.Programstructures));
        return this.listofForums;
      }


    });
  }




  onGetForumTopicById() {
    //alert(id);
    this.adminservice.onGetForumTopicById(this.forumtId).subscribe(result => {

      this.status = result;
      if (this.status.StatusCode == "1") {


        this.forumtopic = this.status.Data;
        // alert(JSON.stringify(this.eventmaster));
      }
      else {
        alert(this.status.Data);
      }
      console.log(this.forumtopic);

    });

    return this.forumtopic;
  }



  test(id) {
    //  alert("Hi");
    //console.log(id);
    //alert(id);
    console.log(this.forumtopic.Forum_Id);
    alert(this.forumtopic.Forum_Id);
  }

  test3(id) {
    //  alert("Hi");
    //console.log(id);
    //alert(id);
    console.log(this.forumtopic.Member_Id);
    alert(this.forumtopic.Member_Id);
  }





  onUpdateForumTopic() {

    let files1 = this.ele.nativeElement.querySelector('#ForumTopicFile1').files;
    let files2 = this.ele.nativeElement.querySelector('#ForumTopicFile2').files;
    let files3 = this.ele.nativeElement.querySelector('#ForumTopicFile3').files;
    //let files4 = this.ele.nativeElement.querySelector('#ForumDiscussionFile4').files;
    if (files1.length > 0 && files1.count != 0 && files1 != null) {

      let formData = new FormData();
      let file = files1[0];
      this.Dfile1 = file.name;
      formData.append('ForumTopicFile1', file, file.name);
      console.log(formData);
      console.log(files1);
      this.adminservice.onfileupload(formData).subscribe(res => this.dataLoaded = res);
    }
    if (files2.length > 0 && files2.count != 0 && files2 != null) {

      let formData = new FormData();
      let file = files2[0];
      this.Dfile2 = file.name;
      formData.append('ForumTopicFile2', file, file.name);
      console.log(formData);
      console.log(files2);
      this.adminservice.onfileupload(formData).subscribe(res => this.dataLoaded = res);
    }
    if (files3.length > 0 && files3.count != 0 && files3 != null) {

      let formData = new FormData();
      let file = files3[0];
      this.Dfile3 = file.name;
      formData.append('ForumTopicFile3', file, file.name);
      console.log(formData);
      console.log(files3);
      this.adminservice.onfileupload(formData).subscribe(res => this.dataLoaded = res);
    }
    let date = new Date();

    const ForumTopic = {

      Id: this.forumtopic.Id,
      Forum_Id: this.Viewforumtopicform.controls["forumtopic.Forum_Id"].value,
      Member_Id: this.Viewforumtopicform.controls["forumtopic.Member_Id"].value,
      ForumTopicTitle: this.Viewforumtopicform.controls["forumtopic.ForumTopicTitle"].value,
      ForumTopicContent: this.Viewforumtopicform.controls["forumtopic.ForumTopicContent"].value,
      ForumTopicType: this.Viewforumtopicform.controls["forumtopic.ForumTopicType"].value,
      ForumTopicFile1: "Tcsimages/forumtopicimages/" + this.Dfile1,
      ForumTopicFile2: "Tcsimages/forumtopicimages/" + this.Dfile2,
      ForumTopicFile3: "Tcsimages/forumtopicimages/" + this.Dfile3,
      ForumTopicIsModerated: this.Viewforumtopicform.controls["forumtopic.ForumTopicIsModerated"].value,
      ForumTopicStatus: this.Viewforumtopicform.controls["forumtopic.ForumTopicStatus"].value,
      ForumTopicCreatedBy: this.forumtopic.ForumTopicCreatedBy,
      ForumTopicCreatedDate: this.forumtopic.ForumTopicCreatedDate,
      ForumTopicUpdatedBy: this.createdby,
      ForumTopicUpdatedDate: this.datepipe.transform(date, "yyyy-MM-dd")
    }

    if (this.Viewforumtopicform.valid) {
      this.isSubmitted = false;
     // alert(JSON.stringify(ForumTopic));

      this.adminservice.onUpdateForumTopic(ForumTopic).subscribe(data => {

      });
      alert("Successfully Updated");
      this.router.navigate(['/forumtopic/']);


    }
    else {
      this.isSubmitted = true;
    }

  }

  ViewForumTopicform() {
    this.Viewforumtopicform = this.fb.group({

      'forumtopic.Forum_Id': ['', [Validators.required]],
      'forumtopic.Member_Id': ['', [Validators.required]],
      'forumtopic.ForumTopicTitle': ['', [Validators.required]],
      'forumtopic.ForumTopicContent': ['', [Validators.required]],
      'forumtopic.ForumTopicType': ['', [Validators.required]],

      'forumtopic.ForumTopicIsModerated': [''],
      'forumtopic.ForumTopicStatus': ['', [Validators.required]]
    });
  }


}
