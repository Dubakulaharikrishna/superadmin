import { Component, OnInit, ElementRef } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { CustomValidators } from 'ng2-validation';
import { DatePipe } from '@angular/common'
import 'rxjs/add/operator/debounceTime';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/share';

import { Router  ,ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';
import { ExcelService } from '../services/excel.service';
import { AdminService } from '../services/admin.service';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { CommonModule } from '@angular/common';
import { Status } from '../Models/Status';
import { LocalDataSource } from 'ng2-smart-table';
import { Exercise } from '../Models/Exercise';
import { ExerciseType } from '../Models/ExerciseType ';





//import
@Component({
  selector: 'app-exercise',
  templateUrl: './exercise.component.html',
  styleUrls: ['./exercise.component.css'],
  providers: [ExcelService, DatePipe]
})
export class ExerciseComponent implements OnInit {
  MasterAccount_Id: any;
  ExerciseType_Id: any;
  source: LocalDataSource;
  success: boolean = false;
  message: any;
  dtTrigger: Subject<any> = new Subject();
  public _success = new Subject<string>();
  staticAlertClosed = false;
  status: Status;
  selectedValue: any = 50;
  page: any;
  createdby: any;
  public data: Object;
  public temp_var: Object = false;
  public ExerciseUpdateFormGroup: FormGroup;
  public ExerciseForm: FormGroup;
  public form: FormGroup;
  exercise: Exercise;
  public isSubmitted: boolean = false;
  id: any;
  listofexercise: Exercise[];
  pagename: any;
  listofAccounts:any;
  accountId: any;
  listofexercisetype: ExerciseType[];
  roleId: any;
  constructor(private adminservice: AdminService, public datepipe: DatePipe, private excelService: ExcelService, public ele: ElementRef, private router: Router, private http: HttpClient, public fb: FormBuilder) {
    //this.status = new Status();
    this.exercise = new Exercise();
  
  }


  onGetAllExerciseList(id) {
   // let id = sessionStorage.getItem('AccountId');
  //  alert(id);
    this.adminservice.onGetAllExerciseList(id).subscribe(data => {
     // alert(JSON.stringify(data));
     
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofexercise = this.status.Data;
        this.source = new LocalDataSource(this.listofexercise);
        return this.listofexercise
      }
     

    });
  }

  onGetAllExerciseTypeList() {
    let id = localStorage.getItem('AccountId');
    //alert(id);
    this.adminservice.onGetAllExerciseTypeList(id).subscribe(data => {

      if (data.StatusCode == "1") {
        this.status = data;
        this.listofexercisetype = this.status.Data;
        // alert(JSON.stringify(this.Programstructures));
       

        return this.listofexercisetype;
      }




    });
  }

  onGetAllAccountsList() {
    this.adminservice.onGetAllAccountsList().subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofAccounts = this.status.Data;

        //alert(JSON.stringify(this.listofAccounts));
        return this.listofAccounts
      }

    });
  }
  test(id) {
    //  alert("Hi");
    //console.log(id);
    //alert(id);
    console.log(this.ExerciseType_Id);
  //  alert(this.ExerciseType_Id);
  }

  onChange(event) {
    console.log(event);
    this.onGetAllExerciseList(event.target.value);
  }

  openCpopup() {

    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "block";
    cmask.style.display = "block";
  }

  closeCpopup() {
    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "none";
    cmask.style.display = "none";
  }

  onEdit(event: any) {


    this.router.navigate(['/viewexercise/', { exId: event.data._id }]);
  }
  reset() {
    this.onGetAllExerciseList(this.accountId);
    this.MasterAccount_Id = undefined;
  }


  ngOnInit() {
   // sessionStorage.clear();
   // localStorage.removeItem('loginSessId');
   // localStorage.clear();
   // localStorage.setTimeout = 1;
   // sessionStorage.removeItem('loginSessId');
    //localStorage.removeItem('loginSessId');
    this.pagename = "Exercise";
    localStorage.setItem('loginSessId', this.pagename);
    this.accountId = localStorage.getItem('AccountId');
    this.Exerciseform();
    this.onGetAllExerciseList(this.accountId);
    this.onGetAllAccountsList();
    this.onGetAllExerciseTypeList();
    this.roleId = localStorage.getItem('RoleId');
    setTimeout(() => this.staticAlertClosed = true, 20000);
    this._success.subscribe((message) => this.message = message);
    this._success.debounceTime(7000).subscribe(() => this.message = null);
    this.createdby = localStorage.getItem('MemberName');
  }




  onCreateExercise() {
    let date = new Date();
    const Exercise = {

      ExerciseTitle: this.form.controls["ExerciseTitle"].value,
      ExerciseType_Id: this.form.controls["ExerciseType_Id"].value,
      NoofQuestions: this.form.controls["NoofQuestions"].value,
      ExerciseDescription: this.form.controls["ExerciseDescription"].value,
      ExerciseSynopsis: this.form.controls["ExerciseSynopsis"].value,
      ExerciseObjectives: this.form.controls["ExerciseObjectives"].value,
      ExerciseAccessType: this.form.controls["ExerciseAccessType"].value,
   
      ExerciseStatus: this.form.controls["ExerciseStatus"].value,
      ExerciseDependency: this.form.controls["ExerciseDependency"].value,
      IsExerciseModerated: this.form.controls["IsExerciseModerated"].value,
      ExerciseCreatedBy: this.createdby,
      ExerciseCreatedDate: this.datepipe.transform(date, "yyyy-MM-dd")


    }

    if (this.form.valid) {
      this.isSubmitted = false;
     // alert(JSON.stringify(Exercise));
      this.adminservice.onCreateExercise(Exercise).subscribe(data => {
        console.log(data);
        if (data.StatusCode == "1") {
          this.success = true;
          this.message = data.Message;
          this._success.next(`Record Saved Successfully`);

          this.onGetAllExerciseList(this.accountId);
          this.closeCpopup();

        }
        else {
          this.success = false;
          this.message = "Failed to create account master";
        }
      });
      
     
   
    }
    else {
      this.isSubmitted = true;
    }

  }


  Exerciseform() {
    this.form = this.fb.group({

      'ExerciseTitle': ['', [Validators.required]],
      'ExerciseType_Id': ['', [Validators.required]],
      'NoofQuestions': ['', [Validators.required]],
      'ExerciseDescription': ['', [Validators.required]],
      'ExerciseSynopsis': ['', [Validators.required]],
      'ExerciseObjectives': ['', [Validators.required]],
      'ExerciseAccessType': ['', [Validators.required]],
    //  'ExerciseCreatedDate': ['', [Validators.required]],
    //  'ExerciseCreatedBy': ['', [Validators.required]],
     // 'ExerciseUpdatedDate': ['', [Validators.required]],
    //  'ExerciseUpdatedBy': ['', [Validators.required]],
      'ExerciseStatus': ['', [Validators.required]],
      'ExerciseDependency': ['', [Validators.required]],
      'IsExerciseModerated':['']
    });
  }


  /*Smart table*/
  settings = {

    columns: {
      ExerciseAccessType: {
        title: 'Exercise Type',
        filter: false
      },
      ExerciseObjectives: {
        title: 'Exercise Objectivies',
        filter: false
      },

    },
    attr: { class: 'table table-bordered' },
    actions: {
      edit: false, //as an example
      delete: false, //as an example
      add: false,
      position: 'right',
      custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button" (click)="onEdit(rec.Id)">Edit</button></span>` }]
    },
    pager: { display: true, perPage: 50 },
  };

  //route(event) {
  //  alert('hi');
  //}

  onSearch(query: string = '') {
    if (query != '') {
      this.source.setFilter([
        // fields we want to include in the search
        {
          field: 'ExerciseAccessType',
          search: query
        },
        {
          field: 'ExerciseObjectives',
          search: query
        }
      ], false);
    }
    else {
      this.source = new LocalDataSource(this.listofexercise);
    }

    // second parameter specifying whether to perform 'AND' or 'OR' search 
    // (meaning all columns should contain search query or at least one)
    // 'AND' by default, so changing to 'OR' by setting false here
  }

  onSelectedFilter(event) {
    this.page = event.target.value;
    this.source = new LocalDataSource(this.listofexercise);
    this.settings = {

      columns: {
        ExerciseAccessType: {
          title: 'Exercise Type',
          filter: false
        },
        ExerciseObjectives: {
          title: 'Exercise Objectivies',
          filter: false
        },

      },
      attr: { class: 'table table-bordered' },
      actions: {
        edit: false, //as an example
        delete: false, //as an example
        add: false,
        position: 'right',
        custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button">Edit</button></span>` }]
      },
      pager: { display: true, perPage: this.page },
    };

  }


  onExport() {
    this.excelService.exportAsExcelFile(this.listofexercise, 'Exercise');
  }

}
