import { Routes } from '@angular/router';
import { ExerciseComponent } from './exercise.component';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';
export const ExerciseRoutes: Routes = [

  {
    path: '',
    component: ExerciseComponent,
    canActivate: [AuthGuard],
    data: {
      heading: 'Exercise'
    }
  },


];
