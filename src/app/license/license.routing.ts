import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';

import { LicenseComponent } from './license.component';

export const LicenseRoutes: Routes = [

  {
    path: '',
    component: LicenseComponent,
    canActivate: [AuthGuard],

    data: {
      heading: 'License'
    }
  },


];
