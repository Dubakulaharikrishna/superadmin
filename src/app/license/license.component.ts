
import { Component, OnInit } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';
import { License } from '../Models/license';
import { AdminService } from '../services/admin.service';
import { ExcelService } from '../services/excel.service';
import { CommonModule } from '@angular/common';
import { Status } from '../Models/Status';
import { Subject } from 'rxjs/Subject';
import { Router, ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common'
import { InstanceManager } from '../Models/InstanceManager';
import { Webclient } from '../Models/Webclient';
@Component({
  selector: 'app-license',
  templateUrl: './license.component.html',
  styleUrls: ['./license.component.css'],
  providers: [ExcelService, DatePipe]
})
export class LicenseComponent implements OnInit {
  count: number;
  Instance_Id: any;
  ClientInfo: any;
  setDate: any;
  //form: any;
  instancemanager: InstanceManager;
  result: any[];
  success: boolean = false;
  message: any;
  webclient: Webclient;
  dtTrigger: Subject<any> = new Subject();
  public _success = new Subject<string>();
  staticAlertClosed = false;
  title = 'Simple Datatable Example using Angular 4';
  public data: Object;
  estaDate: any;
  expDate: any;
  public temp_var: Object = false;
  public form: FormGroup;
  license: License;
  public LicenseForm: FormGroup;
  isSubmitted: boolean = false;
  isSubmittedPwd: boolean = false;
  private showActions: any;
  listofLicense: License[];
  webclientslist: Webclient[];
  _id: any;
  this: any;
  weId: any;
  instid: any;
  licenceid: any;
  pagename: any;
  status: Status;
  //viewlicenceinfo
  createdby: any;
  requestStatus: boolean = false;
  listofinstances: InstanceManager[];
  
  constructor(private adminservice: AdminService, public datepipe: DatePipe, public route: ActivatedRoute, private excelService: ExcelService, private router: Router, private http: HttpClient, public fb: FormBuilder) {
    this.showActions = false;
    this.isSubmittedPwd = false;
    this.license = new License();
    this.webclient = new Webclient();
    this.instancemanager = new InstanceManager();
  }



  showPopActions(id) {
    //  alert(id);
    this.showActions = id;
    this.ViewLicense(id);
    return id;
  }
  //popup(id) {
  //  this.onGetLicenseById(id);
  //  return true;
  //}

  hidePopActions() {
    this.showActions = false;
  }

  GenerateLicense(memId: any) {
    this.licenceid = memId;
   // alert(memId);
   // this.adminService.onGetMemberById(this.celebid);
    this.router.navigate(['/generatelicense/', { memId: this.licenceid }]);
  }


  ViewLicenseInfo(licId: any) {
  //  alert(licId);
    this.licenceid = licId;
    this.router.navigate(['/viewlicenseinfo/', { licId: this.licenceid }]);
    //alert(licId);
  }

  openCpopup() {

    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "block";
    cmask.style.display = "block";
  }

  closeCpopup() {
    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "none";
    cmask.style.display = "none";
    this.form.reset();
  }


  //viewlicenseinfo

  ViewLicense(id: any) {
    //alert(id);
    this.onGetLicenseById(id);

    // return true;
  }

  onGetLicenseById(id) {
    this.adminservice.onGetLicenseById(id).subscribe(result => {
     // this.status = result;
      if (result.success == 1) {


        this.license = result.data;
      }
      else {
        alert(result.success);
      }
      //this.requestStatus = result.RequestStatus;
      //this.licenseStatus = result.LicenseStatus;
      console.log(this.license);
      if (this.license.RequestStatus == "InProcess") {
        this.requestStatus = true;
        console.log(this.requestStatus);
      }
      else {
        this.requestStatus = false;
        console.log(this.requestStatus);
      }
    });
    return true;
  }




  onEdit(id: any) {
   // alert(id);
    this.router.navigate(['/viewlicense/', { licId: id }]);
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.success = true;
     
      this.message = params['message'];
      setTimeout(() => this.staticAlertClosed = true, 20000);
      this._success.subscribe((message) => this.message = message);
      this._success.debounceTime(7000).subscribe(() => this.message = null);

    });
    //sessionStorage.clear();
  //  localStorage.removeItem('loginSessId');
   // localStorage.clear();
   // localStorage.setTimeout = 1;
  //  sessionStorage.removeItem('loginSessId');
   // localStorage.removeItem('loginSessId');
    this.pagename = "License manager";
    localStorage.setItem('loginSessId', this.pagename);
    setTimeout(() => this.staticAlertClosed = true, 20000);
    this._success.subscribe((message) => this.message = message);
    this._success.debounceTime(7000).subscribe(() => this.message = null);
    this.createdby = localStorage.getItem('MemberName');
  
    this.onGetAllInstanceManagerList();
    this.Licenseform();
    this.onGetAllLicenseList();
    this.onGetAllWebClientList();
    this.instid = localStorage.getItem('InstanceId');
  }

  onGetAllLicenseList() {
    this.adminservice.onGetAllLicenseList().subscribe(result => {

      if (result.success == 1) {
        
        this.listofLicense = result.data;
        this.count = this.listofLicense.length;
        // alert(JSON.stringify(this.Programstructures));
        return this.listofLicense
      }
     
    });
  }
  
  onGetAllInstanceManagerList() {
    this.adminservice.onGetAllInstanceManagerList().subscribe(result => {
      if (result.success == 1) {
        //  this.status = data;
        this.listofinstances = result.data;

        // alert(JSON.stringify(this.Programstructures));
        return this.listofinstances
      }

    });
  }

  onGetAllWebClientList() {
    this.adminservice.onGetAllWebClientList().subscribe(result => {

      if (result.success == 1) {

        this.webclientslist = result.data;

        // alert(JSON.stringify(this.Programstructures));
        return this.webclientslist
      }

    });
  }

  onGetWebClientById() {
    //alert(this.ClientInfo);
    this.adminservice.onGetWebClientById(this.ClientInfo).subscribe(result => {
    //  alert(JSON.stringify(result));
      console.log("hiii",result);
      //  this.status = result;
      if (result.success == 1) {


        this.webclient = result.data;
      }
      else {
        alert(result.data);
      }
      console.log(this.webclient);

      if (this.webclient.CompanyEstablishedDate != null && this.webclient.CompanyEstablishedDate != "" && this.webclient.CompanyEstablishedDate != undefined) {
        this.estaDate = this.datepipe.transform(this.webclient.CompanyEstablishedDate, 'yyyy-MM-dd');
        this.webclient.CompanyEstablishedDate = this.estaDate;
      }


    });
    return this.webclient;
  }

  onGetInstanceManagerById() {
    this.adminservice.onGetInstanceManagerById(this.Instance_Id).subscribe(result => {
      //  this.status = result;
      //alert(JSON.stringify(result));
      if (result.success == 1) {


        this.instancemanager = result.data;
      }
      else {
        alert(result.data);
      }

      console.log(this.instancemanager);

      if (this.instancemanager.SetupDateTime != null && this.instancemanager.SetupDateTime != "" && this.instancemanager.SetupDateTime != undefined) {
        this.setDate = this.datepipe.transform(this.instancemanager.SetupDateTime, 'yyyy-MM-dd');
        this.instancemanager.SetupDateTime = this.setDate;
      }
      if (this.instancemanager.LicenseExpiryDate != null && this.instancemanager.LicenseExpiryDate != "" && this.instancemanager.LicenseExpiryDate != undefined) {
        this.expDate = this.datepipe.transform(this.instancemanager.LicenseExpiryDate, 'yyyy-MM-dd');
        this.instancemanager.LicenseExpiryDate = this.expDate;
      }

    });
    return this.instancemanager;
  }

  client(id) {
   // alert(this.ClientInfo);
    this.onGetWebClientById();
    console.log(this.ClientInfo);
    //    (this.ClientInfo);

  }

  test(id) {
   // alert(this.ClientInfo);
    this.onGetInstanceManagerById();
    console.log(this.Instance_Id);
    //    (this.ClientInfo);

  }
  onCreateLicenses(license: any) {
    let date = new Date();

    const License = {
      InstanceManagerId: this.instid,
      Instance_Id: this.form.get('Instance_Id').value,
      ClientInfo: this.form.get('ClientInfo').value,
      CompanyName: this.form.controls["webclient.CompanyName"].value,
      LegalName: this.form.controls["webclient.LegalName"].value,
      RegistrationNumber: this.form.controls["webclient.CompanyRegistrationNumber"].value,
      CompanyType: this.form.controls["webclient.CompanyType"].value,
      AddressLine1: this.form.controls["webclient.CompanyAddressLine1"].value,
      AddressLine2: this.form.controls["webclient.CompanyAddressLine2"].value,
      City: this.form.controls["City"].value,
      State: this.form.controls["State"].value,
      ZipCode: this.form.controls["ZipCode"].value,
      Country: this.form.controls["Country"].value,
      Industry: this.form.controls["Industry"].value,
      AreaOfBusiness: this.form.controls["AreaOfBusiness"].value,
      EstablishedDate: this.form.controls["webclient.CompanyEstablishedDate"].value,
      OperatingCountries: this.form.controls["OperatingCountries"].value,
      CompanyWebsite: this.form.controls["CompanyWebsite"].value,
      CompanySocialUrls: this.form.controls["CompanySocialUrls"].value,
      PrimaryAdminEmailID: this.form.controls["webclient.PrimaryAdminEmailID"].value,
      PrimaryAdminPassword: this.form.controls["PrimaryAdminPassword"].value,
      PrimaryAdminPhone: this.form.controls["webclient.AdminPhoneNumber"].value,
      CompanyEmailID: this.form.controls["webclient.CompanyEmailID"].value,
      CompanyPhone: this.form.controls["webclient.CompanyPhone"].value,
      InstancePublicIPAddress: this.form.controls["instancemanager.InstancePublicIPAddress"].value,
    //  ClientAgentDetails: this.form.controls["ClientAgentDetails"].value,
     // CreatedDate: this.form.controls["CreatedDate"].value,
     // CreatedBy: this.form.controls["CreatedBy"].value,
     // UpdatedDate: this.form.controls["UpdatedDate"].value,
     // UpdatedBy: this.form.controls["UpdatedBy"].value,
      RequestStatus: this.form.controls["RequestStatus"].value,
     // LicenseStatus: this.form.controls["LicenseStatus"].value,
      Remarks: this.form.controls["Remarks"].value,
      //LicenseStartDate: this.form.controls["LicenseStartDate"].value,
     // LicenseEndDate: this.form.controls["LicenseEndDate"].value,
     // LicenseKey: this.form.controls["LicenseKey"].value,
     // DecodeScript: this.form.controls["DecodeScript"].value,
    //  LicenseGeneratedDate: this.form.controls["LicenseGeneratedDate"].value,
     // LicenseGeneratedBy: this.form.controls["LicenseGeneratedBy"].value,
    //  LicenseType: this.form.controls["LicenseType"].value,
     // RecentSecurityToken: this.form.controls["RecentSecurityToken"].value,
     // RecentTokenReqDate: this.form.controls["RecentTokenReqDate"].value
      CreatedBy: this.createdby,
      //CreatedDate: this.datepipe.transform(date, "yyyy-MM-dd")
    }
   //alert(JSON.stringify(License));
    if (this.form.valid) {
      this.isSubmitted = false;
      this.isSubmittedPwd = false;
      this.adminservice.onCreateLicense(License).subscribe(result => {
        this.isSubmittedPwd = true;
        console.log("Create obj")
        console.log(result);
        if (result.success == 1) {
          this.success = true;
          this.message = result.Message;
          this._success.next(`Record Saved Successfully`);

          this.onGetAllLicenseList();
          this.closeCpopup();
          this.form.reset();

        }
        else {
          this.success = false;
          this.message = "Failed to create License";
        }
      });
      //alert("Submitted Successfully");
     
    }
    else {
      this.isSubmitted = true;
      this.isSubmittedPwd = true;
    }


    //this.adminservice.onCreateLicense(license).subscribe(res => this.listofLicense = res);



  }


  //validatePassword(controls) {
  //  const regExp = new RegExp(/^(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[\d])(?=.*?[\W]).{8,35}$/);
  //  if (regExp.test(controls.value)) {
  //    return null;
  //  } else {
  //    return { 'validatePassword': true }
  //  }
  //}


  Licenseform() {
    this.form = this.fb.group({
      'Instance_Id': ['', [Validators.required]],
      'webclient.CompanyName': ['', [Validators.required]],
      'webclient.LegalName': ['', [Validators.required]],

      'webclient.CompanyRegistrationNumber': ['', [Validators.required]],
      'ClientInfo': ['', [Validators.required]],
      'webclient.CompanyType': ['', [Validators.required]],
      'webclient.CompanyAddressLine1': ['', [Validators.required]],
      'webclient.CompanyAddressLine2': ['', [Validators.required]],
      'City': ['', [Validators.required]],
      'State': ['', [Validators.required]],
      'ZipCode': ['', [Validators.required]],
      'Country': ['', [Validators.required]],
      'Industry': ['', [Validators.required]],
      'AreaOfBusiness': ['', [Validators.required]],
      'webclient.CompanyEstablishedDate': ['', [Validators.required]],
      'OperatingCountries': ['', [Validators.required]],
      'CompanyWebsite': ['', [Validators.required]],
      'CompanySocialUrls': ['', [Validators.required]],
      'webclient.PrimaryAdminEmailID': ['', [Validators.required]],
      'PrimaryAdminPassword': ['', [Validators.required]],
      //'PrimaryAdminPassword': [null, Validators.compose([
      //  Validators.required,
      //  Validators.minLength(8),
      //  Validators.maxLength(8),
      //  this.validatePassword
      //])],
      'webclient.AdminPhoneNumber': ['', [Validators.required]],
      //// RegistrationNumber': ['', [Validators.required]],

      'webclient.CompanyEmailID': ['', [Validators.required]],
      'webclient.CompanyPhone': ['', [Validators.required]],
      'instancemanager.InstancePublicIPAddress': ['', [Validators.required]],
     // 'ClientAgentDetails': ['', [Validators.required]],
      //'CreatedDate': ['', [Validators.required]],
     // 'CreatedBy': ['', [Validators.required]],
     // 'UpdatedDate': ['', [Validators.required]],
     // 'UpdatedBy': ['', [Validators.required]],
      'RequestStatus': ['', [Validators.required]],
    //  'LicenseStatus': ['', [Validators.required]],
      'Remarks': ['', [Validators.required]],
     // 'LicenseStartDate': ['', [Validators.required]],
     // 'LicenseEndDate': ['', [Validators.required]],
      //'LicenseKey': ['', [Validators.required]],
     // 'DecodeScript': ['', [Validators.required]],
     // 'LicenseGeneratedDate': ['', [Validators.required]],

     // 'LicenseGeneratedBy': ['', [Validators.required]],
      // 'LicenseType': ['', [Validators.required]],
     // 'RecentSecurityToken': ['', [Validators.required]],
     // 'RecentTokenReqDate': ['', [Validators.required]]


    });
  }

  onExport() {
    this.excelService.exportAsExcelFile(this.listofLicense, 'LicenseManager');
  }

}
