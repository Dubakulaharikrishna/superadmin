import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgramcategoryComponent } from './programcategory.component';

describe('ProgramcategoryComponent', () => {
  let component: ProgramcategoryComponent;
  let fixture: ComponentFixture<ProgramcategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgramcategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgramcategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
