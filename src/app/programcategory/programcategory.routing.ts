import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';
import { ProgramcategoryComponent } from './programcategory.component';

export const ProgramcategoryRoutes: Routes = [

  {
    path: '',
    component: ProgramcategoryComponent,
    canActivate: [AuthGuard],

    data: {
      heading: 'Accountmanagement works!'
    }    
  },

  
];

