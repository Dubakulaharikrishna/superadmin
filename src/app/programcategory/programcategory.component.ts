import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { Router } from '@angular/router';
//changes
import { Subject } from 'rxjs/Subject';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';
import { ExcelService } from '../services/excel.service';
import { AdminService } from '../services/admin.service';
import { programcategory } from '../Models/programcategory';
import { Status } from '../Models/Status';
import { Ng2SmartTableModule, LocalDataSource } from 'ng2-smart-table';
import { AccountManagement } from '../Models/AccountManagement';
import { DatePipe } from '@angular/common'
@Component({
  selector: 'app-programcategory',
  templateUrl: './programcategory.component.html',
  styleUrls: ['./programcategory.component.css'],
  providers: [ExcelService, DatePipe]
})
export class ProgramcategoryComponent implements OnInit {
  MasterAccount: any;
  dataLoaded: any;
  createdby: any;
  coverPic: any;
  success: boolean = false;
  message: any;
  dtTrigger: Subject<any> = new Subject();
  public _success = new Subject<string>();
  staticAlertClosed = false;
  MasterAccount_Id: any;
  public isSubmitted: boolean = false;
  result: any[];
  title = 'Simple Datatable Example using Angular 4';
  source: LocalDataSource;
  selectedValue: any = 50;
  page: any;
  public form: FormGroup;
  programcategory: programcategory;
  pagename: any;
  id: any;
  listofAccounts: AccountManagement[];
  Id: any;
  roleId: any;
  status: Status;
  accountId: any;
listofprogramcategory: programcategory[];
  constructor(private adminservice: AdminService, public datepipe: DatePipe, public ele: ElementRef, private excelService: ExcelService, private router: Router, private http: HttpClient, public fb: FormBuilder) {
    this.programcategory = new programcategory();
  }

  ngOnInit() {

    //sessionStorage.clear();
  //  localStorage.removeItem('loginSessId');
   // localStorage.clear();
   // localStorage.setTimeout = 1;
   // sessionStorage.removeItem('loginSessId');
   // localStorage.removeItem('loginSessId');
    this.pagename = "Program category";
    localStorage.setItem('loginSessId', this.pagename);
    this.accountId = localStorage.getItem('AccountId');
    this.roleId = localStorage.getItem('RoleId');
    this.ProgramCategoryform();
    this.onGetAllProgramCategoryList(this.accountId);
    this.onGetAllAccountsList();
    setTimeout(() => this.staticAlertClosed = true, 20000);
    this._success.subscribe((message) => this.message = message);
    this._success.debounceTime(7000).subscribe(() => this.message = null);
    this.createdby = localStorage.getItem('MemberName');

  }
  reset() {
    this.onGetAllProgramCategoryList(this.accountId);
    this.MasterAccount = undefined;
  }
  openCpopup() {

    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "block";
    cmask.style.display = "block";
  }



  closeCpopup() {
    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "none";
    cmask.style.display = "none";
  }


  onChange(event) {
    console.log(event);
    this.onGetAllProgramCategoryList(event.target.value);
  }
  onEdit(event: any) {
    this.router.navigate(['/viewprogramcategory/', { pcId: event.data.Id }]);
    // alert(id);
    //alert(this.accountmanagement);
  }


  onGetAllAccountsList() {
    this.adminservice.onGetAllAccountsList().subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofAccounts = this.status.Data;

        //alert(JSON.stringify(this.listofAccounts));
        return this.listofAccounts
      }

    });
  }
  test(id) {
    //  alert("Hi");
    //console.log(id);
    //alert(id);
    console.log(this.MasterAccount_Id);
   // alert(this.MasterAccount_Id);
  }
  onGetAllProgramCategoryList(id) {
   // let id = sessionStorage.getItem('AccountId');
    //alert(id)
    this.adminservice.onGetAllProgramCategory().subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofprogramcategory = this.status.Data;
        this.source = new LocalDataSource(this.listofprogramcategory);
        // alert(JSON.stringify(this.Programstructures));
        return this.listofprogramcategory;
      }
    
    });
  }

  onCreateProgramCategory() {

    let files = this.ele.nativeElement.querySelector('#ProgramCategoryImgUrl').files;
    if (files.length > 0 && files.count != 0 && files != null) {

      let formData = new FormData();
      let file = files[0];
      this.coverPic = file.name;
      formData.append('ProgramCategoryImgUrl', file, file.name);
      console.log(formData);
      console.log(files);
      this.adminservice.onfileupload(formData).subscribe(res => this.dataLoaded = res);
    }
    let date = new Date();

    if (this.form.valid) {
      this.isSubmitted = false;
      // alert(JSON.stringify(license));
      const pc = {
        // AccountID: this.form.get('AccountID').value,
        MasterAccount_Id: this.form.get('MasterAccount_Id').value,
        ProgramCategoryName: this.form.get('ProgramCategoryName').value,
        ProgramCategoryDescription: this.form.get('ProgramCategoryDescription').value,
        ProgramCategoryStatus: this.form.get('ProgramCategoryStatus').value,
        ProgramCategoryImgUrl: "Tcsimages/prgcategoryimages/" + this.coverPic,
        ProgramCategoryCreatedBy: this.createdby,
        ProgramCategoryCreatedDate: this.datepipe.transform(date, "yyyy-MM-dd")
       


      }
    //  alert(JSON.stringify(lic));
      this.adminservice.onCreateProgramCategory(pc).subscribe(data => {
        console.log(data);
        if (data.StatusCode == "1") {
          this.success = true;
          this.message = data.Message;
          this._success.next(`Record Saved Successfully`);

          this.onGetAllProgramCategoryList(this.accountId);
          this.closeCpopup();

        }
        else {
          this.success = false;
          this.message = "Failed to create account master";
        }

      });
    //  alert("Successfully Submitted");
     
      }


    else {
          this.isSubmitted = true;
          //alert(JSON.stringify(license));
        }
    //alert(JSON.stringify(account));
  }


  ProgramCategoryform() {
    this.form = this.fb.group({
      // AccountID: ['', [Validators.required]],
      MasterAccount_Id: ['', [Validators.required]],
      ProgramCategoryName: ['', [Validators.required]],
      ProgramCategoryDescription: ['', [Validators.required]],
      ProgramCategoryStatus: ['', [Validators.required]],
     

    });
  }


  /*Smart table*/
  settings = {

    columns: {
      ProgramCategoryName: {
        title: 'Program Category Name',
        filter: false
      },
      ProgramCategoryDescription: {
        title: 'Program Category Description',
        filter: false
      },
    
    },
    attr: { class: 'table table-bordered' },
    actions: {
      edit: false, //as an example
      delete: false, //as an example
      add: false,
      position: 'right',
      custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button" (click)="onEdit(rec.Id)">Edit</button></span>` }]
    },
    pager: { display: true, perPage: 50 },
  };

  //route(event) {
  //  alert('hi');
  //}

  onSearch(query: string = '') {
    if (query != '') {
      this.source.setFilter([
        // fields we want to include in the search
        {
          field: 'Program Category Name',
          search: query
        },
        {
          field: 'Program Category Description',
          search: query
        }
      ], false);
    }
    else {
      this.source = new LocalDataSource(this.listofprogramcategory);
    }

    // second parameter specifying whether to perform 'AND' or 'OR' search 
    // (meaning all columns should contain search query or at least one)
    // 'AND' by default, so changing to 'OR' by setting false here
  }

  onSelectedFilter(event) {
    this.page = event.target.value;
    this.source = new LocalDataSource(this.listofprogramcategory);
    this.settings = {

      columns: {
        ProgramCategoryName: {
          title: 'Program Category Name',
          filter: false
        },
        ProgramCategoryDescription: {
          title: 'Program Category Description',
          filter: false
        },
       
      },
      attr: { class: 'table table-bordered' },
      actions: {
        edit: false, //as an example
        delete: false, //as an example
        add: false,
        position: 'right',
        custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button">Edit</button></span>` }]
      },
      pager: { display: true, perPage: this.page },
    };

  }

  onExport() {
    this.excelService.exportAsExcelFile(this.listofprogramcategory, 'ProgramCategory');
  }

}
