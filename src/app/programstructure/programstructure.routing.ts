import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';

import { ProgramstructureComponent } from './programstructure.component';
//import { ProgramManagementComponent } './programmanagement/programmanagement.component'
export const ProgramstructureRoutes: Routes = [

  {
    path: '',
    component: ProgramstructureComponent,
    canActivate: [AuthGuard],

    data: {
      heading: 'Programmanagement'
    }
  },


];
