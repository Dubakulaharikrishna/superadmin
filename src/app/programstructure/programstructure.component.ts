import { Component, OnInit, ElementRef } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';
//import { Programmanagement } from '../programmanagement/programmanagement.module';
import { CommonModule } from '@angular/common';
import { Subject } from 'rxjs/Subject';

//import { ProgramManagementModule } from './programmanagement.module';
import { Ng2SmartTableModule, LocalDataSource } from 'ng2-smart-table';
import { ExcelService } from '../services/excel.service';
import { AdminService } from '../services/admin.service';
import { Programstructure } from '../Models/Programstructure';
//import { ProgramManagement } from '../Models/programmanagement';
import { Exercise } from '../Models/Exercise';
import { Status } from '../Models/Status';
import { Dropdownresults } from '../Models/Dropdownresults';
import { Observable } from 'rxjs/Observable';
import { programcategory } from '../Models/programcategory';
import { Capabilities } from '../Models/capabilities';
import { Modules } from '../Models/modules';
import { AccountManagement } from '../Models/AccountManagement';
//import { Capabilities } from 'selenium-webdriver';
import { DatePipe } from '@angular/common'
@Component({
  selector: 'app-programstructure',
  templateUrl: './programstructure.component.html',
  styleUrls: ['./programstructure.component.css'],
  providers: [ExcelService, DatePipe]
})
export class ProgramstructureComponent implements OnInit {
  dataLoaded: any;
  MasterAccount_Id: any;
  success: boolean = false;
  message: any;
  coverpic: any;
  dtTrigger: Subject<any> = new Subject();
  public _success = new Subject<string>();
  staticAlertClosed = false;
  dropdown: Dropdownresults[];
  public form: FormGroup;
  isSubmitted: boolean = false;
  listofprogramstructure: Programstructure[];
  pagename: any;
  source: LocalDataSource;
  selectedValue: any = 50;
  page: any;

  programstructure: Programstructure;
  listofcapabilities: Capabilities[];
  expanded: boolean = false;
  secretprogram: boolean = false;
  secret: boolean = false;
  secretmodule: boolean = false;
  secretexercise: boolean = false;
  selectedcapabilities: any = [];
  selectedmodules: any = [];
  selectedexercise: any = [];
  selectedcatagorys: any = [];
  listofprogramcategory: programcategory[];
  listofModules: Modules[];
  listofexercise: Exercise[];
  accountId: any;
  status: Status;
  selected: any;
  createdby: any;
  dropdownresults: any = {};
  AddprogramStructure: any;
  listofAccounts: AccountManagement[];
  roleId: any;
  constructor(public router: Router, public datepipe: DatePipe, public http: HttpClient, private excelService: ExcelService, public fb: FormBuilder, private adminservice: AdminService, public ele: ElementRef) {

    this.programstructure = new Programstructure();
    this.status = new Status();
    this.dropdown = new Array<Dropdownresults>();

  }
  devices = 'one two three'.split(' ');
  selectedDevice = 'two';
  //onChange(newValue) {
  //  console.log(newValue);
  //  this.selectedDevice = newValue;
  //  alert(newValue);
  //}
  onGetAllProgramStructureList(id) {
    // let id = sessionStorage.getItem('AccountId');
    //alert(id);
    this.adminservice.onGetAllProgramStructureList(id).subscribe(data => {

      if (data.StatusCode == "1") {

        this.listofprogramstructure = data.Data;
        this.source = new LocalDataSource(this.listofprogramstructure);
        // alert(JSON.stringify(this.listofprogramstructure));
        return this.listofprogramstructure;

      }
    });

  }


  name: any;

  test(id) {
    //  alert("Hi");
    console.log(id);
    //alert(id);
    console.log(this.name);
    // alert(this.name)
  }

  onChange(event) {
    console.log(event);
    this.onGetAllProgramStructureList(event.target.value);
  }

  reset() {
    this.onGetAllProgramStructureList(this.accountId);
    this.MasterAccount_Id = undefined;
  }

  onGetAllAccountsList() {
    this.adminservice.onGetAllAccountsList().subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofAccounts = this.status.Data;

        //alert(JSON.stringify(this.listofAccounts));
        return this.listofAccounts
      }

    });
  }

  //onGetAllProgramManagementList() {
  //ChnageCapabilities(id) {
  //  // alert(this.form.controls["ProgramType"].value)
  //  if (this.form.controls["ProgramStrType"].value == "Capabilities") {

  //    this.dropdown.length = 0;
  //    this.adminservice.onGetAllProgramManagementList().subscribe(data => {


  //      this.dropdownresults = data;
  //      console.log(this.listofprogrammanagement);
  //      data.forEach((i) => {

  //        // alert(i.ProgramName);
  //        this.dropdown.push(
  //          {
  //            id: i.Id,
  //            value: i.ProgramName
  //          });
  //        // this.dropdown.value = i.;
  //      });

  //      alert(JSON.stringify(this.dropdown));
  //    });
  //    //}
  //    //  alert("capabilities");
  //  }
  //  else if (this.form.controls["ProgramStrType"].value == "Modules") {
  //    //onGetAllProgramsList() {
  //    this.dropdown.length = 0;
  //    this.adminservice.onGetAllProgramsList().subscribe(data => {


  //      this.dropdownresults = data;

  //      console.log(this.listofPrograms);

  //      data.forEach((i) => {

  //        // alert(i.ProgramName);
  //        this.dropdown.push(
  //          {
  //            id: i.Id,
  //            value: i.ContentTitle
  //          });
  //        // this.dropdown.value = i.;
  //      });
  //      alert(JSON.stringify(this.dropdown));
  //    });

  //    alert("Modules");
  //    //}
  //  }
  //  else {
  //    this.dropdown.length = 0;
  //    this.adminservice.onGetAllExerciseList().subscribe(data => {

  //      this.dropdownresults = data;
  //      console.log(this.listofexercise);

  //      data.forEach((i) => {

  //        // alert(i.ProgramName);
  //        this.dropdown.push(
  //          {
  //            id: i.Id,
  //            value: i.ExerciseTitle
  //          });
  //        // this.dropdown.value = i.;
  //      });




  //      alert(JSON.stringify(this.dropdown));
  //    });
  //    alert("Exercise");
  //  }
  //  // this.secret = true;

  //}


  onGetAllProgramManagementList() {
    let id = localStorage.getItem('AccountId');
    this.adminservice.onGetAllCapabilitiesList(id).subscribe(data => {
      // alert(data);

      if (data.StatusCode == "1") {

        this.listofcapabilities = data.Data;
       //  alert(JSON.stringify(this.listofcapabilities));
        return this.listofcapabilities;

      }
      // this.listofcapabilities = data;
      //// alert(this.listofprogrammanagement);
      // console.log(this.listofcapabilities);
    });
  }


  onGetAllProgramsList() {
    let id = localStorage.getItem('AccountId');
    this.adminservice.onGetAllModulesList(id).subscribe(data => {
      if (data.StatusCode == "1") {

        this.listofModules = data.Data;
         //alert(JSON.stringify(this.listofModules));
        return this.listofModules;

      }

    });
  }

  onGetAllExerciseList() {
    let id = localStorage.getItem('AccountId');
    // alert(id);
    this.adminservice.onGetAllExerciseList(id).subscribe(data => {
      if (data.StatusCode == "1") {

        this.listofexercise = data.Data;
        // alert(JSON.stringify(this.listofexercise));
        return this.listofexercise;

      }


    });
  }


  onGetAllProgramCategoryList() {
    let id = localStorage.getItem('AccountId');
    //  alert(id);
    this.adminservice.onGetAllProgramCategory().subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofprogramcategory = this.status.Data;
       // alert(JSON.stringify(this.listofprogramcategory));
        return this.listofprogramcategory;

      }
    });
  }

  openCpopup() {

    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "block";
    cmask.style.display = "block";
  }

  //openCpopup() {
  //  this.router.navigate(['/createstructure/']);
  //}

  closeCpopup() {
    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "none";
    cmask.style.display = "none";
  }
  onEdit(event: any) {
    this.router.navigate(['/viewprogramstructure/', { prmId: event.data.Id }]);
  }

  ngOnInit() {

    // sessionStorage.clear();
    //localStorage.removeItem('loginSessId');
    // localStorage.clear();
    // localStorage.setTimeout = 1;
    //sessionStorage.removeItem('loginSessId');
    //  localStorage.removeItem('loginSessId');
    this.pagename = "Programs";
    localStorage.setItem('loginSessId', this.pagename);
    setTimeout(() => this.staticAlertClosed = true, 20000);
    this._success.subscribe((message) => this.message = message);
    this._success.debounceTime(7000).subscribe(() => this.message = null);
    this.accountId = localStorage.getItem('AccountId');
    this.ProgramStructureform();
    this.onGetAllProgramStructureList(this.accountId);
    this.onGetAllProgramManagementList();
    this.onGetAllProgramsList();
    this.onGetAllExerciseList();
    this.onGetAllAccountsList();
    this.onGetAllProgramCategoryList();
    this.roleId = localStorage.getItem('RoleId');
    this.createdby = localStorage.getItem('MemberName');
    // document.getElementById('ProgramStrCapabilities')

  }




  values: any;
  moveUp(value, index) {

    if (index > 0) {
      const tmp = this.yy[index - 1];
      this.yy[index - 1] = this.yy[index];
      this.yy[index] = tmp;
    }
  }

  moveDown(value, index) {
    if (index < this.yy.length) {
      const tmp = this.yy[index + 1];
      this.yy[index + 1] = this.yy[index];
      this.yy[index] = tmp;
    }
  }

  yy: any = [];

  // OnAdd() {
  ////  (<HTMLInputElement>document.getElementById("label")).innerHTML  = "testggg";

  //   const AddprogramStructure = {
  //     ProgramStrName: this.form.controls["ProgramStrName"].value,
  //     //ProgramStrName: (<HTMLInputElement>document.getElementById("ProgramStrName")).value,
  //     //NoOfCapabilities: this.form.controls["NoOfCapabilities"].value,
  //     //NoOfModules: this.form.controls["NoOfModules"].value,
  //     ProgramStrType: this.form.controls["ProgramStrType"].value,
  //     ProgramStrDependency: this.form.controls["ProgramStrDependency"].value,
  //     // ProgramStrTitle: (<HTMLInputElement>document.getElementById("ProgramStrTitle")).value,
  //     ProgramStrTitle: this.form.controls["ProgramStrTitle"].value,

  //     // ProgramStrTitle: this.selectedcapabilities,
  //     //ProgramStrModules: this.selectedmodules,
  //     //ProgramStrExercise: this.selectedexercise,
  //     ProgramStrAccessType: this.form.controls["ProgramStrAccessType"].value,
  //     ProgramStrStatus: this.form.controls["ProgramStrStatus"].value,

  //   }
  //   //var rows = [];
  //   //var output = [];
  //   //var columns = [];

  //   alert(JSON.stringify(AddprogramStructure));
  //   this.yy = AddprogramStructure;


  //   (<HTMLInputElement>document.getElementById("Type")).innerHTML = this.form.controls["ProgramStrType"].value,
  //     (<HTMLInputElement>document.getElementById("Title")).innerHTML = this.form.controls["ProgramStrTitle"].value,
  //     (<HTMLInputElement>document.getElementById("Dependency")).innerHTML = this.form.controls["ProgramStrDependency"].value,
  //     (<HTMLInputElement>document.getElementById("Status")).innerHTML = this.form.controls["ProgramStrStatus"].value,


  //     //(<HTMLInputElement>document.getElementById("ProgramStrType")).innerHTML = "",
  //     //(<HTMLInputElement>document.getElementById("ProgramStrTitle")).innerHTML = "",
  //     //(<HTMLInputElement>document.getElementById("ProgramStrDependency")).innerHTML = "",
  //     //(<HTMLInputElement>document.getElementById("ProgramStrStatus")).innerHTML = "",



  //   //columns.push(this.yy);
  //   //rows.push(columns);
  //   //for (var i = 0; i < rows.length; i++) {
  //   //  output[i] = [];
  //   //  for (var j = 0; j < rows[i].length; j++) {
  //   //    output[i][j] = rows[j][i];
  //   //  }
  //   //}
  //   alert(JSON.stringify(this.yy));
  //   return this.yy;



  // }

  ProgramCategorytest() {
    this.secretprogram = true;
    var checkboxes = document.getElementById("ProgramCategory");
    //  alert(this.expanded);
    if (!this.expanded) {
      checkboxes.style.display = "block";
      this.expanded = true;
    } else {
      checkboxes.style.display = "none";
      this.expanded = false;
    }


    //   alert("hi");
  }

  Programcategory(id) {

    var removedId;

    this.selectedcatagorys.forEach((existingId) => {

      if (existingId == id) {
        removedId = existingId;
        //  this.selectedcapabilities.pop(id);
      }

    });
    if (removedId != null) {
      var ob = this.selectedcatagorys.indexOf(removedId)
      this.selectedcatagorys.splice(ob, 1);
    }

    if (removedId != id) {
      this.selectedcatagorys.push(id);
    }


  }
  onCreateprogramstructure() {



    //('#ProgramStrCapabilities').({

    //  includeSelectAllOption: true
    //});
    let files = this.ele.nativeElement.querySelector('#ProgramStrCoverPicture').files;
    if (files.length > 0 && files.count != 0 && files != null) {

      let formData = new FormData();
      let file = files[0];
      this.coverpic = file.name;
      formData.append('ProgramStrCoverPicture', file, file.name);
      console.log(formData);
      console.log(files);
      this.adminservice.onfileupload(formData).subscribe(res => this.dataLoaded = res);
    }
    let date = new Date();

    const programStructure = {
      ProgramStrName: this.form.controls["ProgramStrName"].value,
      //NoOfCapabilities: this.form.controls["NoOfCapabilities"].value,
      //NoOfModules: this.form.controls["NoOfModules"].value,
      ProgramStrType: this.form.controls["ProgramStrType"].value,
      ProgramStrDependency: this.form.controls["ProgramStrDependency"].value,
      // ProgramStrTitle: (<HTMLInputElement>document.getElementById("ProgramStrTitle")).value,
      //  ProgramStrTitle: this.form.controls["ProgramStrTitle"].value,
      // ProgramCategory: this.form.controls["ProgramCategory"].value,
      ProgramStrCoverPicture: "Tcsimages/programstrimages/" + this.coverpic,
      ProgramCategory: this.selectedcatagorys,
      ProgramStrCapabilities: this.selectedcapabilities,
      ProgramStrModules: this.selectedmodules,
      ProgramStrExercise: this.selectedexercise,
      ProgramStrDescription: this.form.controls["ProgramStrDescription"].value,
      ProgramStrAccessType: this.form.controls["ProgramStrAccessType"].value,
      ProgramStrStatus: this.form.controls["ProgramStrStatus"].value,
      ProgramStrDiscountPrice: this.form.controls["ProgramStrDiscountPrice"].value,
      ProgramStrListedPrice: this.form.controls["ProgramStrListedPrice"].value,
      ProgramStrIsFree: this.form.controls["ProgramStrIsFree"].value,
      ProgramStrCreatedBy: this.createdby,
      ProgramStrCreatedDate: this.datepipe.transform(date, "yyyy-MM-dd")
    }
    // alert(JSON.stringify(programStructure));
    if (this.form.valid) {
      this.isSubmitted = false;
      //alert(JSON.stringify(programStructure));

      this.adminservice.onCreateProgramStructure(programStructure).subscribe(data => {
        console.log(data);
        if (data.StatusCode == "1") {
          this.success = true;
          this.message = data.Message;
          this._success.next(`Record Saved Successfully`);

          this.onGetAllProgramStructureList(this.accountId);
          this.closeCpopup();

        }
        else {
          this.success = false;
          this.message = "Failed to create account master";
        }
      });
      //  alert("Successfully Submitted");

    }

    else {
      this.isSubmitted = true;
    }


  }


  capabilitiestest() {
    this.secret = true;
    var checkboxes = document.getElementById("ProgramStrCapabilities");
    //  alert(this.expanded);
    if (!this.expanded) {
      checkboxes.style.display = "block";
      this.expanded = true;
    } else {
      checkboxes.style.display = "none";
      this.expanded = false;
    }


    //   alert("hi");
  }

  capabilities(id) {

    var removedId;

    this.selectedcapabilities.forEach((existingId) => {

      if (existingId == id) {
        removedId = existingId;
        //  this.selectedcapabilities.pop(id);
      }

    });
    if (removedId != null) {
      var ob = this.selectedcapabilities.indexOf(removedId)
      this.selectedcapabilities.splice(ob, 1);
    }

    if (removedId != id) {
      this.selectedcapabilities.push(id);
    }


    //this.selectedcapabilities.push(id);
    //console.log(this.selectedcapabilities);
    //alert(this.selectedcapabilities);

  }





  modulestest() {
    this.secretmodule = true;
    var checkboxes = document.getElementById("ProgramStrModules");
    //  alert(this.expanded);
    if (!this.expanded) {
      checkboxes.style.display = "block";
      this.expanded = true;
    } else {
      checkboxes.style.display = "none";
      this.expanded = false;
    }


    //   alert("hi");
  }



  modules(id) {


    var remId;

    this.selectedmodules.forEach((existingId) => {

      if (existingId == id) {
        remId = existingId;
        //  this.selectedcapabilities.pop(id);
      }

    });

    if (remId != null) {
      var ob = this.selectedmodules.indexOf(remId)
      this.selectedmodules.splice(ob, 1);
    }

    if (remId != id) {
      this.selectedmodules.push(id);
    }

    //this.selectedmodules.push(id);
    //console.log(this.selectedmodules);


  }

  exercisetest() {
    this.secretexercise = true;
    var checkboxes = document.getElementById("ProgramStrExercise");
    //  alert(this.expanded);
    if (!this.expanded) {
      checkboxes.style.display = "block";
      this.expanded = true;
    } else {
      checkboxes.style.display = "none";
      this.expanded = false;
    }


    //   alert("hi");
  }

  exercise(id) {


    var removeId;
    this.selectedexercise.forEach((existingId) => {

      if (existingId == id) {
        removeId = existingId;
      }
    });
    if (removeId != null) {
      var ob = this.selectedexercise.indexOf(removeId)
      this.selectedexercise.splice(ob, 1);
    }

    if (removeId != id) {
      this.selectedexercise.push(id);
    }


    //this.selectedexercise.push(id);
    //console.log(this.selectedmodules);


  }

  ProgramStructureform() {
    this.form = this.fb.group({
      'ProgramStrName': ['', [Validators.required]],
      // 'NoOfCapabilities': ['', [Validators.required]],
      // 'NoOfModules': ['', [Validators.required]],
      'ProgramCategory': [''],
      'ProgramStrDescription': ['', [Validators.required]],
      'ProgramStrType': ['', [Validators.required]],
      'ProgramStrDependency': ['', [Validators.required]],
      // 'ProgramStrTitle': ['', [Validators.required]],
      'ProgramStrCapabilities': [''],
      'ProgramStrModules': [''],
      'ProgramStrExercise': [''],
      'ProgramStrAccessType': ['', [Validators.required]],
      'ProgramStrStatus': ['', [Validators.required]],
      // 'ProgramCategory': ['', [Validators.required]]
      'ProgramStrListedPrice': ['', [Validators.required]],
      'ProgramStrDiscountPrice': ['', [Validators.required]],
      'ProgramStrIsFree': ['']
    });
  }






  /*Smart table*/
  settings = {

    columns: {
      ProgramStrName: {
        title: 'ProgramStr Name',
        filter: false
      },
      ProgramStrStatus: {
        title: 'ProgramStr Status',
        filter: false
      },

    },
    attr: { class: 'table table-bordered' },
    actions: {
      edit: false, //as an example
      delete: false, //as an example
      add: false,
      position: 'right',
      custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button" (click)="onEdit(rec.Id)">Edit</button></span>` }]
    },
    pager: { display: true, perPage: 50 },
  };

  //route(event) {
  //  alert('hi');
  //}

  onSearch(query: string = '') {
    if (query != '') {
      this.source.setFilter([
        // fields we want to include in the search
        {
          field: 'ProgramStrName',
          search: query
        },
        {
          field: 'ProgramStrStatus',
          search: query
        }
      ], false);
    }
    else {
      this.source = new LocalDataSource(this.listofprogramstructure);
    }

    // second parameter specifying whether to perform 'AND' or 'OR' search 
    // (meaning all columns should contain search query or at least one)
    // 'AND' by default, so changing to 'OR' by setting false here
  }

  onSelectedFilter(event) {
    this.page = event.target.value;
    this.source = new LocalDataSource(this.listofprogramstructure);
    this.settings = {

      columns: {
        ProgramStrName: {
          title: 'ProgramStr Name',
          filter: false
        },
        ProgramStrStatus: {
          title: 'ProgramStr Status',
          filter: false
        },

      },
      attr: { class: 'table table-bordered' },
      actions: {
        edit: false, //as an example
        delete: false, //as an example
        add: false,
        position: 'right',
        custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button">Edit</button></span>` }]
      },
      pager: { display: true, perPage: this.page },
    };

  }

  onExport() {
    this.excelService.exportAsExcelFile(this.listofprogramstructure, 'ProgramStructure');
  }







}
