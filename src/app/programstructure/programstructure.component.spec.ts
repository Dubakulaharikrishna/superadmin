import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgramstructureComponent } from './programstructure.component';

describe('ProgramstructureComponent', () => {
  let component: ProgramstructureComponent;
  let fixture: ComponentFixture<ProgramstructureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgramstructureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgramstructureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
