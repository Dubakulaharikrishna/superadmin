import { Routes } from '@angular/router';

import { ViewcoachComponent } from './viewcoach.component';

export const ViewcoachRoutes: Routes = [

  {
    path: '',
    component: ViewcoachComponent,
    data: {
      heading: 'Viewcoach works!'
    }    
  },

  
];

