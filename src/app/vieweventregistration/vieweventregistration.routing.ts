import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';

import { VieweventregistrationComponent } from './vieweventregistration.component';

export const VieweventregistrationRoutes: Routes = [

  {
    path: '',
    component: VieweventregistrationComponent,
    canActivate: [AuthGuard],
    data: {
      heading: 'Viewaccount works!'
    }    
  },

  
];

