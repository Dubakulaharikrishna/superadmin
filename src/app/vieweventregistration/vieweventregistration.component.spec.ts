import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VieweventregistrationComponent } from './vieweventregistration.component';

describe('VieweventregistrationComponent', () => {
  let component: VieweventregistrationComponent;
  let fixture: ComponentFixture<VieweventregistrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VieweventregistrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VieweventregistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
