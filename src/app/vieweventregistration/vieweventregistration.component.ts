
import { Component, OnInit, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';

import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { Router, ActivatedRoute } from '@angular/router';
import { AdminService } from '../services/admin.service';

import { retry } from 'rxjs/operator/retry';

import { Status } from '../Models/Status';
import { Eventtype } from '../Models/Eventtype';
import { Eventmaster } from '../Models/Eventmaster';
import { programcategory } from '../Models/programcategory';
import { DatePipe } from '@angular/common';
import { EventRegistration } from '../Models/EventRegistration';
import { Members } from '../Models/Members';
@Component({
  selector: 'app-vieweventregistration',
  templateUrl: './vieweventregistration.component.html',
  styleUrls: ['./vieweventregistration.component.css'],
  providers: [DatePipe]
})
export class VieweventregistrationComponent implements OnInit {
  evepId: any;
  endDate: any;
  startTime: any;
  endTime: any;
  startDate: any;
  status: Status;
  listofMembers: Members[];
  userName: any;
  eventregistration: EventRegistration;
  constructor(private adminservice: AdminService, public datepipe: DatePipe, public ele: ElementRef, private router: Router, public route: ActivatedRoute, private http: HttpClient, public fb: FormBuilder) {
    this.eventregistration = new EventRegistration();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.evepId = params['evepId'];

    });

    this.onGetEventmasterById();
  }



  onGetEventmasterById() {
    //alert(id);
    let id = localStorage.getItem('AccountId');
    this.adminservice.onGetAllMembersList(id).subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofMembers = this.status.Data;
        console.log(this.listofMembers);
        console.log(data);
        this.adminservice.onGetEventRegistrationsById(this.evepId).subscribe(result => {

          this.status = result;
          if (this.status.StatusCode == "1") {


            this.eventregistration = this.status.Data;
            // alert(JSON.stringify(this.eventmaster));
            this.userName = this.listofMembers.filter(m => m.Id == this.eventregistration.Member_Id)[0].UserName;

          }
          else {
            alert(this.status.Data);
          }
          console.log(this.eventregistration);


          if (this.eventregistration.EventStartDate != null && this.eventregistration.EventStartDate != "" && this.eventregistration.EventStartDate != undefined) {
            this.startDate = this.datepipe.transform(this.eventregistration.EventStartDate, 'yyyy-MM-dd');
            this.eventregistration.EventStartDate = this.startDate;
          }



          if (this.eventregistration.EventEndDate != null && this.eventregistration.EventEndDate != "" && this.eventregistration.EventEndDate != undefined) {
            this.endDate = this.datepipe.transform(this.eventregistration.EventEndDate, 'yyyy-MM-dd');
            this.eventregistration.EventEndDate = this.endDate;
          }

          if (this.eventregistration.EventStartTime != null && this.eventregistration.EventStartTime != "" && this.eventregistration.EventStartTime != undefined) {
            this.startTime = this.datepipe.transform(this.eventregistration.EventStartTime, 'yyyy-MM-dd');
            this.eventregistration.EventStartTime = this.startDate;
          }
          if (this.eventregistration.EventEndTime != null && this.eventregistration.EventEndTime != "" && this.eventregistration.EventEndTime != undefined) {
            this.endTime = this.datepipe.transform(this.eventregistration.EventEndTime, 'yyyy-MM-dd');
            this.eventregistration.EventEndTime = this.startDate;
          }





        });

        return this.eventregistration;
      }
    });
  }
}
