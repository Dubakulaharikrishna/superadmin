import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewlicenseinfoComponent } from './viewlicenseinfo.component';

describe('ViewlicenseinfoComponent', () => {
  let component: ViewlicenseinfoComponent;
  let fixture: ComponentFixture<ViewlicenseinfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewlicenseinfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewlicenseinfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
