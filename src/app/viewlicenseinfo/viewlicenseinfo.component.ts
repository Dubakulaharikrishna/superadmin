
import { AdminService } from '../services/admin.service';
import { CustomValidators } from 'ng2-validation';
import { DatePipe } from '@angular/common'
import 'rxjs/add/operator/debounceTime';
import { Subject } from 'rxjs/Subject';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/share';
import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl, } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { CustomFormsModule } from 'ng2-validation';
import { BrowserModule } from '@angular/platform-browser';
import { License } from '../Models/license';
import { retry } from 'rxjs/Operator/retry';
import 'rxjs/Rx';
import { Status } from '../Models/Status';




@Component({
  selector: 'app-viewlicenseinfo',
  templateUrl: './viewlicenseinfo.component.html',
  styleUrls: ['./viewlicenseinfo.component.css']
})
export class ViewlicenseinfoComponent implements OnInit {

  public viewlicenseinfoFormGroup: FormGroup;
  //public form: FormGroup;
  license: License;
  id: any;
  isSubmitted: boolean = false;
  licId: any;
  status: Status;

  constructor(private fb: FormBuilder, private adminservice: AdminService, private router: Router, public route: ActivatedRoute, private http: HttpClient) {


    this.license = new License();


  }

  ngOnInit() {
    this.Licenseform();
    this.route.params.subscribe(params => {
      this.licId = params['licId'];
    //  alert(this.licId);
    });

    this.onGetLicenseById();

  }
  Licenseform() {
    this.viewlicenseinfoFormGroup = this.fb.group({
      'license.CompanyName': ['', [Validators.required]],
      //  'license.LegalName': ['', [Validators.required]],

      'license.RegistrationNumber': ['', [Validators.required]],

      //'license.CompanyType': ['', [Validators.required]],
      // 'license.AddressLine1': ['', [Validators.required]],
      //'license.AddressLine2': ['', [Validators.required]],
      //'license.City': ['', [Validators.required]],
      //'license.State': ['', [Validators.required]],
      //'license.ZipCode': ['', [Validators.required]],
      //'license.Country': ['', [Validators.required]],
      //'license.Industry': ['', [Validators.required]],
      // 'license.AreaOfBusiness': ['', [Validators.required]],
      // 'license.EstablishedDate': ['', [Validators.required]],
      // 'license.OperatingCountries': ['', [Validators.required]],
      // 'license.CompanyWebsite': ['', [Validators.required]],
      // 'license.CompanySocialUrls': ['', [Validators.required]],
      'license.PrimaryAdminEmailID': ['', [Validators.required]],
      // 'license.PrimaryAdminPassword': ['', [Validators.required]],
      //'license.PrimaryAdminPhone': ['', [Validators.required]],
      //// RegistrationNumber': ['', [Validators.required]],

      // 'license.CompanyEmailID': ['', [Validators.required]],
      'license.CompanyPhone': ['', [Validators.required]],
      //'license.PublicIPAddress': ['', [Validators.required]],
      //'license.ClientAgentDetails': ['', [Validators.required]],
      // 'license.CreatedDate': ['', [Validators.required]],
      // 'license.CreatedBy': ['', [Validators.required]],
      //'license.UpdatedDate': ['', [Validators.required]],
      //'license.UpdatedBy': ['', [Validators.required]],
      //'license.RequestStatus': ['', [Validators.required]],
      'license.LicenseStatus': ['', [Validators.required]],
      //'license.Remarks': ['', [Validators.required]],
      'license.LicenseStartDate': ['', [Validators.required]],
      'license.LicenseEndDate': ['', [Validators.required]],
      //'license.LicenseKey': ['', [Validators.required]],
      // 'license.DecodeScript': ['', [Validators.required]],
      // 'license.LicenseGeneratedDate': ['', [Validators.required]],

      // 'license.LicenseGeneratedBy': ['', [Validators.required]],
      'license.LicenseType': ['', [Validators.required]],
      // 'license.RecentSecurityToken': ['', [Validators.required]],
      // 'license.RecentTokenReqDate': ['', [Validators.required]]


    });
  }



  //onGenerateLicenses(license) {
  //  // alert(JSON.stringify(license));
  //  if (this.viewlicenseinfoFormGroup.valid) {
  //    //  alert(JSON.stringify(accountmanagement));
  //    this.isSubmitted = false;
  //    // alert(JSON.stringify(license));
  //    //const lic = {
  //    //  // AccountID: this.form.get('AccountID').value,
  //    //  AccountName: this.accountUpdateFormGroup.get('AccountName').value,
  //    //  AccountWebUrl: this.accountUpdateFormGroup.get('AccountWebUrl').value,
  //    //  IsMobileEnableOrNot: this.accountUpdateFormGroup.get('IsMobileEnableOrNot').value,
  //    //  IsAndroidAppPublic: this.accountUpdateFormGroup.get('IsAndroidAppPublic').value,
  //    //  IsIOSAppPublic: this.accountUpdateFormGroup.get('IsIOSAppPublic').value,
  //    //  AccountAdmin: this.accountUpdateFormGroup.get('AccountAdmin').value,
  //    //  AccountCreatedDate: this.accountUpdateFormGroup.get('AccountCreatedDate').value,
  //    //  IsHttpsCertificateEnabled: this.accountUpdateFormGroup.get('IsHttpsCertificateEnabled').value,
  //    //  DevUrl: this.accountUpdateFormGroup.get('DevUrl').value,
  //    //  IsLiveStatus: this.accountUpdateFormGroup.get('IsLiveStatus').value,
  //    //  ClientInfo: this.accountUpdateFormGroup.get('ClientInfo').value,
  //    //  InstanceInfo: this.accountUpdateFormGroup.get('InstanceInfo').value,
  //    //  LicenseInfo: this.accountUpdateFormGroup.get('LicenseInfo').value,
  //    //  AppUrl: this.accountUpdateFormGroup.get('AppUrl').value,


  //    //}
  //    alert(JSON.stringify(license));
  //    this.adminservice.onGenerateLicense(license).subscribe(res => {
  //      console.log(res);
  //      this.license = res
  //    });

  //  }
  //  else {
  //    this.isSubmitted = true;
  //    //alert(JSON.stringify(license));
  //  }
  //  //alert(JSON.stringify(account));
  //}


  onGetLicenseById() {
    this.adminservice.onGetLicenseById(this.licId).subscribe(result => {
      this.status = result;
      if (result.success == 1) {


        this.license = result.data;
      }
      else {
        alert(result.data);
      }

      console.log(this.license);
    });
    return this.license;
  }
}




