import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';

import { ViewlicenseinfoComponent } from './viewlicenseinfo.component';

export const ViewlicenseinfoRoutes: Routes = [

  {
    path: '',
    component: ViewlicenseinfoComponent,
    canActivate: [AuthGuard],
    data: {
      heading: 'Viewevent works!'
    }
  },


];
