import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';
//import { Programmanagement } from '../programmanagement/programmanagement.module';
import { CommonModule } from '@angular/common';
//import { ProgramManagementModule } from './programmanagement.module';
import { DatePipe } from '@angular/common'

import { AdminService } from '../services/admin.service';
import { CustomValidators } from 'ng2-validation';
import 'rxjs/add/operator/debounceTime';
import { Subject } from 'rxjs/Subject';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { retry } from 'rxjs/Operator/retry';
import { Forum } from '../Models/Forum';
import { Status } from '../Models/Status';
import { AccountManagement } from '../Models/AccountManagement';




@Component({
  selector: 'app-viewforum',
  templateUrl: './viewforum.component.html',
  styleUrls: ['./viewforum.component.css'],
  providers: [DatePipe]
})
export class ViewForumComponent implements OnInit {
  forumicon: any;

  listofAccounts: AccountManagement[];
  public ForumUpdateFormGroup: FormGroup;
  public form: FormGroup;
  forum: Forum;
  id: any;
  dataLoaded: any;
  media: any;
  isSubmitted: boolean = false;
  frmId: any;
  createdby: any;
  status: Status;
  constructor(private adminservice: AdminService, public datepipe: DatePipe, public ele: ElementRef, private router: Router, public route: ActivatedRoute, private http: HttpClient, public fb: FormBuilder) {
    this.forum = new Forum();
  }

  ngOnInit() {
    this.ViewForumform();
    this.route.params.subscribe(params => {
      this.frmId = params['frmId'];

    });
    this.onGetForumById();
    this.onGetAllAccountsList();
    this.createdby = localStorage.getItem('MemberName');

  }
  test(id) {
    //  alert("Hi");
    //console.log(id);
    //alert(id);
    console.log(this.forum.MasterAccount_Id);
    alert(this.forum.MasterAccount_Id);
  }
  onGetAllAccountsList() {
    this.adminservice.onGetAllAccountsList().subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofAccounts = this.status.Data;

        //alert(JSON.stringify(this.listofAccounts));
        return this.listofAccounts
      }

    });
  }
  onUpdateForum(forum) {

    let files = this.ele.nativeElement.querySelector('#ForumIcon').files;
    if (files.length > 0 && files.count != 0 && files != null) {

      let formData = new FormData();
      let file = files[0];
      this.forumicon = file.name;
      formData.append('ForumIcon', file, file.name);
      console.log(formData);
      console.log(files);
      this.adminservice.onfileupload(formData).subscribe(res => this.dataLoaded = res);
    }


    let date = new Date();

    const Forum = {

      Id: this.forum.Id,
      //MasterAccount_Id: this.form.controls["MasterAccount_Id"].value,
      MasterAccount_Id: this.ForumUpdateFormGroup.controls["forum.MasterAccount_Id"].value,
      ForumName: this.ForumUpdateFormGroup.controls["forum.ForumName"].value,
      ForumIsModerated: this.ForumUpdateFormGroup.controls["forum.ForumIsModerated"].value,
      ForumIsDeleted: this.ForumUpdateFormGroup.controls["forum.ForumIsDeleted"].value,
      ForumIcon: "Tcsimages/prgcoverimages/" + this.forumicon,
      ForumStatus: this.ForumUpdateFormGroup.controls["forum.ForumStatus"].value,
      ForumCreatedBy: this.forum.ForumCreatedBy,
      ForumCreatedDate: this.forum.ForumCreatedDate,
      ForumUpdatedBy: this.createdby,
      ForumUpdatedDate: this.datepipe.transform(date, "yyyy-MM-dd")
    }

    if (this.ForumUpdateFormGroup.valid) {
      this.isSubmitted = false;
    //  alert(JSON.stringify(Forum));
      this.adminservice.onUpdateForum(Forum).subscribe(data => {

      });
      alert("Successfully Updated");
      this.router.navigate(['/forum/']);
    }

    else {
      this.isSubmitted = true;
    }

  }

  ViewForumform() {
    this.ForumUpdateFormGroup = this.fb.group({
     
     
      'forum.MasterAccount_Id': ['', [Validators.required]],

      'forum.ForumName': ['', [Validators.required]],
      'forum.ForumIsModerated': ['', [Validators.required]],
      'forum.ForumIsDeleted': ['', [Validators.required]],
      'forum.ForumStatus': ['', [Validators.required]]
    });
  }
  

  onGetForumById() {
    //alert(id);
    this.adminservice.onGetForumById(this.frmId).subscribe(result => {
      this.status = result;
      if (this.status.StatusCode == "1") {


        this.forum = this.status.Data;
      }
      else {
        alert(this.status.Data);
      }
      console.log(this.forum);
    });
    return this.forum;
  }

}

