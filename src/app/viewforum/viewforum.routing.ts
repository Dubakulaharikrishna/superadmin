import { Routes } from '@angular/router';

import { ViewForumComponent } from '../viewforum/viewforum.component';
import { NotAuthGuard } from '../guards/notAuth.guard';
import { AuthGuard } from '../guards/auth.guard';


export const ViewForumRoutes: Routes = [

  {
    path: '',
    component: ViewForumComponent,
    canActivate: [AuthGuard],
    data: {
      heading: 'ViewForum'
    }
  },


];
