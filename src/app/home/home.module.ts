//import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//import { DashboardComponent } from './dashboard.component';
import { RouterModule } from '@angular/router';
import { HomeRoutes } from './home.routing';
import { HomeComponent } from '../home/home.component';


@NgModule({
  imports: [
    RouterModule.forChild(HomeRoutes),
    //BrowserModule
  ],
  declarations: [
   
    HomeComponent
    
  ],
 
  providers: [],
  bootstrap: [HomeComponent]
})
export class HomeModule { }
