import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';
import { ContentcategoryComponent } from './contentcategory.component';

export const ContentcategoryRoutes: Routes = [

  {
    path: '',
    component: ContentcategoryComponent,
    canActivate: [AuthGuard],

    data: {
      heading: 'Coachmanagement'
    }    
  },

  
];

