import { Subject } from 'rxjs/Subject';
import { Component, OnInit } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { Router } from '@angular/router';
//changes
import { DatePipe } from '@angular/common'
import { ExcelService } from '../services/excel.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';
import { contentcategory } from '../Models/contentcategory';
import { AdminService } from '../services/admin.service';
import { Form } from '@angular/forms/src/directives/form_interface';
import { Status } from '../Models/Status';
import { Ng2SmartTableModule, LocalDataSource } from 'ng2-smart-table';

@Component({
  selector: 'app-contentcategory',
  templateUrl: './contentcategory.component.html',
  styleUrls: ['./contentcategory.component.css'],
  providers: [ExcelService, DatePipe]
})
export class ContentcategoryComponent implements OnInit {
  source: LocalDataSource;
  selectedValue: any = 50;
  page: any;
  public isSubmitted: boolean = false;
  result: any[];
  title = 'Simple Datatable Example using Angular 4';
  public Createform: FormGroup;
  public form: FormGroup;
  contentcategory: contentcategory;
  pagename: any;
  id: any;
  Id: any;
  status: Status;
  success: boolean = false;
  message: any;
  dtTrigger: Subject<any> = new Subject();
  public _success = new Subject<string>();
  staticAlertClosed = false;
  listofcontentcategory: contentcategory[];
  createdby: any;
  constructor(private adminservice: AdminService, private excelService: ExcelService, public datepipe: DatePipe, private router: Router, private http: HttpClient, public fb: FormBuilder) {
    this.contentcategory = new contentcategory();
  }
  ngOnInit() {

    //sessionStorage.clear();
   // localStorage.removeItem('loginSessId');
   // localStorage.clear();
   // localStorage.setTimeout = 1;
   // sessionStorage.removeItem('loginSessId');
   // localStorage.removeItem('loginSessId');
    this.pagename = "Content category";
    localStorage.setItem('loginSessId', this.pagename);

    setTimeout(() => this.staticAlertClosed = true, 20000);
    this._success.subscribe((message) => this.message = message);
    this._success.debounceTime(7000).subscribe(() => this.message = null);
    this.createdby = localStorage.getItem('MemberName');



    this.ContentCategoryform();
    this.onGetAllContentCategory();
  
  }

  openCpopup() {

    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "block";
    cmask.style.display = "block";
  }



  closeCpopup() {
    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "none";
    cmask.style.display = "none";
  }

  onEdit(event: any) {
    this.router.navigate(['/viewcontentcategory/', { ccId: event.data.Id  }]);
    // alert(id);
    //alert(this.accountmanagement);
  }

  onGetAllContentCategory() {
    this.adminservice.onGetAllContentCategory().subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofcontentcategory = this.status.Data;
        this.source = new LocalDataSource(this.listofcontentcategory);
        // alert(JSON.stringify(this.Programstructures));
        return this.listofcontentcategory;
      }
     
    });
  }

  //onGetAccountsById() {
  //  this.adminservice.onGetAccountsById(this.id)
  //    .subscribe(result => {
  //      this.accountmanagement = result;
  //      console.log(this.accountmanagement);
  //    });
  //  return this.accountmanagement;
  //}




  onCreateContentCategory() {
    let date = new Date();

    if (this.form.valid) {
      this.isSubmitted = false;
      // alert(JSON.stringify(license));
      const lic = {
        // AccountID: this.form.get('AccountID').value,
        ContentCategoryName: this.form.get('ContentCategoryName').value,
        ContentCategoryType: this.form.get('ContentCategoryType').value,
        ContentCategoryStartedDate: this.form.get('ContentCategoryStartedDate').value,
        ContentCategoryValidTill: this.form.get('ContentCategoryValidTill').value,
        ContentCategoryStatus: this.form.get('ContentCategoryStatus').value,
        ContentCategoryCreatedBy: this.createdby,
        ContentCategoryCreatedDate: this.datepipe.transform(date, "yyyy-MM-dd")

      }
    //  alert(JSON.stringify(lic));
      this.adminservice.onCreateContentCategory(lic).subscribe(data => {
        console.log(data);
        if (data.StatusCode == "1") {
          this.success = true;
          this.message = data.Message;
          this._success.next(`Record Saved Successfully`);

          this.onGetAllContentCategory();

          this.closeCpopup();

        }
        else {
          this.success = false;
          this.message = "Failed to create account master";
        }
      });
     // alert("Successfully Submitted");
    }


    else {
      this.isSubmitted = true;
      //alert(JSON.stringify(license));
    }
    //alert(JSON.stringify(account));
  }


  ContentCategoryform() {
    this.form = this.fb.group({
      // AccountID: ['', [Validators.required]],
      ContentCategoryName: ['', [Validators.required]],
      ContentCategoryType: ['', [Validators.required]],
      ContentCategoryStartedDate: ['', [Validators.required]],
      ContentCategoryValidTill: ['', [Validators.required]],
    //  ContentCategoryCreatedDate: ['', [Validators.required]],
    //  ContentCategoryCreatedBy: ['', [Validators.required]],
     // ContentCategoryUpdatedDate: ['', [Validators.required]],
    //  ContentCategoryUpdatedBy: ['', [Validators.required]],
      ContentCategoryStatus: ['', [Validators.required]],
    

    });
  }
  /*Smart table*/
  settings = {

    columns: {
      ContentCategoryName: {
        title: 'Content Category Name',
        filter: false
      },
      ContentCategoryStatus: {
        title: 'Content Category Status',
        filter: false
      },
   
    },
    attr: { class: 'table table-bordered' },
    actions: {
      edit: false, //as an example
      delete: false, //as an example
      add: false,
      position: 'right',
      custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button" (click)="onEdit(rec.Id)">Edit</button></span>` }]
    },
    pager: { display: true, perPage: 50 },
  };

  //route(event) {
  //  alert('hi');
  //}

  onSearch(query: string = '') {
    if (query != '') {
      this.source.setFilter([
        // fields we want to include in the search
        {
          field: 'Content Category Name',
          search: query
        },
        {
          field: 'Content Category Status',
          search: query
        }
      ], false);
    }
    else {
      this.source = new LocalDataSource(this.listofcontentcategory);
    }

    // second parameter specifying whether to perform 'AND' or 'OR' search 
    // (meaning all columns should contain search query or at least one)
    // 'AND' by default, so changing to 'OR' by setting false here
  }

  onSelectedFilter(event) {
    this.page = event.target.value;
    this.source = new LocalDataSource(this.listofcontentcategory);
    this.settings = {

      columns: {
        ContentCategoryName: {
          title: 'Content Category Name',
          filter: false
        },
        ContentCategoryStatus: {
          title: 'Content Category Status',
          filter: false
        },
     
      },
      attr: { class: 'table table-bordered' },
      actions: {
        edit: false, //as an example
        delete: false, //as an example
        add: false,
        position: 'right',
        custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button">Edit</button></span>` }]
      },
      pager: { display: true, perPage: this.page },
    };

  }

  onExport() {
    this.excelService.exportAsExcelFile(this.listofcontentcategory, 'ContentCategory');
  }

}
