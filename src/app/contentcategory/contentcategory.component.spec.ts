import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentcategoryComponent } from './contentcategory.component';

describe('ContentcategoryComponent', () => {
  let component: ContentcategoryComponent;
  let fixture: ComponentFixture<ContentcategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContentcategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentcategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
