//import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//import { DashboardComponent } from './dashboard.component';
import { RouterModule } from '@angular/router';
import { ViewservicepurchasesRoutes } from './viewservicepurchases.routing';
import { ViewservicepurchasesComponent } from '../viewservicepurchases/viewservicepurchases.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { DataTablesModule } from 'angular-datatables';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
//import { BrowserModule } from '@angular/platform-browser';
//import { FileUploadModule } from 'ng2-file-upload/ng2-file-upload';
//changes   
import { NgbProgressbarModule, NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { NgbAccordionModule } from '@ng-bootstrap/ng-bootstrap';
import { CustomFormsModule } from 'ng2-validation';
import { FormsModule, FormBuilder, FormGroup, Validators, FormControl, ReactiveFormsModule } from '@angular/forms';
import { AdminService } from '../services/admin.service';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';


@NgModule({

  imports: [

    DataTablesModule, CommonModule,
    RouterModule,
    HttpClientModule,
    // BrowserModule,
    FormsModule,
    CustomFormsModule,
    NgbModule,
    ReactiveFormsModule,
    RouterModule.forChild(ViewservicepurchasesRoutes),
    HttpModule,
    Ng2SmartTableModule

  ],
  declarations: [

    ViewservicepurchasesComponent

  ],

  providers: [AdminService, AuthGuard, NotAuthGuard],
  bootstrap: [ViewservicepurchasesComponent]
})
export class ViewServicePurchaseModule { }
