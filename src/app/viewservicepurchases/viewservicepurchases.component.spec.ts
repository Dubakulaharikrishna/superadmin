import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewservicepurchasesComponent } from './viewservicepurchases.component';

describe('ViewservicepurchasesComponent', () => {
  let component: ViewservicepurchasesComponent;
  let fixture: ComponentFixture<ViewservicepurchasesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewservicepurchasesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewservicepurchasesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
