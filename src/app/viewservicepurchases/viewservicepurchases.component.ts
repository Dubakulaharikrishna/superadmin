import { Component, OnInit, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';

import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { Router, ActivatedRoute } from '@angular/router';
import { AdminService } from '../services/admin.service';
import { DatePipe } from '@angular/common';
import { retry } from 'rxjs/operator/retry';

import { Status } from '../Models/Status';
import { ServicePurchase } from '../Models/ServicePurchase';
import { Members } from '../Models/Members';


@Component({
  selector: 'app-viewservicepurchases',
  templateUrl: './viewservicepurchases.component.html',
  styleUrls: ['./viewservicepurchases.component.css'],
  providers: [DatePipe]
})
export class ViewservicepurchasesComponent implements OnInit {
  endDate: any;
  startDate: any;
  servicepurchase: ServicePurchase;
  serpId: any;
  renewalDate: any;
  status: Status;
  accessDate: any;
  userName: any;
  listofMembers: Members[];
  constructor(private adminservice: AdminService, public datepipe: DatePipe, public ele: ElementRef, private router: Router, public route: ActivatedRoute, private http: HttpClient, public fb: FormBuilder) {

    //this.servicepurchase = new ServicePurchase();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.serpId = params['serpId'];

    });
    this.onGetServicePurchaseById();



  }


  onGetServicePurchaseById() {
    //alert(id);

    let id = localStorage.getItem('AccountId');
    this.adminservice.onGetAllMembersList(id).subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofMembers = this.status.Data;
        console.log(this.listofMembers);
        console.log(data);
        this.adminservice.onGetServicePurchaseById(this.serpId).subscribe(result => {

          this.status = result;
          if (this.status.StatusCode == "1") {


            this.servicepurchase = this.status.Data;
            // alert(JSON.stringify(this.eventmaster));
            this.userName = this.listofMembers.filter(m => m.Id == this.servicepurchase.Member_Id)[0].UserName;
          }
          else {
            alert(this.status.Data);
          }
          console.log(this.servicepurchase);


          if (this.servicepurchase.ServiceValidStartDate != null && this.servicepurchase.ServiceValidStartDate != "" && this.servicepurchase.ServiceValidStartDate != undefined) {
            this.startDate = this.datepipe.transform(this.servicepurchase.ServiceValidStartDate, 'yyyy-MM-dd');
            this.servicepurchase.ServiceValidStartDate = this.startDate;
          }



          if (this.servicepurchase.ServiceValidTillDate != null && this.servicepurchase.ServiceValidTillDate != "" && this.servicepurchase.ServiceValidTillDate != undefined) {
            this.endDate = this.datepipe.transform(this.servicepurchase.ServiceValidTillDate, 'yyyy-MM-dd');
            this.servicepurchase.ServiceValidTillDate = this.endDate;
          }
          if (this.servicepurchase.NextRenewalDate != null && this.servicepurchase.NextRenewalDate != "" && this.servicepurchase.NextRenewalDate != undefined) {
            this.renewalDate = this.datepipe.transform(this.servicepurchase.NextRenewalDate, 'yyyy-MM-dd');
            this.servicepurchase.NextRenewalDate = this.renewalDate;
          }
          if (this.servicepurchase.ServiceAccessDuration != null && this.servicepurchase.ServiceAccessDuration != "" && this.servicepurchase.ServiceAccessDuration != undefined) {
            this.accessDate = this.datepipe.transform(this.servicepurchase.ServiceAccessDuration, 'yyyy-MM-dd');
            this.servicepurchase.ServiceAccessDuration = this.accessDate;
          }
          //if (this.eventregistration.EventEndTime != null && this.eventregistration.EventEndTime != "" && this.eventregistration.EventEndTime != undefined) {
          //  this.endTime = this.datepipe.transform(this.eventregistration.EventEndTime, 'yyyy-MM-dd');
          //  this.eventregistration.EventEndTime = this.startDate;
          //}





        });

        return this.servicepurchase;
      }
    });

  }
}
