import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';

import { ViewservicepurchasesComponent } from './viewservicepurchases.component';

export const ViewservicepurchasesRoutes: Routes = [

  {
    path: '',
    component: ViewservicepurchasesComponent,
    canActivate: [AuthGuard],
    data: {
      heading: 'Service'
    }
  },


];
