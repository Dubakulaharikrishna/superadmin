import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';

import { InstancemanagerComponent } from './instancemanager.component';

export const InstancemanagerRoutes: Routes = [

  {
    path: '',
    component: InstancemanagerComponent,
    canActivate: [AuthGuard],

    data: {
      heading: 'Login'
    }
  },


];

