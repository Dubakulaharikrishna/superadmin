import { Component, OnInit } from '@angular/core';
//changes
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { Subject } from 'rxjs/Subject';
import { ExcelService } from '../services/excel.service';

import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';
import { InstanceManager } from '../Models/InstanceManager';
import { AdminService } from '../services/admin.service';
import { Status } from '../Models/Status';
import { Ng2SmartTableModule, LocalDataSource } from 'ng2-smart-table';
import { DatePipe } from '@angular/common'
import { License } from '../Models/license';

@Component({
  selector: 'app-instancemanager',
  templateUrl: './instancemanager.component.html',
  styleUrls: ['./instancemanager.component.css'],
  providers: [DatePipe, ExcelService],
   
})
export class InstancemanagerComponent implements OnInit {
  upinstid: any;
  AssociatedLicenseInfo: any;
  source: LocalDataSource;
  selectedValue: any = 50;
  page: any;
  doBCheck: boolean = false;
  SetupDateTime: any;
  isSubmitted: boolean = false;
  public data: Object;
  public temp_var: Object = false;
  Instanceform: FormGroup;
  public form: FormGroup;
  instancemanager: InstanceManager;
  public InstancemanagerForm: FormGroup;
  listofinstances: InstanceManager[];
  listofLicense: License[];
  id: any;
  instid: any;
  success: boolean = false;
  message: any;
  dtTrigger: Subject<any> = new Subject();
  public _success = new Subject<string>();
  staticAlertClosed = false;
  pagename: any;
  status: Status;
  createdby: any;
  count: any;
  constructor(public router: Router, public route: ActivatedRoute, private excelService: ExcelService, public http: HttpClient, public datepipe: DatePipe, public fb: FormBuilder, private adminservice: AdminService) {

    this.instancemanager = new InstanceManager();
  }

  openCpopup() {

    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "block";
    cmask.style.display = "block";
  }

  closeCpopup() {
    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "none";
    cmask.style.display = "none";
    this.form.reset();
  }
  onEdit(event: any) {
    console.log(event)
    this.router.navigate(['/viewportalmanagement/', { instId: event.data._id }]);
    console.log(event)
  }
  onGetAllInstanceManagerList() {
    this.adminservice.onGetAllInstanceManagerList().subscribe(result => {
      if (result.success == 1) {
       // this.status = data;
        this.listofinstances = result.data;
        this.count = this.listofinstances.length;
        this.source = new LocalDataSource(this.listofinstances);
        // alert(JSON.stringify(this.Programstructures));
        return this.listofinstances
      }
     
    });
  }


  onGetAllLicenseList() {
    this.adminservice.onGetAllLicenseList().subscribe(result => {

      if (result.success == 1) {

        this.listofLicense = result.data;
        // alert(JSON.stringify(this.Programstructures));
        return this.listofLicense
      }

    });
  }
  license(id) {
    console.log(this.AssociatedLicenseInfo);
   // alert(this.AssociatedLicenseInfo);
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.success = true;
     
      this.upinstid = params['upinstid'];
      setTimeout(() => this.staticAlertClosed = true, 20000);
      this._success.subscribe((message) => this.message = message);
      this._success.debounceTime(7000).subscribe(() => this.message = null);

    });
    //sessionStorage.clear();
 //   localStorage.removeItem('loginSessId');
   // localStorage.clear();
  //  localStorage.setTimeout = 1;
  //  localStorage.removeItem('loginSessId');
   // localStorage.removeItem('loginSessId');
    this.pagename = "Instance manager";
    localStorage.setItem('loginSessId', this.pagename);
     setTimeout(() => this.staticAlertClosed = true, 20000);
    this._success.subscribe((message) => this.message = message);
    this._success.debounceTime(7000).subscribe(() => this.message = null);
    this.createdby = localStorage.getItem('MemberName');
    this.Instancemanagerform();
    this.onGetAllInstanceManagerList();
    this.onGetAllLicenseList();
    this.instid = localStorage.getItem('InstanceId');
   
    //void {
    //  this: this.http.get('https://jsonplaceholder.typicode.com/posts').subscribe((res: Response) => {
    //    this.data = res;
    //    this.temp_var = true;
    //  })

    //}

  }
  onCreateInstance() {
    let date = new Date();

    if (this.form.valid) {
      this.isSubmitted = false;
      // alert(JSON.stringify(license));
      const inst = {
        InstanceManagerId: this.instid,
        InstanceName: this.form.get('InstanceName').value,
        ServerName: this.form.get('ServerName').value,
        OSDetails: this.form.get('OSDetails').value,
        InstanceAdmin: this.form.get('InstanceAdmin').value,
        SetupDateTime: this.form.get('SetupDateTime').value,
       // AssociatedLicenseInfo: this.form.get('AssociatedLicenseInfo').value,
       // LicenseExpiryDate: this.form.get('LicenseExpiryDate').value,
       // LicenseType: this.form.get('LicenseType').value,
        NumberofClients: this.form.get('NumberofClients').value,
        NumberOfAccounts: this.form.get('NumberOfAccounts').value,
        InstancePublicIPAddress: this.form.get('InstancePublicIPAddress').value,
        InstancePrivateIPAddress: this.form.get('InstancePrivateIPAddress').value,
        CreatedBy: this.createdby,
       // CreatedDate: this.datepipe.transform(date, "yyyy-MM-dd"),
      }
     // alert(JSON.stringify(lic));
      this.adminservice.onCreateInstanceManager(inst).subscribe(result => {
        console.log(result);
        if (result.success == 1) {
          this.success = true;
          this.message = result.Message;
          this._success.next(`Record Saved Successfully`);

          this.onGetAllInstanceManagerList();
          this.closeCpopup();
          this.form.reset();

        }

     else if (result.success == 2) {
          this.success = true;
          this.message = result.Message;
          this._success.next(`This Details is already Exists`);

          this.onGetAllInstanceManagerList();
          this.closeCpopup();

        }


        else {
          this.success = false;
          this.message = "Failed to create account master";
        }
      });
    }
    else {
      this.isSubmitted = true;
      //alert(JSON.stringify(license));
    }

    // alert(JSON.stringify(lic));
  }
  //onUpdateInstanceManager(instancemanager) {
  //  this.adminservice.onUpdateInstanceManager(instancemanager).subscribe(res => this.instancemanager = res);

  //}


  Instancemanagerform() {
    this.form = this.fb.group({
      InstanceName: ['', [Validators.required]],
      ServerName: ['', [Validators.required]],
      OSDetails: ['', [Validators.required]],
      InstanceAdmin: ['', [Validators.required]],
      SetupDateTime: ['', [Validators.required]],
    //  AssociatedLicenseInfo: ['', [Validators.required]],
     // LicenseExpiryDate: ['', [Validators.required]],
    //  LicenseType: ['', [Validators.required]],
      NumberofClients: ['', [Validators.required]],
      NumberOfAccounts: ['', [Validators.required]],
      InstancePublicIPAddress: ['', [Validators.required]],
      InstancePrivateIPAddress: ['', [Validators.required]],

    });
  }

  onCheckDOB() {
    let date = new Date();
    let date1 = new Date(date),
      mnth = ("0" + (date.getMonth() + 1)).slice(-2),
      day = ("0" + date.getDate()).slice(-2);
    let todaydate = [date.getFullYear(), mnth, day].join("-");
    if (this.SetupDateTime <= todaydate) {
      this.doBCheck = false;
    }
    else {
      this.doBCheck = true;
    }
  }

  /*Smart table*/
  settings = {

    columns: {
      InstanceName: {
        title: 'Instance Name',
        filter: false
      },
      ServerName: {
        title: 'Server Name',
        filter: false
      },
      InstanceAdmin: {
        title: 'Instance Admin',
        filter: false
      },
      //LicenseExpiryDate: {
      //  title: 'License ExpiryDate',
      //  filter: false,
        //valuePrepareFunction: (LicenseExpiryDate) => {
        //  var raw = new Date(LicenseExpiryDate);
        //  var formatted = this.datepipe.transform(raw, 'yyyy-MM-dd');
        //  return formatted;
        //}
     // },
    },
    attr: { class: 'table table-bordered' },
    actions: {
      edit: false, //as an example
      delete: false, //as an example
      add: false,
      position: 'right',
      custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button" (click)="onEdit(rec._id)">Edit</button></span>` }]
    },
    pager: { display: true, perPage: 50 },
  };

  //route(event) {
  //  alert('hi');
  //}

  onSearch(query: string = '') {
    if (query != '') {
      this.source.setFilter([
        // fields we want to include in the search
        {
          field: 'InstanceName',
          search: query
        },
        {
          field: 'InstanceAdmin',
          search: query
        }
      ], false);
    }
    else {
      this.source = new LocalDataSource(this.listofinstances);
    }

    // second parameter specifying whether to perform 'AND' or 'OR' search 
    // (meaning all columns should contain search query or at least one)
    // 'AND' by default, so changing to 'OR' by setting false here
  }

  onSelectedFilter(event) {
    this.page = event.target.value;
    this.source = new LocalDataSource(this.listofinstances);
    this.settings = {

      columns: {
        InstanceName: {
          title: 'Instance Name',
          filter: false
        },
        ServerName: {
          title: 'Server Name',
          filter: false
        },
        InstanceAdmin: {
          title: 'Instance Admin',
          filter: false
        },
        //LicenseExpiryDate: {
        //  title: 'License ExpiryDate',
        //  filter: false,
        //  valuePrepareFunction: (LicenseExpiryDate) => {
        //    var raw = new Date(LicenseExpiryDate);
        //    var formatted = this.datepipe.transform(raw, 'dd-MM-yyyy HH:mm');
        //    return formatted;
        //  }
        //},
      },
      attr: { class: 'table table-bordered' },
      actions: {
        edit: false, //as an example
        delete: false, //as an example
        add: false,
        position: 'right',
        custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button">Edit</button></span>` }]
      },
      pager: { display: true, perPage: this.page },
    };

  }
  onExport() {
    this.excelService.exportAsExcelFile(this.listofinstances, 'InstanceManager');
  }

}
