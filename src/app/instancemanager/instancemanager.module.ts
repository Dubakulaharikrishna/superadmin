//import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//import { DashboardComponent } from './dashboard.component';
import { RouterModule } from '@angular/router';
import { InstancemanagerRoutes } from './instancemanager.routing';
import { InstancemanagerComponent } from '../instancemanager/instancemanager.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { HttpModule } from '@angular/http';
import { DataTablesModule } from 'angular-datatables';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
//import { FileUploadModule } from 'ng2-file-upload/ng2-file-upload';
//changes
import { NgbProgressbarModule, NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { NgbAccordionModule } from '@ng-bootstrap/ng-bootstrap';
import { CustomFormsModule } from 'ng2-validation';
import { FormsModule, FormBuilder, FormGroup, Validators, FormControl, ReactiveFormsModule } from '@angular/forms';
import { AdminService } from '../services/admin.service';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';
import { Ng2SmartTableModule } from 'ng2-smart-table';


@NgModule({

  imports: [
    DataTablesModule,
    HttpClientModule,
    CommonModule,
    NgbModule,
    ReactiveFormsModule,
    RouterModule.forChild(InstancemanagerRoutes),
    HttpModule, FormsModule, Ng2SmartTableModule
  ],
  declarations: [

    InstancemanagerComponent

  ],

  providers: [AdminService, AuthGuard, NotAuthGuard],
  bootstrap: [InstancemanagerComponent]
})
export class InstancemanagerModule { }







