import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstancemanagerComponent } from './instancemanager.component';

describe('InstancemanagerComponent', () => {
  let component: InstancemanagerComponent;
  let fixture: ComponentFixture<InstancemanagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstancemanagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstancemanagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
