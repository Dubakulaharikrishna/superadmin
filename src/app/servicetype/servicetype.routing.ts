import { Routes } from '@angular/router';
import { ServiceTypeComponent } from './servicetype.component';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';
export const ServiceTypeRoutes: Routes = [

  {
    path: '',
    component: ServiceTypeComponent,
    canActivate: [AuthGuard],
    data: {
      heading: 'ServiceType'
    }
  },


];


