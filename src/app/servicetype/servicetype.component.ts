import { Component, OnInit, ElementRef } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { CustomValidators } from 'ng2-validation';
import { DatePipe } from '@angular/common'
import 'rxjs/add/operator/debounceTime';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/share';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';
import { AdminService } from '../services/admin.service';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { CommonModule } from '@angular/common';
import { Status } from '../Models/Status';
import { LocalDataSource } from 'ng2-smart-table';
import { ServiceType } from '../Models/ServiceType';
import { ExcelService } from '../services/excel.service';
import { AccountManagement } from '../Models/AccountManagement';


@Component({
  selector: 'app-servicetype',
  templateUrl: './servicetype.component.html',
  styleUrls: ['./servicetype.component.css'],
  providers: [ExcelService, DatePipe]
})
export class ServiceTypeComponent implements OnInit {
  dataLoaded: any;
  coverPic: any;
  MasterAccount_Id: any;
  MasterAccount: any;
  source: LocalDataSource;
  status: Status;
  success: boolean = false;
  message: any;
  dtTrigger: Subject<any> = new Subject();
  public _success = new Subject<string>();
  staticAlertClosed = false;
  selectedValue: any = 50;
  page: any;
  public data: Object;
  public temp_var: Object = false;
  accountId: any;
  public ServiceTypeUpdateFormGroup: FormGroup;
  public ServiceTypeForm: FormGroup;
  public form: FormGroup;
  servicetype: ServiceType;
  roleId: any;
  public isSubmitted: boolean = false;
  id: any;
  listofAccounts: AccountManagement[];
  listofservicetype: ServiceType[];
  pagename: any;
  createdby: any;
  constructor(private adminservice: AdminService, public datepipe: DatePipe, public ele: ElementRef, private excelService: ExcelService, private router: Router, private http: HttpClient, public fb: FormBuilder) {
    //this.status = new Status();
    this.servicetype = new ServiceType();

  }


  onGetAllServiceTypeList(id) {

    this.adminservice.onGetAllServiceTypeList(id).subscribe(data => {
      // alert(JSON.stringify(data));

      if (data.StatusCode == "1") {
        this.status = data;
        this.listofservicetype = this.status.Data;
        this.source = new LocalDataSource(this.listofservicetype);
        return this.listofservicetype;
      }


    });
  }


  onGetAllAccountsList() {
    this.adminservice.onGetAllAccountsList().subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofAccounts = this.status.Data;

        //alert(JSON.stringify(this.listofAccounts));
        return this.listofAccounts
      }

    });
  }



  test(id) {
    //  alert("Hi");
    //console.log(id);
    //alert(id);
    console.log(this.MasterAccount_Id);
   // alert(this.MasterAccount_Id);
  }
  openCpopup() {

    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "block";
    cmask.style.display = "block";
  }

  closeCpopup() {
    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "none";
    cmask.style.display = "none";
  }

  onEdit(event: any) {


    this.router.navigate(['/viewservicetype/', { stId: event.data.Id }]);
  }



  reset() {
    this.onGetAllServiceTypeList(this.accountId);
    this.MasterAccount= undefined;
  }


  ngOnInit() {
    // sessionStorage.clear();
    //localStorage.removeItem('loginSessId');
  //  localStorage.clear();
   // localStorage.setTimeout = 1;
   // sessionStorage.removeItem('loginSessId');
  //  localStorage.removeItem('loginSessId');
    this.pagename = "ServiceType";
    localStorage.setItem('loginSessId', this.pagename);
    this.accountId = localStorage.getItem('AccountId');
    this.ServiceTypeform();
    this.onGetAllServiceTypeList(this.accountId);
    this.onGetAllAccountsList();
    this.roleId = localStorage.getItem('RoleId');
    setTimeout(() => this.staticAlertClosed = true, 20000);
    this._success.subscribe((message) => this.message = message);
    this._success.debounceTime(7000).subscribe(() => this.message = null);
    this.createdby = localStorage.getItem('MemberName');

  }




  onChange(event) {
    console.log(event);
    this.onGetAllServiceTypeList(event.target.value);
  }
  onCreateServiceType() {
    let files = this.ele.nativeElement.querySelector('#ServiceIcon').files;
    if (files.length > 0 && files.count != 0 && files != null) {

      let formData = new FormData();
      let file = files[0];
      this.coverPic = file.name;
      formData.append('ServiceIcon', file, file.name);
      console.log(formData);
      console.log(files);
      this.adminservice.onfileupload(formData).subscribe(res => this.dataLoaded = res);
    }
    let date = new Date();

    const ServiceType = {

      ServiceName: this.form.controls["ServiceName"].value,
      MasterAccount_Id: this.form.controls["MasterAccount_Id"].value,
      ServiceIcon: "Tcsimages/serviceiconimages/" + this.coverPic,
      ServiceTypeStatus: this.form.controls["ServiceTypeStatus"].value,
      ServiceTypeCreatedBy: this.createdby,
      ServiceTypeCreatedDate: this.datepipe.transform(date, "yyyy-MM-dd")
    }

    if (this.form.valid) {
      this.isSubmitted = false;
      // alert(JSON.stringify(Exercise));
      this.adminservice.onCreateServiceType(ServiceType).subscribe(data => {
        console.log(data);
        if (data.StatusCode == "1") {
          this.success = true;
          this.message = data.Message;
          this._success.next(`Record Saved Successfully`);

          this.onGetAllServiceTypeList(this.accountId);
          this.closeCpopup();
         

        }
        else {
          this.success = false;
          this.message = "Failed to create account master";
        }
      });
     // alert("Successfully Submitted");
     
    }
    else {
      this.isSubmitted = true;
    }

  }


  ServiceTypeform() {
    this.form = this.fb.group({

      'ServiceName': ['', [Validators.required]],
      'MasterAccount_Id': ['', [Validators.required]],
      'ServiceTypeStatus': ['', [Validators.required]]
    });
  }


  /*Smart table*/
  settings = {

    columns: {
      ServiceName: {
        title: 'ServiceName',
        filter: false
      },
      ServiceTypeStatus: {
        title: 'ServiceTypeStatus',
        filter: false
      },

    },
    attr: { class: 'table table-bordered' },
    actions: {
      edit: false, //as an example
      delete: false, //as an example
      add: false,
      position: 'right',
      custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button" (click)="onEdit(rec.Id)">Edit</button></span>` }]
    },
    pager: { display: true, perPage: 50 },
  };

  //route(event) {
  //  alert('hi');
  //}

  onSearch(query: string = '') {
    if (query != '') {
      this.source.setFilter([
        // fields we want to include in the search
        {
          field: 'ServiceName',
          search: query
        },
        {
          field: 'ServiceTypeStatus',
          search: query
        }
      ], false);
    }
    else {
      this.source = new LocalDataSource(this.listofservicetype);
    }

    // second parameter specifying whether to perform 'AND' or 'OR' search 
    // (meaning all columns should contain search query or at least one)
    // 'AND' by default, so changing to 'OR' by setting false here
  }

  onSelectedFilter(event) {
    this.page = event.target.value;
    this.source = new LocalDataSource(this.listofservicetype);
    this.settings = {

      columns: {
        ServiceName: {
          title: 'ServiceName',
          filter: false
        },
        ServiceTypeStatus: {
          title: 'ServiceTypeStatus',
          filter: false
        },

      },
      attr: { class: 'table table-bordered' },
      actions: {
        edit: false, //as an example
        delete: false, //as an example
        add: false,
        position: 'right',
        custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button">Edit</button></span>` }]
      },
      pager: { display: true, perPage: this.page },
    };

  }


  onExport() {
    this.excelService.exportAsExcelFile(this.listofservicetype, 'ServiceType');
  }

}

