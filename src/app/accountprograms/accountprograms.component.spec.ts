import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountprogramsComponent } from './accountprograms.component';

describe('AccountprogramsComponent', () => {
  let component: AccountprogramsComponent;
  let fixture: ComponentFixture<AccountprogramsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountprogramsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountprogramsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
