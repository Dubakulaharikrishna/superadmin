

//import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, FormBuilder, FormGroup, Validators, FormControl, ReactiveFormsModule } from '@angular/forms';
//import { CustomFormsModule } from 'ng2-validation';

//import { DashboardComponent } from './dashboard.component';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DataTablesModule } from 'angular-datatables';
import { AccountprogramsRoutes } from './accountprograms.routing';
import { AccountprogramsComponent } from '../accountprograms/accountprograms.component';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { AdminService } from '../services/admin.service';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';
import { Ng2SmartTableModule } from 'ng2-smart-table';
@NgModule({
  imports: [
    DataTablesModule, CommonModule, NgbModule, FormsModule, ReactiveFormsModule, Ng2SmartTableModule,
    RouterModule.forChild(AccountprogramsRoutes), HttpClientModule, HttpModule

    //BrowserModule
  ],
  declarations: [
    AccountprogramsComponent
  ],
  providers: [AdminService, AuthGuard, NotAuthGuard],
  bootstrap: [AccountprogramsComponent]
})
export class AccountProgramsModule {

}
