import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl, } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { accountprograms } from '../Models/accountprograms';
import { AdminService } from '../services/admin.service';
import { HttpClient } from '@angular/common/http';
import { CustomValidators } from 'ng2-validation';
import { DatePipe } from '@angular/common'
import 'rxjs/add/operator/debounceTime';
import { Subject } from 'rxjs/Subject';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/share';
import { Status } from '../Models/Status';
import { Ng2SmartTableModule, LocalDataSource } from 'ng2-smart-table';
import { AccountManagement } from '../Models/AccountManagement';
import { Webclient } from '../Models/Webclient';
import { Capabilities } from 'selenium-webdriver';
import { ExcelService } from '../services/excel.service';
@Component({
  selector: 'app-accountprograms',
  templateUrl: './accountprograms.component.html',
  styleUrls: ['./accountprograms.component.css'],
  providers: [ExcelService, DatePipe]
})
export class AccountprogramsComponent implements OnInit {
  MasterAccount: any;
  dtTrigger: Subject<any> = new Subject();
  public _success = new Subject<string>();
  staticAlertClosed = false;
  success: boolean = false;
  message: any;
  Capability_Id: any;
  Client_Id: any;
  MasterAccount_Id: any;
  source: LocalDataSource;
  selectedValue: any = 50;
  page: any;
  roleId: any;
  AccountProgramEndDate: any;
  AccountProgramStartDate: any;
  dataLoaded: any;
  coverPic: any;
  createdby: any;
  public data: Object;
  public temp_var: Object = false;
  public AccountProgramsUpdateFormGroup: FormGroup;
  public AccountprogramsForm: FormGroup;
  accountprograms: accountprograms;
  public isSubmitted: boolean = false;
  //public btnDisable: boolean = false;
  id: any;
  webclientslist: Webclient[];
  pagename: any;
  programinfo: any = {};
  status: Status;
  listofAccountPrograms: accountprograms[];
  listofAccounts: AccountManagement[];
  accountId: any;
  listofcapabilities: Capabilities[];
  constructor(private fb: FormBuilder, public datepipe: DatePipe, private excelService: ExcelService, private adminservice: AdminService, private router: Router, private http: HttpClient, public ele: ElementRef) {
    this.accountprograms = new accountprograms();
  }


  onGetAllAccountProgramsList(id) {
    //let id = sessionStorage.getItem('AccountId');
   // alert(id);
    this.adminservice.onGetAllAccountProgramsList(id).subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofAccountPrograms = this.status.Data;
        this.source = new LocalDataSource(this.listofAccountPrograms);

        // alert(JSON.stringify(this.Programstructures));
        return this.listofAccountPrograms;
      }
      
    });
  }

  onGetAllCapabilitiesList() {
    let id = localStorage.getItem('AccountId');
    this.adminservice.onGetAllCapabilitiesList(id).subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;


        this.listofcapabilities = this.status.Data;
        this.source = new LocalDataSource(this.listofcapabilities);
        return this.listofcapabilities
      }

    });
  }


 

  onGetAllWebClientList() {
    this.adminservice.onGetAllWebClientList().subscribe(data => {

      if (data.StatusCode == "1") {
        this.status = data;
        this.webclientslist = this.status.Data;
        this.source = new LocalDataSource(this.webclientslist);
        // alert(JSON.stringify(this.Programstructures));
        return this.webclientslist
      }

    });
  }

  onGetAllAccountsList() {
    this.adminservice.onGetAllAccountsList().subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofAccounts = this.status.Data;

        //alert(JSON.stringify(this.listofAccounts));
        return this.listofAccounts
      }

    });
  }
  reset() {
    this.onGetAllAccountProgramsList(this.accountId);
    this.MasterAccount = undefined;
    
  }

  onEdit(event: any) {
    this.router.navigate(['/viewprogram/', { accId: event.data.Id }]);
  }

  ngOnInit() {

   // sessionStorage.clear();
    localStorage.removeItem('loginSessId');
    //localStorage.clear();
   // localStorage.setTimeout = 1;
   // sessionStorage.removeItem('loginSessId');
   // localStorage.removeItem('loginSessId');
    this.pagename = "Account program";
    localStorage.setItem('loginSessId', this.pagename);
    this.roleId = localStorage.getItem('RoleId');
    this.accountId = localStorage.getItem('AccountId');
    setTimeout(() => this.staticAlertClosed = true, 20000);
    this._success.subscribe((message) => this.message = message);
    this._success.debounceTime(7000).subscribe(() => this.message = null);
    this.createdby = localStorage.getItem('MemberName');
    this.AccountProgramsForm();
    this.onGetAllAccountProgramsList(this.accountId);
    this.onGetAllAccountsList();
    this.onGetAllWebClientList();
    this.onGetAllCapabilitiesList();
    
  }
  onChange(event) {
    console.log(event);
    this.onGetAllAccountProgramsList(event.target.value);
  }

  AccountProgramsForm() {
    this.AccountprogramsForm = this.fb.group({
      Client_Id: ['', Validators.compose([
        Validators.required
      ])],
      Capability_Id: ['', Validators.compose([
        Validators.required
      ])],
      MasterAccount_Id: ['', Validators.compose([
        Validators.required
      ])],
      AccountProgramStartDate: ['', Validators.compose([
        Validators.required
      ])],
      AccountProgramEndDate: ['', Validators.compose([
        Validators.required
      ])],
      //AccountProgramCreatedBy: ['', Validators.compose([
      //  Validators.required
      //])],
      //AccountProgramCreatedDate: ['', Validators.compose([
      //  Validators.required
      //])],
      //AccountProgramUpdatedDate: ['', Validators.compose([
      //  Validators.required
      //])],
   //   AccountProgramUpdatedBy: ['', Validators.compose([
      //  Validators.required
    //  ])],
      AccountProgramAccessType: ['', Validators.compose([
        Validators.required
      ])],
      AccountProgramBusinessRule: ['', Validators.compose([
        Validators.required
      ])],
      AccountProgramStatus: ['', Validators.compose([
        Validators.required
      ])],



      //ProgramCoverPicture: ['', Validators.compose([
      //  Validators.required
      //])],
      ProgramDescription: ['', Validators.compose([
        Validators.required
      ])],
      ProgramName: ['', Validators.compose([
        Validators.required
      ])],




    });


  }

  //checkDate() {
  //  if (this.AccountProgramStartDate > this.AccountProgramEndDate) {
  //    //this.btnDisable = true;
  //  }
  //  if (this.AccountProgramStartDate < this.AccountProgramEndDate) {
  //    //this.btnDisable = false;
  //  }
  //}
  onRegisterSubmit() {


    let files = this.ele.nativeElement.querySelector('#ProgramCoverPicture').files;
    if (files.length > 0 && files.count != 0 && files != null) {

      let formData = new FormData();
      let file = files[0];
      this.coverPic = file.name;
      formData.append('ProgramCoverPicture', file, file.name);
      console.log(formData);
      console.log(files);
      this.adminservice.onfileupload(formData).subscribe(res => this.dataLoaded = res);
    }
    let date = new Date();
    const user = {
      Client_Id: this.AccountprogramsForm.get('Client_Id').value,
      Capability_Id: this.AccountprogramsForm.get('Capability_Id').value,
      MasterAccount_Id: this.AccountprogramsForm.get('MasterAccount_Id').value,
      AccountProgramStartDate: this.AccountprogramsForm.get('AccountProgramStartDate').value,
      AccountProgramEndDate: this.AccountprogramsForm.get('AccountProgramEndDate').value,
      AccountProgramAccessType: this.AccountprogramsForm.get('AccountProgramAccessType').value,
      AccountProgramBusinessRule: this.AccountprogramsForm.get('AccountProgramBusinessRule').value,
      AccountProgramStatus: this.AccountprogramsForm.get('AccountProgramStatus').value,
      ProgramCoverPicture: "Tcsimages/prgcoverimages/" + this.coverPic,
      ProgramDescription: this.AccountprogramsForm.get('ProgramDescription').value,
      ProgramName: this.AccountprogramsForm.get('ProgramName').value,
      AccountProgramCreatedBy: this.createdby,
      AccountProgramCreatedDate: this.datepipe.transform(date, "yyyy-MM-dd")
      //ProgramInfo: this.programinfo,
      // 

    }


    //this.programinfo.ProgramName = (<HTMLInputElement>document.getElementById("ProgramName")).value;
    //this.programinfo.ProgramDescription = (<HTMLInputElement>document.getElementById("ProgramName")).value;
    //this.programinfo.ProgramCoverPicture = (<HTMLInputElement>document.getElementById("ProgramCoverPicture")).value;

   // alert(JSON.stringify( this.programinfo));
    
    if (this.AccountprogramsForm.valid) {
      this.isSubmitted = false;
      // alert(JSON.stringify(license));
   
      //alert(JSON.stringify(user));
      this.adminservice.onCreateAccountPrograms(user).subscribe(data => {
        console.log(data);
        if (data.StatusCode == "1") {
          this.success = true;
          this.message = data.Message;
          this._success.next(`Record Saved Successfully`);
          this.onGetAllAccountProgramsList(this.accountId);
          this.closeCpopup();

        }
        else {
          this.success = false;
          this.message = "Failed to create account master";
        }
      });
    //  alert("Submitted Successfully");
   
     
    }
    else {
      this.isSubmitted = true;
      //alert(JSON.stringify(license));
    }

  }

  client(id) {
    
    console.log(this.Client_Id);
   // alert(this.Client_Id);
  }
  account(id) {

    console.log(this.MasterAccount_Id);
    //alert(this.MasterAccount_Id);
  }
  capability(id) {

    console.log(this.Capability_Id);
    //alert(this.Capability_Id);
  }
  openCpopup() {

    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "block";
    cmask.style.display = "block";
  }

  closeCpopup() {
    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "none";
    cmask.style.display = "none";
  }


  /*Smart table*/
  settings = {

    columns: {
      AccountProgramAccessType: {
        title: 'Account Program Access Type',
        filter: false
      },
      AccountProgramStatus: {
        title: 'Status',
        filter: false
      },
    
    },
    attr: { class: 'table table-bordered' },
    actions: {
      edit: false, //as an example
      delete: false, //as an example
      add: false,
      position: 'right',
      custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button" (click)="onEdit(rec.Id)">Edit</button></span>` }]
    },
    pager: { display: true, perPage: 50 },
  };

  //route(event) {
  //  alert('hi');
  //}

  onSearch(query: string = '') {
    if (query != '') {
      this.source.setFilter([
        // fields we want to include in the search
        {
          field: 'Account Program Access Type',
          search: query
        },
        {
          field: 'Account Program Status',
          search: query
        }
      ], false);
    }
    else {
      this.source = new LocalDataSource(this.listofAccountPrograms);
    }

    // second parameter specifying whether to perform 'AND' or 'OR' search 
    // (meaning all columns should contain search query or at least one)
    // 'AND' by default, so changing to 'OR' by setting false here
  }

  onSelectedFilter(event) {
    this.page = event.target.value;
    this.source = new LocalDataSource(this.listofAccountPrograms);
    this.settings = {

      columns: {
        AccountProgramAccessType: {
          title: 'Account Program Access Type',
          filter: false
        },
        AccountProgramStatus: {
          title: 'Status',
          filter: false
        },
   
      },
      attr: { class: 'table table-bordered' },
      actions: {
        edit: false, //as an example
        delete: false, //as an example
        add: false,
        position: 'right',
        custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button">Edit</button></span>` }]
      },
      pager: { display: true, perPage: this.page },
    };

  }

  onExport() {
    this.excelService.exportAsExcelFile(this.listofAccountPrograms, 'AccountPrograms');
  }


}







