import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';

import { AccountprogramsComponent } from './accountprograms.component';

export const AccountprogramsRoutes: Routes = [

  {
    path: '',
    component: AccountprogramsComponent,
    canActivate: [AuthGuard],

    data: {
      heading: 'Accountprograms'
    }
  },


];
