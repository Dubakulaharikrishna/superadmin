import { Component, OnInit } from '@angular/core';
import { MenuItems } from '../Models/menu-items';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  providers: [MenuItems]
})
export class HeaderComponent implements OnInit {
  pagename: any;
  IsTCSAdmin: boolean = false;
  constructor(public menuItems: MenuItems, private router: Router,
    public route: ActivatedRoute,) { }

  ngOnInit() {

    var role = localStorage.getItem('RoleId');

    this.onGetRole(role);

  this.pagename = localStorage.getItem('loginSessId');
    //sessionStorage.clear();
   // sessionStorage.removeItem('loginSessId');
    //localStorage.removeItem('loginSessId');
   // localStorage.clear();
    //localStorage.setTimeout = 1;
  }
  // Get Role and Redirect

  onGetRole(roleid) {
    // alert(roleid);
    if (roleid == "5acb3f62a4e676248031953d") {
      this.IsTCSAdmin = true;
    //  alert(this.IsTCSAdmin);
    }
    return this.IsTCSAdmin;
  }


  ongetpagename() {
    this.pagename = localStorage.getItem('loginSessId');
    return this.pagename;
  }

  showSubchild() {
    let clkItem = document.getElementById("pgcnt");

    clkItem.onclick = function () {
      let opdnItem = document.getElementById("cnc");
      opdnItem.style.display = "block";
      //alert('dsf');

    }
  }

  showSubMenu1() {
    let submenu1 = document.getElementById("cm_dropdown");
    let submenu2 = document.getElementById("pm_dropdown");
    let submenu3 = document.getElementById("ex_dropdown");
    let submenu4 = document.getElementById("cmr_dropdown");

    let icon1 = document.getElementById("arw1");
    let icon2 = document.getElementById("arw2");

    submenu1.classList.add('show_sublist');
   // submenu2.classList.remove('show_sublist');
    submenu3.classList.remove('show_sublist');
    submenu4.classList.remove('show_sublist');
    icon1.classList.add('tgl_icn');
    icon2.classList.remove('tgl_icn');
  }
 
  showSubMenu2() {
    let submenu1 = document.getElementById("cm_dropdown");
    let submenu2 = document.getElementById("pm_dropdown");
    let submenu3 = document.getElementById("ex_dropdown");
    let submenu4 = document.getElementById("cmr_dropdown");
    let icon1 = document.getElementById("arw1");
    let icon2 = document.getElementById("arw2");

    submenu2.classList.add('show_sublist');
    submenu1.classList.remove('show_sublist');
    submenu3.classList.remove('show_sublist');
    submenu4.classList.remove('show_sublist');
    icon1.classList.remove('tgl_icn');
    icon2.classList.add('tgl_icn');
  }
  showSubMenu3() {
    let submenu1 = document.getElementById("cm_dropdown");
    let submenu2 = document.getElementById("pm_dropdown");
    let submenu3 = document.getElementById("ex_dropdown");
    let submenu4 = document.getElementById("cmr_dropdown");
    let icon1 = document.getElementById("arw1");
    let icon2 = document.getElementById("arw2");

    submenu3.classList.add('show_sublist');
    submenu1.classList.remove('show_sublist');
    submenu2.classList.remove('show_sublist');
    submenu4.classList.remove('show_sublist');
    icon1.classList.add('tgl_icn');
    icon2.classList.remove('tgl_icn');
  }
  showSubMenu4() {
    let submenu1 = document.getElementById("cm_dropdown");
    let submenu2 = document.getElementById("pm_dropdown");
    let submenu3 = document.getElementById("ex_dropdown");
    let submenu4 = document.getElementById("cmr_dropdown");

    let icon1 = document.getElementById("arw1");
    let icon2 = document.getElementById("arw2");

    submenu4.classList.add('show_sublist');
    submenu1.classList.remove('show_sublist');
    submenu3.classList.remove('show_sublist');
    submenu2.classList.remove('show_sublist');
    icon1.classList.add('tgl_icn');
    icon2.classList.remove('tgl_icn');
  }


  showSubMenu5() {
    let submenu1 = document.getElementById("cm_dropdown");
    let submenu2 = document.getElementById("pm_dropdown");
    let submenu3 = document.getElementById("ex_dropdown");
    let submenu4 = document.getElementById("cmr_dropdown");
    let submenu5 = document.getElementById("ev_dropdown");

    let icon1 = document.getElementById("arw1");
    let icon2 = document.getElementById("arw2");

    submenu5.classList.add('show_sublist');
    submenu4.classList.remove('show_sublist');
    submenu1.classList.remove('show_sublist');
    submenu3.classList.remove('show_sublist');
    submenu2.classList.remove('show_sublist');
    icon1.classList.add('tgl_icn');
    icon2.classList.remove('tgl_icn');
  }

  showSubMenu6() {
    let submenu1 = document.getElementById("cm_dropdown");
    let submenu2 = document.getElementById("pm_dropdown");
    let submenu3 = document.getElementById("ex_dropdown");
    let submenu4 = document.getElementById("cmr_dropdown");
    let submenu5 = document.getElementById("ev_dropdown");
    let submenu6 = document.getElementById("frm_dropdown");

    let icon1 = document.getElementById("arw1");
    let icon2 = document.getElementById("arw2");

    submenu6.classList.add('show_sublist');
    submenu5.classList.remove('show_sublist');
    submenu4.classList.remove('show_sublist');
    submenu1.classList.remove('show_sublist');
    submenu3.classList.remove('show_sublist');
    submenu2.classList.remove('show_sublist');
    icon1.classList.add('tgl_icn');
    icon2.classList.remove('tgl_icn');
  }

  showSubMenu7() {
    let submenu1 = document.getElementById("cm_dropdown");
    let submenu2 = document.getElementById("pm_dropdown");
    let submenu3 = document.getElementById("ex_dropdown");
    let submenu4 = document.getElementById("cmr_dropdown");
    let submenu5 = document.getElementById("ev_dropdown");
    let submenu6 = document.getElementById("frm_dropdown");
    let submenu7 = document.getElementById("ser_dropdown");


    let icon1 = document.getElementById("arw1");
    let icon2 = document.getElementById("arw2");

    submenu7.classList.add('show_sublist');
    submenu6.classList.add('show_sublist');
    submenu5.classList.remove('show_sublist');
    submenu4.classList.remove('show_sublist');
    submenu1.classList.remove('show_sublist');
    submenu3.classList.remove('show_sublist');
    submenu2.classList.remove('show_sublist');
    icon1.classList.add('tgl_icn');
    icon2.classList.remove('tgl_icn');
  }





  hideSubmenu() {
    
    let submenu1 = document.getElementById("cm_dropdown");
    let submenu2 = document.getElementById("pm_dropdown");
    let submenu3 = document.getElementById("ex_dropdown");
    let submenu4 = document.getElementById("cmr_dropdown");
    let submenu5 = document.getElementById("ev_dropdown");
    let submenu6 = document.getElementById("frm_dropdown");
    let submenu7 = document.getElementById("ser_dropdown");

    let icon1 = document.getElementById("arw1");
    let icon2 = document.getElementById("arw2");

    submenu1.classList.remove('show_sublist');
    submenu2.classList.remove('show_sublist');
    submenu3.classList.remove('show_sublist');
    submenu4.classList.remove('show_sublist');
    submenu5.classList.remove('show_sublist');
    submenu6.classList.remove('show_sublist');
    submenu7.classList.remove('show_sublist');

    icon1.classList.remove('tgl_icn');
    icon2.classList.remove('tgl_icn');

  }

  showMsgBox() {
    let msgbx = document.getElementById("msg");
    let notbx = document.getElementById("nots");
    let logbx = document.getElementById("log");
    let tpr1 = document.getElementById("ms");
    let tpr2 = document.getElementById("nt");
    let tpr3 = document.getElementById("lg");

    msgbx.classList.toggle("show_boxes");
    notbx.classList.remove("show_boxes");
    logbx.classList.remove("show_boxes");

    tpr1.classList.toggle("add_active");
    tpr2.classList.remove("add_active");
    tpr3.classList.remove("add_active");
  }

  showNotsBox() {
    let msgbx = document.getElementById("msg");
    let notbx = document.getElementById("nots");
    let logbx = document.getElementById("log");
    let tpr1 = document.getElementById("ms");
    let tpr2 = document.getElementById("nt");
    let tpr3 = document.getElementById("lg");

    msgbx.classList.remove("show_boxes");
    notbx.classList.toggle("show_boxes");
    logbx.classList.remove("show_boxes");

    tpr1.classList.remove("add_active");
    tpr2.classList.toggle("add_active");
    tpr3.classList.remove("add_active");
  }

  showlogsBox() {
    let msgbx = document.getElementById("msg");
    let notbx = document.getElementById("nots");
    let logbx = document.getElementById("log");
    let tpr1 = document.getElementById("ms");
    let tpr2 = document.getElementById("nt");
    let tpr3 = document.getElementById("lg");

    msgbx.classList.remove("show_boxes");
    notbx.classList.remove("show_boxes");
    logbx.classList.toggle("show_boxes");

    tpr1.classList.remove("add_active");
    tpr2.classList.remove("add_active");
    tpr3.classList.toggle("add_active");
  }

  logout() {
    localStorage.clear();
    this.router.navigate(['/']);
    //alert("hi");
  }

}
