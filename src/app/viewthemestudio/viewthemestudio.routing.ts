
import { Routes } from '@angular/router';

import { ViewThemeStudioComponent } from '../viewthemestudio/viewthemestudio.component';

export const ViewThemeStudioRoutes: Routes = [

  {
    path: '',
    component: ViewThemeStudioComponent,
    data: {
      heading: 'ViewThemeStudio'
    }
  },


];
