import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewThemeStudioComponent } from './viewthemestudio.component';

describe('ViewThemeStudioComponent', () => {
  let component: ViewThemeStudioComponent;
  let fixture: ComponentFixture<ViewThemeStudioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ViewThemeStudioComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewThemeStudioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
