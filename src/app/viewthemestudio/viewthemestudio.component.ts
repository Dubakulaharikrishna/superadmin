import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';
//import { Programmanagement } from '../programmanagement/programmanagement.module';
import { CommonModule } from '@angular/common';
//import { ProgramManagementModule } from './programmanagement.module';
import { DatePipe } from '@angular/common'
import { ThemeStudio } from '../Models/themestudio';
import { AdminService } from '../services/admin.service';
import { CustomValidators } from 'ng2-validation';
import 'rxjs/add/operator/debounceTime';
import { Subject } from 'rxjs/Subject';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { retry } from 'rxjs/Operator/retry';




@Component({
  selector: 'app-viewthemestudio',
  templateUrl: './viewthemestudio.component.html',
  styleUrls: ['./viewthemestudio.component.css']
})
export class ViewThemeStudioComponent implements OnInit {


  public themestudioUpdateFormGroup: FormGroup;
  public form: FormGroup;
  themestudio: ThemeStudio;
  id: any;
  isSubmitted: boolean = false;
  tmsId: any;

  constructor(private fb: FormBuilder, private adminservice: AdminService, private router: Router, public route: ActivatedRoute, private http: HttpClient) {

    this.themestudio = new ThemeStudio();



  }

  ngOnInit() {
    this.ViewThemeStudioform();
    this.route.params.subscribe(params => {
      this.tmsId = params['tmsId'];

    });
    this.onGetThemeStudioById();
  }


  onUpdateThemeStudio(themestudio) {

    if (this.themestudioUpdateFormGroup.valid) {
      //  alert(JSON.stringify(accountmanagement));
      this.isSubmitted = false;
      // alert(JSON.stringify(license));

      alert(JSON.stringify(themestudio));
      this.adminservice.onUpdateThemeStudio(themestudio).subscribe(res => {
        console.log(res);
        this.themestudio = res
      });

    }
    else {
      this.isSubmitted = true;
      //alert(JSON.stringify(license));
    }
    //alert(JSON.stringify(account));
  }

  ViewThemeStudioform() {
    this.themestudioUpdateFormGroup = this.fb.group({
      // 'themestudio.ThemeStudio_Id': ['', [Validators.required]],
      'themestudio.Client_Id': ['', [Validators.required]],

      'themestudio.ThemeName': ['', [Validators.required]],

      'themestudio.ThemeTitle': ['', [Validators.required]],
      'themestudio.ThemePrimaryColor': ['', [Validators.required]],
      'themestudio.ThemeSecondaryColor': ['', [Validators.required]],
      'themestudio.ThemeLogoUrl': ['', [Validators.required]],
      'themestudio.ThemeH1': ['', [Validators.required]],
      'themestudio.ThemeH2': ['', [Validators.required]],
      'themestudio.ThemeH3': ['', [Validators.required]],

      'themestudio.ThemeHeader': ['', [Validators.required]],

      'themestudio.ThemeSubHeader': ['', [Validators.required]],
      'themestudio.ThemeNormalText': ['', [Validators.required]],
      'themestudio.ThemeParagraphText': ['', [Validators.required]],
      'themestudio.ThemePrimaryFont': ['', [Validators.required]],
      'themestudio.ThemeSecondaryFont': ['', [Validators.required]],
      'themestudio.ThemeBorderColor': ['', [Validators.required]],
      'themestudio.ThemeBorderWidth': ['', [Validators.required]],
      'themestudio.ThemeALink': ['', [Validators.required]],

      'themestudio.ThemeHrefLink': ['', [Validators.required]],

      'themestudio.ThemeLiStyle': ['', [Validators.required]],
      'themestudio.ThemeButton': ['', [Validators.required]],
      'themestudio.ThemeControlColor': ['', [Validators.required]],
      'themestudio.ThemeTextBoxStyle': ['', [Validators.required]],
      'themestudio.ThemeAreaStyle': ['', [Validators.required]],
      'themestudio.ThemeControlStyle': ['', [Validators.required]],
      'themestudio.ThemeEmbeddedStyle': ['', [Validators.required]],

      //'themestudio.ThemeCreatedDate': ['', [Validators.required]],

      //'themestudio.ThemeCreatedBy': ['', [Validators.required]],
      //'themestudio.ThemeUpdatedDate': ['', [Validators.required]],
      //'themestudio.ThemeUpdatedBy': ['', [Validators.required]],
      'themestudio.ThemeStatus': ['', [Validators.required]],




    });
  }
  onGetThemeStudioById() {

    this.adminservice.onGetThemeStudioById(this.tmsId).subscribe(result => {
      this.themestudio = result;
      console.log(this.themestudio);
    });
    return this.themestudio;
  }

}
