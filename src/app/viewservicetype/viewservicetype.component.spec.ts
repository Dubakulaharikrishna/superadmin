import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewServiceTypeComponent } from './viewservicetype.component';

describe('ViewserviceTypeComponent', () => {
  let component: ViewServiceTypeComponent;
  let fixture: ComponentFixture<ViewServiceTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ViewServiceTypeComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewServiceTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});


