import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import 'rxjs/add/operator/share';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';
//import { Programmanagement } from '../programmanagement/programmanagement.module';
import { CommonModule } from '@angular/common';
//import { ProgramManagementModule } from './programmanagement.module';
import { DatePipe } from '@angular/common'

import { AdminService } from '../services/admin.service';
import { CustomValidators } from 'ng2-validation';
import 'rxjs/add/operator/debounceTime';
import { Subject } from 'rxjs/Subject';
import 'rxjs/Rx';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { retry } from 'rxjs/Operator/retry';
import { ServiceType } from '../Models/ServiceType';
import { Status } from '../Models/Status';
@Component({
  selector: 'app-viewservicetype',
  templateUrl: './viewservicetype.component.html',
  styleUrls: ['./viewservicetype.component.css'],
  providers: [DatePipe]
})
export class ViewServiceTypeComponent implements OnInit {
  dataLoaded: any;
  coverPic: any;
  listofAccounts: any;


  public servicetypeUpdateFormGroup: FormGroup;
  public form: FormGroup;
  servicetype: ServiceType;
  id: any;
  isSubmitted: boolean = false;
  stId: any;
  createdby: any;
  status: Status;
  constructor(private fb: FormBuilder, public datepipe: DatePipe, public ele: ElementRef, private adminservice: AdminService, private router: Router, public route: ActivatedRoute, private http: HttpClient) {

    this.servicetype = new ServiceType();



  }

  ngOnInit() {
    this.UpdateServiceTypeform();
    this.route.params.subscribe(params => {
      this.stId = params['stId'];
     

    });
    this.onGetServiceTypeById();
    this.onGetAllAccountsList();
    this.createdby = localStorage.getItem('MemberName');

  }



  onGetAllAccountsList() {
    this.adminservice.onGetAllAccountsList().subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofAccounts = this.status.Data;

        //alert(JSON.stringify(this.listofAccounts));
        return this.listofAccounts
      }

    });
  }
  test(id) {
    //  alert("Hi");
    //console.log(id);
    //alert(id);
    console.log(this.servicetype.MasterAccount_Id);
  //  alert(this.servicetype.MasterAccount_Id);
  }
  onUpdateServiceType(servicetype) {
    let files = this.ele.nativeElement.querySelector('#ServiceIcon').files;
    if (files.length > 0 && files.count != 0 && files != null) {

      let formData = new FormData();
      let file = files[0];
      this.coverPic = file.name;
      formData.append('ServiceIcon', file, file.name);
      console.log(formData);
      console.log(files);
      this.adminservice.onfileupload(formData).subscribe(res => this.dataLoaded = res);
    }
    let date = new Date();


    const exe = {
      Id: this.servicetype.Id,
      MasterAccount_Id: this.servicetypeUpdateFormGroup.controls["servicetype.MasterAccount_Id"].value,
      ServiceIcon: "Tcsimages/prgcoverimages/" + this.coverPic,
      ServiceName: this.servicetypeUpdateFormGroup.controls["servicetype.ServiceName"].value,
      ServiceTypeStatus: this.servicetypeUpdateFormGroup.controls["servicetype.ServiceTypeStatus"].value,
      ServiceTypeCreatedBy: this.servicetype.ServiceTypeCreatedBy,
      ServiceTypeCreatedDate: this.servicetype.ServiceTypeCreatedDate,
      ServiceTypeUpdatedBy: this.createdby,
      ServiceTypeUpdatedDate: this.datepipe.transform(date, "yyyy-MM-dd")

    }

    if (this.servicetypeUpdateFormGroup.valid) {
      //  alert(JSON.stringify(accountmanagement));
      this.isSubmitted = false;
      // alert(JSON.stringify(license));

      // alert(JSON.stringify(exercisetype));
      this.adminservice.onUpdateServiceType(exe).subscribe(res => {
        console.log(res);
        this.servicetype = res
      });
      alert("Updated Successfilly");
      this.router.navigate(['/servicetype/']);
    }
    else {
      this.isSubmitted = true;
      //alert(JSON.stringify(license));
    }
    //alert(JSON.stringify(account));
  }

  UpdateServiceTypeform() {
    this.servicetypeUpdateFormGroup = this.fb.group({
      // 'themestudio.ThemeStudio_Id': ['', [Validators.required]],
      'servicetype.ServiceName': ['', [Validators.required]],
      //'servicetype.ServiceType_Id': ['', [Validators.required]],
      'servicetype.MasterAccount_Id': ['', [Validators.required]],
      'servicetype.ServiceTypeStatus': ['', [Validators.required]]



    });
  }
  onGetServiceTypeById() {

    this.adminservice.onGetServiceTypeById(this.stId).subscribe(result => {
      this.status = result;
      if (this.status.StatusCode == "1") {


        this.servicetype = this.status.Data;
      }
      else {
        alert(this.status.Data);
      }

      console.log(this.servicetype);
    });
    return this.servicetype;
  }

}


