
import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';

import { ViewServiceTypeComponent } from '../viewservicetype/viewservicetype.component';

export const ViewServiceTypeRoutes: Routes = [
  {
    path: '',
    component: ViewServiceTypeComponent,
    canActivate: [AuthGuard],
    data: {
      heading: 'ViewServiceType'
    }
  },


];

