import { Routes } from '@angular/router';

import { ThemeStudioComponent } from './themestudio.component';

export const ThemeStudioRoutes: Routes = [

  {
    path: '',
    component: ThemeStudioComponent,
    data: {
      heading: 'ThemeStudio'
    }
  },


];
