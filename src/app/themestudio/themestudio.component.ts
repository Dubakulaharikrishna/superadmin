import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl, } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ThemeStudio } from '../Models/themestudio';
import { AdminService } from '../services/admin.service';
import { CustomValidators } from 'ng2-validation';
import { DatePipe } from '@angular/common'
import 'rxjs/add/operator/debounceTime';
import { Subject } from 'rxjs/Subject';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/share';
import { CommonModule } from '@angular/common';


@Component({
  selector: 'app-themestudio',
  templateUrl: './themestudio.component.html',
  styleUrls: ['./themestudio.component.css']
})
export class ThemeStudioComponent implements OnInit {
  public data: Object;
  public temp_var: Object = false;
  public ThemeStudioUpdateFormGroup: FormGroup;
  public ThemeStudioForm: FormGroup;
  public form: FormGroup;
  themestudio: ThemeStudio;
  public isSubmitted: boolean = false;
  id: any;
  listofthemestudio: ThemeStudio[];
  pagename: any;

  constructor(private fb: FormBuilder, private adminservice: AdminService, private router: Router, private http: HttpClient) {

    this.themestudio = new ThemeStudio();
  }


  onGetAllThemeStudioList() {

    this.adminservice.onGetAllThemeStudioList().subscribe(data => {
      this.listofthemestudio = data;
      console.log(this.listofthemestudio);

    });
  }


  openCpopup() {

    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "block";
    cmask.style.display = "block";
  }

  closeCpopup() {
    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "none";
    cmask.style.display = "none";
  }

  onEdit(id: any) {


    this.router.navigate(['/viewthemestudio/', { tmsId: id }]);
  }



  ngOnInit() {
   // sessionStorage.clear();
   // localStorage.removeItem('loginSessId');
  //  localStorage.clear();
  //  localStorage.setTimeout = 1;
  //  sessionStorage.removeItem('loginSessId');
   // localStorage.removeItem('loginSessId');
    this.pagename = "ThemeStudio";
    localStorage.setItem('loginSessId', this.pagename);

    this.ThemeStudioform();
    this.onGetAllThemeStudioList();


  }
  onCreateThemeStudio(themestudio: any) {

    const ThemeStudio = {
      Client_Id: this.form.controls["Client_Id"].value,
      ThemeName: this.form.controls["ThemeName"].value,
      ThemeTitle: this.form.controls["ThemeTitle"].value,
      ThemePrimaryColor: this.form.controls["ThemePrimaryColor"].value,
      ThemeSecondaryColor: this.form.controls["ThemeSecondaryColor"].value,
      ThemeLogoUrl: this.form.controls["ThemeLogoUrl"].value,
      ThemeH1: this.form.controls["ThemeH1"].value,
      ThemeH2: this.form.controls["ThemeH2"].value,
      ThemeH3: this.form.controls["ThemeH3"].value,
      ThemeHeader: this.form.controls["ThemeHeader"].value,
      ThemeSubHeader: this.form.controls["ThemeSubHeader"].value,
      ThemeNormalText: this.form.controls["ThemeNormalText"].value,
      ThemeParagraphText: this.form.controls["ThemeParagraphText"].value,
      ThemePrimaryFont: this.form.controls["ThemePrimaryFont"].value,
      ThemeSecondaryFont: this.form.controls["ThemeSecondaryFont"].value,
      ThemeBorderColor: this.form.controls["ThemeBorderColor"].value,
      ThemeBorderWidth: this.form.controls["ThemeBorderWidth"].value,
      ThemeALink: this.form.controls["ThemeALink"].value,
      ThemeHrefLink: this.form.controls["ThemeHrefLink"].value,
      ThemeLiStyle: this.form.controls["ThemeLiStyle"].value,
      ThemeButton: this.form.controls["ThemeButton"].value,
      ThemeControlColor: this.form.controls["ThemeControlColor"].value,
      ThemeTextBoxStyle: this.form.controls["ThemeTextBoxStyle"].value,
      ThemeAreaStyle: this.form.controls["ThemeAreaStyle"].value,
      ThemeControlStyle: this.form.controls["ThemeControlStyle"].value,
      ThemeEmbeddedStyle: this.form.controls["ThemeEmbeddedStyle"].value,
      //ThemeCreatedDate: this.form.controls["ThemeCreatedDate"].value,
      //ThemeCreatedBy: this.form.controls["ThemeCreatedBy"].value,
      //ThemeUpdatedDate: this.form.controls["ThemeUpdatedDate"].value,
      //ThemeUpdatedBy: this.form.controls["ThemeUpdatedBy"].value,
      ThemeStatus: this.form.controls["ThemeStatus"].value,


    }

    if (this.form.valid) {
      this.isSubmitted = false;
      this.adminservice.onCreateThemeStudio(ThemeStudio).subscribe(res => this.themestudio = res);
      alert(JSON.stringify(ThemeStudio));
    }
    else {
      this.isSubmitted = true;
    }

  }


  ThemeStudioform() {
    this.form = this.fb.group({
      'Client_Id': ['', [Validators.required]],
      'ThemeName': ['', [Validators.required]],
      'ThemeTitle': ['', [Validators.required]],
      'ThemePrimaryColor': ['', [Validators.required]],
      'ThemeSecondaryColor': ['', [Validators.required]],
      'ThemeLogoUrl': ['', [Validators.required]],
      'ThemeH1': ['', [Validators.required]],
      'ThemeH2': ['', [Validators.required]],
      'ThemeH3': ['', [Validators.required]],
      'ThemeHeader': ['', [Validators.required]],
      'ThemeSubHeader': ['', [Validators.required]],
      'ThemeNormalText': ['', [Validators.required]],
      'ThemeParagraphText': ['', [Validators.required]],
      'ThemePrimaryFont': ['', [Validators.required]],
      'ThemeSecondaryFont': ['', [Validators.required]],
      'ThemeBorderColor': ['', [Validators.required]],
      'ThemeBorderWidth': ['', [Validators.required]],
      'ThemeALink': ['', [Validators.required]],
      'ThemeHrefLink': ['', [Validators.required]],
      'ThemeLiStyle': ['', [Validators.required]],
      'ThemeButton': ['', [Validators.required]],
      'ThemeControlColor': ['', [Validators.required]],
      'ThemeTextBoxStyle': ['', [Validators.required]],
      'ThemeAreaStyle': ['', [Validators.required]],
      'ThemeControlStyle': ['', [Validators.required]],
      'ThemeEmbeddedStyle': ['', [Validators.required]],
      //'ThemeCreatedDate': ['', [Validators.required]],
      // 'ThemeCreatedBy': ['', [Validators.required]],
      // 'ThemeUpdatedDate': ['', [Validators.required]],
      // 'ThemeUpdatedBy': ['', [Validators.required]],
      // 'RecentSecurityToken': ['', [Validators.required]],
       'ThemeStatus': ['', [Validators.required]]


    });
  }

}
