import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThemeStudioComponent } from './themestudio.component';

describe('ThemeStudioComponent', () => {
  let component: ThemeStudioComponent;
  let fixture: ComponentFixture<ThemeStudioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ThemeStudioComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThemeStudioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
