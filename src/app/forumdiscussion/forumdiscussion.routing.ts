import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';

import { ForumdiscussionComponent } from './forumdiscussion.component';

export const ForumdiscussionRoutes: Routes = [

  {
    path: '',
    component: ForumdiscussionComponent,
    canActivate: [AuthGuard],

    data: {
      heading: 'Login'
    }
  },


];

