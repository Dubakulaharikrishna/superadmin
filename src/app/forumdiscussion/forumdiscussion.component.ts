import { Component, OnInit, ElementRef } from '@angular/core';


//changes
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';
import { ExcelService } from '../services/excel.service';
import { Subject } from 'rxjs/Subject';
import { AdminService } from '../services/admin.service';
import { Status } from '../Models/Status';
import { Ng2SmartTableModule, LocalDataSource } from 'ng2-smart-table';
import { Eventmaster } from '../Models/Eventmaster';
import { Eventtype } from '../Models/Eventtype';
import { programcategory } from '../Models/programcategory';
import { Members } from '../Models/Members';
import { Forum } from '../Models/Forum';
import { DatePipe } from '@angular/common'
import { Forumdiscussion } from '../Models/forumdiscussion';
import { ForumTopic } from '../Models/ForumTopic';
import { AccountManagement } from '../Models/AccountManagement';

@Component({
  selector: 'app-forumdiscussion',
  templateUrl: './forumdiscussion.component.html',
  styleUrls: ['./forumdiscussion.component.css'],
  providers: [ExcelService, DatePipe]
})
export class ForumdiscussionComponent implements OnInit {
  MasterAccount_Id: any;
  Dfile4: any;
  success: boolean = false;
  message: any;
  dtTrigger: Subject<any> = new Subject();
  public _success = new Subject<string>();
  staticAlertClosed = false;
  Dfile3: any;
  Dfile2: any;
  Member_Id: any;
  ForumTopics_Id: any;
  Forum_Id: any;
  name: any;
  source: LocalDataSource;
  selectedValue: any = 50;
  page: any;
  createdby: any;
  listofMembers: Members[];
  dataLoaded: any;
  media: any;
  Dfile1: any;

  isSubmitted: boolean = false;
  public data: Object;
  public temp_var: Object = false;
  accountId: any;
  public form: FormGroup;
  forumdiscussion: Forumdiscussion;
  listofForumdiscussions: Forumdiscussion[];
  listofForums: Forum[];
  listofForumtopics: ForumTopic[];
  id: any;
  listofAccounts: AccountManagement[];
  pagename: any;
  roleId: any;
  status: Status;
  constructor(public router: Router, public datepipe: DatePipe, public ele: ElementRef, private excelService: ExcelService, public http: HttpClient, public fb: FormBuilder, private adminservice: AdminService) {

    this.forumdiscussion = new Forumdiscussion();
  }

  ngOnInit() {


 //   localStorage.removeItem('loginSessId');
   // localStorage.clear();
   // localStorage.setTimeout = 1;
   // sessionStorage.removeItem('loginSessId');
   // localStorage.removeItem('loginSessId');
    this.pagename = "Forum Discussion";
    localStorage.setItem('loginSessId', this.pagename);
    this.accountId = localStorage.getItem('AccountId');
    this.onGetAllMembersList();
    this.Forumdiscussionform();
    this.onGetAllForumdiscussionList(this.accountId);
    this.onGetAllForumList();
    this.onGetAllForumTopicsList();
    this.onGetAllAccountsList();
    this.roleId = localStorage.getItem('RoleId');
    setTimeout(() => this.staticAlertClosed = true, 20000);
    this._success.subscribe((message) => this.message = message);
    this._success.debounceTime(7000).subscribe(() => this.message = null);
    this.createdby = localStorage.getItem('MemberName');

  }


  onGetAllAccountsList() {
    this.adminservice.onGetAllAccountsList().subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofAccounts = this.status.Data;

        //alert(JSON.stringify(this.listofAccounts));
        return this.listofAccounts
      }

    });
  }

  onChange(event) {
    console.log(event);
    this.onGetAllForumdiscussionList(event.target.value);
  }




  reset() {
    this.onGetAllForumdiscussionList(this.accountId);
    this.MasterAccount_Id = undefined;
  }

  test(id) {
    //  alert("Hi");
    //console.log(id);
    //alert(id);
    console.log(this.Forum_Id);
   // alert(this.Forum_Id);
  }
  test1(id) {
    //  alert("Hi");
    //console.log(id);
    //alert(id);
    console.log(this.ForumTopics_Id);
   // alert(this.ForumTopics_Id);
  }


  test2(id) {
    //  alert("Hi");
    //console.log(id);
    //alert(id);
    console.log(this.Member_Id);
   // alert(this.Member_Id);
  }



  openCpopup() {

    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "block";
    cmask.style.display = "block";
  }

  closeCpopup() {
    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "none";
    cmask.style.display = "none";
  }
  onEdit(event: any) {
    this.router.navigate(['/viewforumdiscussion/', { forumId: event.data.Id }]);


    
  }


  onGetAllForumdiscussionList(id) {
    //let id = sessionStorage.getItem('AccountId');
   // alert(id);
    this.adminservice.onGetAllForumdiscussionList(id).subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofForumdiscussions = this.status.Data;
        this.source = new LocalDataSource(this.listofForumdiscussions);
        // alert(JSON.stringify(this.Programstructures));
        return this.listofForumdiscussions;
      }


    });
  }


  onGetAllForumList() {
    let id = localStorage.getItem('AccountId');
   // alert(id);
    this.adminservice.onGetAllForumList(id).subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofForums = this.status.Data;
        // alert(JSON.stringify(this.Programstructures));
        return this.listofForums;
      }


    });
  }
  onGetAllForumTopicsList() {
    let id = localStorage.getItem('AccountId');
   // alert(id);
    this.adminservice.onGetAllForumTopicsList(id).subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofForumtopics = this.status.Data;
        // alert(JSON.stringify(this.Programstructures));
        return this.listofForumtopics;
      }


    });
  }





  onGetAllMembersList() {
    let id = localStorage.getItem('AccountId');
  //  alert(id);
    this.adminservice.onGetAllMembersList(id).subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofMembers = this.status.Data;
        // alert(JSON.stringify(this.Programstructures));
        return this.listofMembers
      }


    });
  }


  onCreateForumdiscussion() {


    let files1 = this.ele.nativeElement.querySelector('#ForumDiscussionFile1').files;
    let files2 = this.ele.nativeElement.querySelector('#ForumDiscussionFile2').files;
    let files3 = this.ele.nativeElement.querySelector('#ForumDiscussionFile3').files;
    //let files4 = this.ele.nativeElement.querySelector('#ForumDiscussionFile4').files;
    if (files1.length > 0 && files1.count != 0 && files1 != null) {

      let formData = new FormData();
      let file = files1[0];
      this.Dfile1 = file.name;
      formData.append('ForumDiscussionFile1', file, file.name);
      console.log(formData);
      console.log(files1);
      this.adminservice.onfileupload(formData).subscribe(res => this.dataLoaded = res);
    }
    if (files2.length > 0 && files2.count != 0 && files2 != null) {

      let formData = new FormData();
      let file = files2[0];
      this.Dfile2 = file.name;
      formData.append('ForumDiscussionFile2', file, file.name);
      console.log(formData);
      console.log(files2);
      this.adminservice.onfileupload(formData).subscribe(res => this.dataLoaded = res);
    }
    if (files3.length > 0 && files3.count != 0 && files3 != null) {

      let formData = new FormData();
      let file = files3[0];
      this.Dfile3 = file.name;
      formData.append('ForumDiscussionFile3', file, file.name);
      console.log(formData);
      console.log(files3);
      this.adminservice.onfileupload(formData).subscribe(res => this.dataLoaded = res);
    }
    //if (files4.length > 0 && files4.count != 0 && files4 != null) {

    //  let formData = new FormData();
    //  let file = files4[0];
    //  this.Dfile4 = file.name;
    //  formData.append('EventImageUrl', file, file.name);
    //  console.log(formData);
    //  console.log(files3);
    //  this.adminservice.onfileupload(formData).subscribe(res => this.dataLoaded = res);
    //}
    



    let date = new Date();

    const Forumdiscussion = {
      Forum_Id: this.form.controls["Forum_Id"].value,
      ForumTopics_Id: this.form.controls["ForumTopics_Id"].value,
      Member_Id: this.form.controls["Member_Id"].value,

      ForumDiscussionComments: this.form.get('ForumDiscussionComments').value,

      ForumDiscussionFile1: "Tcsimages/forumdiscussionimages/" + this.Dfile1,
      ForumDiscussionFile2: "Tcsimages/forumdiscussionimages/" + this.Dfile2,
      ForumDiscussionFile3: "Tcsimages/forumdiscussionimages/" + this.Dfile3,
      //ForumDiscussionFile4: "Tcsimages/prgcoverimages/" + this.Dfile4,
     // ForumDiscussionIsModerated: this.form.controls["ForumDiscussionIsModerated"].value,
      //ForumDiscussionPositiveCounters: this.form.controls["ForumDiscussionPositiveCounters"].value,
     // ForumDiscussionNegativeCounters: this.form.controls["ForumDiscussionNegativeCounters"].value,
      ForumDiscussionStatus: this.form.controls["ForumDiscussionStatus"].value,
      ForumDiscussionCreatedBy: this.createdby,
      ForumDiscussionCreatedDate: this.datepipe.transform(date, "yyyy-MM-dd")
    }
    // alert(JSON.stringify(programManagement));
    if (this.form.valid) {
      this.isSubmitted = false;
      //  alert(JSON.stringify(programManagement));
      this.adminservice.onCreateForumdiscussion(Forumdiscussion).subscribe(data => {
        console.log(data);
        if (data.StatusCode == "1") {
          this.success = true;
          this.message = data.Message;
          this._success.next(`Record Saved Successfully`);

          this.onGetAllForumdiscussionList(this.accountId);
          this.closeCpopup();

        }
        else {
          this.success = false;
          this.message = "Failed to create account master";
        }
      });
     // alert("Successfully Submitted");
    
    }

    else {
      this.isSubmitted = true;
    }


  }



  Forumdiscussionform() {
    this.form = this.fb.group({
      'Forum_Id': ['', [Validators.required]],
      'ForumTopics_Id': ['', [Validators.required]],

      'Member_Id': ['', [Validators.required]],

      'ForumDiscussionComments': ['', [Validators.required]],
   //   'ForumDiscussionIsModerated': ['', [Validators.required]],
      //'ForumDiscussionPositiveCounters': ['', [Validators.required]],
     // 'ForumDiscussionNegativeCounters': ['', [Validators.required]],
      'ForumDiscussionStatus': ['', [Validators.required]]
     



    });
  }

  /*Smart table*/
  settings = {

    columns: {
      ForumDiscussionComments: {
        title: 'Forum Discussion Comments',
        filter: false
      },
      ForumDiscussionStatus: {
        title: 'Forum Discussion Status',
        filter: false
      },

    },
    attr: { class: 'table table-bordered' },
    actions: {
      edit: false, //as an example
      delete: false, //as an example
      add: false,
      position: 'right',
      custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button" (click)="onEdit(rec.Id)">Edit</button></span>` }]
    },
    pager: { display: true, perPage: 50 },
  };

  //route(event) {
  //  alert('hi');
  //}

  onSearch(query: string = '') {
    if (query != '') {
      this.source.setFilter([
        // fields we want to include in the search
        {
          field: 'ForumDiscussionComments',
          search: query
        },
        {
          field: 'ForumDiscussionStatus',
          search: query
        }
      ], false);
    }
    else {
      this.source = new LocalDataSource(this.listofForumdiscussions);
    }

    // second parameter specifying whether to perform 'AND' or 'OR' search 
    // (meaning all columns should contain search query or at least one)
    // 'AND' by default, so changing to 'OR' by setting false here
  }

  onSelectedFilter(event) {
    this.page = event.target.value;
    this.source = new LocalDataSource(this.listofForumdiscussions);
    this.settings = {

      columns: {
        ForumDiscussionComments: {
          title: 'ForumDiscussionComments',
          filter: false
        },
        ForumDiscussionStatus: {
          title: 'ForumDiscussion Status',
          filter: false
        },

      },
      attr: { class: 'table table-bordered' },
      actions: {
        edit: false, //as an example
        delete: false, //as an example
        add: false,
        position: 'right',
        custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button">Edit</button></span>` }]
      },
      pager: { display: true, perPage: this.page },
    };

  }
  onExport() {
    this.excelService.exportAsExcelFile(this.listofForumdiscussions, 'ForumDiscussion');
  }


}
