import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForumdiscussionComponent } from './forumdiscussion.component';

describe('ForumdiscussionComponent', () => {
  let component: ForumdiscussionComponent;
  let fixture: ComponentFixture<ForumdiscussionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForumdiscussionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForumdiscussionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
