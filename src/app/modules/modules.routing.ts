import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';

import { ModulesComponent } from './modules.component';

export const ModulesRoutes: Routes = [

  {
    path: '',
    component: ModulesComponent,
    canActivate: [AuthGuard],

    data: {
      heading: 'Modules'
    }
  },


];
