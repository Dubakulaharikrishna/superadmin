import { NgModule } from '@angular/core';
import { FormsModule, FormBuilder, FormGroup, Validators, FormControl, ReactiveFormsModule }
  from '@angular/forms';

import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ModulesRoutes } from './modules.routing';
import { ModulesComponent } from '../modules/modules.component';
import { AdminService } from '../services/admin.service';
import { DataTablesModule } from 'angular-datatables';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';
import { Ng2SmartTableModule } from 'ng2-smart-table';




@NgModule({
  imports: [
    DataTablesModule, CommonModule, NgbModule, FormsModule, ReactiveFormsModule, Ng2SmartTableModule,
    RouterModule.forChild(ModulesRoutes), HttpClientModule, HttpModule


  ],
  declarations: [

    ModulesComponent

  ],

  providers: [AdminService, AuthGuard, NotAuthGuard],
  bootstrap: [ModulesComponent]
})
export class ModulesModule { }
