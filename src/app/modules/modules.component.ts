import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl, } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Modules } from '../Models/modules';
import { AdminService } from '../services/admin.service';
import { CustomValidators } from 'ng2-validation';
import { DatePipe } from '@angular/common'
import 'rxjs/add/operator/debounceTime';
import { Subject } from 'rxjs/Subject';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/share';
import { Capabilities } from '../Models/capabilities';
import { Programstructure } from '../Models/Programstructure';
import { Status } from '../Models/Status';
import { programcategory } from '../Models/programcategory';
import { Ng2SmartTableModule, LocalDataSource } from 'ng2-smart-table';
import { ExcelService } from '../services/excel.service';


@Component({
  selector: 'app-modules',
  templateUrl: './modules.component.html',
  styleUrls: ['./modules.component.css'],
  providers: [ExcelService, DatePipe]
})
export class ModulesComponent implements OnInit {
  mediathmb: any;
  MasterAccount_Id: any;
  ProgramStr_Id: any;
  Capability_Id: any;
  selectedmodules: any = [];
  dataLoaded: any;
  mediasrc: any;
  success: boolean = false;
  message: any;
  dtTrigger: Subject<any> = new Subject();
  public _success = new Subject<string>();
  staticAlertClosed = false;
  source: LocalDataSource;
  selectedValue: any = 50;
  page: any;
  createdby: any;
  public data: Object;
  public temp_var: Object = false;
  public ModulesUpdateFormGroup: FormGroup;
  public ModulesForm: FormGroup;
  public Form: FormGroup;
  modules: Modules;
  listofcapabilities: Capabilities[];
  listofprogramstructure: Programstructure[];
  listofprogramcategory: programcategory[];
  public isSubmitted: boolean = false;
  expanded: boolean = false;
  secret: boolean = false;
  selectedcatagorys: any = [];
  id: any;
  accountId: any;
  listofAccounts: any;
  listofmodules: Modules[];
  pagename: any;
  status: Status;
  roleId: any;
  constructor(private fb: FormBuilder, public datepipe: DatePipe,  private excelService: ExcelService, private adminservice: AdminService, private router: Router, private http: HttpClient, public ele: ElementRef) {
    this.modules = new Modules();
  //  this.status = new Status();
  }

  
  onGetAllModulesList(id) {
   // let id = sessionStorage.getItem('AccountId');
    this.adminservice.onGetAllModulesList(id).subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofmodules = this.status.Data;
        // alert(JSON.stringify(this.Programstructures));
        this.source = new LocalDataSource(this.listofmodules);
        return this.listofmodules;
      }
      console.log(this.listofmodules);
    });
  }
  onGetAllCapabilitiesList() {
    let id = localStorage.getItem('AccountId');
    this.adminservice.onGetAllCapabilitiesList(id).subscribe(data => {
     
      
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofcapabilities = this.status.Data;

        return this.listofcapabilities;
      }
      console.log(this.listofcapabilities);
    });
  }

  onGetAllAccountsList() {
    this.adminservice.onGetAllAccountsList().subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofAccounts = this.status.Data;

        //alert(JSON.stringify(this.listofAccounts));
        return this.listofAccounts
      }

    });
  }

  onChange(event) {
   // alert(event);
    console.log(event);
    this.onGetAllModulesList(event.target.value);
  }
  reset() {
    this.onGetAllModulesList(this.accountId);
    this.MasterAccount_Id = undefined;
  }
  onGetAllProgramStructureList() {
    let id = localStorage.getItem('AccountId');
    this.adminservice.onGetAllProgramStructureList(id).subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofprogramstructure = this.status.Data;
        return this.listofprogramstructure;

      }
      console.log(this.listofprogramstructure);
    });

  }


  onGetAllProgramCategoryList() {
 // let id = localStorage.getItem('AccountId');
   // alert(id);
    this.adminservice.onGetAllProgramCategory().subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofprogramcategory = this.status.Data;
        return this.listofprogramcategory;

      }
    });
  }
 
  test(id) {
   
    console.log(this.ProgramStr_Id);
  //  alert(this.ProgramStr_Id);
  }
  capability(id) {
   
    console.log(this.Capability_Id);
  //  alert(this.Capability_Id);
  }



  ProgramCategorytest() {
    this.secret = true;
    var checkboxes = document.getElementById("ProgramCategory_Id");
    //  alert(this.expanded);
    if (!this.expanded) {
      checkboxes.style.display = "block";
      this.expanded = true;
    } else {
      checkboxes.style.display = "none";
      this.expanded = false;
    }


    //   alert("hi");
  }

  Programcategory(id) {

    var removedId;

    this.selectedcatagorys.forEach((existingId) => {

      if (existingId == id) {
        removedId = existingId;
        //  this.selectedcapabilities.pop(id);
      }

    });
    if (removedId != null) {
      var ob = this.selectedcatagorys.indexOf(removedId)
      this.selectedcatagorys.splice(ob, 1);
    }

    if (removedId != id) {
      this.selectedcatagorys.push(id);
    }
    

  }


  onEdit(event: any) {
    this.router.navigate(['/viewmodules/', { mmId: event.data.Id}]);
  }

  ngOnInit() {

    // sessionStorage.clear();
   // localStorage.removeItem('loginSessId');
   // localStorage.clear();
   // localStorage.setTimeout = 1;
   // sessionStorage.removeItem('loginSessId');
   // localStorage.removeItem('loginSessId');
    this.pagename = "Modules";
    localStorage.setItem('loginSessId', this.pagename);
    this.accountId = localStorage.getItem('AccountId');
    this.roleId = localStorage.getItem('RoleId');
    this.Modulesform();
    this.onGetAllModulesList(this.accountId);
    this.onGetAllCapabilitiesList();
    this.onGetAllProgramStructureList();
    this.onGetAllProgramCategoryList();
    this.Addmediaform();
    this.onGetAllAccountsList();

    setTimeout(() => this.staticAlertClosed = true, 20000);
    this._success.subscribe((message) => this.message = message);
    this._success.debounceTime(7000).subscribe(() => this.message = null);
    this.createdby = localStorage.getItem('MemberName');
  }

  Addmediaform() {
    this.Form = this.fb.group({
      subtitle: ['', Validators.compose([
        Validators.required
      ])],
      title: ['', Validators.compose([
        Validators.required
      ])],
      description: ['', Validators.compose([
        Validators.required
      ])],
      ModuleMediaType: ['', Validators.compose([
        Validators.required
      ])]
    });
  }

  Modulesform() {
    this.ModulesForm = this.fb.group({
      Capability_Id: ['', Validators.compose([
       Validators.required
      ])],
      ProgramStr_Id: ['', Validators.compose([
        Validators.required
      ])],
      ModuleTitle: ['', Validators.compose([
        Validators.required
      ])],
      ModuleType: ['', Validators.compose([
        Validators.required
      ])],
      ModuleArea: ['', Validators.compose([
        Validators.required
      ])],
      //ModuleMedia: ['', Validators.compose([
      //  Validators.required
      //])],
      ModuleAccessType: ['', Validators.compose([
        Validators.required
      ])],
      ModuleListedPrice: ['', Validators.compose([
        Validators.required
      ])],
      ModuleDiscountPrice: ['', Validators.compose([
        Validators.required
      ])],
      ModuleDisplayDate:
        ['', Validators.compose([
          Validators.required
        ])],
      ModuleValidTill:
        ['', Validators.compose([
          Validators.required
        ])],
      ModuleStatus: ['', Validators.compose([
        Validators.required
      ])],
      ModuleDependency: ['', Validators.compose([
        Validators.required
      ])],
      IsModuleTracking: [''],
      ModuleIsFree: [''],
      ProgramCategory_Id: [''],
    });
  }

  onAddMedia() {
    let files = this.ele.nativeElement.querySelector('#sources').files;
    let filesMedia = this.ele.nativeElement.querySelector('#thumb').files;

    if (files.length > 0 && files.count != 0 && files != null) {

      let formData = new FormData();
      let file = files[0];
      this.mediasrc = file.name;
      formData.append('sources', file, file.name);
      console.log(formData);
      console.log(files);
      this.adminservice.onfileupload(formData).subscribe(res => this.dataLoaded = res);
    }
    if (filesMedia.length > 0 && filesMedia.count != 0 && filesMedia != null) {

      let formData = new FormData();
      let file = filesMedia[0];
      this.mediathmb = file.name;
      formData.append('thumb', file, file.name);
      console.log(formData);
      console.log(filesMedia);
      this.adminservice.onfileupload(formData).subscribe(res => this.dataLoaded = res);
    }
 //  if (this.Form.valid) {
    const md = {
      "subtitle": (<HTMLInputElement>document.getElementById("subtitle")).value,
      "title": (<HTMLInputElement>document.getElementById("title")).value,
      "description": (<HTMLInputElement>document.getElementById("description")).value,
      "sources": "Tcsimages/prgcoverimages/" + this.mediasrc,
      "thumb": "Tcsimages/prgcoverimages/" + this.mediathmb
    }
    // var mdn = JSON.stringify(md);
    // var mdobj = JSON.parse(mdn);
    //this.selectedmodules[this.selectedmodules.length] = md;
    this.selectedmodules.push(md);
   // console.log(this.selectedmodules);
   // alert(JSON.stringify(this.selectedmodules));
  //  }
  //  else {
  //    this.isSubmitted = true;

  //  }
  }

  onCreateModules() {
    let date = new Date();
    const modules = {
      Capability_Id: this.ModulesForm.get('Capability_Id').value,
      ProgramStr_Id: this.ModulesForm.get('ProgramStr_Id').value,
       ProgramCategory_Id: this.selectedcatagorys,
      ModuleTitle: this.ModulesForm.get('ModuleTitle').value,
      ModuleType: this.ModulesForm.get('ModuleType').value,
      ModuleArea: this.ModulesForm.get('ModuleArea').value,
      // ModuleMedia: this.ModulesForm.get('ModuleMedia').value,
      ModuleAccessType: this.ModulesForm.get('ModuleAccessType').value,
      ModuleListedPrice: this.ModulesForm.get('ModuleListedPrice').value,
      ModuleDiscountPrice: this.ModulesForm.get('ModuleDiscountPrice').value,
      ModuleDisplayDate: this.ModulesForm.get('ModuleDisplayDate').value,
      ModuleValidTill: this.ModulesForm.get('ModuleValidTill').value,
      ModuleStatus: this.ModulesForm.get('ModuleStatus').value,
      IsModuleTracking: this.ModulesForm.get('IsModuleTracking').value,
      ModuleIsFree: this.ModulesForm.get('ModuleIsFree').value,
      ModuleMedia: this.selectedmodules,
      ModuleDependency: this.ModulesForm.get('ModuleDependency').value,
      ModuleCreatedBy: this.createdby,
      ModuleCreatedDate: this.datepipe.transform(date, "yyyy-MM-dd")
    }
    //console.log(modules);
    //alert(JSON.stringify(modules));
    if (this.ModulesForm.valid) {
      this.isSubmitted = false;

    
    console.log(modules);
      this.adminservice.onCreateModules(modules).subscribe(data => {
       // console.log(data);
        if (data.StatusCode == "1") {
          this.success = true;
          this.message = data.Message;
          this._success.next(`Record Saved Successfully`);

          this.onGetAllModulesList(this.accountId);
          this.closeCpopup();

        }
        else {
          this.success = false;
          this.message = "Failed to create account master";
        }
      });
     // alert("Submitted Successfully");
    
    }
    else {
      this.isSubmitted = true;

    }

  }
  openCpopup() {

    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "block";
    cmask.style.display = "block";
  }

  closeCpopup() {
    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "none";
    cmask.style.display = "none";
  }
  /*Smart table*/
  settings = {

    columns: {
      ModuleTitle: {
        title: 'ModuleTitle',
        filter: false
      },
      ModuleType: {
        title: 'ModuleType',
        filter: false
      },

    },
    attr: { class: 'table table-bordered' },
    actions: {
      edit: false, //as an example
      delete: false, //as an example
      add: false,
      position: 'right',
      custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button" (click)="onEdit(rec.Id)">Edit</button></span>` }]
    },
    pager: { display: true, perPage: 50 },
  };

  //route(event) {
  //  alert('hi');
  //}

  onSearch(query: string = '') {
    if (query != '') {
      this.source.setFilter([
        // fields we want to include in the search
        {
          field: 'ModuleTitle',
          search: query
        },
        {
          field: 'ModuleType',
          search: query
        }
      ], false);
    }
    else {
      this.source = new LocalDataSource(this.listofmodules);
    }

    // second parameter specifying whether to perform 'AND' or 'OR' search 
    // (meaning all columns should contain search query or at least one)
    // 'AND' by default, so changing to 'OR' by setting false here
  }

  onSelectedFilter(event) {
    this.page = event.target.value;
    this.source = new LocalDataSource(this.listofmodules);
    this.settings = {

      columns: {
        ModuleTitle: {
          title: 'ModuleTitle',
          filter: false
        },
        ModuleType: {
          title: 'ModuleType',
          filter: false
        },

      },
      attr: { class: 'table table-bordered' },
      actions: {
        edit: false, //as an example
        delete: false, //as an example
        add: false,
        position: 'right',
        custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button">Edit</button></span>` }]
      },
      pager: { display: true, perPage: this.page },
    };

  }


  onExport() {
    this.excelService.exportAsExcelFile(this.listofmodules, 'Modules');
  }


}


