import { Routes } from '@angular/router';

import { EventmanagementComponent } from './eventmanagement.component';

export const EventManagementRoutes: Routes = [

  {
    path: '',
    component: EventmanagementComponent,
    data: {
      heading: 'Eventmanagement'
    }    
  },

  
];

