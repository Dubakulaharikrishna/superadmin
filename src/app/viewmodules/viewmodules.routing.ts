import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';
import { ViewModulesComponent } from './viewmodules.component';

export const ViewModulesRoutes: Routes = [

  {
    path: '',
    component: ViewModulesComponent,
    canActivate: [AuthGuard],

    data: {
      heading: 'ViewModules'
    }
  },


];
