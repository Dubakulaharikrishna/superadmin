import { NgModule } from '@angular/core';

import { RouterModule } from '@angular/router';
import { ViewModulesRoutes } from './viewmodules.routing';
import { ViewModulesComponent } from '../viewmodules/viewmodules.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { HttpModule } from '@angular/http';
import { DataTablesModule } from 'angular-datatables';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, FormBuilder, FormGroup, Validators, FormControl, ReactiveFormsModule }
  from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
//import { ProgramcontentComponent } from '../programcontent/programcontent.component';
import { AdminService } from '../services/admin.service';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';
import { CustomFormsModule } from 'ng2-validation';



@NgModule({
  imports: [
    RouterModule.forChild(ViewModulesRoutes),
  
   
     DataTablesModule,
    HttpClientModule,
    CommonModule,
    FormsModule,
    CustomFormsModule,
    NgbModule,
    ReactiveFormsModule,

    HttpModule
  ],
  declarations: [

    ViewModulesComponent

  ],

  providers: [AdminService, AuthGuard, NotAuthGuard],
  bootstrap: [ViewModulesComponent]
})
export class ViewModulesModule { }
