import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';
import { Input, ElementRef, ViewChild } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { Router, ActivatedRoute } from '@angular/router';
import { AdminService } from '../services/admin.service';
import { NgModule } from '@angular/core';

import { Modules } from '../Models/modules';
import { retry } from 'rxjs/operator/retry';
import { DatePipe } from '@angular/common';
import { Status } from '../Models/Status';
import { Capabilities } from '../Models/capabilities';
import { Programstructure } from '../Models/Programstructure';
import { programcategory } from '../Models/programcategory';

@Component({
  selector: 'app-viewmodules',
  templateUrl: './viewmodules.component.html',
  styleUrls: ['./viewmodules.component.css'],
  providers: [DatePipe]
})
export class ViewModulesComponent implements OnInit {
  validDate: any;
  pagename: any;
  disDate: any;
  public viewmodulesUpdateFormGroup: FormGroup;
  public Form: FormGroup;
  ModulesForm: FormGroup;
  viewmodules: Modules;
  dataLoaded: any;
  mediapic: any;
  listofcapabilities: Capabilities[];
  listofprogramstructure: Programstructure[];
  listofprogramcategory: programcategory[];
  public isSubmitted: boolean = false;
  id: any;
  ProgramStr_Id: any;
  Capability_Id: any;
  mmId: any;
  createdby: any;
  expanded: boolean = false;
  secret: boolean = false;
  selectedcatagorys: any = [];
  selectedmodules: any = [];
  status: Status;
  constructor(private adminservice: AdminService, private router: Router,
    public route: ActivatedRoute, private http: HttpClient, public ele: ElementRef, public fb: FormBuilder, public datepipe: DatePipe) {
    this.viewmodules = new Modules();
    this.status = new Status();
  }

  ngOnInit() {
    this.Modulesform();
    this.route.params.subscribe(params => {
      this.mmId = params['mmId'];
      this.pagename = "Modules";
    
    });
    this.onGetmodulesById();
    this.createdby = localStorage.getItem('MemberName');
  }


  onGetAllCapabilitiesList() {
    let id = localStorage.getItem('AccountId');
    this.adminservice.onGetAllCapabilitiesList(id).subscribe(data => {


      if (data.StatusCode == "1") {
        this.status = data;
        this.listofcapabilities = this.status.Data;

        return this.listofcapabilities;
      }
      console.log(this.listofcapabilities);
    });
  }


  onGetAllProgramStructureList() {
    let id = localStorage.getItem('AccountId');
    this.adminservice.onGetAllProgramStructureList(id).subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofprogramstructure = this.status.Data;
        return this.listofprogramstructure;

      }
      console.log(this.listofprogramstructure);
    });

  }


  onGetAllProgramCategoryList() {
    let id = localStorage.getItem('AccountId');
    alert(id);
    this.adminservice.onGetAllProgramCategory().subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofprogramcategory = this.status.Data;
        return this.listofprogramcategory;

      }
    });
  }

  test(id) {
    //  alert("Hi");
    //console.log(id);
    //alert(id);
    console.log(this.ProgramStr_Id);
    alert(this.ProgramStr_Id);
  }
  capability(id) {
    //  alert("Hi");
    //console.log(id);
    //alert(id);
    console.log(this.Capability_Id);
    alert(this.Capability_Id);
  }







  ProgramCategorytest() {
    this.secret = true;
    var checkboxes = document.getElementById("ProgramCategory_Id");
    //  alert(this.expanded);
    if (!this.expanded) {
      checkboxes.style.display = "block";
      this.expanded = true;
    } else {
      checkboxes.style.display = "none";
      this.expanded = false;
    }


    //   alert("hi");
  }

  Programcategory(id) {

    var removedId;

    this.selectedcatagorys.forEach((existingId) => {

      if (existingId == id) {
        removedId = existingId;
        //  this.selectedcapabilities.pop(id);
      }

    });
    if (removedId != null) {
      var ob = this.selectedcatagorys.indexOf(removedId)
      this.selectedcatagorys.splice(ob, 1);
    }

    if (removedId != id) {
      this.selectedcatagorys.push(id);
    }


  }


  onAddMedia() {
    let files = this.ele.nativeElement.querySelector('#ModuleMediaFilePath').files;
    if (files.length > 0 && files.count != 0 && files != null) {

      let formData = new FormData();
      let file = files[0];
      this.mediapic = file.name;
      formData.append('CapabilityCoverPicture', file, file.name);
      console.log(formData);
      console.log(files);
      this.adminservice.onfileupload(formData).subscribe(res => this.dataLoaded = res);
    }
    //  if (this.Form.valid) {
    const md = {
      ModuleMediaName: (<HTMLInputElement>document.getElementById("ModuleMediaName")).value,
      ModuleMediaType: (<HTMLInputElement>document.getElementById("ModuleMediaType")).value,
      //ModuleMediaName: this.Form.get('ModuleMediaName').value,
      //ModuleMediaType: this.Form.get('ModuleMediaType').value,
      ModuleMediaFilePath: "Tcsimages/prgcoverimages/" + this.mediapic,
    }
    // alert(JSON.stringify(md));
    this.selectedmodules.push(md);
    alert(JSON.stringify(this.selectedmodules));
    //  }
    //  else {
    //    this.isSubmitted = true;

    //  }
  }



  onUpdatemodules(viewmodules) {
    let date = new Date();
    const account = {
      Capability_Id: this.viewmodulesUpdateFormGroup.get('viewmodules.Capability_Id').value,
      ProgramStr_Id: this.viewmodulesUpdateFormGroup.get('viewmodules.ProgramStr_Id').value,
      ProgramCategory_Id: this.selectedcatagorys,
      ModuleTitle: this.viewmodulesUpdateFormGroup.get('viewmodules.ModuleTitle').value,
      ModuleType: this.viewmodulesUpdateFormGroup.get('viewmodules.ModuleType').value,
      ModuleArea: this.viewmodulesUpdateFormGroup.get('viewmodules.ModuleArea').value,
      // ModuleMedia: this.ModulesForm.get('ModuleMedia').value,
      ModuleAccessType: this.viewmodulesUpdateFormGroup.get('viewmodules.ModuleAccessType').value,
      ModuleListedPrice: this.viewmodulesUpdateFormGroup.get('viewmodules.ModuleListedPrice').value,
      ModuleDiscountPrice: this.viewmodulesUpdateFormGroup.get('viewmodules.ModuleDiscountPrice').value,
      ModuleDisplayDate: this.viewmodulesUpdateFormGroup.get('viewmodules.ModuleDisplayDate').value,
      ModuleValidTill: this.viewmodulesUpdateFormGroup.get('viewmodules.ModuleValidTill').value,
      ModuleStatus: this.viewmodulesUpdateFormGroup.get('viewmodules.ModuleStatus').value,
      IsModuleTracking: this.viewmodulesUpdateFormGroup.get('viewmodules.IsModuleTracking').value,
      ModuleIsFree: this.viewmodulesUpdateFormGroup.get('viewmodules.ModuleIsFree').value,
      ModuleMedia: this.selectedmodules,
      ModuleDependency: this.viewmodulesUpdateFormGroup.get('viewmodules.ModuleDependency').value,
      ModuleCreatedBy: this.viewmodules.ModuleCreatedBy,
      ModuleCreatedDate: this.viewmodules.ModuleCreatedDate,
      ModuleUpdatedBy: this.createdby,
      ModuleUpdatedDate: this.datepipe.transform(date, "yyyy-MM-dd")
    }



    if (this.viewmodulesUpdateFormGroup.valid) {

      this.isSubmitted = false;
     // alert(JSON.stringify(viewmodules));
      this.adminservice.onUpdateModules(viewmodules).subscribe(res => {
        console.log(res);

       
      });
      alert("Successfully Updated");
      this.router.navigate(['/modules/']);
    }
    else {
      this.isSubmitted = true;

    }

  }




  Modulesform() {
    this.viewmodulesUpdateFormGroup = this.fb.group({

      'viewmodules.ProgramStr_Id': ['', [Validators.required]],
      'viewmodules.ProgramCategory_Id':[''],
      'viewmodules.Capability_Id': ['', [Validators.required]],
      'viewmodules.ModuleTitle': ['', [Validators.required]],
      'viewmodules.ModuleType': ['', [Validators.required]],
      'viewmodules.ModuleArea': ['', [Validators.required]],
     // 'viewmodules.ModuleMedia': ['', [Validators.required]],
      'viewmodules.ModuleAccessType': ['', [Validators.required]],
      'viewmodules.ModuleListedPrice': ['', [Validators.required]],
      'viewmodules.ModuleDiscountPrice': ['', [Validators.required]],
      'viewmodules.ModuleDisplayDate': ['', [Validators.required]],
      'viewmodules.ModuleValidTill': ['', [Validators.required]],
      'viewmodules.ModuleIsFree': ['', [Validators.required]],
      'viewmodules.ModuleStatus': ['', [Validators.required]],
      'viewmodules.IsModuleTracking': ['', [Validators.required]],
      'viewmodules.ModuleDependency': ['', [Validators.required]],


    });
  }

  Addmediaform() {
    this.Form = this.fb.group({
      ModuleMediaName: ['', Validators.compose([
        Validators.required
      ])],
      ModuleMediaType: ['', Validators.compose([
        Validators.required
      ])]

    });
  }

  onGetmodulesById() {

    this.adminservice.onGetModulesById(this.mmId).subscribe(result => {
      this.status = result;
      if (this.status.StatusCode == "1") {


        this.viewmodules = this.status.Data;
      }
      else {
        alert(this.status.Data);
      }


      console.log(this.viewmodules);

      if (this.viewmodules.ModuleDisplayDate != null && this.viewmodules.ModuleDisplayDate != "" && this.viewmodules.ModuleDisplayDate != undefined) {
        this.disDate = this.datepipe.transform(this.viewmodules.ModuleDisplayDate, 'yyyy-MM-dd');
        this.viewmodules.ModuleDisplayDate = this.disDate;
      }

      if (this.viewmodules.ModuleValidTill != null && this.viewmodules.ModuleValidTill != "" && this.viewmodules.ModuleValidTill != undefined) {
        this.validDate = this.datepipe.transform(this.viewmodules.ModuleValidTill, 'yyyy-MM-dd');
        this.viewmodules.ModuleValidTill = this.validDate;
      }
      this.viewmodules.ModuleMedia = JSON.parse(this.viewmodules.ModuleMedia[0]);

    });
    return this.viewmodules;
  }

}
