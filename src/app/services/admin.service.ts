import { Component, Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { HttpParams, HttpHeaders } from '@angular/common/http'; 
import { Subject } from 'rxjs/Rx';
import { AccountManagement } from '../Models/AccountManagement';
import { HttpClient } from '@angular/common/http';
import { InstanceManager } from '../Models/InstanceManager';
import { Webclient } from '../Models/Webclient';
import { Members } from '../Models/Members';
//import { programs } from '../Models/programs';
import { accountprograms } from '../Models/accountprograms';
import { License } from '../Models/license';
import { AccountMembership } from '../Models/accountmembership';
import { contentcategory } from '../Models/contentcategory';
import { programcategory } from '../Models/programcategory';
import { ExerciseQuestion } from '../Models/ExerciseQuestion';
import { ExerciseAnswer } from '../Models/ExerciseAnswer ';
import { ThemeStudio } from '../Models/themestudio';
import { Exercise } from '../Models/Exercise';
import { ExerciseType } from '../Models/ExerciseType ';
import { Programstructure } from '../Models/Programstructure';
import { tokenNotExpired } from 'angular2-jwt';
import { Capabilities } from '../Models/capabilities';
import { Modules } from '../Models/modules';
//import { Capabilities } from 'selenium-webdriver';


//import { Injectable } from '@angular/core';

@Injectable()
export class AdminService {
  public isUserLoggedIn: Subject<any>;
  result: any;
  id: number;
  authToken;
  user;
  options;
  response: Response;
  //baseUrl = "http://52.77.87.237:8080/";
 // baseUrl = "http://localhost:50240/";
 // baseUrl = "http://52.77.61.221:4311/api/";
 // baseUrl = "http://123.176.43.171:4311/api/";
  baseUrl = "http://139.162.7.182:4311/api/";
  constructor(private http: Http) {
    this.isUserLoggedIn = new Subject<any>();
  }
 


  createAuthenticationHeaders() {

    this.loadToken();
    this.options = new RequestOptions({
      headers: new Headers({
        'Content-Type': 'application/json',
        'authorization': this.authToken
      })
    });
  }

  loadToken() {
    this.authToken = localStorage.getItem('token');
  }

  logout() {
    this.authToken = null;
    this.user = null;
    localStorage.clear();
    sessionStorage.clear();
  }

  storeUserData(token, email) {
    localStorage.setItem('token', token);
    localStorage.setItem('user', JSON.stringify(email));
    this.authToken = token;
    this.user = email;
  }

  loggedIn() {
    return true;
   // return tokenNotExpired();
  }



  //loggedIn() {
  //  return "jjj";
  // // return tokenNotExpired();
  //}

  //onLogin(Email, Password) {
  //  let headers = new Headers({
  //    'Content-Type': 'application/x-www-form-urlencoded'
  //  });

  //  var params = new URLSearchParams();
  //  params.set('email', Email);
  //  params.set('password', Password);
  //  // return this.http.post('http://52.77.87.237:8080/api/TCSCore/AdminLogin', params.toString(), {
  //  return this.http.post(this.baseUrl +'api/TCSCore/GenerateTokens', params.toString(), {
  //    headers: headers
  //  }).map(res =>
  //    res.json());

  //}

  onLogin(Email, Password) {
    let body: any = {};
    body.UserLoginId = Email;
    body.Password=Password;
    return this.http.post(this.baseUrl + 'masterAdmin/login', body).map(res =>
      res.json());
  }

  onGetAllAccountsList(): Observable<any> {

    return this.http.get(this.baseUrl +'admin/accountMasterManager/allAccountMasterInfo').map((res: Response) =>
      res.json());
  }

  onGetAccountsById(id: any): Observable<any> {
  //  alert(id);
    console.log("service id" + id);
    return this.http.get(this.baseUrl +'admin/accountMasterManager/getAccountMasterDetails/' + id + '').map((res: Response) =>
   
      res.json());
  }


  onCreateAccount(account) {

    let body: any = {};
    body.InstanceManagerId = account.InstanceManagerId;
    body.AccountName = account.AccountName;
    body.AccountWebUrl = account.AccountWebUrl;
  //  body.IsAndroidAppPublic = account.IsAndroidAppPublic;
  //  body.IsIOSAppPublic = account.IsIOSAppPublic;
    body.DevUrl = account.DevUrl;
//    body.IsLiveStatus = account.IsLiveStatus;
    body.ClientInfo = account.ClientInfo;
    body.InstanceInfo = account.InstanceInfo;
    body.LicenseInfo = account.LicenseInfo;
    body.AppUrl = account.AppUrl;
  //  body.IsMobileEnabled = account.IsMobileEnabled;
    body.AccountAdmin = account.AccountAdmin;
 //   body.IsHttpsEnabled = account.IsHttpsEnabled;
    body.CreatedBy = account.CreatedBy;
 //   body.AccountCreatedDate = account.AccountCreatedDate;

    return this.http.post(this.baseUrl + 'admin/accountMasterManager/createAccountMaster', body).map(res =>
      res.json());
  }
  onUpdateAccount(account) {
    return this.http.put(this.baseUrl + 'admin/accountMasterManager/updateAccountMaster/' + account._id + '', account).map(res =>
      res.json());
  }


  //** Instance manager **//
  onGetAllInstanceManagerList(): Observable<any> {
    return this.http.get(this.baseUrl +'admin/instanceManager/allInsatncesInfo').map((res: Response) =>
  //  return this.http.get('http://localhost:50240/api/tcscore/GetAllServerInstances').map((res: Response) =>
      res.json());

  }

  onGetInstanceManagerById(id: any): Observable<any> {
    console.log("id" + id);
    return this.http.get(this.baseUrl + 'admin/instanceManager/getInstanceDetails/' + id + '').map((res: Response) => res.json());
   // return this.http.get('http://localhost:50240/api/tcscore/GetServerInstanceById?instanceid=' + id + '').map((res: Response) =>
     
  }


  onCreateInstanceManager(instance: any) {
    let body: any = {};
    body.InstanceManagerId = instance.InstanceManagerId;
    body.InstanceName = instance.InstanceName;
    body.ServerName = instance.ServerName;
    body.OSDetails = instance.OSDetails;
    body.InstanceAdmin = instance.InstanceAdmin;
    body.SetupDateTime = instance.SetupDateTime;
    body.AssociatedLicenseInfo = instance.AssociatedLicenseInfo;
    body.LicenseExpiryDate = instance.LicenseExpiryDate;
    body.LicenseType = instance.LicenseType;
    body.NumberofClients = instance.NumberofClients;
    body.NumberOfAccounts = instance.NumberOfAccounts;
    body.InstancePublicIPAddress = instance.InstancePublicIPAddress;
    body.InstancePrivateIPAddress = instance.InstancePrivateIPAddress;
    body.CreatedBy = instance.CreatedBy;
    body.CreatedDate = instance.CreatedDate
    return this.http.post(this.baseUrl + 'admin/instanceManager/createInstance', body).map(res =>
      res.json());
   
  }


  onUpdateInstanceManager(instance) {


    return this.http.put(this.baseUrl + 'admin/instanceManager/updateInstance/' + instance._id + '', instance).map(res =>
      res.json());
    
  }
                          //**    Webclient Start   **//
  onGetAllWebClientList(): Observable<any> {
    return this.http.get(this.baseUrl +'admin/webClientManager/allWebClients').map((res: Response) =>
 // return this.http.get('http://localhost:50240/api/tcscore/GetAllWebClients').map((res: Response) =>

      res.json());
    // console.log(res);
  }

  onGetWebClientById(id: any): Observable<any> {
    console.log("id" + id);
    return this.http.get(this.baseUrl +'admin/webClientManager/getWebClient/' + id + '').map((res: Response) =>
 //  return this.http.get('http://localhost:50240/api/tcscore/GetWebClientById?webclientid=' + id + '').map((res: Response) =>
      res.json());
  }

  onCreateWebClient(webclient: any) {

    let body: any = {};
    body.InstanceManagerId = webclient.InstanceManagerId;
    body.CompanyName = webclient.CompanyName;
    body.LegalName = webclient.LegalName;
    body.CompanyAddressLine1 = webclient.CompanyAddressLine1;
    body.CompanyAddressLine2 = webclient.CompanyAddressLine2;
    body.CompanyRegistrationNumber = webclient.CompanyRegistrationNumber;
    body.CompanyEmailID = webclient.CompanyEmailID;
    body.CompanyAdminEmailID = webclient.CompanyAdminEmailID;
    body.CompanyType = webclient.CompanyType;
    body.CompanyEstablishedDate = webclient.CompanyEstablishedDate;
   // body.Instance_Id = webclient.Instance_Id;
    //  body.LicenseInfo = webclient.LicenseInfo;
    body.CompanyPhone = webclient.CompanyPhone;
    body.DisplayNameOfCompany = webclient.DisplayNameOfCompany;
    body.LogoUrl = webclient.LogoUrl;
    body.AdminUserName = webclient.AdminUserName;
    body.PrimaryAdminEmailID = webclient.PrimaryAdminEmailID;
    body.CompanyPersonName = webclient.CompanyPersonName;
    body.CompanyPersonEmailID = webclient.CompanyPersonEmailID;
    body.AdminPhoneNumber = webclient.AdminPhoneNumber;
    body.CompanyPersonPhoneNumber = webclient.CompanyPersonPhoneNumber;
    body.CreatedBy = webclient.CreatedBy;
  // body.ClientCreatedDate = webclient.ClientCreatedDate;
    body.AdminPersonDesignation = webclient.AdminPersonDesignation;
    // console.log(body);
    return this.http.post(this.baseUrl + 'admin/webClientManager/createWebClient', body).map(res =>
      res.json());
  
  }
  onUpdateWebClient(webclient) {

    return this.http.put(this.baseUrl + 'admin/webClientManager/updateWebClient/' + webclient._id + '', webclient).map(res =>
      res.json());
   
  
  }
  ///*** WebClient Ends ***///

  ///*** Members Starts  ***///

  onGetAllMembersList(id: any): Observable<any> {
    console.log("service id" + id);

    return this.http.get(this.baseUrl + 'member/getAdminPenalMemberLists/' + id + '').map((res: Response) =>
      res.json());
    // console.log(res);
  }

  onGetMembersById(id: any): Observable<any> {
    console.log("id" + id);
    return this.http.get(this.baseUrl +'member/getMemberProfileById/' + id + '').map((res: Response) =>
   // return this.http.get('http://localhost:50240/api/tcscore/GetMemberById?memberid=' + id + '').map((res: Response) =>
    
      res.json());

  }



  onUpdateMembers(members, formData) {
    let body: any = {};
    formData.append("_id", members._id);
    formData.append("UserName", members.UserName);
    //formData.append("Password", members.Password);
    // formData.append("ConfirmPassword", members.ConfirmPassword);
    formData.append("MobileNumber", members.MobileNumber);
    formData.append("Email", members.Email);
    formData.append("IsMobileVerified", members.IsMobileVerified);
    formData.append("IsEmailVerified", members.IsEmailVerified);
    formData.append("FirstName", members.FirstName);
    formData.append("LastName", members.LastName);
    formData.append("MiddleName", members.MiddleName);
    formData.append("DOB", members.DOB);
    formData.append("Gender", members.Gender);
    formData.append("Industry", members.Industry);
    formData.append("Profession", members.Profession);
    formData.append("AddressLine1", members.AddressLine1);
    formData.append("AddressLine2", members.AddressLine2);
    formData.append("City", members.City);
    formData.append("State", members.State);
    formData.append("ZipCode", members.ZipCode);
    formData.append("Country", members.Country);
    formData.append("CompanyName", members.CompanyName);
    formData.append("CompanyAddressLine1", members.CompanyAddressLine1);
    formData.append("CompanyAddressLine2", members.CompanyAddressLine2);
    formData.append("CompanyCity", members.CompanyCity);
    formData.append("CompanyCountry", members.CompanyCountry);
    formData.append("CompanyEmail", members.CompanyEmail);
    formData.append("AlternatePhoneNumber", members.AlternatePhoneNumber);
    formData.append("BloodGroup", members.BloodGroup);
    formData.append("FamilyType", members.FamilyType);
    formData.append("Origin", members.Origin);
    formData.append("TimeZone", members.TimeZone);
    formData.append("Hobbies", members.Hobbies);
    formData.append("MaritalStatus", members.MaritalStatus);
    formData.append("ImageUrl", members.ImageUrl);
    formData.append("MemberStatus", members.MemberStatus);
    formData.append("CreatedBy", members.CreatedBy);
    formData.append("UpdatedBy", members.UpdatedBy);
    return this.http.put(this.baseUrl + 'member/updateMemberByAdmin/' + members._id + '', formData).map(res =>
      res.json())
  }


  onChangePassword(members: any) {

    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });

    var params = new URLSearchParams();
    params.set('Member_Id', members.Id);
    params.set('Password', members.Password);
    params.set('ConfirmPassword', members.ConfirmPassword);
  

    return this.http.post(this.baseUrl +'api/TCSCore/ChangePassword', params.toString(), {
  //  return this.http.post('http://52.77.87.237:8080/api/TCSCore/ChangePassword', params.toString(), {
      headers: headers
    }).map(res =>
      res.json());
  }
  onCreateMembers(members, formData) {
    let body: any = {};
    formData.append("UserName", members.UserName);
    formData.append("Password", members.Password);
   // formData.append("ConfirmPassword", members.ConfirmPassword);
    formData.append("MobileNumber", members.MobileNumber);
    formData.append("Email", members.Email);
    formData.append("IsMobileVerified", members.IsMobileVerified);
    formData.append("IsEmailVerified", members.IsEmailVerified);
    formData.append("FirstName", members.FirstName);
    formData.append("LastName", members.LastName);
    formData.append("MiddleName", members.MiddleName);
    formData.append("DOB", members.DOB);
    formData.append("Gender", members.Gender);
    formData.append("Industry", members.Industry);
    formData.append("Profession", members.Profession);
    formData.append("AddressLine1", members.AddressLine1);
    formData.append("AddressLine2", members.AddressLine2);
    formData.append("City", members.City);
    formData.append("State", members.State);
    formData.append("ZipCode", members.ZipCode);
    formData.append("Country", members.Country);
    formData.append("CompanyName", members.CompanyName);
    formData.append("CompanyAddressLine1", members.CompanyAddressLine1);
    formData.append("CompanyAddressLine2", members.CompanyAddressLine2);
    formData.append("CompanyCity", members.CompanyCity);
    formData.append("CompanyCountry", members.CompanyCountry);
    formData.append("CompanyEmail", members.CompanyEmail);
    formData.append("AlternatePhoneNumber", members.AlternatePhoneNumber);
    formData.append("BloodGroup", members.BloodGroup);
    formData.append("FamilyType", members.FamilyType);
    formData.append("Origin", members.Origin);
    formData.append("TimeZone", members.TimeZone);
    formData.append("Hobbies", members.Hobbies);
    formData.append("MaritalStatus", members.MaritalStatus);
    formData.append("ImageUrl", members.ImageUrl);
    formData.append("MemberStatus", members.MemberStatus);
    formData.append("CreatedBy", members.CreatedBy);
    //formData.append("CreatedDate", members.CreatedDate)
    console.log(formData);
    return this.http.post(this.baseUrl + 'member/createMember1', formData).map((response: any) => response);

  }
  //onCreateMembers(members: any) {


  //  let body: any = {};
  //  body.UserName = members.UserName;
  //  body.Password = members.Password;
  //  body.MobileNumber = members.MobileNumber;
  //  body.Email = members.Email;
  //  body.IsMobileVerified = members.IsMobileVerified;
  //  body.IsEmailVerified = members.IsEmailVerified;
  //  body.FirstName = members.FirstName;
  //  body.LastName = members.LastName;
  //  body.MiddleName = members.MiddleName;
  //  body.DOB = members.DOB;
  //  body.Gender = members.Gender;
  //  body.Industry = members.Industry;
  //  body.Profession = members.Profession;
  //  body.AddressLine1 = members.AddressLine1;
  //  body.AddressLine2 = members.AddressLine2;
  //  body.City = members.City;
  //  body.State = members.State;
  //  body.ZipCode = members.ZipCode;
  //  body.Country = members.Country;
  //  body.CompanyName = members.CompanyName;
  //  body.CompanyAddressLine1 = members.CompanyAddressLine1;
  //  body.CompanyAddressLine2 = members.CompanyAddressLine2;
  //  body.CompanyCity = members.CompanyCity;
  //  body.CompanyCountry = members.CompanyCountry;
  //  body.CompanyEmail = members.CompanyEmail;
  //  body.AlternatePhoneNumber = members.AlternatePhoneNumber;
  //  body.BloodGroup = members.BloodGroup;
  //  body.FamilyType = members.FamilyType;
  //  body.Origin = members.Origin;
  //  body.TimeZone = members.TimeZone;
  //  body.Hobbies = members.Hobbies;
  //  body.MaritalStatus = members.MaritalStatus;
  //  body.MemberStatus = members.MemberStatus;
  //  body.imageFile = members.ImageUrl;
  //  body.CreatedBy = members.CreatedBy;
  //  body.CreatedDate = members.CreatedDate;

  //  return this.http.post(this.baseUrl + 'member/createMember1', body).map(res =>
  //    res.json());

  //}
  ///*** Members Ends ***////


  ///*** ProgramContent ***///
  onGetAllModulesList(id: any): Observable<any> {
   // return this.http.get('http://52.77.87.237:8080/api/TCSCore/GetAllModules?AccountId='+ id + '').map((res: Response) =>
    return this.http.get(this.baseUrl +'api/TCSCore/GetAllModules?AccountId=' + id + '').map((res: Response) =>
      res.json());
    // console.log(res);
  }
  onGetModulesById(id: any): Observable<any> {
    console.log("id" + id);
   // return this.http.get('http://52.77.87.237:8080/api/TCSCore/GetModuleByID?moduleid=' + id + '')
    return this.http.get(this.baseUrl +'api/TCSCore/GetModuleByID?moduleid=' + id + '')
      .map((res: Response) =>
        res.json());
  }
  onUpdateModules(modules) {

    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });

    var params = new URLSearchParams();
    params.set('Module_Id', modules.Id);
    params.set('Capability_Id', modules.Capability_Id);
    params.set('ProgramStr_Id', modules.ProgramStr_Id);
    params.set('ProgramCategory_Id', modules.ProgramCategory_Id);
    params.set('ModuleTitle', modules.ModuleTitle);
    params.set('ModuleType', modules.ModuleType);
    params.set('ModuleDependency', modules.ModuleDependency);
    params.set('ModuleArea', modules.ModuleArea);
    //for (let ModuleMedia in json) {
    //  params.set('ModuleMedia', [ ModuleMedia ]);
    //}
    //for (let item of modules.ModuleMedia) {
    //  params.append('ModuleMedia', item);
    //}
    console.log(modules.ModuleMedia);
    params.set('ModuleMedia', JSON.stringify(modules.ModuleMedia));
    params.set('IsModuleTracking', modules.IsModuleTracking);
    params.set('ModuleAccessType', modules.ModuleAccessType);
    params.set('ModuleDiscountPrice', modules.ModuleDiscountPrice);
    params.set('ModuleListedPrice', modules.ModuleListedPrice);
    params.set('ModuleDisplayDate', modules.ModuleDisplayDate);
    params.set('ModuleValidTill', modules.ModuleValidTill);
    params.set('ModuleCreatedBy', modules.ModuleCreatedBy);
    params.set('ModuleCreatedDate', modules.ModuleCreatedDate);
    params.set('ModuleUpdatedBy', modules.ModuleUpdatedBy);
    params.set('ModuleUpdatedDate', modules.ModuleUpdatedDate);
    params.set('ModuleStatus', modules.ModuleStatus);
    params.set('ModuleIsFree', modules.ModuleIsFree);

    return this.http.post(this.baseUrl +'api/TCSCore/UpdateModule', params.toString(), {
        //return this.http.post('http://localhost:50240/api/TCSCore/UpdateProgramContent', params.toString(), {
      headers: headers
    }).map(res => res.json());


  }
  onCreateModules(modules) {
  //  //let headers = new Headers({
  //  //  'Content-Type': 'application/json'

  //  //});
  ////  const headers = new HttpHeaders().set('content-type', 'application/json');
  //  let body: any = {};
  //  body.Capability_Id = modules.Capability_Id;

  //  body.ProgramStr_Id = modules.ProgramStr_Id;
  //  body.ProgramCategory_Id = modules.ProgramCategory_Id;
  //  body.ModuleTitle = modules.ModuleTitle;
  //  body.ModuleType = modules.ModuleType;
  //  body.ModuleDependency = modules.ModuleDependency;

  //  body.ModuleArea = modules.ModuleArea;
  //  body.ModuleMedia = modules.ModuleMedia;
  //  body.IsModuleTracking = modules.IsModuleTracking;
  //  body.ModuleAccessType = modules.ModuleAccessType;

  //  body.ModuleDiscountPrice = modules.ModuleDiscountPrice;
  //  body.ModuleListedPrice = modules.ModuleListedPrice;
  //  body.ModuleDisplayDate = modules.ModuleDisplayDate;
  //  body.ModuleValidTill = modules.ModuleValidTill;

  //  body.ModuleCreatedBy = modules.ModuleCreatedBy;
  //  body.ModuleCreatedDate = modules.ModuleCreatedDate;
  //  body.ModuleUpdatedBy = modules.ModuleUpdatedBy;
  //  body.ModuleUpdatedDate = modules.ModuleUpdatedDate;
  //  body.ModuleStatus = modules.ModuleStatus;
  //  body.ModuleIsFree = modules.ModuleIsFree;
  //  console.log(body);
  //  alert(JSON.stringify(body));
  //  return this.http.post(this.baseUrl + 'api/TCSCore/CreateModule', body,).map((res: Response) =>
  //    res.json());
  //  // return null;
  //}
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });

    var params = new URLSearchParams();
    //var mdn = JSON.stringify(modules.ModuleMedia);
    //var ss = JSON.parse(mdn);
    params.set('Capability_Id', modules.Capability_Id);
    params.set('ProgramStr_Id', modules.ProgramStr_Id);
    params.set('ProgramCategory_Id', modules.ProgramCategory_Id);
    params.set('ModuleTitle', modules.ModuleTitle);
    params.set('ModuleType', modules.ModuleType);
    params.set('ModuleDependency', modules.ModuleDependency);
    params.set('ModuleArea', modules.ModuleArea);
    //for (let ModuleMedia in json) {
    //  params.set('ModuleMedia', [ ModuleMedia ]);
    //}
    for (let item of modules.ModuleMedia) {
      params.append('ModuleMedia', JSON.stringify(item));
    }
    //console.log(modules.ModuleMedia);
   // params.set('ModuleMedia', JSON.stringify(modules.ModuleMedia));
    params.set('IsModuleTracking', modules.IsModuleTracking);
    params.set('ModuleAccessType', modules.ModuleAccessType);
    params.set('ModuleDiscountPrice', modules.ModuleDiscountPrice);
    params.set('ModuleListedPrice', modules.ModuleListedPrice);
    params.set('ModuleDisplayDate', modules.ModuleDisplayDate);
    params.set('ModuleValidTill', modules.ModuleValidTill);
    params.set('ModuleCreatedBy', modules.ModuleCreatedBy);
    params.set('ModuleCreatedDate', modules.ModuleCreatedDate);
    params.set('ModuleUpdatedBy', modules.ModuleUpdatedBy);
    params.set('ModuleUpdatedDate', modules.ModuleUpdatedDate);
    params.set('ModuleStatus', modules.ModuleStatus);
    params.set('ModuleIsFree', modules.ModuleIsFree);
    //alert(JSON.stringify(modules));
    console.log(params);
    console.log(params.toString);
    return this.http.post(this.baseUrl + 'api/TCSCore/CreateModule', params.toString(), {
      //return this.http.post('http://localhost:50240/api/TCSCore/UpdateProgramContent', params.toString(), {
      headers: headers
    }).map(res => res.json());


  

  }


  //onCreateModules(url: string, modules: any): Observable<any> {
  //  let body = JSON.stringify(modules);
  //  let headers = new Headers({
  //    'Content-Type': 'application/json'
  //  });
  //  let options = new RequestOptions({
  //    headers: headers
  //  });
  //  return this.http.post(this.baseUrl + 'api/TCSCore/CreateModule', body, options).map((response: Response) => <any>response.json()).catch(this.handleError);
  //}
  //private handleError(error: Response) {
  //  console.error(error);
  //  return Observable.throw(error.json().error || 'Server error');
  //}  
  ///*** ProgramContent Ends ***///

  ///*** Account Programs Starts ***///

  onGetAllAccountProgramsList(id: any){
   // return this.http.get('http://52.77.87.237:8080/api/TCSCore/GetAllAccountPrograms?AccountId='+ id + '').map((res: Response) =>
    return this.http.get(this.baseUrl +'api/TCSCore/GetAllAccountPrograms?AccountId=' + id + '').map((res: Response) =>
      res.json());
  }
  onGetAccountsProgramsById(id: any): Observable<any> {
    console.log("id" + id);
    return this.http.get(this.baseUrl +'api/TCSCore/GetAccountProgramById?accountprogramid=' + id + '')
   // return this.http.get('http://localhost:50240/api/TCSCore/GetAccountProgramById?accountprogramid=' + id + '')
      .map((res: Response) =>
        res.json());
  }
  onUpdateAccountPrograms(accountprograms) {

    let pg: any = {};
    pg.Capability_Id = accountprograms.Capability_Id;
    pg.ProgramName = accountprograms.ProgramName;
    pg.ProgramDescription = accountprograms.ProgramDescription;
    pg.ProgramCoverPicture = accountprograms.ProgramCoverPicture;


    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });

    var params = new URLSearchParams();
    params.set('AccountProgram_Id', accountprograms.Id);
    params.set('Capability_Id', accountprograms.Capability_Id);
    params.set('Client_Id', accountprograms.Client_Id);
    params.set('MasterAccount_Id', accountprograms.MasterAccount_Id);
    params.set('AccountProgramStartDate', accountprograms.AccountProgramStartDate);
    params.set('AccountProgramEndDate', accountprograms.AccountProgramEndDate);
  //  params.set('AccountProgramCreatedBy', accountprograms.AccountProgramCreatedBy);
  //  params.set('AccountProgramCreatedDate', accountprograms.AccountProgramCreatedDate);
  //  params.set('AccountProgramUpdatedDate', accountprograms.AccountProgramUpdatedDate);
   // params.set('AccountProgramUpdatedBy', accountprograms.AccountProgramUpdatedBy);
    params.set('AccountProgramAccessType', accountprograms.AccountProgramAccessType);
    params.set('AccountProgramBusinessRule', accountprograms.AccountProgramBusinessRule);
    params.set('AccountProgramStatus', accountprograms.AccountProgramStatus);
    params.set('ProgramInfo', JSON.stringify(pg));
    params.set('AccountProgramCreatedBy', accountprograms.AccountProgramCreatedBy);
    params.set('AccountProgramCreatedDate', accountprograms.AccountProgramCreatedDate);
    params.set('AccountProgramUpdatedBy', accountprograms.AccountProgramUpdatedBy);
    params.set('AccountProgramUpdatedDate', accountprograms.AccountProgramUpdatedDate);
   // return this.http.post('http://localhost:50240/api/TCSCore/UpdateAccountPrograms', params.toString(), {
    return this.http.post(this.baseUrl +'api/TCSCore/UpdateAccountPrograms', params.toString(), {
      headers: headers
    }).map(res =>
      res.json());
  
  }
  onCreateAccountPrograms(accountprograms) {
   // alert(JSON.stringify(accountprograms));

    //alert(JSON.stringify(accountprograms.ProgramName));

    let pg: any = {};
    pg.Capability_Id = accountprograms.Capability_Id;
    pg.ProgramName = accountprograms.ProgramName;
    pg.ProgramDescription = accountprograms.ProgramDescription;
    pg.ProgramCoverPicture = accountprograms.ProgramCoverPicture;

    //var finalData = pg.replace(/\\/g,);

    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });

    var params = new URLSearchParams();
    params.set('Capability_Id', accountprograms.Capability_Id);
    params.set('Client_Id', accountprograms.Client_Id);
    params.set('MasterAccount_Id', accountprograms.MasterAccount_Id);
    params.set('AccountProgramStartDate', accountprograms.AccountProgramStartDate);
    params.set('AccountProgramEndDate', accountprograms.AccountProgramEndDate);
  //  params.set('AccountProgramCreatedBy', accountprograms.AccountProgramCreatedBy);
  //  params.set('AccountProgramCreatedDate', accountprograms.AccountProgramCreatedDate);
  //  params.set('AccountProgramUpdatedDate', accountprograms.AccountProgramUpdatedDate);
  //  params.set('AccountProgramUpdatedBy', accountprograms.AccountProgramUpdatedBy);
    params.set('AccountProgramAccessType', accountprograms.AccountProgramAccessType);
    params.set('AccountProgramBusinessRule', accountprograms.AccountProgramBusinessRule);
    params.set('AccountProgramStatus', accountprograms.AccountProgramStatus);
    params.set('ProgramInfo', JSON.stringify(pg));
    params.set('AccountProgramCreatedBy', accountprograms.AccountProgramCreatedBy);
    params.set('AccountProgramCreatedDate', accountprograms.AccountProgramCreatedDate);
    params.set('AccountProgramUpdatedBy', accountprograms.AccountProgramUpdatedBy);
    params.set('AccountProgramUpdatedDate', accountprograms.AccountProgramUpdatedDate);
  // return this.http.post('http://localhost:50240/api/TCSCore/CreateAccountPrograms', params.toString(), {
    return this.http.post(this.baseUrl +'api/TCSCore/CreateAccountPrograms', params.toString(), {
      headers: headers
    }).map(res =>
      res.json());
  }

  ///*** Account Programs Ends ***///


  ///*** License  ***///

 

  ///** License  **///

  ///////// start License////////////////

  onGetAllLicenseList(): Observable<any> {
    return this.http.get(this.baseUrl +'admin/licenseManager/allClientsInfo').map((res: Response) => res.json());
   // return this.http.get('http://localhost:50240/api/TCSCore/GetAllCentralRepo').map((res: Response) => res.json());
    // console.log(res);
  }

  onGetLicenseById(id: any): Observable<any> {
    console.log("id" + id);

  // return this.http.get('http://localhost:50240/api/tcscore/GetCentralRepoByID?centralrepoid=' + id + '').map((res: Response) => res.json());
    return this.http.get(this.baseUrl +'admin/licenseManager/getClientDetails/' + id + '').map((res: Response) => res.json());
  }

  onCreateLicense(license: any) {
    let body: any = {};
    body.InstanceManagerId = license.InstanceManagerId;
    body.Client_Id = license.ClientInfo;
    body.Instance_Id = license.Instance_Id;
    body.CompanyName = license.CompanyName;
    body.LegalName = license.LegalName;
    body.RegistrationNumber = license.RegistrationNumber;
    body.CompanyType = license.CompanyType;
    body.AddressLine1 = license.AddressLine1;
    body.AddressLine2 = license.AddressLine2;
    body.City = license.City;
    body.State = license.State;
    body.ZipCode = license.ZipCode;
    body.Country = license.Country;
    body.Industry = license.Industry;
    body.AreaOfBusiness = license.AreaOfBusiness;
    body.EstablishedDate = license.EstablishedDate;
    body.OperatingCountries = license.OperatingCountries;
    body.CompanyWebsite = license.CompanyWebsite;
    body.CompanySocialUrls = license.CompanySocialUrls;
    body.PrimaryAdminEmailID = license.PrimaryAdminEmailID;
    body.PrimaryAdminPassword = license.PrimaryAdminPassword;
    body.PrimaryAdminPhone = license.PrimaryAdminPhone;
    body.CompanyEmailID = license.CompanyEmailID;
    body.CompanyPhone = license.CompanyPhone;
    body.InstancePublicIPAddress = license.InstancePublicIPAddress;
   // body.ClientAgentDetails = license.ClientAgentDetails;
  //  body.CreatedDate = license.CreatedDate;
    body.CreatedBy = license.CreatedBy;
   // body.UpdatedDate = license.UpdatedDate;
   // body.UpdatedBy = license.UpdatedBy;
    body.RequestStatus = license.RequestStatus;
    body.Remarks = license.Remarks;
    //body.DecodeScript = license.DecodeScript;
   
 
    return this.http.post(this.baseUrl + 'admin/licenseManager/createClient', body).map(res =>
      res.json());
  }


  onUpdateLicense(license) {
    
    return this.http.put(this.baseUrl + 'admin/licenseManager/updateClientDetails/'+license._id+'', license).map(res =>
      res.json());
 
  }



  onGenerateLicense(license) {

    let body: any = {};
    body._id = license._id;
    body.CompanyName = license.CompanyName;
    body.RegistrationNumber = license.RegistrationNumber;
    body.PrimaryAdminEmailID = license.PrimaryAdminEmailID;
    body.CompanyPhone = license.CompanyPhone;
  //  body.LicenseStatus = license.LicenseStatus;
    body.LicenseStartDate = license.LicenseStartDate;
    body.LicenseEndDate = license.LicenseEndDate;
    body.LicenseGeneratedBy = license.LicenseGeneratedBy;
 //   body.LicenseType = license.LicenseType;
    body.RequestStatus = license.RequestStatus;
    return this.http.put(this.baseUrl + 'admin/licenseManager/generateLicenceKey/' + license.Client_Id+'', body).map(res =>
      res.json());
  }

  ///** License Ends **////


  //...............//Start program management///////////////////
  onGetAllCapabilitiesList(id: any): Observable<any> {
    return this.http.get(this.baseUrl +'api/tcscore/GetAllCapabilities?AccountId=' + id + '').map((res: Response) => res.json());
   // return this.http.get('http://52.77.87.237:8080/api/tcscore/GetAllCapabilities?AccountId=' + id + '').map((res: Response) => res.json());
    // console.log(res);
  }

  onGetCapabilitiesById(id: any): Observable<any> {
    console.log("id" + id);
    return this.http.get(this.baseUrl +'api/tcscore/GetCapabilityById?capabilityid=' + id + '').map((res: Response) => res.json());
   // return this.http.get('http://localhost:50240/api/TCSCore/GetCapabilityById?capabilityid=' + id + '').map((res: Response) => res.json());
  }




  onfileupload(formdata: any) {
    //formdata.id = "5a9cd36741249d75a4d62834";
    //formdata.avtar_imgPath = "2";
  // return this.http.post('http://localhost:50240/api/TCSCore/UploadFile', formdata).map((res: Response) =>
    return this.http.post(this.baseUrl +'api/TCSCore/UploadFile', formdata).map((res: Response) =>
      res.json());
  }

  onCreateCapabilities(capabilities) {

    //let pm: any = {};
    //pm.MetaTitle = program.MetaTitle;
    //pm.MetaKeywords = program.MetaKeywords;
    //pm.MetaDescrition = program.MetaDescrition;
    let pm: any = {};
    pm = { MetaTitle: capabilities.MetaTitle, MetaKeywords: capabilities.MetaKeywords, MetaDescrition: capabilities.MetaDescrition };
    // alert(JSON.stringify(pm));
    // alert(JSON.stringify(program.ProgramCoverPicture));

    //var serverpath = "13.251.70.173/";

    //var fakepath = "C:\\fakepath\\";

    //var op = program.ProgramCoverPicture.trim(fakepath);

    //alert(op);





    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });

    var params = new URLSearchParams();
    //params.set('ProgramManagementId', program.Id);
    params.set('ProgramStr_Id', capabilities.ProgramStr_Id);
    params.set('ProgramCategory_Id', capabilities.ProgramCategory_Id);
    params.set('CapabilityName', capabilities.CapabilityName);
    params.set('CapabilityDescription', capabilities.CapabilityDescription);
    params.set('CapabilitySynopsis', capabilities.CapabilitySynopsis);
    params.set('CapabilityCoverPicture', capabilities.CapabilityCoverPicture);
    params.set('CapabilityType', capabilities.CapabilityType);
    params.set('CapabilityTags', capabilities.CapabilityTags);
    params.set('CapabilityStartDate', capabilities.CapabilityStartDate);
    params.set('CapabilityValidTill', capabilities.CapabilityValidTill);
    params.set('CapabilityListedPrice', capabilities.CapabilityListedPrice);
    params.set('CapabilityAccessType', capabilities.CapabilityAccessType);
    params.set('CapabilityIsTracking', capabilities.CapabilityIsTracking);
    params.set('CapabilityPlatforms', capabilities.CapabilityPlatforms);
    params.set('CapabilityCoverMediaFile', capabilities.CapabilityCoverMediaFile);
    params.set('CapabilityCategory', capabilities.CapabilityCategory);
    params.set('CapabilityDiscountPrice', capabilities.CapabilityDiscountPrice);
    params.set('CapabilityIsFree', capabilities.CapabilityIsFree);
    params.set('CapabilityDependency', capabilities.CapabilityDependency);
    params.set('CapabilityCreatedBy', capabilities.CapabilityCreatedBy);
    params.set('CapabilityCreatedDate', capabilities.CapabilityCreatedDate);
    params.set('CapabilityUpdatedBy', capabilities.CapabilityUpdatedBy);
    params.set('CapabilityUpdatedDate', capabilities.CapabilityUpdatedDate);
    params.set('CapabilityStatus', capabilities.CapabilityStatus);
    params.set('CapabilitySocialElements', JSON.stringify(pm));

    return this.http.post(this.baseUrl +'api/TCSCore/CreateCapability', params.toString(), {
     // return this.http.post('http://localhost:50240/api/TCSCore/CreateProgramManagement', params.toString(), {
      headers: headers
    }).map(res =>
      res.json());


  }
  onUpdateCapabilities(capabilities) {

    let pm: any = {};
    pm = { MetaTitle: capabilities.MetaTitle, MetaKeywords: capabilities.MetaKeywords, MetaDescrition: capabilities.MetaDescrition };
    //pm.MetaTitle = program.MetaTitle;
    //pm.MetaKeywords = program.MetaKeywords;
    //pm.MetaDescrition = program.MetaDescrition;
  //  alert(JSON.stringify(pm));
  //  alert("hi");
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });

    var params = new URLSearchParams();
    params.set('Capability_Id', capabilities.Id);
    params.set('ProgramStr_Id', capabilities.ProgramStr_Id);
    params.set('ProgramCategory_Id', capabilities.ProgramCategory_Id);
    params.set('CapabilityName', capabilities.CapabilityName);
    params.set('CapabilityDescription', capabilities.CapabilityDescription);
    params.set('CapabilitySynopsis', capabilities.CapabilitySynopsis);
    params.set('CapabilityCoverPicture', capabilities.CapabilityCoverPicture);
    params.set('CapabilityType', capabilities.CapabilityType);
    params.set('CapabilityTags', capabilities.CapabilityTags);
    params.set('CapabilityStartDate', capabilities.CapabilityStartDate);
    params.set('CapabilityValidTill', capabilities.CapabilityValidTill);
    params.set('CapabilityListedPrice', capabilities.CapabilityListedPrice);
    params.set('CapabilityAccessType', capabilities.CapabilityAccessType);
    params.set('CapabilityIsTracking', capabilities.CapabilityIsTracking);
    params.set('CapabilityPlatforms', capabilities.CapabilityPlatforms);
    params.set('CapabilityCoverMediaFile', capabilities.CapabilityCoverMediaFile);
    params.set('CapabilityCategory', capabilities.CapabilityCategory);
    params.set('CapabilityDiscountPrice', capabilities.CapabilityDiscountPrice);
    params.set('CapabilityDependency', capabilities.CapabilityDependency);
    params.set('CapabilityCreatedBy', capabilities.CapabilityCreatedBy);
    params.set('CapabilityCreatedDate', capabilities.CapabilityCreatedDate);
    params.set('CapabilityUpdatedBy', capabilities.CapabilityUpdatedBy);
    params.set('CapabilityUpdatedDate', capabilities.CapabilityUpdatedDate);
    params.set('CapabilityIsFree', capabilities.CapabilityIsFree);
    params.set('CapabilityStatus', capabilities.CapabilityStatus);
    params.set('CapabilitySocialElements', JSON.stringify(pm));
    return this.http.post(this.baseUrl +'api/TCSCore/UpdateCapability', params.toString(), {
   // return this.http.post('http://localhost:50240/api/TCSCore/UpdateCapability', params.toString(), {
      headers: headers
    }).map(res =>
      res.json());


  }
   //...............//End program management///////////////////

  onGetAllAccountMemberShipList(id: any): Observable<any> {
   // return this.http.get('http://52.77.87.237:8080/api/tcscore/GetAllAccountMemberShip?AccountId='  + id + '').map((res: Response) => res.json());
    // console.log(res);


    return this.http.get(this.baseUrl+ 'api/TCSCore/GetAllAccountMemberShip?AccountId=' + id + '').map((res: Response) => res.json());
  }

  onGetAccountMemberShipById(id: any): Observable<any> {
    console.log("id" + id);
    return this.http.get(this.baseUrl +'api/tcscore/GetAccountMemberShipById?accountmemid=' + id + '').map((res: Response) => res.json());

   // return this.http.get('http://localhost:50240/api/tcscore/GetAccountMemberShipById?accountmemid=' + id + '').map((res: Response) => res.json());
  }

  onCreateAccountMemberShip(account: any) {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    var params = new URLSearchParams();
    params.set('MemberAccount_Id', account.MemberAccount_Id);
    params.set('Member_Id', account.Member_Id);
    params.set('ACM_UserName', account.ACM_UserName);
    params.set('ACM_Password', account.ACM_Password);
    params.set('ACM_ImageUrl', account.ACM_ImageUrl);
    params.set('ACM_Email', account.ACM_Email);
    params.set('ACM_LastLogin', account.ACM_LastLogin);
    params.set('ACM_Role_Id', account.ACM_Role_Id);
    params.set('ACM_LoginStatus', account.ACM_LoginStatus);
    params.set('ACM_CreatedBy', account.ACM_CreatedBy);
    params.set('ACM_CreatedDate', account.ACM_CreatedDate);
    params.set('ACM_UpdatedBy', account.ACM_UpdatedBy);
    params.set('ACM_UpdatedDate', account.ACM_UpdatedDate);

  

   // return this.http.post('http://localhost:50240/api/TCSCore/CreateAccountMemberShip', params.toString(), {
    return this.http.post(this.baseUrl +'api/TCSCore/CreateAccountMemberShip', params.toString(), {
      headers: headers
    }).map(res =>
      res.json());
  }
  onUpdateAccountMembership(account) {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    var params = new URLSearchParams();
    params.set('AccountMember_Id', account.Id);
    params.set('MemberAccount_Id', account.MemberAccount_Id);
    params.set('Member_Id', account.Member_Id);
    params.set('ACM_ImageUrl', account.ACM_ImageUrl);
    params.set('ACM_UserName', account.ACM_UserName);
    params.set('ACM_Password', account.ACM_Password);
    params.set('ACM_Email', account.ACM_Email);
    params.set('ACM_LastLogin', account.ACM_LastLogin);
    params.set('ACM_Role_Id', account.ACM_Role_Id);
    params.set('ACM_LoginStatus', account.ACM_LoginStatus);
    params.set('ACM_CreatedBy', account.ACM_CreatedBy);
    params.set('ACM_CreatedDate', account.ACM_CreatedDate);
    params.set('ACM_UpdatedBy', account.ACM_UpdatedBy);
    params.set('ACM_UpdatedDate', account.ACM_UpdatedDate);

  //  return this.http.post('http://localhost:50240/api/TCSCore/UpdateAccountMemberShip', params.toString(), {
    //  headers: headers
    //}).map(res =>
    //  res.json());
    return this.http.post(this.baseUrl +'api/tcscore/UpdateAccountMemberShip', params.toString(), {
    headers: headers
  }).map(res =>
    res.json());
  }



  //** ContentCategory **//

  onGetAllContentCategory(): Observable<any> {
    return this.http.get(this.baseUrl +'api/tcscore/GetAllContentCategory').map((res: Response) =>
   // return this.http.get('http://localhost:50240/api/tcscore/GetAllContentCategory').map((res: Response) =>
      res.json());

  }

  onGetContentCategoryById(id: any): Observable<any> {
    console.log("id" + id);
    return this.http.get(this.baseUrl +'api/tcscore/GetContentCategoryById?contentcategoryid=' + id + '').map((res: Response) =>
   // return this.http.get('http://localhost:50240/api/tcscore/GetContentCategoryById?contentcategoryid=' + id + '').map((res: Response) =>
      res.json());
  }


  onCreateContentCategory(content: any) {

    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });

    var params = new URLSearchParams();
    params.set('ContentCategoryName', content.ContentCategoryName);
    params.set('ContentCategoryType', content.ContentCategoryType);
    params.set('ContentCategoryStartedDate', content.ContentCategoryStartedDate);
    params.set('ContentCategoryValidTill', content.ContentCategoryValidTill);
    params.set('ContentCategoryCreatedBy', content.ContentCategoryCreatedBy);
    params.set('ContentCategoryCreatedDate', content.ContentCategoryCreatedDate);
    params.set('ContentCategoryUpdatedBy', content.ContentCategoryUpdatedBy);
    params.set('ContentCategoryUpdatedDate', content.ContentCategoryUpdatedDate);
    params.set('ContentCategoryStatus', content.ContentCategoryStatus);
    

 
    // console.log(body);
    return this.http.post(this.baseUrl +'api/tcscore/CreateContentCategory', params.toString(), {
  // return this.http.post('http://localhost:50240/api/TCSCore/CreateContentCategory', params.toString(), {
      headers: headers
    }).map(res =>
      res.json());
  
  }
  onUpdateContentCategory(content) {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });

    var params = new URLSearchParams();
    params.set('ContentCategoryId', content.Id);
    params.set('ContentCategoryName', content.ContentCategoryName);
    params.set('ContentCategoryType', content.ContentCategoryType);
    params.set('ContentCategoryStartedDate', content.ContentCategoryStartedDate);
    params.set('ContentCategoryValidTill', content.ContentCategoryValidTill);
    params.set('ContentCategoryCreatedBy', content.ContentCategoryCreatedBy);
    params.set('ContentCategoryCreatedDate', content.ContentCategoryCreatedDate);
    params.set('ContentCategoryUpdatedBy', content.ContentCategoryUpdatedBy);
    params.set('ContentCategoryUpdatedDate', content.ContentCategoryUpdatedDate);
    params.set('ContentCategoryStatus', content.ContentCategoryStatus);
    return this.http.post(this.baseUrl +'api/tcscore/UpdateContentCategory' , params.toString(), {
  
  //  return this.http.post('http://localhost:50240/api/TCSCore/UpdateContentCategory', params.toString(), {
      headers: headers
    }).map(res =>
      res.json());

  }


  ///*** ProgramCategory ***///

  onGetAllProgramCategory(): Observable<any> {
   // return this.http.get('http://52.77.87.237:8080/api/tcscore/GetAllProgramCategory?AccountId=' + id + '').map((res: Response) =>
    return this.http.get(this.baseUrl +'programCategory/getALLProgramCategories').map((res: Response) =>
      res.json());

  }

  onGetProgramCategoryById(id: any): Observable<any> {
    console.log("id" + id);
    return this.http.get(this.baseUrl +'api/tcscore/GetProgramCategoryById?programcategoryid=' + id + '').map((res: Response) =>
   // return this.http.get('http://localhost:50240/api/tcscore/GetProgramCategoryById?programcategoryid=' + id + '').map((res: Response) =>
      res.json());
  }


  onCreateProgramCategory(program: any) {

    let headers = new Headers({
        'Content-Type': 'application/x-www-form-urlencoded'
      });

    var params = new URLSearchParams();
    params.set('MasterAccount_Id', program.MasterAccount_Id);
    params.set('ProgramCategoryName', program.ProgramCategoryName);
    params.set('ProgramCategoryImgUrl', program.ProgramCategoryImgUrl);
    params.set('ProgramCategoryDescription', program.ProgramCategoryDescription);
    params.set('ProgramCategoryStatus', program.ProgramCategoryStatus);
    params.set('ProgramCategoryCreatedBy', program.ProgramCategoryCreatedBy);
    params.set('ProgramCategoryCreatedDate', program.ProgramCategoryCreatedDate);
    params.set('ProgramCategoryUpdatedBy', program.ProgramCategoryUpdatedBy);
    params.set('ProgramCategoryUpdatedDate', program.ProgramCategoryUpdatedDate);

      // console.log(body);
    return this.http.post(this.baseUrl +'api/tcscore/CreateProgramCategory', params.toString(), {
  // return this.http.post('http://localhost:50240/api/TCSCore/CreateProgramCategory', params.toString(), {
        headers: headers
      }).map(res =>
        res.json());
   
  }
  onUpdateProgramCategory(program) {


    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });

    var params = new URLSearchParams();
   
    params.set('ProgramCategory_Id', program.Id);
    params.set('MasterAccount_Id', program.MasterAccount_Id);
    params.set('ProgramCategoryName', program.ProgramCategoryName);
    params.set('ProgramCategoryImgUrl', program.ProgramCategoryImgUrl);

    params.set('ProgramCategoryDescription', program.ProgramCategoryDescription);
    params.set('ProgramCategoryStatus', program.ProgramCategoryStatus);
    params.set('ProgramCategoryCreatedBy', program.ProgramCategoryCreatedBy);
    params.set('ProgramCategoryCreatedDate', program.ProgramCategoryCreatedDate);
    params.set('ProgramCategoryUpdatedBy', program.ProgramCategoryUpdatedBy);
    params.set('ProgramCategoryUpdatedDate', program.ProgramCategoryUpdatedDate);

    // console.log(body);
    return this.http.post(this.baseUrl +'api/tcscore/UpdateProgramCategory', params.toString(), {
   // return this.http.post('http://localhost:50240/api/TCSCore/UpdateProgramCategory', params.toString(), {
      headers: headers
    }).map(res =>
      res.json());
    
  }

  //** ExerciseQuestions **//


  onGetAllExerciseQuestionList(id: any) {
   // return this.http.get('http://52.77.87.237:8080/api/tcscore/GetAllExerciseQuestion?AccountId=' + id + '').map((res: Response) => res.json());
    // console.log(res);



    return this.http.get(this.baseUrl +'api/TCSCore/GetAllExerciseQuestion?AccountId=' + id + '').map((res: Response) => res.json());
  }

  onGetExerciseQuestionById(id: any): Observable<any> {
    console.log("id" + id);
    return this.http.get(this.baseUrl +'api/tcscore/GetExerciseQuestionById?exercisequid=' + id + '').map((res: Response) => res.json());


   // return this.http.get('http://localhost:50240/api/tcscore/GetExerciseQuestionById?exercisequid=' + id + '').map((res: Response) => res.json());
  }

  onCreateExerciseQuestion(exercisequestion: any) {

    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });

    var params = new URLSearchParams();
    params.set('Exercise_Id', exercisequestion.Exercise_Id);
    params.set('ExerciseQuestion', exercisequestion.ExerciseQuestion);
    params.set('ExerciseQuestionSummary', exercisequestion.ExerciseQuestionSummary);
    params.set('ExerciseQuestionDescription', exercisequestion.ExerciseQuestionDescription);
    params.set('ExerciseQuestionAnswers', exercisequestion.ExerciseQuestionAnswers);
    params.set('ExerciseQuestionAnswerOptions', exercisequestion.ExerciseQuestionAnswerOptions);
    params.set('ExerciseQuestionAnswerHints', exercisequestion.ExerciseQuestionAnswerHints);
    params.set('ExerciseQuestionAnswerDisplayType', exercisequestion.ExerciseQuestionAnswerDisplayType);
    params.set('ExerciseQuestionAnswerExclusionGroups', exercisequestion.ExerciseQuestionAnswerExclusionGroups);
    params.set('ExerciseQuestionIsMandatory', exercisequestion.ExerciseQuestionIsMandatory);
    params.set('ExerciseQuestionIsProficient', exercisequestion.ExerciseQuestionIsProficient);
    params.set('ExerciseQuestionCreatedBy', exercisequestion.ExerciseQuestionCreatedBy);
    params.set('ExerciseQuestionCreatedDate', exercisequestion.ExerciseQuestionCreatedDate);
    params.set('ExerciseQuestionUpdatedBy', exercisequestion.ExerciseQuestionUpdatedBy);
    params.set('ExerciseQuestionUpdatedDate', exercisequestion.ExerciseQuestionUpdatedDate);
    params.set('ExerciseQuestionStatus', exercisequestion.ExerciseQuestionStatus);



    // console.log(body);
    return this.http.post(this.baseUrl +'api/tcscore/CreateExerciseQuestion', params.toString(), {
    //return this.http.post('http://localhost:50240/api/TCSCore/CreateExerciseQuestion', params.toString(), {
      headers: headers
    }).map(res =>
      res.json());
  }




  onUpdateExerciseQuestion(exercisequestion) {
      let headers = new Headers({
  'Content-Type': 'application/x-www-form-urlencoded'
});

    var params = new URLSearchParams();
    params.set('ExQuestion_Id', exercisequestion.Id);
    params.set('Exercise_Id', exercisequestion.Exercise_Id);
params.set('ExerciseQuestion', exercisequestion.ExerciseQuestion);
params.set('ExerciseQuestionSummary', exercisequestion.ExerciseQuestionSummary);
params.set('ExerciseQuestionDescription', exercisequestion.ExerciseQuestionDescription);
params.set('ExerciseQuestionAnswers', exercisequestion.ExerciseQuestionAnswers);
params.set('ExerciseQuestionAnswerOptions', exercisequestion.ExerciseQuestionAnswerOptions);
params.set('ExerciseQuestionAnswerHints', exercisequestion.ExerciseQuestionAnswerHints);
params.set('ExerciseQuestionAnswerDisplayType', exercisequestion.ExerciseQuestionAnswerDisplayType);
params.set('ExerciseQuestionAnswerExclusionGroups', exercisequestion.ExerciseQuestionAnswerExclusionGroups);
params.set('ExerciseQuestionIsMandatory', exercisequestion.ExerciseQuestionIsMandatory);
params.set('ExerciseQuestionIsProficient', exercisequestion.ExerciseQuestionIsProficient);
    params.set('ExerciseQuestionCreatedBy', exercisequestion.ExerciseQuestionCreatedBy);
    params.set('ExerciseQuestionCreatedDate', exercisequestion.ExerciseQuestionCreatedDate);
    params.set('ExerciseQuestionUpdatedBy', exercisequestion.ExerciseQuestionUpdatedBy);
    params.set('ExerciseQuestionUpdatedDate', exercisequestion.ExerciseQuestionUpdatedDate);
params.set('ExerciseQuestionStatus', exercisequestion.ExerciseQuestionStatus);



// console.log(body);
    return this.http.post(this.baseUrl +'api/tcscore/UpdateExerciseQuestion', params.toString(), {
   // return this.http.post('http://localhost:50240/api/TCSCore/UpdateExerciseQuestion', params.toString(), {
  headers: headers
}).map(res =>
  res.json());
 
  }

  //** ExerciseAnswer **//

  onGetAllExerciseAnswerList(id: any): Observable<any> {
    //return this.http.get('http://52.77.87.237:8080/api/tcscore/GetAllExerciseAnswer?AccountId=' + id + '' ).map((res: Response) => res.json());
    return this.http.get(this.baseUrl +'api/TCSCore/GetAllExerciseAnswer?AccountId=' + id + '' ).map((res: Response) => res.json());
    // console.log(res);
  }

  onGetExerciseAnswerById(id: any): Observable<any> {
    console.log("id" + id);
    return this.http.get(this.baseUrl +'api/tcscore/GetExerciseAnswerById?id=' + id + '').map((res: Response) => res.json());
   // return this.http.get('http://localhost:50240/api/tcscore/GetExerciseAnswerById?id=' + id + '').map((res: Response) => res.json());
  }

  onCreateExerciseAnswer(exerciseanswer: any) {
  
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });

    var params = new URLSearchParams();
    params.set('ExerciseAnswer', exerciseanswer.ExerciseAnswer);
    params.set('Submission_Id', exerciseanswer.Submission_id);
    params.set('Exercise_Question_Id', exerciseanswer.Exercise_Question_Id);
    params.set('Exercise_Id', exerciseanswer.Exercise_Id);
    params.set('Member_Id', exerciseanswer.Member_Id);
    params.set('ExAnsSelectedAnswerOptions', exerciseanswer.ExAnsSelectedAnswerOptions);
    params.set('ExerciseAnswerDetails', exerciseanswer.ExerciseAnswerDetails);
    params.set('ExerciseAnswerRemarks', exerciseanswer.ExerciseAnswerRemarks);
    params.set('ExerciseAnswerSubmissionDateTime', exerciseanswer.ExerciseAnswerSubmissionDateTime);
    params.set('ExerciseAnswerValidityStatus', exerciseanswer.ExerciseAnswerValidityStatus);
   // params.set('ExerciseAnswerCreatedDate', exerciseanswer.ExerciseAnswerCreatedDate);
  //  params.set('ExerciseAnswerCreatedBy', exerciseanswer.ExerciseAnswerCreatedBy);
   // params.set('ExerciseAnswerUpdatedDate', exerciseanswer.ExerciseAnswerUpdatedDate);
   // params.set('ExerciseAnswerUpdatedBy', exerciseanswer.ExerciseAnswerUpdatedBy);
    params.set('ExerciseAnswerStatus', exerciseanswer.ExerciseAnswerStatus);




    // console.log(body);
    return this.http.post(this.baseUrl +'api/tcscore/CreateExerciseAnswer', params.toString(), {
   // return this.http.post('http://localhost:50240/api/TCSCore/CreateExerciseAnswer', params.toString(), {
      headers: headers
    }).map(res =>
      res.json());
   

  }

  
  onUpdateExerciseAnswer(exerciseanswer) {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });

    var params = new URLSearchParams();
    params.set('exAnswerId', exerciseanswer.Id);
    params.set('ExerciseAnswer', exerciseanswer.ExerciseAnswer);
    params.set('Submission_Id', exerciseanswer.Submission_Id);
    params.set('Exercise_Question_Id', exerciseanswer.Exercise_Question_Id);
    params.set('Exercise_Id', exerciseanswer.Exercise_Id);
    params.set('Member_Id', exerciseanswer.Member_Id);
    params.set('ExAnsSelectedAnswerOptions', exerciseanswer.ExAnsSelectedAnswerOptions);
    params.set('ExerciseAnswerDetails', exerciseanswer.ExerciseAnswerDetails);
    params.set('ExerciseAnswerRemarks', exerciseanswer.ExerciseAnswerRemarks);
    params.set('ExerciseAnswerSubmissionDateTime', exerciseanswer.ExerciseAnswerSubmissionDateTime);
    params.set('ExerciseAnswerValidityStatus', exerciseanswer.ExerciseAnswerValidityStatus);
   // params.set('ExerciseAnswerCreatedDate', exerciseanswer.ExerciseAnswerCreatedDate);
   // params.set('ExerciseAnswerCreatedBy', exerciseanswer.ExerciseAnswerCreatedBy);
   // params.set('ExerciseAnswerUpdatedDate', exerciseanswer.ExerciseAnswerUpdatedDate);
  //  params.set('ExerciseAnswerUpdatedBy', exerciseanswer.ExerciseAnswerUpdatedBy);
    params.set('ExerciseAnswerStatus', exerciseanswer.ExerciseAnswerStatus);



    // console.log(body);
    return this.http.post(this.baseUrl +'api/tcscore/UpdateExerciseAnswer', params.toString(), {
  //  return this.http.post('http://localhost:50240/api/TCSCore/UpdateExerciseAnswer', params.toString(), {
      headers: headers
    }).map(res =>
      res.json());

  }

  // ** Themestudio **//



  onGetAllThemeStudioList(): Observable<ThemeStudio[]> {
    return this.http.get(this.baseUrl +'api/tcscore/GetAllThemeStudio').map((res: Response) => res.json());
  //  return this.http.get('http://localhost:50240/api/TCSCore/GetAllThemeStudio').map((res: Response) => res.json());
    // console.log(res);
  }

  onGetThemeStudioById(id: any): Observable<any> {
    console.log("id" + id);

   // return this.http.get('http://localhost:50240/api/tcscore/GetThemeStudioByID?id=' + id + '').map((res: Response) => res.json());
    return this.http.get(this.baseUrl +'api/TCSCore/GetThemeStudioByID?id=' + id + '').map((res: Response) => res.json());
  }

  onCreateThemeStudio(themestudio: any) {

    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });

    var params = new URLSearchParams();
    params.set('Client_Id', themestudio.Client_Id);
    params.set('ThemeName', themestudio.ThemeName);
    params.set('ThemeTitle', themestudio.ThemeTitle);
    params.set('ThemePrimaryColor', themestudio.ThemePrimaryColor);
    params.set('ThemeSecondaryColor', themestudio.ThemeSecondaryColor);
    params.set('ThemeLogoUrl', themestudio.ThemeLogoUrl);
    params.set('ThemeH1', themestudio.ThemeH1);
    params.set('ThemeH2', themestudio.ThemeH2);
    params.set('ThemeH3', themestudio.ThemeH3);
    params.set('ThemeHeader', themestudio.ThemeHeader);
    params.set('ThemeSubHeader', themestudio.ThemeSubHeader);
    params.set('ThemeNormalText', themestudio.ThemeNormalText);
    params.set('ThemeParagraphText', themestudio.ThemeParagraphText);
    params.set('ThemePrimaryFont', themestudio.ThemePrimaryFont);
    params.set('ThemeSecondaryFont', themestudio.ThemeSecondaryFont);
    params.set('ThemeBorderColor', themestudio.ThemeBorderColor);
    params.set('ThemeBorderWidth', themestudio.ThemeBorderWidth);
    params.set('ThemeALink', themestudio.ThemeALink);
    params.set('ThemeHrefLink', themestudio.ThemeHrefLink);
    params.set('ThemeLiStyle', themestudio.ThemeLiStyle);
    params.set('ThemeButton', themestudio.ThemeButton);
    params.set('ThemeControlColor', themestudio.ThemeControlColor);
    params.set('ThemeTextBoxStyle', themestudio.ThemeTextBoxStyle);
    params.set('ThemeAreaStyle', themestudio.ThemeAreaStyle);

    params.set('ThemeControlStyle', themestudio.ThemeControlStyle);

    params.set('ThemeEmbeddedStyle', themestudio.ThemeEmbeddedStyle);

    //params.set('ThemeCreatedDate', themestudio.ThemeCreatedDate);
    //params.set('ThemeCreatedBy', themestudio.ThemeCreatedBy);

    //params.set('ThemeUpdatedDate', themestudio.ThemeUpdatedDate);

    //params.set('ThemeUpdatedBy', themestudio.ThemeUpdatedBy);

    //params.set('ThemeStatus', themestudio.ThemeStatus);






    return this.http.post(this.baseUrl +'api/tcscore/CreateThemeStudio', params.toString(), {
   // return this.http.post('http://localhost:50240/api/TCSCore/CreateThemeStudio', params.toString(), {
      headers: headers
    }).map(res =>
      res.json());
  }


  onUpdateThemeStudio(themestudio) {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    var params = new URLSearchParams();
    params.set('ThemeStudio_Id', themestudio.Id);
    params.set('Client_Id', themestudio.Client_Id);
    params.set('ThemeName', themestudio.ThemeName);
    params.set('ThemeTitle', themestudio.ThemeTitle);
    params.set('ThemePrimaryColor', themestudio.ThemePrimaryColor);
    params.set('ThemeSecondaryColor', themestudio.ThemeSecondaryColor);
    params.set('ThemeLogoUrl', themestudio.ThemeLogoUrl);
    params.set('ThemeH1', themestudio.ThemeH1);
    params.set('ThemeH2', themestudio.ThemeH2);
    params.set('ThemeH3', themestudio.ThemeH3);
    params.set('ThemeHeader', themestudio.ThemeHeader);
    params.set('ThemeSubHeader', themestudio.ThemeSubHeader);
    params.set('ThemeNormalText', themestudio.ThemeNormalText);
    params.set('ThemeParagraphText', themestudio.ThemeParagraphText);
    params.set('ThemePrimaryFont', themestudio.ThemePrimaryFont);
    params.set('ThemeSecondaryFont', themestudio.ThemeSecondaryFont);
    params.set('ThemeBorderColor', themestudio.ThemeBorderColor);
    params.set('ThemeBorderWidth', themestudio.ThemeBorderWidth);
    params.set('ThemeALink', themestudio.ThemeALink);
    params.set('ThemeHrefLink', themestudio.ThemeHrefLink);
    params.set('ThemeLiStyle', themestudio.ThemeLiStyle);
    params.set('ThemeButton', themestudio.ThemeButton);
    params.set('ThemeControlColor', themestudio.ThemeControlColor);
    params.set('ThemeTextBoxStyle', themestudio.ThemeTextBoxStyle);
    params.set('ThemeAreaStyle', themestudio.ThemeAreaStyle);
    params.set('ThemeControlStyle', themestudio.ThemeControlStyle);
    params.set('ThemeEmbeddedStyle', themestudio.ThemeEmbeddedStyle);
    //params.set('ThemeCreatedDate', themestudio.ThemeCreatedDate);
    //params.set('ThemeCreatedBy', themestudio.ThemeCreatedBy);
    //params.set('ThemeUpdatedDate', themestudio.ThemeUpdatedDate);
    //params.set('ThemeUpdatedBy', themestudio.ThemeUpdatedBy);
    //params.set('ThemeStatus', themestudio.ThemeStatus);





    //var url = "http://localhost:50240/api/tcscore/UpdateLicenseDetails";
    //var id = account.Id    
    //let body: any = {};
    //body.CompanyName = license.CompanyName;
    //console.log(body);
    // return this.http.put('http://192.168.2.186:90/api/tcscore/UpdateLicenseDetails?id' + account._id + '', account).map((res: Response) =>
    return this.http.post(this.baseUrl +'api/tcscore/UpdateThemeStudio', params.toString(), {
    //return this.http.post('http://localhost:50240/api/tcscore/UpdateThemeStudio', params.toString(), {
      headers: headers
    }).map((response: Response) => <any>response.json());
  }




  //** Exercise **//

  //onGetAllExerciseList(): Observable<any> {
  //   return this.http.get('http://52.77.87.237:8080/api/tcscore/GetAllExercises').map((res => res.json());
  // // return this.http.get('http://localhost:50240/api/TCSCore/GetAllExercises').map((res: Response) => res.json());
  //  // console.log(res);
  //}



  onGetAllExerciseList(id: any): Observable<any> {
   // return this.http.get('http://52.77.87.237:8080/api/TcsCore/GetAllExercises').map((res: Response) => res.json());
    return this.http.get(this.baseUrl +'api/TCSCore/GetAllExercises?AccountId=' + id + '').map((res: Response) => res.json());
    // console.log(res);
  }



  onGetExerciseById(id: any): Observable<any> {
    console.log("id" + id);

    return this.http.get(this.baseUrl +'api/TcsCore/GetExerciseByID?id=' + id + '').map((res: Response) => res.json());
   //return this.http.get('http://localhost:50240/api/TCSCore/GetExerciseByID?Exerciseid=' + id + '').map((res: Response) => res.json());
  }

  onCreateExercise(exercise: any) {

    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });

    var params = new URLSearchParams();

    params.set('ExerciseTitle', exercise.ExerciseTitle);
    params.set('ExerciseType_Id', exercise.ExerciseType_Id);
    params.set('NoofQuestions', exercise.NoofQuestions);
    params.set('ExerciseDescription', exercise.ExerciseDescription);
    params.set('ExerciseSynopsis', exercise.ExerciseSynopsis);
    params.set('ExerciseObjectives', exercise.ExerciseObjectives);
    params.set('ExerciseAccessType', exercise.ExerciseAccessType);
    params.set('IsExerciseModerated', exercise.IsExerciseModerated);
    params.set('ExerciseCreatedBy', exercise.ExerciseCreatedBy);
    params.set('ExerciseCreatedDate', exercise.ExerciseCreatedDate);
    params.set('ExerciseUpdatedBy', exercise.ExerciseUpdatedBy);
    params.set('ExerciseUpdatedDate', exercise.ExerciseUpdatedDate);
    params.set('ExerciseStatus', exercise.ExerciseStatus);
    params.set('ExerciseDependency', exercise.ExerciseDependency);





    return this.http.post(this.baseUrl +'api/TcsCore/CreateExercise', params.toString(), {
   // return this.http.post('http://localhost:50240/api/TCSCore/CreateExercise', params.toString(), {
      headers: headers
    }).map(res =>
      res.json());
  }


  onUpdateExercise(exercise) {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    var params = new URLSearchParams();
    params.set('Exercise_Id', exercise.Id);
    params.set('ExerciseTitle', exercise.ExerciseTitle);
    params.set('ExerciseType_Id', exercise.ExerciseType_Id);
    params.set('NoofQuestions', exercise.NoofQuestions);
    params.set('IsExerciseModerated', exercise.IsExerciseModerated);
    params.set('ExerciseDescription', exercise.ExerciseDescription);
    params.set('ExerciseSynopsis', exercise.ExerciseSynopsis);
    params.set('ExerciseObjectives', exercise.ExerciseObjectives);
    params.set('ExerciseAccessType', exercise.ExerciseAccessType);
    params.set('ExerciseStatus', exercise.ExerciseStatus);
    params.set('ExerciseDependency', exercise.ExerciseDependency);
    params.set('ExerciseCreatedBy', exercise.ExerciseCreatedBy);
    params.set('ExerciseCreatedDate', exercise.ExerciseCreatedDate);
    params.set('ExerciseUpdatedBy', exercise.ExerciseUpdatedBy);
    params.set('ExerciseUpdatedDate', exercise.ExerciseUpdatedDate);



    //var url = "http://localhost:50240/api/tcscore/UpdateLicenseDetails";
    //var id = account.Id    
    //let body: any = {};
    //body.CompanyName = license.CompanyName;
    //console.log(body);
    // return this.http.put('http://192.168.2.186:90/api/tcscore/UpdateLicenseDetails?id' + account._id + '', account).map((res: Response) =>
    return this.http.post(this.baseUrl +'api/TcsCore/UpdateExercise', params.toString(), {
   // return this.http.post('http://localhost:50240/api/tcscore/UpdateExercise', params.toString(), {
      headers: headers
    }).map((response: Response) => <any>response.json());
  }


  //** Exercise Type ** //


  onGetAllExerciseTypeList(id: any): Observable<any> {
   // return this.http.get('http://52.77.87.237:8080/api/tcscore/GetAllExerciseTypes?AccountId=' + id + '').map((res: Response) => res.json());
    return this.http.get(this.baseUrl +'api/TCSCore/GetAllExerciseTypes?AccountId='+ id + '').map((res: Response) => res.json());
    // console.log(res);
  }

  onGetExerciseTypeById(id: any): Observable<any> {
    console.log("id" + id);

   // return this.http.get('http://localhost:50240/api/tcscore/GetExerciseTypeById?id=' + id + '').map((res: Response) => res.json());
    return this.http.get(this.baseUrl +'api/TCSCore/GetExerciseTypeById?exercisetypeid=' + id + '').map((res: Response) => res.json());
  }

  onCreateExerciseType(exercisetype) {

    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });

    var params = new URLSearchParams();
    params.set('MasterAccount_Id', exercisetype.MasterAccount_Id);
    params.set('ExerciseTypeName', exercisetype.ExerciseTypeName);
    params.set('ExerciseTypeDescription', exercisetype.ExerciseTypeDescription);
    params.set('ExerciseTypeIcon', exercisetype.ExerciseTypeIcon);
    params.set('ExerciseTypeCreatedBy', exercisetype.ExerciseTypeCreatedBy);
    params.set('ExerciseTypeCreatedDate', exercisetype.ExerciseTypeCreatedDate);
    params.set('ExerciseTypeUpdatedBy', exercisetype.ExerciseTypeUpdatedBy);
    params.set('ExerciseTypeUpdatedDate', exercisetype.ExerciseTypeUpdatedDate);
    params.set('ExerciseTypeStatus', exercisetype.ExerciseTypeStatus);
    return this.http.post(this.baseUrl +'api/tcscore/CreateExerciseType', params.toString(), {
   // return this.http.post('http://localhost:50240/api/TCSCore/CreateExerciseType', params.toString(), {
      headers: headers
    }).map(res =>
      res.json());
  }


  onUpdateExerciseType(exercisetype) {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    var params = new URLSearchParams();
    params.set('ExerciseType_Id', exercisetype.Id);
    params.set('MasterAccount_Id', exercisetype.MasterAccount_Id);
    params.set('ExerciseTypeIcon', exercisetype.ExerciseTypeIcon);
    params.set('ExerciseTypeName', exercisetype.ExerciseTypeName);
    params.set('ExerciseTypeDescription', exercisetype.ExerciseTypeDescription);
    params.set('ExerciseTypeCreatedBy', exercisetype.ExerciseTypeCreatedBy);
    params.set('ExerciseTypeCreatedDate', exercisetype.ExerciseTypeCreatedDate);
    params.set('ExerciseTypeUpdatedBy', exercisetype.ExerciseTypeUpdatedBy);
    params.set('ExerciseTypeUpdatedDate', exercisetype.ExerciseTypeUpdatedDate);
    params.set('ExerciseTypeStatus', exercisetype.ExerciseTypeStatus);
    params.set('ExerciseTypeCreatedBy', exercisetype.ExerciseTypeCreatedBy);
    params.set('ExerciseTypeCreatedDate', exercisetype.ExerciseTypeCreatedDate);
    params.set('ExerciseTypeUpdatedBy', exercisetype.ExerciseTypeUpdatedBy);
    params.set('ExerciseTypeUpdatedDate', exercisetype.ExerciseTypeUpdatedDate);

    //var url = "http://localhost:50240/api/tcscore/UpdateLicenseDetails";
    //var id = account.Id    
    //let body: any = {};
    //body.CompanyName = license.CompanyName;
    //console.log(body);
    return this.http.post(this.baseUrl +'api/tcscore/UpdateExerciseType', params.toString(), {
    // return this.http.put('http://localhost:50240/api/tcscore/UpdateLicenseDetails?id=' + account.Id + '', body, options).map(res => res.json());
   // return this.http.post('http://localhost:50240/api/tcscore/UpdateExerciseType', params.toString(), {
      headers: headers
    }).map((response: Response) => <any>response.json());
  }



  //** Program Structure ** //



  onGetAllProgramStructureList(id: any): Observable<any> {
   // return this.http.get('http://52.77.87.237:8080/api/tcscore/GetAllProgramStructure?AccountId=' + id + '').map((res: Response) =>
    return this.http.get(this.baseUrl +'api/tcscore/GetAllProgramStructures?AccountId=' + id + '').map((res: Response) =>
      res.json());

  }

  onGetProgramStructureById(id: any): Observable<any> {
    console.log("id" + id);
    return this.http.get(this.baseUrl +'api/tcscore/GetProgramStructureById?pgrstrid=' + id + '').map((res: Response) =>
   // return this.http.get('http://localhost:50240/api/tcscore/GetProgramStructureById?id=' + id + '').map((res: Response) =>
      res.json());
  }


  onCreateProgramStructure(prmst: any) {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });

   // alert(JSON.stringify(prmst));

    //let body :any= {};
    //body.ProgramStrName = prmst.ProgramStrName;

    //var dd = {
    //  "ProgramStrName":"test",
    //}

    var params = new URLSearchParams();
    //for (let prmst of programs) {
      params.set('ProgramStrName', prmst.ProgramStrName);
      params.set('ProgramStrType', prmst.ProgramStrType);
      params.set('ProgramStrTitle', prmst.ProgramStrTitle);
    params.set('ProgramStrDependency', prmst.ProgramStrDependency);
    params.set('ProgramStrCoverPicture', prmst.ProgramStrCoverPicture);
    params.set('ProgramStrDescription', prmst.ProgramStrDescription);
      //params.set('NoOfCapabilities', prmst.NoOfCapabilities);
      //params.set('NoOfModules', prmst.NoOfModules);
        params.set('ProgramStrCapabilities', prmst.ProgramStrCapabilities);
       params.set('ProgramStrModules', prmst.ProgramStrModules);
        params.set('ProgramStrExercise', prmst.ProgramStrExercise);
    params.set('ProgramCategory', prmst.ProgramCategory);
      params.set('ProgramStrAccessType', prmst.ProgramStrAccessType);
      params.set('ProgramStrStatus', prmst.ProgramStrStatus);
    params.set('ProgramStrListedPrice', prmst.ProgramStrListedPrice);
    params.set('ProgramStrDiscountPrice', prmst.ProgramStrDiscountPrice);
    params.set('ProgramStrIsFree', prmst.ProgramStrIsFree);
    params.set('ProgramStrCreatedBy', prmst.ProgramStrCreatedBy);
    params.set('ProgramStrCreatedDate', prmst.ProgramStrCreatedDate);
    params.set('ProgramStrUpdatedBy', prmst.ProgramStrUpdatedBy);
    params.set('ProgramStrUpdatedDate', prmst.ProgramStrUpdatedDate);
      console.log(params.toString());

      return this.http.post(this.baseUrl + 'api/tcscore/CreateProgramStructure', params.toString(), { headers: headers }).map(res =>this.response=res.json());
    //}




    //this.response;
   // alert(params.toString());

    //let options = new RequestOptions({ headers: headers, params: params })

   
   //  return this.http.post('http://localhost:50240/api/TCSCore/CreateProgramStructure', params.toString(), {
  //  return this.http.post('http://localhost:50240/api/TCSCore/CreateProgramStructure', dd, {
    
  }


  onUpdateProgramStructure(prmst) {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    var params = new URLSearchParams();
    params.set('ProgramStructure_Id', prmst.Id);
    params.set('ProgramStrName', prmst.ProgramStrName);
    params.set('ProgramStrType', prmst.ProgramStrType);
    params.set('ProgramStrTitle', prmst.ProgramStrTitle);
    params.set('ProgramStrDependency', prmst.ProgramStrDependency);
    params.set('ProgramStrCoverPicture', prmst.ProgramStrCoverPicture);
    params.set('ProgramStrDescription', prmst.ProgramStrDescription);
    //params.set('NoOfCapabilities', prmst.NoOfCapabilities);
    //params.set('NoOfModules', prmst.NoOfModules);
      params.set('ProgramStrCapabilities', prmst.ProgramStrCapabilities);
     params.set('ProgramStrModules', prmst.ProgramStrModules);
      params.set('ProgramStrExercise', prmst.ProgramStrExercise);
    params.set('ProgramCategory', prmst.ProgramCategory);
    params.set('ProgramStrAccessType', prmst.ProgramStrAccessType);
    params.set('ProgramStrStatus', prmst.ProgramStrStatus);
    params.set('ProgramStrListedPrice', prmst.ProgramStrListedPrice);
    params.set('ProgramStrDiscountPrice', prmst.ProgramStrDiscountPrice);
    params.set('ProgramStrIsFree', prmst.ProgramStrIsFree);
    params.set('ProgramStrCreatedBy', prmst.ProgramStrCreatedBy);
    params.set('ProgramStrCreatedDate', prmst.ProgramStrCreatedDate);
    params.set('ProgramStrUpdatedBy', prmst.ProgramStrUpdatedBy);
    params.set('ProgramStrUpdatedDate', prmst.ProgramStrUpdatedDate);
    //var url = "http://localhost:50240/api/tcscore/UpdateAccountMaster";
    //var id = account.Id    

    return this.http.post(this.baseUrl +'api/tcscore/UpdateProgramStructure', params.toString(), {
      // return this.http.put('http://localhost:50240/api/tcscore/UpdateAccountMaster?id=' + account.Id + '', body, options).map(res => res.json());
     //  return this.http.post('http://localhost:50240/api/tcscore/UpdateProgramStructure', params.toString(), {
      headers: headers
    }).map((response: Response) => <any>response.json());
  }



  //** Events  **//
  onGetAllEventtypes(id:any): Observable<any> {
  //  return this.http.get('http://52.77.87.237:8080/api/tcscore/GetAllEventType').map((res: Response) =>
    return this.http.get(this.baseUrl +'eventType/getAllEventTypesByAccountId/'+ id + '').map((res: Response) =>
      res.json());

  }

  onGetEventtypeById(id: any): Observable<any> {
    console.log("id" + id);
    return this.http.get(this.baseUrl +'eventType/getEventTypeByEventTypeId/' + id + '').map((res: Response) =>
     // return this.http.get('http://localhost:50240/api/tcscore/GetEventTypeById?id=' + id + '').map((res: Response) =>
      res.json());
  }


  onCreateEventType(event, formData) {
    let body: any = {};
    formData.append("MasterAccount_Id", event.MasterAccount_Id);
    formData.append("EventName", event.EventName);
    formData.append("EventIcon", event.EventIcon);
    formData.append("EventDescription", event.EventDescription);
    formData.append("EventCreatedBy", event.EventCreatedBy);
    formData.append("EventStatus", event.EventStatus);
  

    
    return this.http.post(this.baseUrl + 'eventType/createEventType', formData).map((response: any) => response);
    // console.log(body);
   
  }


  onUpdateEventType(event) {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    var params = new URLSearchParams();
    params.set('EventType_Id', event.Id);
    params.set('MasterAccount_Id', event.MasterAccount_Id);
    params.set('EventName', event.EventName);
    params.set('EventIcon', event.EventIcon);
    params.set('EventDescription', event.EventDescription);
    params.set('EventCreatedBy', event.EventCreatedBy);
    params.set('EventCreatedDate', event.EventCreatedDate);
    params.set('EventUpdatedBy', event.EventUpdatedBy);
    params.set('EventUpdatedDate', event.EventUpdatedDate);
    params.set('EventStatus', event.EventStatus);

    //var url = "http://localhost:50240/api/tcscore/UpdateAccountMaster";
    //var id = account.Id    

    return this.http.post(this.baseUrl +'api/tcscore/UpdateEventType', params.toString(), {
      // return this.http.put('http://localhost:50240/api/tcscore/UpdateAccountMaster?id=' + account.Id + '', body, options).map(res => res.json());
      // return this.http.post('http://localhost:50240/api/tcscore/UpdateEventType', params.toString(), {
      headers: headers
    }).map((response: Response) => <any>response.json());
  }

  //Webinars

  onGetAllUpcomingWebinars(web): Observable<any> {
    if(web)
      return this.http.get(this.baseUrl + 'webinar/upcomingWebinars/' + web.organizer_key + '/' + web.access_token).map((res: Response) =>
      res.json());

  }


  onGetAllHistoricalWebinars(web): Observable<any> {
    if (web)
      return this.http.get(this.baseUrl + 'webinar/historicalWebinars/' + web.organizer_key + '/' + web.access_token).map((res: Response) =>
        res.json());

  }

  onCreateWebinarpath(): Observable<any> {
    return this.http.get(this.baseUrl + 'webinarAuthToken').map((res: Response) =>
      res.json());
  }

  //EventMaster
  onGetAllEventMaster(id: any): Observable<any> {

    return this.http.get(this.baseUrl +'eventMaster/getEventsByFilter/'+ id + '').map((res: Response) =>
      res.json());

  }

  onGetEventMasterById(id: any): Observable<any> {
    console.log("id" + id);
    return this.http.get(this.baseUrl +'api/tcscore/GetEventMasterById?id=' + id + '').map((res: Response) =>
   // return this.http.get('http://localhost:50240/api/tcscore/GetEventMasterById?id=' + id + '').map((res: Response) =>
      res.json());
  }

  onsetWebinarSession() {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    headers.append('Access-Control-Allow-Credentials', 'true');
    headers.append('Access-Control-Allow-Methods', 'POST, GET, HEAD,OPTIONS, DELETE, PUT');
    headers.append('Access-Control-Allow-Origin', '*');
   // headers.append("Access-Control-Allow-Header", "accept, content-type");
    headers.append('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept,Authorization');

 //   headers.append("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers,Access-Control-Allow-Origin");
    let options = new RequestOptions({ headers: headers });       
    return this.http.get(this.baseUrl + 'test').map((response: any) => response);

  }
  onCreateEventmaster(event, formData) {


    //let gl: any = {};
    //gl = { GuidelineName: event.GuidelineName, GuidelineDescription: event.GuidelineDescription };


    //let rt: any = {};
    //rt = { TemplateName: event.TemplateName, Attributes: event.Attributes };

    formData.append("EventType_Id", event.EventType_Id);
    formData.append("PresenterId", event.Member_Id);
    formData.append("EventTitle", event.EventTitle);
    formData.append("EventName", event.EventName);
    formData.append("EventImageUrl", event.EventImageUrl);
    formData.append("EventPresenterImageUrl", event.EventPresenterImageUrl);
    formData.append("EventDescription", event.EventDescription);
    formData.append("EventScheduledStartDate", event.EventScheduledStartDate);
    formData.append("EventScheduledStartTime", event.EventScheduledStartTime);
    formData.append("EventScheduledEndDate", event.EventScheduledEndDate);
    formData.append("EventScheduledEndTime", event.EventScheduledEndTime);
    formData.append("EventAccessUrl", event.EventAccessUrl);
    formData.append("EventRegistrationUrl", event.EventRegistrationUrl);
    formData.append("EventCompletionTemplate", event.EventCompletionTemplate);
    formData.append("EventReminderTemplate", event.EventReminderTemplate);
    formData.append("EventIsRemindersEnabled", event.EventIsRemindersEnabled);
    formData.append("EventIsFree", event.EventIsFree);
    formData.append("EventIsNotificationsEnabled", event.EventIsNotificationsEnabled);
    formData.append("EventResultsUrl", event.EventResultsUrl);
    formData.append("EventSurveyUrl", event.EventSurveyUrl);
    formData.append("EventExerciseUrl", event.EventExerciseUrl);
    formData.append("EventAssignmentUrl", event.EventAssignmentUrl);
    formData.append("EventFeedbackUrl", event.EventFeedbackUrl);
    formData.append("EventParticipationCount", event.EventParticipationCount);
    formData.append("EventCreatedBy", event.EventCreatedBy);
    formData.append("EventStatus", event.EventStatus);
    formData.append("ProgramCategory_Id", event.ProgramCategory_Id);
    formData.append("EventLocation", event.EventLocation);
    formData.append("EventPrice", event.EventPrice);
    formData.append("EventCategoryName", event.EventCategoryName);
    formData.append("EventRegistrationTemplate", event.EventRegistrationTemplate);
    formData.append("EventGuidelines", event.EventGuidelines);
    formData.append("webinarToken", event.access_token);
    formData.append("organizerId", event.organizer_key);

    return this.http.post(this.baseUrl + 'eventMaster/createEventMaster', formData).map((response: any) => response);
  
  }







  onUpdateEventmaster(event: any) {


    let gl: any = {};
    gl = { GuidelineName: event.GuidelineName, GuidelineDescription: event.GuidelineDescription };


    let rt: any = {};
    rt = { TemplateName: event.TemplateName, Attributes: event.Attributes };

    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });

    var params = new URLSearchParams();
    params.set('EventMaster_Id', event.Id);
    params.set('EventType_Id', event.EventType_Id);
    params.set('EventTitle', event.EventTitle);
    params.set('EventName', event.EventName);
    params.set('EventImageUrl', event.EventImageUrl);
    params.set('EventPresenterImageUrl', event.EventPresenterImageUrl);
    params.set('EventDescription', event.EventDescription);
    params.set('EventScheduledStartDate', event.EventScheduledStartDate);
    params.set('EventScheduledStartTime', event.EventScheduledStartTime);
    params.set('EventScheduledEndDate', event.EventScheduledEndDate);
    params.set('EventScheduledEndTime', event.EventScheduledEndTime);
    params.set('EventAccessUrl', event.EventAccessUrl);
    params.set('EventRegistrationUrl', event.EventRegistrationUrl);
    params.set('EventCompletionTemplate', event.EventCompletionTemplate);
    params.set('EventReminderTemplate', event.EventReminderTemplate);
    params.set('EventIsRemindersEnabled', event.EventIsRemindersEnabled);
    params.set('EventIsNotificationsEnabled', event.EventIsNotificationsEnabled);
    params.set('EventResultsUrl', event.EventResultsUrl);
    params.set('EventSurveyUrl', event.EventSurveyUrl);
    params.set('EventExerciseUrl', event.EventExerciseUrl);
    params.set('EventAssignmentUrl', event.EventAssignmentUrl);
    params.set('EventFeedbackUrl', event.EventFeedbackUrl);
    params.set('EventParticipationCount', event.EventParticipationCount);
    params.set('EventCreatedBy', event.EventCreatedBy);
    params.set('EventCreatedDate', event.EventCreatedDate);
    params.set('EventUpdatedBy', event.EventUpdatedBy);
    params.set('EventUpdatedDate', event.EventUpdatedDate);
    params.set('EventStatus', event.EventStatus);
    params.set('ProgramCategory_Id', event.ProgramCategory_Id);
    params.set('EventLocation', event.EventLocation);
    params.set('EventPrice', event.EventPrice);
    params.set('EventCategoryName', event.EventCategoryName);
    params.set('EventRegistrationTemplate', JSON.stringify(rt));
    params.set('EventGuidelines', JSON.stringify(gl));

    // console.log(body);
    return this.http.post(this.baseUrl +'api/TCSCore/UpdateEventMaster', params.toString(), {
  //  return this.http.post('http://localhost:50240/api/TCSCore/UpdateEventMaster', params.toString(), {
      headers: headers
    }).map(res =>
      res.json());
  }



  //** Forum Discussion  **//
  onGetAllForumdiscussionList(id: any): Observable<any> {
  //  return this.http.get('http://52.77.87.237:8080/api/tcscore/GetAllForumDiscussions?AccountId=' + id + '').map((res: Response) =>
    return this.http.get(this.baseUrl +'api/TCSCore/GetAllForumDiscussions?AccountId=' + id + '').map((res: Response) =>
      res.json());

  }

  onGetForumdiscussionById(id: any): Observable<any> {
    console.log("id" + id);
    return this.http.get(this.baseUrl +'api/tcscore/GetForumDiscussionById?ForumDiscussionid=' + id + '').map((res: Response) =>
   // return this.http.get('http://localhost:50240/api/tcscore/GetForumDiscussionById?ForumDiscussionid=' + id + '').map((res: Response) =>
      res.json());
  }


  onCreateForumdiscussion(forum: any) {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });

    var params = new URLSearchParams();
    params.set('Forum_Id', forum.Forum_Id);
    params.set('ForumTopics_Id', forum.ForumTopics_Id);
    params.set('Member_Id', forum.Member_Id);
    params.set('ForumDiscussionComments', forum.ForumDiscussionComments);
    params.set('ForumDiscussionFile1', forum.ForumDiscussionFile1);
    params.set('ForumDiscussionFile2', forum.ForumDiscussionFile2);
    params.set('ForumDiscussionFile3', forum.ForumDiscussionFile3);
  //  params.set('ForumDiscussionIsModerated', forum.ForumDiscussionIsModerated);
  //  params.set('ForumDiscussionPositiveCounters', forum.ForumDiscussionPositiveCounters);
   // params.set('ForumDiscussionNegativeCounters', forum.ForumDiscussionNegativeCounters);
    params.set('ForumDiscussionStatus', forum.ForumDiscussionStatus);
    params.set('ForumDiscussionCreatedDate', forum.ForumDiscussionCreatedDate);
    params.set('ForumDiscussionCreatedBy', forum.ForumDiscussionCreatedBy);
    params.set('ForumDiscussionUpdatedBy', forum.ForumDiscussionUpdatedBy);
    params.set('ForumDiscussionUpdatedDate', forum.ForumDiscussionUpdatedDate);
    // console.log(body);
    return this.http.post(this.baseUrl +'api/tcscore/CreateForumDiscussion', params.toString(), {
   // return this.http.post('http://localhost:50240/api/TCSCore/CreateForumDiscussion', params.toString(), {
      headers: headers
    }).map(res =>
      res.json());
  }


  onUpdateForumdiscussion(forum) {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    var params = new URLSearchParams();
    params.set('ForumDiscussion_Id', forum.Id);
    params.set('Forum_Id', forum.Forum_Id);
    params.set('ForumTopics_Id', forum.ForumTopics_Id);
    params.set('Member_Id', forum.Member_Id);
    params.set('ForumDiscussionComments', forum.ForumDiscussionComments);
    params.set('ForumDiscussionFile1', forum.ForumDiscussionFile1);
    params.set('ForumDiscussionFile2', forum.ForumDiscussionFile2);
    params.set('ForumDiscussionFile3', forum.ForumDiscussionFile3);
   // params.set('ForumDiscussionIsModerated', forum.ForumDiscussionIsModerated);
   // params.set('ForumDiscussionPositiveCounters', forum.ForumDiscussionPositiveCounters);
   // params.set('ForumDiscussionNegativeCounters', forum.ForumDiscussionNegativeCounters);
    params.set('ForumDiscussionStatus', forum.ForumDiscussionStatus);
    params.set('ForumDiscussionCreatedDate', forum.ForumDiscussionCreatedDate);
    params.set('ForumDiscussionCreatedBy', forum.ForumDiscussionCreatedBy);
    params.set('ForumDiscussionUpdatedBy', forum.ForumDiscussionUpdatedBy);
    params.set('ForumDiscussionUpdatedDate', forum.ForumDiscussionUpdatedDate);


    //var url = "http://localhost:50240/api/tcscore/UpdateAccountMaster";
    //var id = account.Id    

    return this.http.post(this.baseUrl +'api/tcscore/UpdateForumDiscussion', params.toString(), {
    // return this.http.put('http://localhost:50240/api/tcscore/UpdateAccountMaster?id=' + account.Id + '', body, options).map(res => res.json());
   // return this.http.post('http://localhost:50240/api/tcscore/UpdateForumDiscussion', params.toString(), {
      headers: headers
    }).map((response: Response) => <any>response.json());
  }

  //Forum//



  onGetAllForumList(id: any): Observable<any> {
    // return this.http.get('http://52.77.87.237:8080/api/tcscore/GetAllForums?AccountId='+ id + '').map((res: Response) => res.json());
    return this.http.get(this.baseUrl +'api/TCSCore/GetAllForums?AccountId=' + id + '').map((res: Response) => res.json());
    // console.log(res);
  }

  onGetForumById(id: any): Observable<any> {
    console.log("id" + id);

   // return this.http.get('http://localhost:50240/api/tcscore/GetForumByID?Forumid=' + id + '').map((res: Response) => res.json());
    return this.http.get(this.baseUrl +'api/TCSCore/GetForumByID?Forumid=' + id + '').map((res: Response) => res.json());
  }

  onCreateForum(forum: any) {

    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });

    var params = new URLSearchParams();
   
    params.set('MasterAccount_Id', forum.MasterAccount_Id);
    params.set('ForumName', forum.ForumName);
    params.set('ForumIcon', forum.ForumIcon);
    params.set('ForumIsModerated', forum.ForumIsModerated);
    params.set('ForumIsDeleted', forum.ForumIsDeleted);
    params.set('ForumCreatedDate', forum.ForumCreatedDate);
    params.set('ForumCreatedBy', forum.ForumCreatedBy);
    params.set('ForumUpdatedDate', forum.ForumUpdatedDate);
    params.set('ForumUpdatedBy', forum.ForumUpdatedBy);
    params.set('ForumStatus', forum.ForumStatus);
    return this.http.post(this.baseUrl +'api/tcscore/CreateForum', params.toString(), {
   // return this.http.post('http://localhost:50240/api/TCSCore/CreateForum', params.toString(), {
      headers: headers
    }).map(res =>
      res.json());
  }


  onUpdateForum(forum) {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    var params = new URLSearchParams();
    params.set('Forum_Id', forum.Id);
    params.set('ForumIcon', forum.ForumIcon);
    params.set('MasterAccount_Id', forum.MasterAccount_Id);
    params.set('ForumName', forum.ForumName);
    params.set('ForumIsModerated', forum.ForumIsModerated);
    params.set('ForumIsDeleted', forum.ForumIsDeleted);
    params.set('ForumCreatedDate', forum.ForumCreatedDate);
    params.set('ForumCreatedBy', forum.ForumCreatedBy);
    params.set('ForumUpdatedDate', forum.ForumUpdatedDate);
    params.set('ForumUpdatedBy', forum.ForumUpdatedBy);
    params.set('ForumStatus', forum.ForumStatus);



    return this.http.post(this.baseUrl +'api/tcscore/UpdateForum', params.toString(), {
   // return this.http.post('http://localhost:50240/api/tcscore/UpdateForum', params.toString(), {
      headers: headers
    }).map((response: Response) => <any>response.json());
  }


  //ForimTopics


  onGetAllForumTopicsList(id: any): Observable<any> {
   //  return this.http.get('http://52.77.87.237:8080/api/TCSCore/GetAllForumTopics').map((res: Response) =>
    return this.http.get(this.baseUrl +'api/TCSCore/GetAllForumTopics?AccountId=' + id + '').map((res: Response) =>
      res.json());

  }
 

  onGetForumTopicById(id: any): Observable<any> {
    console.log("id" + id);

   // return this.http.get('http://localhost:50240/api/tcscore/GetForumTopicById?ForumTopicid=' + id + '').map((res: Response) => res.json());
    return this.http.get(this.baseUrl +'api/TCSCore/GetForumTopicById?ForumTopicid=' + id + '').map((res: Response) => res.json());
  }

  onCreateForumTopic(forumtopic) {

    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });

    var params = new URLSearchParams();
  
    params.set('Forum_Id', forumtopic.Forum_Id);
    params.set('Member_Id', forumtopic.Member_Id);
    params.set('ForumTopicTitle', forumtopic.ForumTopicTitle);
    params.set('ForumTopicContent', forumtopic.ForumTopicContent);
    params.set('ForumTopicType', forumtopic.ForumTopicType);
    params.set('ForumTopicFile1', forumtopic.ForumTopicFile1);
    params.set('ForumTopicFile2', forumtopic.ForumTopicFile2);
    params.set('ForumTopicFile3', forumtopic.ForumTopicFile3);
    params.set('ForumTopicIsModerated', forumtopic.ForumTopicIsModerated);
    params.set('ForumTopicCreatedDate', forumtopic.ForumTopicCreatedDate);
    params.set('ForumTopicCreatedBy', forumtopic.ForumTopicCreatedBy);
    params.set('ForumTopicUpdatedDate', forumtopic.ForumTopicUpdatedDate);
    params.set('ForumTopicUpdatedBy', forumtopic.ForumTopicUpdatedBy);
    params.set('ForumTopicStatus', forumtopic.ForumTopicStatus);
    return this.http.post(this.baseUrl +'api/tcscore/CreateForumTopic', params.toString(), {
   // return this.http.post('http://localhost:50240/api/TCSCore/CreateForumTopic', params.toString(), {
      headers: headers
    }).map(res =>
      res.json());
  }


  onUpdateForumTopic(forumtopic) {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    var params = new URLSearchParams();
    params.set('ForumTopics_Id', forumtopic.Id);
    params.set('Forum_Id', forumtopic.Forum_Id);
    params.set('Member_Id', forumtopic.Member_Id);
    params.set('ForumTopicTitle', forumtopic.ForumTopicTitle);
    params.set('ForumTopicContent', forumtopic.ForumTopicContent);
    params.set('ForumTopicType', forumtopic.ForumTopicType);
    params.set('ForumTopicFile1', forumtopic.ForumTopicFile1);
    params.set('ForumTopicFile2', forumtopic.ForumTopicFile2);
    params.set('ForumTopicFile3', forumtopic.ForumTopicFile3);
    params.set('ForumTopicIsModerated', forumtopic.ForumTopicIsModerated);
    params.set('ForumTopicCreatedDate', forumtopic.ForumTopicCreatedDate);
    params.set('ForumTopicCreatedBy', forumtopic.ForumTopicCreatedBy);
    params.set('ForumTopicUpdatedDate', forumtopic.ForumTopicUpdatedDate);
    params.set('ForumTopicUpdatedBy', forumtopic.ForumTopicUpdatedBy);
    params.set('ForumTopicStatus', forumtopic.ForumTopicStatus);


    return this.http.post(this.baseUrl +'api/tcscore/UpdateForumTopic', params.toString(), {
 //  return this.http.post('http://localhost:50240/api/tcscore/UpdateForumTopic', params.toString(), {
      headers: headers
    }).map((response: Response) => <any>response.json());
  }



  OnPostNotifications(members) {

    let body: any = {};
     body.memberIdsList = members.memberIdsList;
  //body.memberIdsList = ["5acb425fa4e676248031953f","5b0248c95fa99457df95abfd"],
      body.activityType = members.activityType;
    body.message = members.message;
    body.meaasgeBody = members.meaasgeBody;
    body.notificationChanelType = members.notificationChanelType;
    console.log(body);
    alert(JSON.stringify(body));
    return this.http.post('http://192.168.1.13:4300/api/notification/sendNotifications1', body).map((res: Response) =>
     res.json());
   // return null;
  }


  OnGetAllEventRegistrations(id: any): Observable<any> {
    console.log("id" + id);

    // return this.http.get('http://localhost:50240/api/tcscore/GetForumByID?Forumid=' + id + '').map((res: Response) => res.json());
    return this.http.get(this.baseUrl + 'api/TCSCore/GetAllEventRegistrations?AccountId=' + id + '').map((res: Response) => res.json());
  }

  onGetEventRegistrationsById(id: any): Observable<any> {
    console.log("id" + id);
    return this.http.get(this.baseUrl + 'api/tcscore/GetEventRegistrationById?everegid=' + id + '').map((res: Response) =>
      // return this.http.get('http://localhost:50240/api/tcscore/GetEventRegistrationById?everegid=' + id + '').map((res: Response) =>
      res.json());
  }

  OnGetAllMemberEventPurchases(id: any): Observable<any> {
    console.log("id" + id);

    // return this.http.get('http://localhost:50240/api/tcscore/GetForumByID?Forumid=' + id + '').map((res: Response) => res.json());
    return this.http.get(this.baseUrl + 'api/TCSCore/GetAllMemberEventPurchases?AccountId=' + id + '').map((res: Response) => res.json());
  }


  onGetMemberEventPurchaseById(id: any): Observable<any> {
    console.log("id" + id);
    return this.http.get(this.baseUrl + 'api/tcscore/GetMemberEventPurchaseById?evepurid=' + id + '').map((res: Response) =>
      // return this.http.get('http://localhost:50240/api/tcscore/GetEventRegistrationById?everegid=' + id + '').map((res: Response) =>
      res.json());
  }


  //Services//

  onGetAllServiceList(id: any) {
    return this.http.get(this.baseUrl + 'api/TCSCore/GetAllServices?AccountId=' + id + '').map((res: Response) =>
      // return this.http.get(this.baseUrl +'api/tcscore/GetAllAccountsMaster').map((res: Response) =>
      res.json());
    // console.log(res);
  }

  onGetServiceById(id: any): Observable<any> {
  
    //return this.http.get(this.baseUrl +'api/tcscore/GetAccountMasterById?accountmasterid=' + id + '').map((res: Response) =>
    return this.http.get(this.baseUrl + 'api/tcscore/GetServiceById?serviceId=' + id + '').map((res: Response) =>
      res.json());
  }

  

  onCreateService(service) {

    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });

    var params = new URLSearchParams();
    params.set('ServiceType_Id', service.ServiceType_Id);
    params.set('ServiceTitle', service.ServiceTitle);
    params.set('ServiceDescription', service.ServiceDescription);
    params.set('ServiceContributor', service.ServiceContributor);
    params.set('ServiceCoverPicture', service.ServiceCoverPicture);
    params.set('ServiceTag', service.ServiceTag);
    params.set('IsFreeService', service.IsFreeService);
    params.set('ServiceDiscountedPrice', service.ServiceDiscountedPrice);
    params.set('ServiceValidStartDate', service.ServiceValidStartDate);
    params.set('ServiceValidEndDate', service.ServiceValidEndDate);
    params.set('ServiceListedPrice', service.ServiceListedPrice);
    params.set('ServiceStatus', service.ServiceStatus);
    params.set('ServiceLink', service.ServiceLink);
    params.set('ServiceCreatedBy', service.ServiceCreatedBy);
    params.set('ServiceCreatedDate', service.ServiceCreatedDate);
    params.set('ServiceUpdatedBy', service.ServiceUpdatedBy);
    params.set('ServiceUpdatedDate', service.ServiceUpdatedDate);

    // console.log(body);
    return this.http.post(this.baseUrl + 'api/tcscore/CreateService', params.toString(), {
      //   return this.http.post(this.baseUrl +'api/TCSCore/CreateAccountMaster', params.toString(), {
      headers: headers
    }).map(res =>
      res.json());
  }

  onUpdateService(service) {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    var params = new URLSearchParams();
    params.set('Service_Id', service.Id);
    params.set('ServiceType_Id', service.ServiceType_Id);
    params.set('ServiceTitle', service.ServiceTitle);
    params.set('ServiceDescription', service.ServiceDescription);
    params.set('ServiceContributor', service.ServiceContributor);
    params.set('ServiceCoverPicture', service.ServiceCoverPicture);
    params.set('ServiceTag', service.ServiceTag);
    params.set('IsFreeService', service.IsFreeService);
    params.set('ServiceDiscountedPrice', service.ServiceDiscountedPrice);
    params.set('ServiceValidStartDate', service.ServiceValidStartDate);
    params.set('ServiceValidEndDate', service.ServiceValidEndDate);
    params.set('ServiceListedPrice', service.ServiceListedPrice);
    params.set('ServiceStatus', service.ServiceStatus);
    params.set('ServiceLink', service.ServiceLink);
    params.set('ServiceCreatedBy', service.ServiceCreatedBy);
    params.set('ServiceCreatedDate', service.ServiceCreatedDate);
    params.set('ServiceUpdatedBy', service.ServiceUpdatedBy);
    params.set('ServiceUpdatedDate', service.ServiceUpdatedDate);

    return this.http.post(this.baseUrl + 'api/tcscore/UpdateService', params.toString(), {
      //   return this.http.post(this.baseUrl +'api/TCSCore/CreateAccountMaster', params.toString(), {
      headers: headers
    }).map(res =>
      res.json());
  }

  //ServiceType//
  onGetAllServiceTypeList(id: any) {
    return this.http.get(this.baseUrl + 'api/TCSCore/GetAllServicesType?AccountId=' + id + '').map((res: Response) =>
    //return this.http.get('http://52.77.87.237:8080/api/tcscore/GetAllServiceType').map((res: Response) =>
      res.json());

  }

  onGetServiceTypeById(id: any): Observable<any> {
    console.log("id" + id);
   // return this.http.get('http://52.77.87.237:8080/api/tcscore/GetServiceTypeById?servicetypeId=' + id + '').map((res: Response) =>
    return this.http.get(this.baseUrl + 'api/TCSCore/GetServiceTypeById?servicetypeId=' + id + '').map((res: Response) =>
      res.json());
  }


  onCreateServiceType(servicetype: any) {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });

    var params = new URLSearchParams();
    params.set('ServiceName', servicetype.ServiceName);
    params.set('ServiceIcon', servicetype.ServiceIcon);
    params.set('MasterAccount_Id', servicetype.MasterAccount_Id);
    params.set('ServiceTypeStatus', servicetype.ServiceTypeStatus);
    params.set('ServiceTypeCreatedBy', servicetype.ServiceTypeCreatedBy);
    params.set('ServiceTypeCreatedDate', servicetype.ServiceTypeCreatedDate);
    params.set('ServiceTypeUpdatedBy', servicetype.ServiceTypeUpdatedBy);
    params.set('ServiceTypeUpdatedDate', servicetype.ServiceTypeUpdatedDate);
    // console.log(body);
   // return this.http.post('http://52.77.87.237:8080/api/tcscore/CreateServiceType', params.toString(), {
    return this.http.post(this.baseUrl + 'api/tcscore/CreateServiceType', params.toString(), {
      headers: headers
    }).map(res =>
      res.json());
  }


  onUpdateServiceType(servicetype) {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    var params = new URLSearchParams();
    params.set('ServiceType_Id', servicetype.Id);
    params.set('ServiceIcon', servicetype.ServiceIcon);
    params.set('ServiceName', servicetype.ServiceName);
    params.set('MasterAccount_Id', servicetype.MasterAccount_Id);
    params.set('ServiceTypeStatus', servicetype.ServiceTypeStatus);
    params.set('ServiceTypeCreatedBy', servicetype.ServiceTypeCreatedBy);
    params.set('ServiceTypeCreatedDate', servicetype.ServiceTypeCreatedDate);
    params.set('ServiceTypeUpdatedBy', servicetype.ServiceTypeUpdatedBy);
    params.set('ServiceTypeUpdatedDate', servicetype.ServiceTypeUpdatedDate);

  //  return this.http.post('http://52.77.87.237:8080/api/tcscore/UpdateServiceType', params.toString(), {
    return this.http.post(this.baseUrl + 'api/tcscore/UpdateServiceType', params.toString(), {

      headers: headers
    }).map((response: Response) => <any>response.json());
  }




  onGetAllServicePurchases(id: any) {
    return this.http.get(this.baseUrl + 'api/TCSCore/GetAllServicePurchase?AccountId=' + id + '').map((res: Response) =>
      //return this.http.get('http://52.77.87.237:8080/api/tcscore/GetAllServiceType').map((res: Response) =>
      res.json());

  }



  onGetServicePurchaseById(id: any): Observable<any> {
    console.log("id" + id);
    return this.http.get(this.baseUrl + 'api/TCSCore/GetServicePurchaseById?servicepurchseId=' + id + '').map((res: Response) =>
      // return this.http.get('http://localhost:50240/api/tcscore/GetServerInstanceById?instanceid=' + id + '').map((res: Response) =>
      res.json());
  }


  onGetAllMemberProgramPurchases(id: any) {
    return this.http.get(this.baseUrl + 'api/TCSCore/GetAllMemberProgramPurchase?AccountId=' + id + '').map((res: Response) =>
      //return this.http.get('http://52.77.87.237:8080/api/tcscore/GetAllServiceType').map((res: Response) =>
      res.json());

  }

  onGetMemberProgramPurchaseById(id: any): Observable<any> {
    console.log("id" + id);
    return this.http.get(this.baseUrl + 'api/TCSCore/GetMemberProgramPurchaseById?memberprogarampurchaseId=' + id + '').map((res: Response) =>
      res.json());
  }

}

















