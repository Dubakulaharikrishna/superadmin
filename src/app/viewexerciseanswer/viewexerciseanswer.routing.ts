import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';

import { ViewexerciseanswerComponent } from './viewexerciseanswer.component';

export const ViewexerciseanswerRoutes: Routes = [

  {
    path: '',
    component: ViewexerciseanswerComponent,
    canActivate: [AuthGuard],
    data: {
      heading: 'Viewexercisequestion!'
    }
  },


];
