import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewexerciseanswerComponent } from './viewexerciseanswer.component';

describe('ViewexerciseanswerComponent', () => {
  let component: ViewexerciseanswerComponent;
  let fixture: ComponentFixture<ViewexerciseanswerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewexerciseanswerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewexerciseanswerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
