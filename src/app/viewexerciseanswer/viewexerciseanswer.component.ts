
import { AdminService } from '../services/admin.service';
import { CustomValidators } from 'ng2-validation';
import { DatePipe } from '@angular/common'
import 'rxjs/add/operator/debounceTime';
import { Subject } from 'rxjs/Subject';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/share';
import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl, } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { CustomFormsModule } from 'ng2-validation';
import { BrowserModule } from '@angular/platform-browser';
import { License } from '../Models/license';
import { retry } from 'rxjs/Operator/retry';
import 'rxjs/Rx';
import { ExerciseAnswer } from '../Models/ExerciseAnswer ';
import { Status } from '../Models/Status';






@Component({
  selector: 'app-viewexerciseanswer',
  templateUrl: './viewexerciseanswer.component.html',
  styleUrls: ['./viewexerciseanswer.component.css'],
  providers: [DatePipe]
})
export class ViewexerciseanswerComponent implements OnInit {
  exeDate: any;


  public exerciseanswerUpdateFormGroup: FormGroup;
  //public form: FormGroup;
  exerciseanswer: ExerciseAnswer;
  id: any;
  isSubmitted: boolean = false;
  exaId: any;
  status: Status;

  constructor(private fb: FormBuilder, private adminservice: AdminService, private router: Router, public route: ActivatedRoute, private http: HttpClient, public datepipe: DatePipe) {


    this.exerciseanswer = new ExerciseAnswer();


  }

  ngOnInit() {
    this.ExerciseAnswerform();
    this.route.params.subscribe(params => {
      this.exaId = params['exaId'];

    });
    this.onGetExerciseAnswerById();

  }

  onUpdateExerciseAnswer(exerciseanswer) {


    const exeans = {
      ExerciseAnswer: this.exerciseanswerUpdateFormGroup.controls["exerciseanswer.ExerciseAnswer"].value,
      Submission_Id: this.exerciseanswerUpdateFormGroup.controls["exerciseanswer.Submission_Id"].value,
      Exercise_Question_Id: this.exerciseanswerUpdateFormGroup.controls["exerciseanswer.Exercise_Question_Id"].value,
      Exercise_Id: this.exerciseanswerUpdateFormGroup.controls["exerciseanswer.Exercise_Id"].value,
      Member_Id: this.exerciseanswerUpdateFormGroup.controls["exerciseanswer.Member_Id"].value,
      ExAnsSelectedAnswerOptions: this.exerciseanswerUpdateFormGroup.controls["exerciseanswer.ExAnsSelectedAnswerOptions"].value,
      ExerciseAnswerDetails: this.exerciseanswerUpdateFormGroup.controls["exerciseanswer.ExerciseAnswerDetails"].value,
      ExerciseAnswerRemarks: this.exerciseanswerUpdateFormGroup.controls["exerciseanswer.ExerciseAnswerRemarks"].value,
      ExerciseAnswerSubmissionDateTime: this.exerciseanswerUpdateFormGroup.controls['exerciseanswer.ExerciseAnswerSubmissionDateTime'].value,
      ExerciseAnswerValidityStatus: this.exerciseanswerUpdateFormGroup.controls['exerciseanswer.ExerciseAnswerValidityStatus'].value,
      ExerciseAnswerStatus: this.exerciseanswerUpdateFormGroup.controls['exerciseanswer.ExerciseAnswerStatus'].value,
      
    }


    if (this.exerciseanswerUpdateFormGroup.valid) {
      //  alert(JSON.stringify(accountmanagement));
      this.isSubmitted = false;
     // alert(JSON.stringify(exerciseanswer));
      this.adminservice.onUpdateExerciseAnswer(exerciseanswer).subscribe(res => {
        console.log(res);
        this.exerciseanswer = res
      });
      alert("Successfully Updated");
    }
    else {
      this.isSubmitted = true;
      //alert(JSON.stringify(license));
    }
    //alert(JSON.stringify(account));
  }




  ExerciseAnswerform() {
    this.exerciseanswerUpdateFormGroup = this.fb.group({
      'exerciseanswer.ExerciseAnswer': ['', [Validators.required]],
      'exerciseanswer.Submission_Id': ['', [Validators.required]],

      'exerciseanswer.Exercise_Question_Id': ['', [Validators.required]],

      'exerciseanswer.Exercise_Id': ['', [Validators.required]],
      'exerciseanswer.Member_Id': ['', [Validators.required]],
      'exerciseanswer.ExAnsSelectedAnswerOptions': ['', [Validators.required]],
      'exerciseanswer.ExerciseAnswerDetails': ['', [Validators.required]],
      'exerciseanswer.ExerciseAnswerRemarks': ['', [Validators.required]],
      'exerciseanswer.ExerciseAnswerSubmissionDateTime': ['', [Validators.required]],
      'exerciseanswer.ExerciseAnswerValidityStatus': ['', [Validators.required]],
      //'exerciseanswer.ExerciseAnswerCreatedDate': ['', [Validators.required]],
      //'exerciseanswer.ExerciseAnswerCreatedBy': ['', [Validators.required]],
     // 'exerciseanswer.ExerciseAnswerUpdatedDate': ['', [Validators.required]],
    //  'exerciseanswer.ExerciseAnswerUpdatedBy': ['', [Validators.required]],
      'exerciseanswer.ExerciseAnswerStatus': ['', [Validators.required]],



    });
  }


  onGetExerciseAnswerById() {

    this.adminservice.onGetExerciseAnswerById(this.exaId).subscribe(result => {

      this.status = result;
      if (this.status.StatusCode == "1") {


        this.exerciseanswer = this.status.Data;
      }
      else {
        alert(this.status.Data);
      }

      console.log(this.exerciseanswer);

      if (this.exerciseanswer.ExerciseAnswerSubmissionDateTime != null && this.exerciseanswer.ExerciseAnswerSubmissionDateTime != "" && this.exerciseanswer.ExerciseAnswerSubmissionDateTime != undefined) {
        this.exeDate = this.datepipe.transform(this.exerciseanswer.ExerciseAnswerSubmissionDateTime, 'yyyy-MM-dd');
        this.exerciseanswer.ExerciseAnswerSubmissionDateTime = this.exeDate;
      }
    });
    return this.exerciseanswer;
  }
}

//onUpdateLicense(license) {
//  this.adminservice.onUpdateAccount(license).subscribe(res => this.license = res);

//  }
