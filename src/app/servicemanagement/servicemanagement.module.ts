//import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//import { DashboardComponent } from './dashboard.component';
import { RouterModule } from '@angular/router';
import { ServicemanagementRoutes } from './servicemanagement.routing';
import { ServicemanagementComponent } from '../servicemanagement/servicemanagement.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { HttpModule } from '@angular/http';
import { DataTablesModule } from 'angular-datatables';
import {HttpClientModule} from '@angular/common/http';
import { CommonModule } from '@angular/common'; 
import { BrowserModule } from '@angular/platform-browser';



@NgModule({
  
    imports: [DataTablesModule,
      HttpClientModule,
      CommonModule,
     
   HttpModule,
    RouterModule.forChild(ServicemanagementRoutes),
    //BrowserModule
  ],
  declarations: [
   
    ServicemanagementComponent
    
  ],
 
  providers: [],
  bootstrap: [ServicemanagementComponent]
})
export class ServicemanagementModule { }






