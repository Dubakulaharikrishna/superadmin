import { Routes } from '@angular/router';

import { ServicemanagementComponent } from './servicemanagement.component';

export const ServicemanagementRoutes: Routes = [

  {
    path: '',
    component: ServicemanagementComponent,
    data: {
      heading: 'Servicemanagement'
    }    
  },

  
];

