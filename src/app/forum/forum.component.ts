import { Component, OnInit, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl, } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ExcelService } from '../services/excel.service';
import { DatePipe } from '@angular/common'
import { AdminService } from '../services/admin.service';
import { CustomValidators } from 'ng2-validation';
import 'rxjs/add/operator/debounceTime';
import { Subject } from 'rxjs/Subject';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/share';
import { CommonModule } from '@angular/common';
import { Status } from '../Models/Status';
import { LocalDataSource } from 'ng2-smart-table';
import { Forum } from '../Models/Forum';
import { AccountManagement } from '../Models/AccountManagement';


@Component({
  selector: 'app-forum',
  templateUrl: './forum.component.html',
  styleUrls: ['./forum.component.css'],
  providers: [ExcelService, DatePipe]
})
export class ForumComponent implements OnInit {
  MasterAccount: any;
  forumicon: any;
  success: boolean = false;
  message: any;
  dtTrigger: Subject<any> = new Subject();
  public _success = new Subject<string>();
  staticAlertClosed = false;
  MasterAccount_Id: any;
  public data: Object;
  public temp_var: Object = false;
  public ForumUpdateFormGroup: FormGroup;
  public ForumForm: FormGroup;
  public form: FormGroup;
  forum: Forum;
  listofAccounts: AccountManagement[];
  dataLoaded: any;
  public isSubmitted: boolean = false;
  id: any;
  listofforum: Forum[];
  pagename: any;
  status: Status;
  source: LocalDataSource;
  selectedValue: any = 50;
  page: any;
  roleId: any;
  createdby: any;
  accountId: any;
  constructor(private fb: FormBuilder, public datepipe: DatePipe, public ele: ElementRef, private excelService: ExcelService, private adminservice: AdminService, private router: Router, private http: HttpClient) {

    this.forum = new Forum();
  }


  

  onGetAllForumList(id) {
    //let id = sessionStorage.getItem('AccountId');
    //alert(id);
    this.adminservice.onGetAllForumList(id).subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofforum = this.status.Data;
        this.source = new LocalDataSource(this.listofforum);
        // alert(JSON.stringify(this.Programstructures));
        return this.listofforum;
      }


    });
  }


  onChange(event) {
    console.log(event);
    this.onGetAllForumList(event.target.value);
  }
  reset() {
    this.onGetAllForumList(this.accountId);
    this.MasterAccount = undefined;
  }

  openCpopup() {

    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "block";
    cmask.style.display = "block";
  }

  closeCpopup() {
    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "none";
    cmask.style.display = "none";
  }

  test(id) {
    //  alert("Hi");
    //console.log(id);
    //alert(id);
    console.log(this.MasterAccount_Id);
   // alert(this.MasterAccount_Id);
  }

  onEdit(event:any) {


    this.router.navigate(['/viewforum/', { frmId: event.data.Id}]);
  }



  ngOnInit() {
   // sessionStorage.clear();
    //localStorage.removeItem('loginSessId');
    //localStorage.clear();
   // localStorage.setTimeout = 1;
  //  sessionStorage.removeItem('loginSessId');
   // localStorage.removeItem('loginSessId');
    this.pagename = "Forum";
    localStorage.setItem('loginSessId', this.pagename);
    this.accountId = localStorage.getItem('AccountId');
    this.Forumform();
    this.onGetAllForumList(this.accountId);
    this.onGetAllAccountsList();
    this.roleId = localStorage.getItem('RoleId');
    setTimeout(() => this.staticAlertClosed = true, 20000);
    this._success.subscribe((message) => this.message = message);
    this._success.debounceTime(7000).subscribe(() => this.message = null);
    this.createdby = localStorage.getItem('MemberName');

  }


  onGetAllAccountsList() {
    this.adminservice.onGetAllAccountsList().subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofAccounts = this.status.Data;

        //alert(JSON.stringify(this.listofAccounts));
        return this.listofAccounts
      }

    });
  }
  onCreateForum() {
    let files = this.ele.nativeElement.querySelector('#ForumIcon').files;
    if (files.length > 0 && files.count != 0 && files != null) {

      let formData = new FormData();
      let file = files[0];
      this.forumicon = file.name;
      formData.append('ForumIcon', file, file.name);
      console.log(formData);
      console.log(files);
      this.adminservice.onfileupload(formData).subscribe(res => this.dataLoaded = res);
    }
    let date = new Date();



    const Forum = {
      
   
      //MasterAccount_Id: this.form.controls["MasterAccount_Id"].value,
      MasterAccount_Id: this.form.controls["MasterAccount_Id"].value,
      ForumName: this.form.controls["ForumName"].value,
      ForumIsModerated: this.form.controls["ForumIsModerated"].value,
      ForumIsDeleted: this.form.controls["ForumIsDeleted"].value,
      ForumIcon: "Tcsimages/forumiconimages/" + this.forumicon,
      ForumStatus: this.form.controls["ForumStatus"].value,
      ForumCreatedBy: this.createdby,
      ForumCreatedDate: this.datepipe.transform(date, "yyyy-MM-dd")
    }

    if (this.form.valid) {
      this.isSubmitted = false;
      //alert(JSON.stringify(Forum));
      this.adminservice.onCreateForum(Forum).subscribe(data => {
        console.log(data);
        if (data.StatusCode == "1") {
          this.success = true;
          this.message = data.Message;
          this._success.next(`Record Saved Successfully`);

          this.onGetAllForumList(this.accountId);
          this.closeCpopup();

        }
        else {
          this.success = false;
          this.message = "Failed to create account master";
        }
      });
     //alert("Successfully Submitted");
     
    }

    else {
      this.isSubmitted = true;
    }

  }


  Forumform() {
    this.form = this.fb.group({
     
      'MasterAccount_Id': ['', [Validators.required]],
      'ForumName': ['', [Validators.required]],
      'ForumIsModerated': ['', [Validators.required]],
      'ForumIsDeleted': ['', [Validators.required]],
    
      'ForumStatus': ['', [Validators.required]],
    });
  }


  /*Smart table*/
  settings = {

    columns: {
      ForumName: {
        title: 'Forum Name',
        filter: false
      },
      ForumStatus: {
        title: 'Forum Status',
        filter: false
      },

    },
    attr: { class: 'table table-bordered' },
    actions: {
      edit: false, //as an example
      delete: false, //as an example
      add: false,
      position: 'right',
      custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button" (click)="onEdit(rec.Id)">Edit</button></span>` }]
    },
    pager: { display: true, perPage: 50 },
  };

  //route(event) {
  //  alert('hi');
  //}

  onSearch(query: string = '') {
    if (query != '') {
      this.source.setFilter([
        // fields we want to include in the search
        {
          field: 'ForumName',
          search: query
        },
        {
          field: 'ForumStatus',
          search: query
        }
      ], false);
    }
    else {
      this.source = new LocalDataSource(this.listofforum);
    }

    // second parameter specifying whether to perform 'AND' or 'OR' search 
    // (meaning all columns should contain search query or at least one)
    // 'AND' by default, so changing to 'OR' by setting false here
  }

  onSelectedFilter(event) {
    this.page = event.target.value;
    this.source = new LocalDataSource(this.listofforum);
    this.settings = {

      columns: {
        ForumName: {
          title: 'ForumName',
          filter: false
        },
        ForumStatus: {
          title: 'Forum Status',
          filter: false
        },

      },
      attr: { class: 'table table-bordered' },
      actions: {
        edit: false, //as an example
        delete: false, //as an example
        add: false,
        position: 'right',
        custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button">Edit</button></span>` }]
      },
      pager: { display: true, perPage: this.page },
    };

  }

  onExport() {
    this.excelService.exportAsExcelFile(this.listofforum, 'Forum');
  }


}
