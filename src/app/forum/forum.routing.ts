import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';

import { ForumComponent } from './forum.component';

export const ForumRoutes: Routes = [

  {
    path: '',
    component: ForumComponent,
    canActivate: [AuthGuard],
    data: {
      heading: 'Forum'
    }
  },


];
