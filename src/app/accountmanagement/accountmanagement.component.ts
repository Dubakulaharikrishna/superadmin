
import { Component, OnInit } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { Subject } from 'rxjs/Subject';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import { Router } from '@angular/router';
//changes
import { ExcelService } from '../services/excel.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';
import { AccountManagement } from '../Models/AccountManagement';
import { AdminService } from '../services/admin.service';
import { Status } from '../Models/Status';
import { Ng2SmartTableModule, LocalDataSource } from 'ng2-smart-table';
import { InstanceManager } from '../Models/InstanceManager';
import { Webclient } from '../Models/Webclient';
import { License } from '../Models/license';
import { DatePipe } from '@angular/common'
@Component({
  selector: 'app-accountmanagement',
  templateUrl: './accountmanagement.component.html',
  styleUrls: ['./accountmanagement.component.css'],
  providers: [ExcelService, DatePipe]
})

export class AccountmanagementComponent implements OnInit {
  AccountName: any;
  licensetype: any;
  lstartdate: any;
  licensekey: any;
  lgenerateddate: any;
  lgeneratedby: any;
  lenddate: any;
  decode: any;
  companyemail: any;
  compemail: any;
  regnum: any;
  count: any;
  companyname: any;
  clientid: any;
  webclient: any;
  instanceadmin: any;
  instancemanager: any;
  instancename: any;
  success: boolean=false;
  message: any;
  LicenseInfo: any;
  ClientInfo: any;
  InstanceInfo: any;
  public isSubmitted: boolean = false;
  result: any[];
  title = 'Simple Datatable Example using Angular 4';
  webclientslist: Webclient[];
  public form: FormGroup;
  accountmanagement: AccountManagement;
  public AccountManagementForm: FormGroup;
  id: any;
  instid: any;
  Id: any;
  createdby: any;
  pagename: any;
  status: Status;
  listofAccounts: AccountManagement[];
  listofLicense: License[];
  source: LocalDataSource;
  selectedValue: any = 50;
  page: any;
  listofinstances: InstanceManager[];
  dtTrigger: Subject<any> = new Subject();
  public _success = new Subject<string>();
  staticAlertClosed = false;
  constructor(private adminservice: AdminService, private router: Router, public datepipe: DatePipe, private excelService: ExcelService, private http: HttpClient, public fb: FormBuilder) {
    this.accountmanagement = new AccountManagement();
    this.success = false;
    this.staticAlertClosed = false;
  }


  ngOnInit() {


   // //sessionStorage.clear();
   // localStorage.removeItem('loginSessId');
   // //localStorage.clear();
   //// localStorage.setTimeout = 1;
   // //sessionStorage.removeItem('loginSessId');
   // localStorage.removeItem('loginSessId');
    this.pagename = "Account master";
    localStorage.setItem('loginSessId', this.pagename);

    setTimeout(() => this.staticAlertClosed = true, 20000);
    this._success.subscribe((message) => this.message = message);
    this._success.debounceTime(7000).subscribe(() => this.message = null);
    this.createdby = localStorage.getItem('MemberName');
    this.Accountform();
    this.onGetAllAccountsList();
    this.onGetAllInstanceManagerList();
    this.onGetAllWebClientList();
    this.instid = localStorage.getItem('InstanceId');
    this.onGetAllLicenseList();
   // alert(JSON.stringify(this.createdby));
  }

  onGetAllInstanceManagerList() {
    this.adminservice.onGetAllInstanceManagerList().subscribe(result => {
      if (result.success == 1) {
        this.listofinstances = result.data;
       
        // alert(JSON.stringify(this.Programstructures));
        return this.listofinstances
      }

    });
  }
  onGetAllWebClientList() {
    this.adminservice.onGetAllWebClientList().subscribe(result => {

      if (result.success == 1) {
        
        this.webclientslist = result.data;
        
        // alert(JSON.stringify(this.Programstructures));
        return this.webclientslist
      }

    });
  }
  onGetAllLicenseList() {
    this.adminservice.onGetAllLicenseList().subscribe(result => {

      if (result.success == 1) {
        this.listofLicense = result.data;
        // alert(JSON.stringify(this.Programstructures));
        return this.listofLicense
      }

    });
  }
  onGetInstanceManagerById() {
    this.adminservice.onGetInstanceManagerById(this.InstanceInfo).subscribe(result => {
      //  this.status = result;
  //    alert(JSON.stringify(result));
      if (result.success == 1) {


        this.instancemanager = result.data;
        this.instancename = result.data.InstanceName;
        this.instanceadmin = result.data.InstanceAdmin;
      }
      else {
        alert(result.data);
      }

      console.log(this.instancemanager);

     

    });
    return this.instancemanager;
  }
  onGetWebClientById() {
   // alert(this.ClientInfo);
    this.adminservice.onGetWebClientById(this.ClientInfo).subscribe(result => {
    //  alert(JSON.stringify(result));
     // console.log("hiii", result);
      //  this.status = result;
      if (result.success == 1) {


        this.webclient = result.data;
        this.clientid = result.data._id;
        this.companyname = result.data.CompanyName;
        this.regnum = result.data.CompanyRegistrationNumber;
        this.compemail = result.data.CompanyEmailID;
        this.companyemail = result.data.PrimaryAdminEmailID;
      }
      else {
        alert(result.data);
      }
      console.log(this.webclient);

     


    });
    return this.webclient;
  }


  onGetLicenseById() {
    this.adminservice.onGetLicenseById(this.LicenseInfo).subscribe(result => {
      // this.status = result;
      if (result.success == 1) {


        this.license = result.data;
        this.decode = result.data.DecodeScript;
        this.lenddate = result.data.LicenseEndDate;
        this.lgeneratedby = result.data.LicenseGeneratedBy;
        this.lgenerateddate = result.data.LicenseGeneratedDate;
        this.licensekey = result.data.LicenseKey;
        this.lstartdate = result.data.LicenseStartDate;
        this.licensetype = result.data.LicenseType;

      }
      else {
        alert(result.data);
      }
      //this.requestStatus = result.RequestStatus;
      //this.licenseStatus = result.LicenseStatus;
      console.log(this.license);

    });
    return true;
  }

  test(id) {
    //  alert("Hi");
    //console.log(id);
    //alert(id);
   // alert(this.InstanceInfo);
    this.onGetInstanceManagerById();
    console.log(this.InstanceInfo);
    //alert(this.InstanceInfo);
  }

  client(id) {
  //  alert(this.ClientInfo);
    this.onGetWebClientById();
    console.log(this.ClientInfo);
  //    (this.ClientInfo);

  }
  license(id) {
   // alert(this.LicenseInfo);
    this.onGetLicenseById();

    console.log(this.LicenseInfo);
  //  alert(this.LicenseInfo);
  }
  openCpopup() {

    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "block";
    cmask.style.display = "block";
  }



  closeCpopup() {
    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "none";
    cmask.style.display = "none";
    this.form.reset();
  }

  onEdit(event: any) {    
    this.router.navigate(['/viewaccount/', { accId: event.data._id }]);
   // alert(id);
    //alert(this.accountmanagement);
  }


  onChange(value) {
    this.AccountName = value;
  }


  onGetAllAccountsList() {
    this.adminservice.onGetAllAccountsList().subscribe(result => {
      if (result.success == 1) {
        this.listofAccounts = result.data;
        this.count = this.listofAccounts.length;
        this.source = new LocalDataSource(this.listofAccounts);
        // alert(JSON.stringify(this.Programstructures));
        return this.listofAccounts
      }
     
    });
  }

  //onGetAccountsById() {
  //  this.adminservice.onGetAccountsById(this.id)
  //    .subscribe(result => {
  //      this.accountmanagement = result;
  //      console.log(this.accountmanagement);
  //    });
  //  return this.accountmanagement;
  //}

 
 
 
  onCreateAccount() {
    this.success = true;
    let date = new Date();
    if (this.form.valid) {
      this.isSubmitted = false;
      // alert(JSON.stringify(license));
      const lic = {
        // AccountID: this.form.get('AccountID').value,
        InstanceManagerId: this.instid,
        AccountName: this.form.get('AccountName').value,
        AccountWebUrl: this.form.get('AccountWebUrl').value,
       // IsMobileEnabled: this.form.get('IsMobileEnabled').value,
       // IsAndroidAppPublic: this.form.get('IsAndroidAppPublic').value,
      //  IsIOSAppPublic: this.form.get('IsIOSAppPublic').value,
        AccountAdmin: this.form.get('AccountAdmin').value,
    //    IsHttpsEnabled: this.form.get('IsHttpsEnabled').value,
        DevUrl: this.form.get('DevUrl').value,
       // IsLiveStatus: this.form.get('IsLiveStatus').value,
        ClientInfo: { Client_Id: this.clientid, CompanyName: this.companyname, CompanyRegistrationNumber: this.regnum, CompanyEmailID: this.compemail, CompanyAdminEmailID: this.companyemail },
        InstanceInfo: {
          InstanceName: this.instancename, InstanceAdmin: this.instanceadmin
        },
        LicenseInfo: { DecodeScript: this.decode, LicenseEndDate: this.lenddate, LicenseGeneratedBy: this.lgeneratedby, LicenseGeneratedDate: this.lgenerateddate, LicenseKey: this.licensekey, LicenseStartDate: this.lstartdate, LicenseType: this.licensetype },
        AppUrl: this.form.get('AppUrl').value,
        CreatedBy: this.createdby,
        //AccountCreatedDate: this.datepipe.transform(date, "yyyy-MM-dd")

      }
    //  alert(JSON.stringify(lic));
      this.adminservice.onCreateAccount(lic).subscribe(result => {
        console.log(result);
        if (result.success == 1) {
          this.success = true;
          this.message = result.Message;
          this._success.next(`Record Saved Successfully`);
          this.onGetAllAccountsList();
          this.closeCpopup();
          this.form.reset();

        }
        else {
          this.success = false;
          this.message = "Failed to create account master";
        }
      });
     // alert("Submitted Successfully");
    
    }

    
    else {
      this.isSubmitted = true;
      //alert(JSON.stringify(license));
    }
    //alert(JSON.stringify(account));
  }


  Accountform() {
    this.form = this.fb.group({
      // AccountID: ['', [Validators.required]],
      AccountName: ['', [Validators.required]],
      AccountWebUrl: ['', [Validators.required]],
     // IsMobileEnabled: [''],
     // IsAndroidAppPublic: [''],
    //  IsIOSAppPublic: [''],
      AccountAdmin: ['', [Validators.required]],
     // AccountCreatedDate: ['', [Validators.required]],
   //   IsHttpsEnabled: [''],
      DevUrl: ['', [Validators.required]],
    //  IsLiveStatus: [''],
      ClientInfo: ['', [Validators.required]],
      InstanceInfo: ['', [Validators.required]],
      LicenseInfo: ['', [Validators.required]],
      AppUrl: ['', [Validators.required]],

    });
  }
  /*Smart table*/
  settings = {

    columns: {
      AccountName: {
        title: 'Account Name',
        filter: false
      },
      AccountAdmin: {
        title: 'Account Admin',
        filter: false
      },
    
    },
    attr: { class: 'table table-bordered' },
    actions: {
      edit: false, //as an example
      delete: false, //as an example
      add: false,
      position: 'right',
      custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button" (click)="onEdit(rec._id)">Edit</button></span>` }]
    },
    pager: { display: true, perPage: 50 },
  };

  //route(event) {
  //  alert('hi');
  //}

  onSearch(query: string = '') {
    if (query != '') {
      this.source.setFilter([
        // fields we want to include in the search
        {
          field: 'Account Name',
          search: query
        },
        {
          field: 'Account Admin',
          search: query
        }
      ], false);
    }
    else {
      this.source = new LocalDataSource(this.listofAccounts);
    }

    // second parameter specifying whether to perform 'AND' or 'OR' search 
    // (meaning all columns should contain search query or at least one)
    // 'AND' by default, so changing to 'OR' by setting false here
  }

  onSelectedFilter(event) {
    this.page = event.target.value;
    this.source = new LocalDataSource(this.listofAccounts);
    this.settings = {

      columns: {
        AccountName: {
          title: 'Account Name',
          filter: false
        },
        AccountAdmin: {
          title: 'Account Admin',
          filter: false
        },
        
      },
      attr: { class: 'table table-bordered' },
      actions: {
        edit: false, //as an example
        delete: false, //as an example
        add: false,
        position: 'right',
        custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button">Edit</button></span>` }]
      },
      pager: { display: true, perPage: this.page },
    };

  }

  onExport() {
    this.excelService.exportAsExcelFile(this.listofAccounts, 'AccountManagement');
  }


}
