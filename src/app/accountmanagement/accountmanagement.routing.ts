import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';
import { AccountmanagementComponent } from './accountmanagement.component';

export const AccountmanagementRoutes: Routes = [

  {
    path: '',
    component: AccountmanagementComponent,
    canActivate: [AuthGuard],
    data: {
      heading: 'Accountmanagement works!'
    }    
  },

  
];

