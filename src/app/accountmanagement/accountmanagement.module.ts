//import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//import { DashboardComponent } from './dashboard.component';
import { RouterModule } from '@angular/router';
import { AccountmanagementRoutes } from './accountmanagement.routing';
import { AccountmanagementComponent } from '../accountmanagement/accountmanagement.component';

//import { DashboardComponent } from './dashboard.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { HttpModule } from '@angular/http';
import { DataTablesModule } from 'angular-datatables';
import {HttpClientModule} from '@angular/common/http';
import { CommonModule } from '@angular/common'; 
import { BrowserModule } from '@angular/platform-browser';

//changes
import { CustomFormsModule } from 'ng2-validation';
import { FormsModule, FormBuilder, FormGroup, Validators, FormControl, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbAccordionModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbProgressbarModule, NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
import { AdminService } from '../services/admin.service';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';
import { Ng2SmartTableModule } from 'ng2-smart-table';


@NgModule({
  imports: [DataTablesModule,
    HttpClientModule,
    CommonModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule, Ng2SmartTableModule,
    RouterModule.forChild(AccountmanagementRoutes),
 HttpModule
],
    
    //BrowserModule
  
  declarations: [
   
    AccountmanagementComponent
    
  ],

  providers: [AdminService, AuthGuard, NotAuthGuard],
  bootstrap: [AccountmanagementComponent]
})
export class AccountmanagementModule { }






