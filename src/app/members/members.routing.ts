import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';

import { MembersComponent } from './members.component';

export const MembersRoutes: Routes = [

  {
    path: '',
    component: MembersComponent,
    canActivate: [AuthGuard],

    data: {
      heading: 'Members'
    }
  },


];
