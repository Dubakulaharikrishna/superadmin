import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl, } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Members } from '../Models/Members';
import { AdminService } from '../services/admin.service';
import { CustomValidators } from 'ng2-validation';
import { DatePipe } from '@angular/common'
import 'rxjs/add/operator/debounceTime';
import { Subject } from 'rxjs/Subject';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/share';
import { Status } from '../Models/Status';
import { Ng2SmartTableModule, LocalDataSource } from 'ng2-smart-table';
import { AccountManagement } from '../Models/AccountManagement';
import { forEach } from '@angular/router/src/utils/collection';
import { ExcelService } from '../services/excel.service';
import { PasswordValidation } from './paswwordmatch';

/*password match*/

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.css'],
  providers: [ExcelService, DatePipe]
})


export class MembersComponent implements OnInit {
  dataLoaded: any;
  imageurl: any;
  source: LocalDataSource;
  selectedValue: any = 50;
  page: any;
  success: boolean = false;
  message: any;
  dtTrigger: Subject<any> = new Subject();
  public _success = new Subject<string>();
  staticAlertClosed = false;
  public Sendnotificationsform: FormGroup;
  public data: Object;
  public temp_var: Object = false;
  public MembersUpdateFormGroup: FormGroup;
  public MembersForm: FormGroup;
  members: Members;
  public isSubmitted: boolean = false;
  id: any;
  listofMembers: Members[];
  pagename: any;
  status: Status;
  listofAccounts: AccountManagement[];
  accountId: any;
  MasterAccount_Id: any;
  roleId: any;
  createdby: any;
  membersObj: any = [];
  constructor(private fb: FormBuilder, public datepipe: DatePipe, public ele: ElementRef, private excelService: ExcelService, private adminservice: AdminService, private router: Router, private http: HttpClient) {
    this.members = new Members();
  }

  //validate(c: AbstractControl): { [key: string]: any } {
  //  // self value (e.g. retype password)
  //  let v = c.value;

  //  // control value (e.g. password)
  //  let e = c.root.get(this.validateEqual);

  //  // value not equal
  //  if (e && v !== e.value) return {
  //    validateEqual: false
  //  }
  //  return null;
  //}
  
  onGetAllMembersList(id) {
    if (id == "") {
      id = null;
    }
    this.adminservice.onGetAllMembersList(id).subscribe(result => {
      if (result.success == 1) {
        
        this.listofMembers = result.data;
        this.source = new LocalDataSource(this.listofMembers);
        // alert(JSON.stringify(this.Programstructures));
        return this.listofMembers
      }
    });
  }
  

  onEdit(event: any) {
    this.router.navigate(['/viewuser/', { memId: event.data._id }]);
  }

  ngOnInit() {

   // sessionStorage.clear();
   // localStorage.removeItem('loginSessId');
    //localStorage.clear();
   // localStorage.setTimeout = 1;
   // sessionStorage.removeItem('loginSessId');
 //   localStorage.removeItem('loginSessId');
    this.pagename = "Members";
    localStorage.setItem('loginSessId', this.pagename);
    setTimeout(() => this.staticAlertClosed = true, 20000);
    this._success.subscribe((message) => this.message = message);
    this._success.debounceTime(7000).subscribe(() => this.message = null);
    this.accountId = localStorage.getItem('AccountId');
    this.roleId = localStorage.getItem('RoleId');
    this.Membersform();
    this.onGetAllMembersList(this.accountId);
    this.onGetAllAccountsList();
    this.Notificationsform();
    this.createdby = localStorage.getItem('MemberName');
  }

  onGetAllAccountsList() {
    this.adminservice.onGetAllAccountsList().subscribe(result => {
      if (result.success == 1) {
        this.listofAccounts = result.data;

        //alert(JSON.stringify(this.listofAccounts));
        return this.listofAccounts
      }

    });
  }


  //onResetFormGroup() {

  //  this.MembersForm = this.fb.group({
  //    Password: ['', [Validators.required]],
  //    ConfirmPassword: ['', [Validators.required]]
  //  }, {
  //      validator: PasswordValidation.MatchPassword // your validation method
  //    });
  //}

  validatePassword(controls) {
    const regExp = new RegExp(/^(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[\d])(?=.*?[\W]).{8,35}$/);
    if (regExp.test(controls.value)) {
      return null;
    } else {
      return { 'validatePassword': true }
    }
  }
  matchingPasswords(Password, ConfirmPassword) {
    return (group: FormGroup) => {
      if (group.controls['resetPassword.Password'].value === group.controls['resetPassword.ConfirmPassword'].value) {
        return null;
      } else {
        return { 'matchingPasswords': true }
      }
    }
  }

  onChange(event) {
    console.log(event);
    this.onGetAllMembersList(event.target.value);
  }

  reset() {
    this.onGetAllMembersList(this.accountId);
    this.MasterAccount_Id = undefined;
  }
  Notificationsform() {
    this.Sendnotificationsform = this.fb.group({
      // Title: ['', [Validators.required]],
      ActivityType: ['', [Validators.required]],
      Message: ['', [Validators.required]],
      MessageBody: ['', [Validators.required]],
      NotificationChanelType: ['', [Validators.required]]
    });
  }
  Membersform() {
    this.MembersForm = this.fb.group({

      UserName: ['', Validators.compose([
        Validators.required
      ])],
      Password: ['', Validators.compose([
        Validators.required
      ])],
      ConfirmPassword: ['', Validators.compose([
        Validators.required
      ])],

      MobileNumber: ['', Validators.compose([
        Validators.required
      ])],

      Email: ['', Validators.compose([
        Validators.required
      ])],
      FirstName: ['', Validators.compose([
        Validators.required
      ])],
      LastName: ['', Validators.compose([
        Validators.required
      ])],
      MiddleName: ['', Validators.compose([
        Validators.required
      ])],
      DOB: ['', Validators.compose([
        Validators.required
      ])],
      Gender: ['', Validators.compose([
        Validators.required
      ])],
      Industry: ['', Validators.compose([
        Validators.required
      ])],
      Profession: ['', Validators.compose([
        Validators.required
      ])],
      AddressLine1: ['', Validators.compose([
        Validators.required
      ])],
      AddressLine2: ['', Validators.compose([
        Validators.required
      ])],
      City: ['', Validators.compose([
        Validators.required
      ])],
      State: ['', Validators.compose([
        Validators.required
      ])],
      ZipCode: ['', Validators.compose([
        Validators.required
      ])],
      Country: ['', Validators.compose([
        Validators.required
      ])],
      CompanyName: ['', Validators.compose([
        Validators.required
      ])],
      CompanyAddressLine1: ['', Validators.compose([
        Validators.required
      ])],
      CompanyAddressLine2: ['', Validators.compose([
        Validators.required
      ])],
      CompanyCity: ['', Validators.compose([
        Validators.required
      ])],

      CompanyCountry: ['', Validators.compose([
        Validators.required
      ])],
      CompanyEmail: ['', Validators.compose([
        Validators.required
      ])],
      AlternatePhoneNumber: ['', Validators.compose([
        Validators.required
      ])],
      BloodGroup: ['', Validators.compose([
        Validators.required
      ])],
      FamilyType: ['', Validators.compose([
        Validators.required
      ])],
      Origin: ['', Validators.compose([
        Validators.required
      ])],
      TimeZone: ['', Validators.compose([
        Validators.required
      ])],
      Hobbies: ['', Validators.compose([
        Validators.required
      ])],
      MaritalStatus: ['', Validators.compose([
        Validators.required
      ])],
      MemberStatus: ['', Validators.compose([
        Validators.required
      ])],
      IsMobileVerified: [''],
      IsEmailVerified: [''],
    }, {
        validator: PasswordValidation.MatchPassword // your validation method
    });
  }

  onGetMemberId(event) {
    this.membersObj = event;
    console.log(event);
  }
  NotificationsSubmit() {
    let memberIds: any = [];
    for (let item of this.membersObj.selected) {
      memberIds.push(item.Id);
    }
    const notify = {
      memberIdsList: memberIds,
      activityType: this.Sendnotificationsform.controls["ActivityType"].value,
      message: this.Sendnotificationsform.controls["Message"].value,
      meaasgeBody: this.Sendnotificationsform.controls["MessageBody"].value,
      notificationChanelType: this.Sendnotificationsform.controls["NotificationChanelType"].value,
    }
  
    if (this.Sendnotificationsform.valid) {
      this.isSubmitted = false;

      // alert(JSON.stringify(notify));
      this.adminservice.OnPostNotifications(notify).subscribe(res => {
        console.log(res);
        //this.viewmembers = res
      });
      alert("Hi Notification Acccepted Successfully");
    }
    else {
      this.isSubmitted = true;
      //alert(JSON.stringify(license));

    }
  }
  memberImage: any;
  readURL(event) {
    let files = event.target.files;
    this.memberImage = files[0];
  }
  onRegisterSubmit() {
    //let files = this.ele.nativeElement.querySelector('#imageFile').files;
    let formData = new FormData();

    //if (files.length > 0 && files.count != 0 && files != null) {

    //  let file = files[0];
    //  this.imageurl = file.name;
    formData.append('imageFile', this.memberImage, this.memberImage.name);
    //  console.log(formData);
    //  console.log(files);
    //  //this.adminservice.onfileupload(formData).subscribe(res => this.dataLoaded = res);
    //}
    let date = new Date();
    if (this.MembersForm.valid) {
      this.isSubmitted = false;
      // alert(JSON.stringify(license));
      const user = {

        UserName: this.MembersForm.get('UserName').value,
        Password: this.MembersForm.get('Password').value,
        ConfirmPassword: this.MembersForm.get('ConfirmPassword').value,
        MobileNumber: this.MembersForm.get('MobileNumber').value,
        Email: this.MembersForm.get('Email').value,
        FirstName: this.MembersForm.get('FirstName').value,
        LastName: this.MembersForm.get('LastName').value,
        MiddleName: this.MembersForm.get('MiddleName').value,
        DOB: this.MembersForm.get('DOB').value,
        Gender: this.MembersForm.get('Gender').value,
        Industry: this.MembersForm.get('Industry').value,
        Profession: this.MembersForm.get('Profession').value,
        AddressLine1: this.MembersForm.get('AddressLine1').value,
        AddressLine2: this.MembersForm.get('AddressLine2').value,
        City: this.MembersForm.get('City').value,
        State: this.MembersForm.get('State').value,
        ZipCode: this.MembersForm.get('ZipCode').value,
        Country: this.MembersForm.get('Country').value,
        CompanyName: this.MembersForm.get('CompanyName').value,
        CompanyAddressLine1: this.MembersForm.get('CompanyAddressLine1').value,
        CompanyAddressLine2: this.MembersForm.get('CompanyAddressLine2').value,
        CompanyCity: this.MembersForm.get('CompanyCity').value,
        CompanyCountry: this.MembersForm.get('CompanyCountry').value,
        CompanyEmail: this.MembersForm.get('CompanyEmail').value,
        AlternatePhoneNumber: this.MembersForm.get('AlternatePhoneNumber').value,
        BloodGroup: this.MembersForm.get('BloodGroup').value,
        FamilyType: this.MembersForm.get('FamilyType').value,
        Origin: this.MembersForm.get('Origin').value,
        TimeZone: this.MembersForm.get('TimeZone').value,
        Hobbies: this.MembersForm.get('Hobbies').value,
        MaritalStatus: this.MembersForm.get('MaritalStatus').value,
        MemberStatus: this.MembersForm.get('MemberStatus').value,
        IsMobileVerified: this.MembersForm.get('IsMobileVerified').value,
        IsEmailVerified: this.MembersForm.get('IsEmailVerified').value,
        ImageUrl: this.memberImage.name,
        CreatedBy: this.createdby,
        //CreatedDate: this.datepipe.transform(date, "yyyy-MM-dd")
      }
     // alert(JSON.stringify(user));
      this.adminservice.onCreateMembers(user,formData).subscribe(result => {
        console.log(result);
        if (result.success == 1) {
          this.success = true;
          this.message = result.Message;
          this._success.next(`Record Saved Successfully`);

          this.onGetAllMembersList(this.accountId);
          this.closeCpopup();

        }
        else {
          this.success = false;
          this.message = "Failed to create Member";
        }
      });
    }
    else {
      this.isSubmitted = true;
      //alert(JSON.stringify(license));
    }

  }

  openCpopup() {

    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "block";
    cmask.style.display = "block";
  }
  openCpopup1() {

    let cpoper = document.getElementById("crpop1");
    let cmask = document.getElementById("crmask1");
    cpoper.style.display = "block";
    cmask.style.display = "block";
  }

  closeCpopup() {
    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "none";
    cmask.style.display = "none";
  }

  closeCpopup1() {
    let cpoper = document.getElementById("crpop1");
    let cmask = document.getElementById("crmask1");
    cpoper.style.display = "none";
    cmask.style.display = "none";
  }

  /*Smart table*/
  settings = {
    selectMode: 'multi',
    columns: {
      UserName: {
        title: 'User Name',
        filter: false
      },
      CompanyName: {
        title: 'Company Name',
        filter: false
      },
  
    },
    attr: { class: 'table table-bordered' },
    actions: {
      edit: false, //as an example
      delete: false, //as an example
      add: false,
      select: true,
      position: 'right',
      custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button" (click)="onEdit(rec._id)">Edit</button></span>` }]
    },
    pager: { display: true, perPage: 50 },
  };

  //route(event) {
  //  alert('hi');
  //}

  onSearch(query: string = '') {
    if (query != '') {
      this.source.setFilter([
        // fields we want to include in the search
        {
          field: 'UserName',
          search: query
        },
        {
          field: 'CompanyName',
          search: query
        }
      ], false);
    }
    else {
      this.source = new LocalDataSource(this.listofMembers);
    }

    // second parameter specifying whether to perform 'AND' or 'OR' search 
    // (meaning all columns should contain search query or at least one)
    // 'AND' by default, so changing to 'OR' by setting false here
  }

  onSelectedFilter(event) {
    this.page = event.target.value;
    this.source = new LocalDataSource(this.listofMembers);
    this.settings = {
      selectMode: 'multi',
      columns: {
        UserName: {
          title: 'Name',
          filter: false
        },
        CompanyName: {
          title: 'Company Name',
          filter: false
        },
        
      },
      attr: { class: 'table table-bordered' },
      actions: {
        edit: false, //as an example
        delete: false, //as an example
        add: false,
        select: true,
        position: 'right',
        custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button">Edit</button></span>` }]
      },
      pager: { display: true, perPage: this.page },
    };

  }




  onExport() {
    this.excelService.exportAsExcelFile(this.listofMembers, 'Members');
  }


}
