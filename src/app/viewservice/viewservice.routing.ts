import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';

import { ViewserviceComponent } from './viewservice.component';

export const ViewserviceRoutes: Routes = [

  {
    path: '',
    component: ViewserviceComponent,
    canActivate: [AuthGuard],
    data: {
      heading: 'Viewservice works!'
    }
  },


];
