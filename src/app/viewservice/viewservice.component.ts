
import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import 'rxjs/add/operator/share';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';
//import { Programmanagement } from '../programmanagement/programmanagement.module';
import { CommonModule } from '@angular/common';
//import { ProgramManagementModule } from './programmanagement.module';
import { DatePipe } from '@angular/common'

import { AdminService } from '../services/admin.service';
import { CustomValidators } from 'ng2-validation';
import 'rxjs/add/operator/debounceTime';
import { Subject } from 'rxjs/Subject';
import 'rxjs/Rx';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { retry } from 'rxjs/Operator/retry';
import { Status } from '../Models/Status';
import { Service } from '../Models/Service';
import { ServiceType } from '../Models/ServiceType';



@Component({
  selector: 'app-viewservice',
  templateUrl: './viewservice.component.html',
  styleUrls: ['./viewservice.component.css'],
  providers: [DatePipe]
})
export class ViewserviceComponent implements OnInit {
  dataLoaded: any;
  coverpic: any;
  endDate: any;
  startDate: any;
  listofservicetypes: ServiceType[];
  public serviceUpdateFormGroup: FormGroup;
  public form: FormGroup;
  service: Service;
  id: any;
  isSubmitted: boolean = false;
  ssId: any;
  status: Status;
  createdby: any;
  constructor(private fb: FormBuilder, public ele: ElementRef, public datepipe: DatePipe, private adminservice: AdminService, private router: Router, public route: ActivatedRoute, private http: HttpClient) {

    this.service = new Service();



  }

  ngOnInit() {
    this.UpdateServiceform();
    this.route.params.subscribe(params => {
      this.ssId = params['ssId'];

    });
    this.onGetServiceById();
    this.onGetAllServiceTypesList();
    this.createdby = localStorage.getItem('MemberName');

  }
  onGetAllServiceTypesList() {
    let id = localStorage.getItem('AccountId');
  //  alert(id);
    this.adminservice.onGetAllServiceTypeList(id).subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofservicetypes = data.Data;
      //  alert(JSON.stringify(this.listofservicetypes));
        //alert(JSON.stringify(this.listofAccounts));
        return this.listofservicetypes
      }

    });
  }

  onUpdateService() {
    let files = this.ele.nativeElement.querySelector('#ServiceCoverPicture').files;
    if (files.length > 0 && files.count != 0 && files != null) {

      let formData = new FormData();
      let file = files[0];
      this.coverpic = file.name;
      formData.append('ServiceCoverPicture', file, file.name);
      console.log(formData);
      console.log(files);
      this.adminservice.onfileupload(formData).subscribe(res => this.dataLoaded = res);
    }
    let date = new Date();

    const ser = {
      Id: this.service.Id,
      ServiceType_Id: this.serviceUpdateFormGroup.controls["service.ServiceType_Id"].value,
      ServiceTitle: this.serviceUpdateFormGroup.controls["service.ServiceTitle"].value,
      ServiceDescription: this.serviceUpdateFormGroup.controls["service.ServiceDescription"].value,
      ServiceContributor: this.serviceUpdateFormGroup.controls["service.ServiceContributor"].value,
      ServiceCoverPicture: "Tcsimages/prgcoverimages/" + this.coverpic,
      ServiceTag: this.serviceUpdateFormGroup.controls["service.ServiceTag"].value,
      IsFreeService: this.serviceUpdateFormGroup.controls["service.IsFreeService"].value,
      ServiceDiscountedPrice: this.serviceUpdateFormGroup.controls["service.ServiceDiscountedPrice"].value,
      ServiceValidStartDate: this.serviceUpdateFormGroup.controls["service.ServiceValidStartDate"].value,
      ServiceValidEndDate: this.serviceUpdateFormGroup.controls["service.ServiceValidEndDate"].value,
      ServiceListedPrice: this.serviceUpdateFormGroup.controls["service.ServiceListedPrice"].value,
      ServiceStatus: this.serviceUpdateFormGroup.controls["service.ServiceStatus"].value,
      ServiceLink: this.serviceUpdateFormGroup.controls["service.ServiceLink"].value,
      ServiceCreatedBy: this.service.ServiceCreatedBy,
      ServiceCreatedDate: this.service.ServiceCreatedDate,
      ServiceUpdatedBy: this.createdby,
      ServiceUpdatedDate: this.datepipe.transform(date, "yyyy-MM-dd"),

    }
    if (this.serviceUpdateFormGroup.valid) {
      this.isSubmitted = false;
      // alert(JSON.stringify(ser));


      this.adminservice.onUpdateService(ser).subscribe(res => this.service = res);
      alert("Successfully Updated");
      this.router.navigate(['/service/']);
    }
    else {
      this.isSubmitted = true;
    }
  }
  services(id) {
  
    console.log(this.service.ServiceType_Id);
   // alert(this.service.ServiceType_Id);
  }


  UpdateServiceform() {
    this.serviceUpdateFormGroup = this.fb.group({

      'service.ServiceType_Id': [''],
      'service.ServiceTitle': ['', [Validators.required]],
      'service.ServiceDescription': ['', [Validators.required]],
      'service.ServiceContributor': ['', [Validators.required]],
      'service.ServiceCoverPicture': [''],
      'service.ServiceTag': ['', [Validators.required]],
      'service.IsFreeService': [''],

      'service.ServiceDiscountedPrice': ['', [Validators.required]],
      'service.ServiceValidStartDate': ['', [Validators.required]],
      'service.ServiceValidEndDate': ['', [Validators.required]],

      'service.ServiceListedPrice': ['', [Validators.required]],
      'service.ServiceStatus': ['', [Validators.required]],
      'service.ServiceLink': ['', [Validators.required]],
   
    });
  }
  onGetServiceById() {

    this.adminservice.onGetServiceById(this.ssId).subscribe(result => {

      this.status = result;
      if (this.status.StatusCode == "1") {


        this.service = this.status.Data;
      }
      else {
        alert(this.status.Data);
      }


      console.log(this.service);
      if (this.service.ServiceValidStartDate != null && this.service.ServiceValidStartDate != "" && this.service.ServiceValidStartDate != undefined) {
        this.startDate = this.datepipe.transform(this.service.ServiceValidStartDate, 'yyyy-MM-dd');
        this.service.ServiceValidStartDate = this.startDate;
      }



      if (this.service.ServiceValidEndDate != null && this.service.ServiceValidEndDate != "" && this.service.ServiceValidEndDate != undefined) {
        this.endDate = this.datepipe.transform(this.service.ServiceValidEndDate, 'yyyy-MM-dd');
        this.service.ServiceValidEndDate = this.endDate;
      }
    });
    return this.service;
  }

}
