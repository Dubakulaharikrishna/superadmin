import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';

import { ViewwebclientComponent } from './viewwebclient.component';

export const ViewwebclientRoutes: Routes = [

  {
    path: '',
    component: ViewwebclientComponent,
    canActivate: [AuthGuard],
    data: {
      heading: 'Viewcontent works!'
    }    
  },

  
];

