import { Component, OnInit } from '@angular/core';
//changes
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { Subject } from 'rxjs/Subject';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';
import { InstanceManager } from '../Models/InstanceManager';
import { AdminService } from '../services/admin.service';
import { Webclient } from '../Models/Webclient';
import { DatePipe } from '@angular/common';
import { Status } from '../Models/Status';
import { License } from '../Models/license';
import { MatSnackBar } from '@angular/material';
@Component({
  selector: 'app-viewwebclient',
  templateUrl: './viewwebclient.component.html',
  styleUrls: ['./viewwebclient.component.css'],
  providers: [DatePipe]
})
export class ViewwebclientComponent implements OnInit {
  estaDate: any;
  createdby: any;
  CompanyEstablishedDate: any;
  isSubmitted: boolean = false;
  doBCheck: boolean = false;
  public viewwebFormGroup: FormGroup;
  public form: FormGroup;
  webclient: Webclient;
  public WebclientForm: FormGroup;
  webclientslist: Webclient[];
  listofLicense: License[];
  listofinstances: InstanceManager[];
  id: any;
  dtTrigger: Subject<any> = new Subject();
  public _success = new Subject<string>();
  staticAlertClosed = false;
  success: boolean = false;
  message: any;
  instid: any;
  weId: any;
  status: Status;
  constructor(public router: Router, public snackBar: MatSnackBar, public http: HttpClient, public route: ActivatedRoute, public fb: FormBuilder, private adminservice: AdminService, public datepipe: DatePipe) {

    // this.instancemanager = new InstanceManager();
    this.webclient = new Webclient();
  }

  ngOnInit() {
    this.WebClientForm();
    this.route.params.subscribe(params => {
      this.weId = params['weId'];

    });
    setTimeout(() => this.staticAlertClosed = true, 20000);
    this._success.subscribe((message) => this.message = message);
    this._success.debounceTime(7000).subscribe(() => this.message = null);
    this.instid = localStorage.getItem('InstanceId');
    this.onGetWebClientById();
    this.createdby = localStorage.getItem('MemberName');
  


  }




  license(id) {
    console.log(this.webclient.LicenseInfo);
  }



  test(id) {
   
    console.log(this.webclient.Instance_Id);
 
  }


  onCheckDOB() {
    let date = new Date();
    let date1 = new Date(date),
      mnth = ("0" + (date.getMonth() + 1)).slice(-2),
      day = ("0" + date.getDate()).slice(-2);
    let todaydate = [date.getFullYear(), mnth, day].join("-");
    if (this.webclient.CompanyEstablishedDate <= todaydate) {
      this.doBCheck = false;
    }
    else {
      this.doBCheck = true;
    }
  }
  onUpdatewebclient(webclient) {
    let date = new Date();
    const account = {
      _id: webclient._id,
      InstanceManagerId: this.instid,
      CompanyName: this.viewwebFormGroup.controls["webclient.CompanyName"].value,
      CompanyAddressLine1: this.viewwebFormGroup.controls["webclient.CompanyAddressLine1"].value,
      CompanyAddressLine2: this.viewwebFormGroup.controls["webclient.CompanyAddressLine2"].value,
      CompanyRegistrationNumber: this.viewwebFormGroup.controls["webclient.CompanyRegistrationNumber"].value,
      CompanyEmailID: this.viewwebFormGroup.controls["webclient.CompanyEmailID"].value,
      LegalName: this.viewwebFormGroup.controls["webclient.LegalName"].value,
      CompanyType: this.viewwebFormGroup.controls["webclient.CompanyType"].value,
      CompanyEstablishedDate: this.viewwebFormGroup.controls["webclient.CompanyEstablishedDate"].value,
      DisplayNameOfCompany: this.viewwebFormGroup.controls['webclient.DisplayNameOfCompany'].value,
      LogoUrl: this.viewwebFormGroup.controls['webclient.LogoUrl'].value,
      AdminUserName: this.viewwebFormGroup.controls['webclient.AdminUserName'].value,
      PrimaryAdminEmailID: this.viewwebFormGroup.controls['webclient.PrimaryAdminEmailID'].value,
      CompanyPersonName: this.viewwebFormGroup.controls['webclient.CompanyPersonName'].value,
      CompanyPersonEmailID: this.viewwebFormGroup.controls['webclient.CompanyPersonEmailID'].value,
      AdminPhoneNumber: this.viewwebFormGroup.controls['webclient.AdminPhoneNumber'].value,
      CompanyPersonPhoneNumber: this.viewwebFormGroup.controls['webclient.CompanyPersonPhoneNumber'].value,
      CompanyPhone: this.viewwebFormGroup.controls['webclient.CompanyPhone'].value,
      CreatedBy: this.webclient.ClientCreatedBy,
      UpdatedBy: this.createdby,
      AdminPersonDesignation: this.viewwebFormGroup.controls['webclient.AdminPersonDesignation'].value,
    }


    if (this.viewwebFormGroup.valid) {
      this.isSubmitted = false;
  
      this.adminservice.onUpdateWebClient(account).subscribe(result => {
        console.log(result);
       // this.webclient = result
        //this.router.navigate(['/webclient/']);
        if (result.success == 1) {
          this.success = true;
          this.message = result.message;
          this._success.next(`Updated Successfully`);

          console.log(this.snackBar, "this.snackBar")
          this.snackBar.open(`Record Updated Successfully`, '', {
            duration: 20000,
          });
          this.router.navigate(['/webclient/', { message: this.message }]);

  

        }
        else {
          this.success = false;
          this.message = "Failed to create License";
        }

      });
      //alert("Successfully Updated");
  }
    
    else {
      this.isSubmitted = true;
    
    }
  }


  WebClientForm() {
    this.viewwebFormGroup = this.fb.group({
      'webclient.CompanyName': ['', [Validators.required]],
      'webclient.CompanyAddressLine1': ['', [Validators.required]],
      'webclient.CompanyAddressLine2': ['', [Validators.required]],
      'webclient.CompanyRegistrationNumber': ['', [Validators.required]],
      'webclient.CompanyEmailID': ['', [Validators.required]],
      'webclient.LegalName': ['', [Validators.required]],
      'webclient.CompanyType': ['', [Validators.required]],
      'webclient.CompanyEstablishedDate': ['', [Validators.required]],
      'webclient.DisplayNameOfCompany': ['', [Validators.required]],
      'webclient.LogoUrl': ['', [Validators.required]],
      'webclient.AdminUserName': ['', [Validators.required]],
      'webclient.PrimaryAdminEmailID': ['', [Validators.required]],
      'webclient.CompanyPersonName': ['', [Validators.required]],
      'webclient.CompanyPersonEmailID': ['', [Validators.required]],
      'webclient.AdminPhoneNumber': ['', [Validators.required]],
      'webclient.CompanyPersonPhoneNumber': ['', [Validators.required]],
      'webclient.CompanyPhone': ['', [Validators.required]],
      'webclient.AdminPersonDesignation': ['', [Validators.required]]
    });
  }

  onGetWebClientById() {
    this.adminservice.onGetWebClientById(this.weId).subscribe(result => {
      if (result.success == 1) {
        this.webclient = result.data;
      }
      else {
        alert(result.data);
      }
      console.log(this.webclient);

      if (this.webclient.CompanyEstablishedDate != null && this.webclient.CompanyEstablishedDate != "" && this.webclient.CompanyEstablishedDate != undefined) {
        this.estaDate = this.datepipe.transform(this.webclient.CompanyEstablishedDate, 'yyyy-MM-dd');
        this.webclient.CompanyEstablishedDate = this.estaDate;
      }
      });
    return this.webclient;
  }
}
