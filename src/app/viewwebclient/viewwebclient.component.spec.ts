import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewwebclientComponent } from './viewwebclient.component';

describe('ViewwebclientComponent', () => {
  let component: ViewwebclientComponent;
  let fixture: ComponentFixture<ViewwebclientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewwebclientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewwebclientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
