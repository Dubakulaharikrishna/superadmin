
import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';

import { ViewExerciseTypeComponent } from '../viewexercisetype/viewexercisetype.component';

export const ViewExerciseTypeRoutes: Routes = [
  {
    path: '',
    component: ViewExerciseTypeComponent,
    canActivate: [AuthGuard],
    data: {
      heading: 'ViewExerciseType'
    }
  },
];
