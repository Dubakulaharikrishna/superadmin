import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewExerciseTypeComponent } from './viewexercisetype.component';

describe('ViewexercisetypeComponent', () => {
  let component: ViewExerciseTypeComponent;
  let fixture: ComponentFixture<ViewExerciseTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ViewExerciseTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewExerciseTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
