import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import 'rxjs/add/operator/share';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';
//import { Programmanagement } from '../programmanagement/programmanagement.module';
import { CommonModule } from '@angular/common';
//import { ProgramManagementModule } from './programmanagement.module';
import { DatePipe } from '@angular/common'

import { AdminService } from '../services/admin.service';
import { CustomValidators } from 'ng2-validation';
import 'rxjs/add/operator/debounceTime';
import { Subject } from 'rxjs/Subject';
import 'rxjs/Rx';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { retry } from 'rxjs/Operator/retry';
import { ExerciseType } from '../Models/ExerciseType ';
import { Status } from '../Models/Status';
import { AccountManagement } from '../Models/AccountManagement';





@Component({
  selector: 'app-viewexercisetype',
  templateUrl: './viewexercisetype.component.html',
  styleUrls: ['./viewexercisetype.component.css'],
  providers: [DatePipe]
})
export class ViewExerciseTypeComponent implements OnInit {
  dataLoaded: any;
  exeicon: any;
  createdby: any;

  listofAccounts: AccountManagement[];
  public exercisetypeUpdateFormGroup: FormGroup;
  public form: FormGroup;
  exercisetype: ExerciseType;
  id: any;
  isSubmitted: boolean = false;
  extId: any;
  status: Status;
  constructor(private fb: FormBuilder, public datepipe: DatePipe, private adminservice: AdminService, public ele: ElementRef, private router: Router, public route: ActivatedRoute, private http: HttpClient) {

    this.exercisetype = new ExerciseType();



  }

  ngOnInit() {
    this.UpdateExerciseTypeform();
    this.route.params.subscribe(params => {
      this.extId = params['extId'];

    });
    this.onGetExerciseTypeById();
    this.onGetAllAccountsList();
    this.createdby = localStorage.getItem('MemberName');
  }

  onGetAllAccountsList() {
    this.adminservice.onGetAllAccountsList().subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofAccounts = this.status.Data;

        //alert(JSON.stringify(this.listofAccounts));
        return this.listofAccounts
      }

    });
  }
  test(id) {
    //  alert("Hi");
    //console.log(id);
    //alert(id);
    console.log(this.exercisetype.MasterAccount_Id);
   // alert(this.exercisetype.MasterAccount_Id);
  }
  onUpdateExerciseType(exercisetype) {
    let files = this.ele.nativeElement.querySelector('#ExerciseTypeIcon').files;
    if (files.length > 0 && files.count != 0 && files != null) {

      let formData = new FormData();
      let file = files[0];
      this.exeicon = file.name;
      formData.append('ExerciseTypeIcon', file, file.name);
      console.log(formData);
      console.log(files);
      this.adminservice.onfileupload(formData).subscribe(res => this.dataLoaded = res);
    }
    let date = new Date();

    const exe = {
      Id: this.exercisetype.Id,
      MasterAccount_Id: this.exercisetypeUpdateFormGroup.controls["exercisetype.MasterAccount_Id"].value,
      ExerciseTypeIcon: "Tcsimages/exeiconimages/" + this.exeicon,
      ExerciseTypeName: this.exercisetypeUpdateFormGroup.controls["exercisetype.ExerciseTypeName"].value,
      ExerciseTypeDescription: this.exercisetypeUpdateFormGroup.controls["exercisetype.ExerciseTypeDescription"].value,
      ExerciseTypeStatus: this.exercisetypeUpdateFormGroup.controls["exercisetype.ExerciseTypeStatus"].value,
      ExerciseTypeCreatedBy: this.exercisetype.ExerciseTypeCreatedBy,
      ExerciseTypeCreatedDate: this.exercisetype.ExerciseTypeCreatedDate,
      ExerciseTypeUpdatedBy: this.createdby,
      ExerciseTypeUpdatedDate: this.datepipe.transform(date, "yyyy-MM-dd")


    }
    if (this.exercisetypeUpdateFormGroup.valid) {
      //  alert(JSON.stringify(accountmanagement));
      this.isSubmitted = false;
   
      this.adminservice.onUpdateExerciseType(exe).subscribe(res => {
        console.log(res);
        this.exercisetype = res
      });
      alert("Updated Successfully");
      this.router.navigate(['/exercisetype/']);

    }
    else {
      this.isSubmitted = true;
      //alert(JSON.stringify(license));
    }
    //alert(JSON.stringify(account));
  }

  UpdateExerciseTypeform() {
    this.exercisetypeUpdateFormGroup = this.fb.group({
      // 'themestudio.ThemeStudio_Id': ['', [Validators.required]],
      'exercisetype.MasterAccount_Id': ['', [Validators.required]],
      'exercisetype.ExerciseTypeName': ['', [Validators.required]],
      'exercisetype.ExerciseTypeDescription': ['', [Validators.required]],
      'exercisetype.ExerciseTypeStatus': ['', [Validators.required]],


    });
  }
  onGetExerciseTypeById() {

    this.adminservice.onGetExerciseTypeById(this.extId).subscribe(result => {
      this.status = result;
      if (this.status.StatusCode == "1") {


        this.exercisetype = this.status.Data;
      }
      else {
        alert(this.status.Data);
      }

      console.log(this.exercisetype);
    });
    return this.exercisetype;
  }

}


