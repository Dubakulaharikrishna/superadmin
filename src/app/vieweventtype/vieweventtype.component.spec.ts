import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VieweventtypeComponent } from './vieweventtype.component';

describe('VieweventtypeComponent', () => {
  let component: VieweventtypeComponent;
  let fixture: ComponentFixture<VieweventtypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VieweventtypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VieweventtypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
