import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';

import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { Router, ActivatedRoute } from '@angular/router';
import { AdminService } from '../services/admin.service';
import { DatePipe } from '@angular/common'
import { retry } from 'rxjs/operator/retry';

import { Status } from '../Models/Status';
import { Eventtype } from '../Models/Eventtype';
import { AccountManagement } from '../Models/AccountManagement';

@Component({
  selector: 'app-vieweventtype',
  templateUrl: './vieweventtype.component.html',
  styleUrls: ['./vieweventtype.component.css'],
  providers: [DatePipe]
})
export class VieweventtypeComponent implements OnInit {
  eveicon: any;
  dataLoaded: any;
  MasterAccount_Id: any;
  public ViewEventtypeform: FormGroup;
  eventtype: Eventtype;
  id: any;
  createdby: any;
  listofAccounts: AccountManagement[];
  isSubmitted: boolean = false;
  eveId: any;
  status: Status;
  constructor(private adminservice: AdminService, public datepipe: DatePipe, public ele: ElementRef, private router: Router, public route: ActivatedRoute, private http: HttpClient, public fb: FormBuilder) {
    this.eventtype = new Eventtype();
  }
  ngOnInit() {
    this.route.params.subscribe(params => {
      this.eveId = params['eveId'];

    });
    this.Eventtypeform();
    this.onGetEventtypeById();
    this.onGetAllAccountsList();
    this.createdby = localStorage.getItem('MemberName');

  }
  onGetAllAccountsList() {
    this.adminservice.onGetAllAccountsList().subscribe(result => {
      if (result.success == 1) {
       
        this.listofAccounts = result.data;

        //alert(JSON.stringify(this.listofAccounts));
        return this.listofAccounts
      }

    });
  }
  test(id) {
    //  alert("Hi");
    //console.log(id);
    //alert(id);
    console.log(this.eventtype.MasterAccount_Id);
  //  alert(this.eventtype.MasterAccount_Id);
  }


  onGetEventtypeById() {
    //alert(id);
    this.adminservice.onGetEventtypeById(this.eveId).subscribe(result => {
     // alert(JSON.stringify(result));
      if (result.success == 1) {


        this.eventtype = result.data;
      }
      else {
        alert(result.data);
      }
      console.log(this.eventtype);
    });
    return this.eventtype;
  }


  onUpdateEventtype(eventtype) {
    let files = this.ele.nativeElement.querySelector('#EventIcon').files;
    if (files.length > 0 && files.count != 0 && files != null) {

      let formData = new FormData();
      let file = files[0];
      this.eveicon = file.name;
      formData.append('EventIcon', file, file.name);
      console.log(formData);
      console.log(files);
      this.adminservice.onfileupload(formData).subscribe(res => this.dataLoaded = res);
    }
    let date = new Date();

    const event = {
      Id: this.eventtype.Id,
      MasterAccount_Id: this.ViewEventtypeform.controls["eventtype.MasterAccount_Id"].value,
      EventName: this.ViewEventtypeform.controls["eventtype.EventName"].value,
      EventIcon: "Tcsimages/eventiconimages/" + this.eveicon,
      EventDescription: this.ViewEventtypeform.controls["eventtype.EventDescription"].value,
      EventStatus: this.ViewEventtypeform.controls["eventtype.EventStatus"].value,
      EventCreatedBy: this.eventtype.EventCreatedBy,
      EventCreatedDate: this.eventtype.EventCreatedDate,
      EventUpdatedBy: this.createdby,
      EventUpdatedDate: this.datepipe.transform(date, "yyyy-MM-dd")
    }
    if (this.ViewEventtypeform.valid) {
      this.isSubmitted = false;
      // alert(JSON.stringify(license));

      //  alert(JSON.stringify(programcategory));
      this.adminservice.onUpdateEventType(event).subscribe(res => this.eventtype = res);
      alert("Successfully Updated");
      this.router.navigate(['/eventtype/']);
    }
    else {
      this.isSubmitted = true;
      //alert(JSON.stringify(license));
    }
    //alert(JSON.stringify(account));
  }


  Eventtypeform() {
    this.ViewEventtypeform = this.fb.group({
      // AccountID: ['', [Validators.required]],
      'eventtype.MasterAccount_Id': ['', [Validators.required]],
      'eventtype.EventName': ['', [Validators.required]],
      'eventtype.EventDescription': ['', [Validators.required]],
      'eventtype.EventStatus': ['', [Validators.required]],


    });
  }







}
