import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';

import { VieweventtypeComponent } from './vieweventtype.component';

export const VieweventtypeRoutes: Routes = [

  {
    path: '',
    component: VieweventtypeComponent,
    canActivate: [AuthGuard],
    data: {
      heading: 'Viewaccount works!'
    }    
  },

  
];

