
import { AdminService } from '../services/admin.service';
import { CustomValidators } from 'ng2-validation';
import { DatePipe } from '@angular/common'
import 'rxjs/add/operator/debounceTime';
import { Subject } from 'rxjs/Subject';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/share';
import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl, } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { CustomFormsModule } from 'ng2-validation';
import { BrowserModule } from '@angular/platform-browser';
import { ExerciseQuestion } from '../Models/ExerciseQuestion';
import { retry } from 'rxjs/Operator/retry';
import 'rxjs/Rx';
import { Status } from '../Models/Status';
import { Exercise } from '../Models/Exercise';




@Component({
  selector: 'app-viewexercisequestion',
  templateUrl: './viewexercisequestion.component.html',
  styleUrls: ['./viewexercisequestion.component.css'],
  providers: [DatePipe]
})
export class ViewexercisequestionComponent implements OnInit {

  public exercisequestionUpdateFormGroup: FormGroup;
  //public form: FormGroup;
  exercisequestion: ExerciseQuestion;
  id: any;
  createdby: any;
  isSubmitted: boolean = false;
  exqId: any;
  status: Status;
  listofexercise: Exercise[];
  constructor(private fb: FormBuilder, public datepipe: DatePipe, private adminservice: AdminService, private router: Router, public route: ActivatedRoute, private http: HttpClient) {


    this.exercisequestion = new ExerciseQuestion();


  }

  ngOnInit() {
    this.ExerciseQuestionform();
    this.route.params.subscribe(params => {
      this.exqId = params['exqId'];

    });
    this.onGetExerciseQuestionById();
    this.onGetAllExerciseList();
    this.createdby = localStorage.getItem('MemberName');

  }



  onGetAllExerciseList() {
    let id = localStorage.getItem('AccountId');
    //  alert(id);
    this.adminservice.onGetAllExerciseList(id).subscribe(data => {
      // alert(JSON.stringify(data));

      if (data.StatusCode == "1") {
        this.status = data;
        this.listofexercise = this.status.Data;
       
        return this.listofexercise
      }


    });
  }
  test(id) {
    //  alert("Hi");
    //console.log(id);
    //alert(id);
    console.log(this.exercisequestion.Exercise_Id);
    alert(this.exercisequestion.Exercise_Id);
  }


  onUpdateExerciseQuestion(exercisequestion) {

    let date = new Date();

    const exe = {
      Id: this.exercisequestion.Id,
      Exercise_Id: this.exercisequestionUpdateFormGroup.controls["exercisequestion.Exercise_Id"].value,
      ExerciseQuestion: this.exercisequestionUpdateFormGroup.controls["exercisequestion.ExerciseQuestion"].value,
      ExerciseQuestionSummary: this.exercisequestionUpdateFormGroup.controls["exercisequestion.ExerciseQuestionSummary"].value,
      ExerciseQuestionDescription: this.exercisequestionUpdateFormGroup.controls["exercisequestion.ExerciseQuestionDescription"].value,
      ExerciseQuestionAnswers: this.exercisequestionUpdateFormGroup.controls["exercisequestion.ExerciseQuestionAnswers"].value,
      ExerciseQuestionAnswerOptions: this.exercisequestionUpdateFormGroup.controls["exercisequestion.ExerciseQuestionAnswerOptions"].value,
      ExerciseQuestionAnswerHints: this.exercisequestionUpdateFormGroup.controls["exercisequestion.ExerciseQuestionAnswerHints"].value,
      ExerciseQuestionAnswerDisplayType: this.exercisequestionUpdateFormGroup.controls["exercisequestion.ExerciseQuestionAnswerDisplayType"].value,
      ExerciseQuestionAnswerExclusionGroups: this.exercisequestionUpdateFormGroup.controls['exercisequestion.ExerciseQuestionAnswerExclusionGroups'].value,
      ExerciseQuestionIsMandatory: this.exercisequestionUpdateFormGroup.controls['exercisequestion.ExerciseQuestionIsMandatory'].value,
      ExerciseQuestionIsProficient: this.exercisequestionUpdateFormGroup.controls['exercisequestion.ExerciseQuestionIsProficient'].value,
      ExerciseQuestionStatus: this.exercisequestionUpdateFormGroup.controls['exercisequestion.ExerciseQuestionStatus'].value,
      ExerciseQuestionCreatedBy: this.exercisequestion.ExerciseQuestionCreatedBy,
      ExerciseQuestionCreatedDate: this.exercisequestion.ExerciseQuestionCreatedDate,
      ExerciseQuestionUpdatedBy: this.createdby,
      ExerciseQuestionUpdatedDate: this.datepipe.transform(date, "yyyy-MM-dd"),

    }

      if (this.exercisequestionUpdateFormGroup.valid) {
        //  alert(JSON.stringify(accountmanagement));
        this.isSubmitted = false;
        //alert(JSON.stringify(exercisequestion));
        this.adminservice.onUpdateExerciseQuestion(exe).subscribe(res => {
          console.log(res);
          this.exercisequestion = res
        });
        alert("Successfully Updated");
        this.router.navigate(['/exercisequestion/']);
      }
      else {
        this.isSubmitted = true;
        //alert(JSON.stringify(license));
      }
      //alert(JSON.stringify(account));
    }



 
  ExerciseQuestionform() {
    this.exercisequestionUpdateFormGroup = this.fb.group({
      'exercisequestion.Exercise_Id': ['', [Validators.required]],
      'exercisequestion.ExerciseQuestion': ['', [Validators.required]],
      'exercisequestion.ExerciseQuestionSummary': ['', [Validators.required]],

      'exercisequestion.ExerciseQuestionDescription': ['', [Validators.required]],

      'exercisequestion.ExerciseQuestionAnswers': ['', [Validators.required]],
      'exercisequestion.ExerciseQuestionAnswerOptions': ['', [Validators.required]],
      'exercisequestion.ExerciseQuestionAnswerHints': ['', [Validators.required]],
      'exercisequestion.ExerciseQuestionAnswerDisplayType': ['', [Validators.required]],
      'exercisequestion.ExerciseQuestionAnswerExclusionGroups': ['', [Validators.required]],
      'exercisequestion.ExerciseQuestionIsMandatory': ['', [Validators.required]],
      'exercisequestion.ExerciseQuestionIsProficient': ['', [Validators.required]],
    //  'exercisequestion.ExerciseQuestionCreatedDate': ['', [Validators.required]],
     // 'exercisequestion.ExerciseQuestionCreatedBy': ['', [Validators.required]],
     // 'exercisequestion.ExerciseQuestionUpdatedDate': ['', [Validators.required]],
     // 'exercisequestion.ExerciseQuestionUpdatedBy': ['', [Validators.required]],
      'exercisequestion.ExerciseQuestionStatus': ['', [Validators.required]],



    });
  }


  onGetExerciseQuestionById() {

    this.adminservice.onGetExerciseQuestionById(this.exqId).subscribe(result => {

      this.status = result;
      if (this.status.StatusCode == "1") {


        this.exercisequestion = this.status.Data;
      }
      else {
        alert(this.status.Data);
      }

      console.log(this.exercisequestion);
    });
    return this.exercisequestion;
  }
}

//onUpdateLicense(license) {
//  this.adminservice.onUpdateAccount(license).subscribe(res => this.license = res);

//  }
