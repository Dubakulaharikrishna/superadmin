import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';

import { ViewexercisequestionComponent } from './viewexercisequestion.component';

export const ViewexercisequestionRoutes: Routes = [

  {
    path: '',
    component: ViewexercisequestionComponent,
    canActivate: [AuthGuard],
    data: {
      heading: 'Viewexercisequestion!'
    }
  },


];
