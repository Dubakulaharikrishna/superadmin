import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewexercisequestionComponent } from './viewexercisequestion.component';

describe('ViewexercisequestionComponent', () => {
  let component: ViewexercisequestionComponent;
  let fixture: ComponentFixture<ViewexercisequestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewexercisequestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewexercisequestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
