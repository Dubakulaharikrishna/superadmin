import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';

import { ViewlicenseComponent } from './viewlicense.component';

export const ViewlicenseRoutes: Routes = [

  {
    path: '',
    component: ViewlicenseComponent,
    canActivate: [AuthGuard],
    data: {
      heading: 'Viewevent works!'
    }
  },


];
