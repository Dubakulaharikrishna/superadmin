
import { AdminService } from '../services/admin.service';
import { CustomValidators } from 'ng2-validation';
import { DatePipe } from '@angular/common'
import 'rxjs/add/operator/debounceTime';
import { Subject } from 'rxjs/Subject';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/share';
import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl, } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { CustomFormsModule } from 'ng2-validation';
import { BrowserModule } from '@angular/platform-browser';
import { License } from '../Models/license';
import { retry } from 'rxjs/Operator/retry';
import 'rxjs/Rx';
import { Status } from '../Models/Status';
import { InstanceManager } from '../Models/InstanceManager';
import { Webclient } from '../Models/Webclient';




@Component({
  selector: 'app-viewlicense',
  templateUrl: './viewlicense.component.html',
  styleUrls: ['./viewlicense.component.css'],
  providers: [DatePipe]
})
export class ViewlicenseComponent implements OnInit {
  instancemanager: any;
  webclient: any;
  establishedtDate: any;
  setDate: any;
  expDate: any;
  public licenseUpdateFormGroup: FormGroup;
  //public form: FormGroup;
  license: License;
  id: any;
  instid: any;
  estaDate: any;
  isSubmitted: boolean = false;
  licId: any;
  listofinstances: InstanceManager[];
  webclientslist: Webclient[];
  status: Status;
  createdby: any;
  constructor(private fb: FormBuilder, private adminservice: AdminService, private router: Router, public route: ActivatedRoute, private http: HttpClient, public datepipe: DatePipe) {

    this.license = new License();
    this.webclient = new Webclient();
    this.instancemanager = new InstanceManager();

  }

  ngOnInit() {
    this.UpdateLicenseform();
    this.route.params.subscribe(params => {
      this.licId = params['licId'];

    });
    this.onGetLicenseById();
    this.createdby = localStorage.getItem('MemberName');
    this.onGetAllInstanceManagerList();
    this.onGetAllWebClientList();
    this.instid = localStorage.getItem('InstanceId');
  }


  onGetAllInstanceManagerList() {
    this.adminservice.onGetAllInstanceManagerList().subscribe(result => {
      if (result.success == 1) {
        //  this.status = data;
        this.listofinstances = result.data;

        // alert(JSON.stringify(this.Programstructures));
        return this.listofinstances
      }

    });
  }

  onGetAllWebClientList() {
    this.adminservice.onGetAllWebClientList().subscribe(result => {

      if (result.success == 1) {

        this.webclientslist = result.data;

        // alert(JSON.stringify(this.Programstructures));
        return this.webclientslist
      }

    });
  }

  onGetWebClientById() {
    //alert(this.ClientInfo);
    this.adminservice.onGetWebClientById(this.license.Client_Id).subscribe(result => {
      //  alert(JSON.stringify(result));
      console.log("hiii", result);
      //  this.status = result;
      if (result.success == 1) {


        this.webclient = result.data;
        this.license.CompanyName = this.webclient.CompanyName;
        this.license.RegistrationNumber = this.webclient.CompanyRegistrationNumber;
        this.license.AddressLine1 = this.webclient.CompanyAddressLine1;
        this.license.AddressLine2 = this.webclient.CompanyAddressLine2;
        this.license.PrimaryAdminPhone = this.webclient.AdminPhoneNumber;
        this.license.LegalName = this.webclient.LegalName;
        this.license.CompanyType = this.webclient.CompanyType;
        this.license.EstablishedDate = this.webclient.CompanyEstablishedDate;
        this.license.PrimaryAdminEmailID = this.webclient.PrimaryAdminEmailID;
        this.license.CompanyEmailID = this.webclient.CompanyEmailID;
        this.license.CompanyPhone = this.webclient.CompanyPhone;
      }
      else {
        alert(result.data);
      }
      console.log(this.webclient);

      if (this.webclient.CompanyEstablishedDate != null && this.webclient.CompanyEstablishedDate != "" && this.webclient.CompanyEstablishedDate != undefined) {
        this.estaDate = this.datepipe.transform(this.webclient.CompanyEstablishedDate, 'yyyy-MM-dd');
        this.webclient.CompanyEstablishedDate = this.estaDate;
      }


    });
    return this.webclient;
  }

  onGetInstanceManagerById() {
    this.adminservice.onGetInstanceManagerById(this.license.Instance_Id).subscribe(result => {
      //  this.status = result;
      //alert(JSON.stringify(result));
      if (result.success == 1) {


        this.instancemanager = result.data;
        this.license.InstancePublicIPAddress = this.instancemanager.InstancePublicIPAddress;
      }
      else {
        alert(result.data);
      }

      console.log(this.instancemanager);

      if (this.instancemanager.SetupDateTime != null && this.instancemanager.SetupDateTime != "" && this.instancemanager.SetupDateTime != undefined) {
        this.setDate = this.datepipe.transform(this.instancemanager.SetupDateTime, 'yyyy-MM-dd');
        this.instancemanager.SetupDateTime = this.setDate;
      }
      if (this.instancemanager.LicenseExpiryDate != null && this.instancemanager.LicenseExpiryDate != "" && this.instancemanager.LicenseExpiryDate != undefined) {
        this.expDate = this.datepipe.transform(this.instancemanager.LicenseExpiryDate, 'yyyy-MM-dd');
        this.instancemanager.LicenseExpiryDate = this.expDate;
      }

    });
    return this.instancemanager;
  }
  client(id) {
    //alert(this.license.Client_Id);
    
    console.log(this.license.Client_Id);
    this.onGetWebClientById();
    //    (this.ClientInfo);

  }

  test(id) {
    // alert(this.ClientInfo);
  //  alert(this.license.Instance_Id);
    console.log(this.license.Instance_Id);
    this.onGetInstanceManagerById();
    //    (this.ClientInfo);

  }

  onUpdateLicense(license) {
   
    let date = new Date();

    const lic = {
      _id: license._id,
      InstanceManagerId: this.instid,
      Instance_Id: this.licenseUpdateFormGroup.controls["license.Instance_Id"].value,
      Client_Id: this.licenseUpdateFormGroup.controls["license.Client_Id"].value,
      CompanyName: this.licenseUpdateFormGroup.controls["license.CompanyName"].value,
      LegalName: this.licenseUpdateFormGroup.controls["license.LegalName"].value,
      RegistrationNumber: this.licenseUpdateFormGroup.controls["license.RegistrationNumber"].value,
      CompanyType: this.licenseUpdateFormGroup.controls["license.CompanyType"].value,
      AddressLine1: this.licenseUpdateFormGroup.controls["license.AddressLine1"].value,
      AddressLine2: this.licenseUpdateFormGroup.controls["license.AddressLine2"].value,
      City: this.licenseUpdateFormGroup.controls["license.City"].value,
      State: this.licenseUpdateFormGroup.controls["license.State"].value,
      ZipCode: this.licenseUpdateFormGroup.controls['license.ZipCode'].value,
      Country: this.licenseUpdateFormGroup.controls['license.Country'].value,
      Industry: this.licenseUpdateFormGroup.controls['license.Industry'].value,
      AreaOfBusiness: this.licenseUpdateFormGroup.controls['license.AreaOfBusiness'].value,
      EstablishedDate: this.licenseUpdateFormGroup.controls['license.EstablishedDate'].value,
      OperatingCountries: this.licenseUpdateFormGroup.controls["license.OperatingCountries"].value,
      CompanyWebsite: this.licenseUpdateFormGroup.controls["license.CompanyWebsite"].value,
      CompanySocialUrls: this.licenseUpdateFormGroup.controls["license.CompanySocialUrls"].value,
      PrimaryAdminEmailID: this.licenseUpdateFormGroup.controls["license.PrimaryAdminEmailID"].value,
      PrimaryAdminPassword: this.licenseUpdateFormGroup.controls["license.PrimaryAdminPassword"].value,
      PrimaryAdminPhone: this.licenseUpdateFormGroup.controls["license.PrimaryAdminPhone"].value,
      CompanyEmailID: this.licenseUpdateFormGroup.controls["license.CompanyEmailID"].value,
      CompanyPhone: this.licenseUpdateFormGroup.controls["license.CompanyPhone"].value,
      InstancePublicIPAddress: this.licenseUpdateFormGroup.controls['license.InstancePublicIPAddress'].value,
     // ClientAgentDetails: this.licenseUpdateFormGroup.controls['license.ClientAgentDetails'].value,
      RequestStatus: this.licenseUpdateFormGroup.controls['license.RequestStatus'].value,
      Remarks: this.licenseUpdateFormGroup.controls['license.Remarks'].value,
      //DecodeScript: this.licenseUpdateFormGroup.controls['license.DecodeScript'].value,
      UpdatedBy: this.createdby,
      UpdatedDate: this.datepipe.transform(date, "yyyy-MM-dd"),
      CreatedBy: this.license.CreatedBy,
      //CreatedDate: this.license.CreatedDate,
    }

  if (this.licenseUpdateFormGroup.valid) {
          this.isSubmitted = false;
         // alert(JSON.stringify(license));
    this.adminservice.onUpdateLicense(lic).subscribe(res => {
      console.log(res);
      alert("Successfully Updated");
      this.router.navigate(['/license/']);

    });
        }
   
        else {
          this.isSubmitted = true;
          //alert(JSON.stringify(license));
        }
       
      }



  
  UpdateLicenseform() {
    this.licenseUpdateFormGroup = this.fb.group({
      'license.Instance_Id': ['', [Validators.required]],
      'license.Client_Id': ['', [Validators.required]],
      'license.CompanyName': ['', [Validators.required]],
      'license.LegalName': ['', [Validators.required]],
      'license.RegistrationNumber': ['', [Validators.required]],
      'license.CompanyType': ['', [Validators.required]],
      'license.AddressLine1': ['', [Validators.required]],
      'license.AddressLine2': ['', [Validators.required]],
      'license.City': ['', [Validators.required]],
      'license.State': ['', [Validators.required]],
      'license.ZipCode': ['', [Validators.required]],
      'license.Country': ['', [Validators.required]],
      'license.Industry': ['', [Validators.required]],
      'license.AreaOfBusiness': ['', [Validators.required]],
      'license.EstablishedDate': ['', [Validators.required]],
      'license.OperatingCountries': ['', [Validators.required]],
      'license.CompanyWebsite': ['', [Validators.required]],
      'license.CompanySocialUrls': ['', [Validators.required]],
      'license.PrimaryAdminEmailID': ['', [Validators.required]],
      'license.PrimaryAdminPassword': ['', [Validators.required]],
      'license.PrimaryAdminPhone': ['', [Validators.required]],     
      'license.CompanyEmailID': ['', [Validators.required]],
      'license.CompanyPhone': ['', [Validators.required]],
      'license.InstancePublicIPAddress': [''],
      //'license.ClientAgentDetails': ['', [Validators.required]],
    //  'license.CreatedDate': ['', [Validators.required]],
     // 'license.CreatedBy': ['', [Validators.required]],
    //  'license.UpdatedDate': ['', [Validators.required]],
    //  'license.UpdatedBy': ['', [Validators.required]],
      'license.RequestStatus': ['', [Validators.required]],
     // 'license.LicenseStatus': ['', [Validators.required]],
      'license.Remarks': ['', [Validators.required]],
      //'license.LicenseStartDate': ['', [Validators.required]],
      //'license.LicenseEndDate': ['', [Validators.required]],
      //'license.LicenseKey': ['', [Validators.required]],
     // 'license.DecodeScript': ['', [Validators.required]],
    //  'license.LicenseGeneratedDate': ['', [Validators.required]],
     // 'license.LicenseGeneratedBy': ['', [Validators.required]],
     // 'license.LicenseType': ['', [Validators.required]],
    //  'license.RecentSecurityToken': ['', [Validators.required]],
    //  'license.RecentTokenReqDate': ['', [Validators.required]],


    });
  }


  onGetLicenseById() {

    this.adminservice.onGetLicenseById(this.licId).subscribe(result => {

      //  this.status = result;
    //   alert(JSON.stringify(result));
      
      if (result.success == 1) {

        
        this.license = result.data;
        console.log("haskar")
        console.log(this.license)
      }
      else {
        alert(result.data);
      }
     
      console.log(this.license);

      if (this.license.EstablishedDate != null && this.license.EstablishedDate != "" && this.license.EstablishedDate  != undefined) {
        this.establishedtDate = this.datepipe.transform(this.license.EstablishedDate, 'yyyy-MM-dd');
        this.license.EstablishedDate = this.establishedtDate;
      }

    });
   
    return this.license;
  }
}



