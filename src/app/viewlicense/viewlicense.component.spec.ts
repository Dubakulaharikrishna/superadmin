import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewlicenseComponent } from './viewlicense.component';

describe('ViewlicenseComponent', () => {
  let component: ViewlicenseComponent;
  let fixture: ComponentFixture<ViewlicenseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewlicenseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewlicenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
