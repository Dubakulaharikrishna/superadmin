import { CommonModule } from '@angular/common';
import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl, } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Members } from '../Models/Members';
import { AdminService } from '../services/admin.service';
import { CustomValidators } from 'ng2-validation';
import { DatePipe } from '@angular/common'
import 'rxjs/add/operator/debounceTime';
import { Subject } from 'rxjs/Subject';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/share';
import { Status } from '../Models/Status';
import { Ng2SmartTableModule, LocalDataSource } from 'ng2-smart-table';
import { AccountManagement } from '../Models/AccountManagement';
import { forEach } from '@angular/router/src/utils/collection';
import { Service } from '../Models/Service';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/share';
import { ServiceType } from '../Models/ServiceType';
import { ServicePurchase } from '../Models/ServicePurchase';
import { ExcelService } from '../services/excel.service';

@Component({
  selector: 'app-servicepurchases',
  templateUrl: './servicepurchases.component.html',
  styleUrls: ['./servicepurchases.component.css'],
  providers: [ExcelService]
})
export class ServicepurchasesComponent implements OnInit {
  MasterAccount_Id: any;
  accountId: any;
  roleId: any;
  pagename: any;
  status: Status;
  listofservicepurchases: ServicePurchase[];
  source: LocalDataSource;
  selectedValue: any = 50;
  page: any;
  listofAccounts: AccountManagement[];
  constructor(public router: Router, public ele: ElementRef, private excelService: ExcelService, public http: HttpClient, public fb: FormBuilder, private adminservice: AdminService) { }

  ngOnInit() {

   // localStorage.removeItem('loginSessId');
   // localStorage.clear();
    //localStorage.setTimeout = 1;
    //sessionStorage.removeItem('loginSessId');
   // localStorage.removeItem('loginSessId');
    this.accountId = localStorage.getItem('AccountId');
    this.pagename = "Service Purchases";
    localStorage.setItem('loginSessId', this.pagename);
    this.onGetAllAccountsList();
    this.onGetAllServicePurchases(this.accountId);
    this.roleId = localStorage.getItem('RoleId');
  }


  onGetAllAccountsList() {
    this.adminservice.onGetAllAccountsList().subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofAccounts = this.status.Data;

        //alert(JSON.stringify(this.listofAccounts));
        return this.listofAccounts
      }

    });
  }

  onEdit(event: any) {
    this.router.navigate(['/viewservicepurchases/', { serpId: event.data.Id }]);
  }
  onChange(event) {
    console.log(event);
    this.onGetAllServicePurchases(event.target.value);
  }
  reset() {
    this.onGetAllServicePurchases(this.accountId);
    this.MasterAccount_Id = undefined;
  }
  onGetAllServicePurchases(id) {
    this.adminservice.onGetAllServicePurchases(id).subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofservicepurchases = this.status.Data;
        this.source = new LocalDataSource(this.listofservicepurchases);

       // alert(JSON.stringify(this.listofservicepurchases));
        return this.listofservicepurchases
      }

    });
  }

  /*Smart table*/
  settings = {

    columns: {
      PaymentMode: {
        title: 'Payment Mode',
        filter: false
      },
      PaymentStatus: {
        title: 'Payment Status',
        filter: false
      },

    },
    attr: { class: 'table table-bordered' },
    actions: {
      edit: false, //as an example
      delete: false, //as an example
      add: false,
      position: 'right',
      custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button" (click)="onEdit(rec.Id)">View</button></span>` }]
    },
    pager: { display: true, perPage: 50 },
  };

  //route(event) {
  //  alert('hi');
  //}

  onSearch(query: string = '') {
    if (query != '') {
      this.source.setFilter([
        // fields we want to include in the search
        {
          field: 'PaymentMode',
          search: query
        },
        {
          field: 'PaymentStatus',
          search: query
        }
      ], false);
    }
    else {
      this.source = new LocalDataSource(this.listofservicepurchases);
    }

    // second parameter specifying whether to perform 'AND' or 'OR' search 
    // (meaning all columns should contain search query or at least one)
    // 'AND' by default, so changing to 'OR' by setting false here
  }

  onSelectedFilter(event) {
    this.page = event.target.value;
    this.source = new LocalDataSource(this.listofservicepurchases);
    this.settings = {

      columns: {
        PaymentMode: {
          title: 'Payment Mode',
          filter: false
        },
        PaymentStatus: {
          title: 'Payment Status',
          filter: false
        },

      },
      attr: { class: 'table table-bordered' },
      actions: {
        edit: false, //as an example
        delete: false, //as an example
        add: false,
        position: 'right',
        custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button">View</button></span>` }]
      },
      pager: { display: true, perPage: this.page },
    };

  }

  onExport() {
    this.excelService.exportAsExcelFile(this.listofservicepurchases, 'ServicePurchase');
  }

}
