import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';
import { ServicepurchasesComponent } from './servicepurchases.component';

export const ServicepurchasesRoutes: Routes = [

  {
    path: '',
    component: ServicepurchasesComponent,
    canActivate: [AuthGuard],
    data: {
      heading: 'Service'
    }
  },


];
