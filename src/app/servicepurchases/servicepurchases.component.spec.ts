import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServicepurchasesComponent } from './servicepurchases.component';

describe('ServicepurchasesComponent', () => {
  let component: ServicepurchasesComponent;
  let fixture: ComponentFixture<ServicepurchasesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServicepurchasesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServicepurchasesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
