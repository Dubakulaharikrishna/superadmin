//import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//import { DashboardComponent } from './dashboard.component';
import { RouterModule } from '@angular/router';
import { DashboardRoutes } from './dashboard.routing';
import { DashboardComponent } from '../dashboard/dashboard.component';


@NgModule({
  imports: [
    RouterModule.forChild(DashboardRoutes),
    //BrowserModule
  ],
  declarations: [
   
    DashboardComponent
    
  ],
 
  providers: [],
  bootstrap: [DashboardComponent]
})
export class DashboardModule { }
