import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { AdminService } from '../services/admin.service';

@Injectable()
export class AuthGuard implements CanActivate {

  redirectUrl;

  constructor(
    private cbOvc: AdminService,
    private router: Router
  ) { }

  // Function to check if user is authorized to view route
  canActivate(
    router: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ) {
    // Check if user is logge din
    //if (this.cbOvc.loggedIn()) {
    //  alert("if");
    //  return true; // Return true: User is allowed to view route
    //}
    if (this.cbOvc.loggedIn()) {
      // alert("if");
     // alert("ok");
      return true; // Return true: User is allowed to view route
    }
    else {
     // alert("Nope");

      this.redirectUrl = state.url; // Grab previous urul
      // alert("else");
      this.router.navigate(['/']); // Return error and route to login page

      return false; // Return false: user not authorized to view page

    }
  }


}
