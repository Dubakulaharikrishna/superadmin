//import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//import { DashboardComponent } from './dashboard.component';
import { RouterModule } from '@angular/router';
import { GeneratelicenseRoutes } from './generatelicense.routing';
import { GeneratelicenseComponent } from '../generatelicense/generatelicense.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { HttpModule } from '@angular/http';
import { DataTablesModule } from 'angular-datatables';
import {HttpClientModule} from '@angular/common/http';
import { CommonModule } from '@angular/common'; 
import { BrowserModule } from '@angular/platform-browser';
import { AdminService } from '../services/admin.service';
import { NgbProgressbarModule, NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';



import { NgbAccordionModule } from '@ng-bootstrap/ng-bootstrap';
import { CustomFormsModule } from 'ng2-validation';
import { FormsModule, FormBuilder, FormGroup, Validators, FormControl, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  imports: [
    DataTablesModule,
    HttpClientModule,
    CommonModule,
    FormsModule,
    CustomFormsModule,
    NgbModule,
    ReactiveFormsModule,

    HttpModule,
    RouterModule.forChild(GeneratelicenseRoutes),
    //BrowserModule
  ],
  declarations: [
   
    GeneratelicenseComponent
    
  ],
 
  providers: [AdminService, AuthGuard, NotAuthGuard],
  bootstrap: [GeneratelicenseComponent]
})
export class GeneratelicenseModule { }
