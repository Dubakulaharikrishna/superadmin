import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';
import { GeneratelicenseComponent } from './generatelicense.component';

export const GeneratelicenseRoutes: Routes = [

  {
    path: '',
    component: GeneratelicenseComponent,
    canActivate: [AuthGuard],

    data: {
      heading: 'Eventmanagement'
    }    
  },

  
];

