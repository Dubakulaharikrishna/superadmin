
import { AdminService } from '../services/admin.service';
import { CustomValidators } from 'ng2-validation';
import { DatePipe } from '@angular/common'
import 'rxjs/add/operator/debounceTime';
import { Subject } from 'rxjs/Subject';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/share';
import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl, } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { CustomFormsModule } from 'ng2-validation';
import { BrowserModule } from '@angular/platform-browser';
import { License } from '../Models/license';
import { retry } from 'rxjs/Operator/retry';
import 'rxjs/Rx';
import { Status } from '../Models/Status';




@Component({
  selector: 'app-generatelicense',
  templateUrl: './generatelicense.component.html',
  styleUrls: ['./generatelicense.component.css'],
  providers: [DatePipe]
})
export class GeneratelicenseComponent implements OnInit {

  public licenseUpdateFormGroup: FormGroup;
  //public form: FormGroup;
  license: License;
  listofLicense: License[];
  id: any;
  dtTrigger: Subject<any> = new Subject();
  public _success = new Subject<string>();
  staticAlertClosed = false;
  title = 'Simple Datatable Example using Angular 4';
  public data: Object;
  result: any[];
  success: boolean = false;
  message: any;
  isSubmitted: boolean = false;
  licId: any;
  status: Status;
  createdby: any;
  constructor(private fb: FormBuilder, public datepipe: DatePipe, private adminservice: AdminService, private router: Router, public route: ActivatedRoute, private http: HttpClient) {


    this.license = new License();


  }

  ngOnInit() {
    this.Licenseform();
    this.route.params.subscribe(params => {
      this.licId = params['memId'];
     // alert(this.licId);
    });
    setTimeout(() => this.staticAlertClosed = true, 20000);
    this._success.subscribe((message) => this.message = message);
    this._success.debounceTime(7000).subscribe(() => this.message = null);
    this.onGetLicenseById();
    this.createdby = localStorage.getItem('MemberName');
    this.onGetAllLicenseList();
  }
  Licenseform() {
    let date = new Date();

    this.licenseUpdateFormGroup = this.fb.group({
      'license.CompanyName': ['', [Validators.required]],
   

      'license.RegistrationNumber': ['', [Validators.required]],

     
      'license.PrimaryAdminEmailID': ['', [Validators.required]],
   
      'license.CompanyPhone': ['', [Validators.required]],
    
      'license.RequestStatus': ['', [Validators.required]],
  
      'license.LicenseStartDate': ['', [Validators.required]],
      'license.LicenseEndDate': ['', [Validators.required]],
    


    });
  }



  onGenerateLicenses(license) {

    let date = new Date();

    const lic = {
      Client_Id:this.license.Client_Id,
      _id: this.license._id,
      CompanyName: this.licenseUpdateFormGroup.controls["license.CompanyName"].value,
      RegistrationNumber: this.licenseUpdateFormGroup.controls["license.RegistrationNumber"].value,
      PrimaryAdminEmailID: this.licenseUpdateFormGroup.controls["license.PrimaryAdminEmailID"].value,
      CompanyPhone: this.licenseUpdateFormGroup.controls["license.CompanyPhone"].value,
    //  LicenseStatus: this.licenseUpdateFormGroup.controls["license.LicenseStatus"].value,
      LicenseStartDate: this.licenseUpdateFormGroup.controls["license.LicenseStartDate"].value,
      LicenseEndDate: this.licenseUpdateFormGroup.controls["license.LicenseEndDate"].value,
     // LicenseType: this.licenseUpdateFormGroup.controls["license.LicenseType"].value,
      RequestStatus: this.licenseUpdateFormGroup.controls["license.RequestStatus"].value,
      LicenseGeneratedBy: this.createdby,
      LicenseGeneratedDate: this.datepipe.transform(date, "yyyy-MM-dd"),
    }
   
    if (this.licenseUpdateFormGroup.valid) {
      //  alert(JSON.stringify(accountmanagement));
      this.isSubmitted = false;
      this.adminservice.onGenerateLicense(lic).subscribe(result => {
        console.log(result);
        if (result.success == 1) {
          this.success = true;
          this.message = result.Message;
          this._success.next(`Genereted License Successfully`);
          this.router.navigate(['/license/', { message: this.message }]);
         
          

        }
        else {
          this.success = false;
          this.message = "Failed to create License";
        }
      });
    }
    else {
      this.isSubmitted = true;
      //alert(JSON.stringify(license));
    }
    //alert(JSON.stringify(account));
  }
  onGetAllLicenseList() {
    this.adminservice.onGetAllLicenseList().subscribe(result => {

      if (result.success == 1) {

        this.listofLicense = result.data;
        // alert(JSON.stringify(this.Programstructures));
        return this.listofLicense
      }

    });
  }

  onGetLicenseById() {
    this.adminservice.onGetLicenseById(this.licId).subscribe(result => {
      // this.status = result;
     // alert(JSON.stringify(result));
      if (result.success == 1) {


        this.license = result.data;
      }
      else {
        alert(result.data);
      }
      console.log(this.license);
    });
    return this.license;
  }
}

//onUpdateLicense(license) {
//  this.adminservice.onUpdateAccount(license).subscribe(res => this.license = res);

//  }


