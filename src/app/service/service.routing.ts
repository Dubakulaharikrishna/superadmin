import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';

import { ServiceComponent } from './service.component';

export const ServiceRoutes: Routes = [

  {
    path: '',
    component: ServiceComponent,
    canActivate: [AuthGuard],
    data: {
      heading: 'Service'
    }
  },


];
