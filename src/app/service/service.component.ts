import { CommonModule } from '@angular/common';
import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl, } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Members } from '../Models/Members';
import { AdminService } from '../services/admin.service';
import { CustomValidators } from 'ng2-validation';
import { DatePipe } from '@angular/common'
import 'rxjs/add/operator/debounceTime';
import { Subject } from 'rxjs/Subject';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/share';
import { Status } from '../Models/Status';
import { Ng2SmartTableModule, LocalDataSource } from 'ng2-smart-table';
import { AccountManagement } from '../Models/AccountManagement';
import { forEach } from '@angular/router/src/utils/collection';
import { Service } from '../Models/Service';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/share';
import { ServiceType } from '../Models/ServiceType';
import { ExcelService } from '../services/excel.service';


@Component({
  selector: 'app-service',
  templateUrl: './service.component.html',
  styleUrls: ['./service.component.css'],
  providers: [ExcelService, DatePipe]
})
export class ServiceComponent implements OnInit {
  MasterAccount_Id: any;
  ServiceType_Id: any;
  createdby: any;
  dataLoaded: any;
  coverpic: any;
  success: boolean = false;
  message: any;
  dtTrigger: Subject<any> = new Subject();
  public _success = new Subject<string>();
  staticAlertClosed = false;
  accountId: any;
  source: LocalDataSource;
  selectedValue: any = 50;
  page: any;
  public data: Object;
  public temp_var: Object = false;
  public ServiceUpdateFormGroup: FormGroup;
  public ServiceForm: FormGroup;
  public form: FormGroup;
  service: Service;
  public isSubmitted: boolean = false;
  id: any;
  roleId: any;
  listofservice: Service[];
  listofservicetypes: ServiceType[];
  pagename: any;
  status: Status;
  listofAccounts: AccountManagement[];
  constructor(private fb: FormBuilder, public datepipe: DatePipe, public ele: ElementRef, private excelService: ExcelService, private adminservice: AdminService, private router: Router, private http: HttpClient) {

    this.service = new Service();
  }



  openCpopup() {

    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "block";
    cmask.style.display = "block";
  }
  closeCpopup() {
    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "none";
    cmask.style.display = "none";
  }

  onEdit(event: any) {


    this.router.navigate(['/viewservice/', { ssId: event.data.Id }]);
  }
  onSelectServiceTag(ServiceTag) {
    console.log(ServiceTag);
  }
  onSelectServiceLink(ServiceLink) {
    console.log(ServiceLink);
  }

  ngOnInit() {
   
    // sessionStorage.clear();
   // localStorage.removeItem('loginSessId');
   // localStorage.clear();
  //  localStorage.setTimeout = 1;
  //  sessionStorage.removeItem('loginSessId');
 //  localStorage.removeItem('loginSessId');
    this.pagename = "Service";
    localStorage.setItem('loginSessId', this.pagename);
    this.accountId = localStorage.getItem('AccountId');
    setTimeout(() => this.staticAlertClosed = true, 20000);
    this._success.subscribe((message) => this.message = message);
    this._success.debounceTime(7000).subscribe(() => this.message = null);
    this.roleId = localStorage.getItem('RoleId');
    this.createdby = localStorage.getItem('MemberName');
    this.Serviceform();
    this.onGetAllServiceList(this.accountId);
    this.onGetAllAccountsList();
    this.onGetAllServiceTypesList();

  }
  //onGetAllServiceList(id) {

  //  this.adminservice.onGetAllServiceList(id).subscribe(data => {
  //    if (data.StatusCode == "1") {
  //      this.status = data;
  //      this.listofservice = this.status.Data;
  //      this.source = new LocalDataSource(this.listofservice);
  //      console.log(this.listofservice);
  //      alert(JSON.stringify(this.listofservice));
  //      return this.listofservice
  //    }

  //  });
  //}


  onGetAllServiceList(id) {
   
    this.adminservice.onGetAllServiceList(id).subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofservice = data.Data;
    this.source = new LocalDataSource(this.listofservice);
       // alert(JSON.stringify(this.listofservice));
        //alert(JSON.stringify(this.listofAccounts));
        return this.listofservice
      }

    });
  }

  services(id) {
    //  alert("Hi");
    //console.log(id);
    //alert(id);
    console.log(this.ServiceType_Id);
    //alert(this.ServiceType_Id);
  }

  onGetAllServiceTypesList() {
    let id = localStorage.getItem('AccountId');
   // alert(id);
    this.adminservice.onGetAllServiceTypeList(id).subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofservicetypes = data.Data;
        //alert(JSON.stringify(this.listofservicetypes));
        //alert(JSON.stringify(this.listofAccounts));
        return this.listofservicetypes
      }

    });
  }

  onGetAllAccountsList() {
    this.adminservice.onGetAllAccountsList().subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofAccounts = this.status.Data;

        //alert(JSON.stringify(this.listofAccounts));
        return this.listofAccounts
      }

    });
  }

  onChange(event) {
    console.log(event);
    this.onGetAllServiceList(event.target.value);
  }
  reset() {
    this.onGetAllServiceList(this.accountId);
    this.MasterAccount_Id = undefined;
  }




  onCreateService(service: any) {
    let files = this.ele.nativeElement.querySelector('#ServiceCoverPicture').files;
    if (files.length > 0 && files.count != 0 && files != null) {

      let formData = new FormData();
      let file = files[0];
      this.coverpic = file.name;
      formData.append('ServiceCoverPicture', file, file.name);
      console.log(formData);
      console.log(files);
      this.adminservice.onfileupload(formData).subscribe(res => this.dataLoaded = res);
    }
    let date = new Date();
    const Service = {
      ServiceType_Id: this.form.controls["ServiceType_Id"].value,
      ServiceTitle: this.form.controls["ServiceTitle"].value,
      ServiceDescription: this.form.controls["ServiceDescription"].value,
      ServiceContributor: this.form.controls["ServiceContributor"].value,
    //  ServiceType: this.form.controls["ServiceType"].value,
      ServiceCoverPicture: "Tcsimages/prgcoverimages/" + this.coverpic,
      ServiceTag: this.form.controls["ServiceTag"].value,
      IsFreeService: this.form.controls["IsFreeService"].value,
      ServiceDiscountedPrice: this.form.controls["ServiceDiscountedPrice"].value,
      ServiceValidStartDate: this.form.controls["ServiceValidStartDate"].value,
      ServiceValidEndDate: this.form.controls["ServiceValidEndDate"].value,
      ServiceListedPrice: this.form.controls["ServiceListedPrice"].value,
      ServiceStatus: this.form.controls["ServiceStatus"].value,
      ServiceLink: this.form.controls["ServiceLink"].value,
      ServiceCreatedBy: this.createdby,
      ServiceCreatedDate: this.datepipe.transform(date, "yyyy-MM-dd")
    }
    console.log(Service);

    if (this.form.valid) {
     // alert(JSON.stringify(Service));
      this.isSubmitted = false;
      this.adminservice.onCreateService(Service).subscribe(data => {
        console.log(data);
        if (data.StatusCode == "1") {
          this.success = true;
          this.message = data.Message;
          this._success.next(`Record Saved Successfully`);

          this.onGetAllServiceList(this.accountId);
          this.closeCpopup();

        }
        else {
          this.success = false;
          this.message = "Failed to create account master";
        }
      });
    }

    else {
      this.isSubmitted = true;
    }

  }

  Serviceform() {
    this.form = this.fb.group({
      'ServiceType_Id': [''],
      'ServiceTitle': ['', [Validators.required]],
      'ServiceDescription': ['', [Validators.required]],
      'ServiceContributor': ['', [Validators.required]],
     // 'ServiceType': ['', [Validators.required]],
      'ServiceTag': ['', [Validators.required]],
      'ServiceCoverPicture': [''],
      'IsFreeService': [''],
      'ServiceDiscountedPrice': ['', [Validators.required]],
      'ServiceValidStartDate': ['', [Validators.required]],
      'ServiceValidEndDate': ['', [Validators.required]],
      'ServiceListedPrice': ['', [Validators.required]],
      'ServiceStatus': ['', [Validators.required]],
      'ServiceLink': ['', [Validators.required]]


    });
  }


  /*Smart table*/
  settings = {
    
    columns: {
      ServiceTitle: {
        title: 'ServiceTitle',
        filter: false
      },
      ServiceStatus: {
        title: 'Service Status',
        filter: false
      },

    },
    attr: { class: 'table table-bordered' },
    actions: {
      edit: false, //as an example
      delete: false, //as an example
      add: false,
      select: true,
      position: 'right',
      custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button" (click)="onEdit(rec.Id)">Edit</button></span>` }]
    },
    pager: { display: true, perPage: 50 },
  };

  //route(event) {
  //  alert('hi');
  //}

  onSearch(query: string = '') {
    if (query != '') {
      this.source.setFilter([
        // fields we want to include in the search
        {
          field: 'ServiceTitle',
          search: query
        },
        {
          field: 'ServiceStatus',
          search: query
        }
      ], false);
    }
    else {
      this.source = new LocalDataSource(this.listofservice);
    }

    // second parameter specifying whether to perform 'AND' or 'OR' search 
    // (meaning all columns should contain search query or at least one)
    // 'AND' by default, so changing to 'OR' by setting false here
  }

  onSelectedFilter(event) {
    this.page = event.target.value;
    this.source = new LocalDataSource(this.listofservice);
    this.settings = {
      
      columns: {
        ServiceTitle: {
          title: 'ServiceTitle',
          filter: false
        },
        ServiceStatus: {
          title: 'ServiceStatus',
          filter: false
        },

      },
      attr: { class: 'table table-bordered' },
      actions: {
        edit: false, //as an example
        delete: false, //as an example
        add: false,
        select: true,
        position: 'right',
        custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button">Edit</button></span>` }]
      },
      pager: { display: true, perPage: this.page },
    };

  }

  onExport() {
    this.excelService.exportAsExcelFile(this.listofservice, 'Service');
  }

}
