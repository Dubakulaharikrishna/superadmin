//import { BrowserModule } from '@angular/platform-browser';//import { DashboardComponent } from './dashboard.component';
import { RouterModule } from '@angular/router';
import { ContentmanagementRoutes } from './contentmanagement.routing';
import { ContentmanagementComponent } from '../contentmanagement/contentmanagement.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { HttpModule } from '@angular/http';
import { DataTablesModule } from 'angular-datatables';
import {HttpClientModule} from '@angular/common/http';
import { CommonModule } from '@angular/common'; 
import { BrowserModule } from '@angular/platform-browser';


import { NgModule } from '@angular/core';




@NgModule({
  imports: [DataTablesModule,
    HttpClientModule,
    CommonModule,
    RouterModule.forChild(ContentmanagementRoutes),
 HttpModule
],
    
    //BrowserModule
  declarations: [
   
    ContentmanagementComponent
    
  ],
 
  providers: [],
  bootstrap: [ContentmanagementComponent]
})
export class ContentmanagementModule { }






