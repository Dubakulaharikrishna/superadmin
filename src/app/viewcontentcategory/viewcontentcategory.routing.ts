import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';
import { ViewcontentcategoryComponent } from './viewcontentcategory.component';

export const ViewcontentcategoryRoutes: Routes = [

  {
    path: '',
    component: ViewcontentcategoryComponent,
    canActivate: [AuthGuard],
    data: {
      heading: 'Viewaccount works!'
    }    
  },

  
];

