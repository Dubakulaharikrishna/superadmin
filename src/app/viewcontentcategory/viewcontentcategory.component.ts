import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';

import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { Router, ActivatedRoute } from '@angular/router';
import { AdminService } from '../services/admin.service';

import { retry } from 'rxjs/operator/retry';
import { contentcategory } from '../Models/contentcategory';
import { DatePipe } from '@angular/common';
import { Status } from '../Models/Status';

@Component({
  selector: 'app-viewcontentcategory',
  templateUrl: './viewcontentcategory.component.html',
  styleUrls: ['./viewcontentcategory.component.css'],
  providers: [DatePipe]
})
export class ViewcontentcategoryComponent implements OnInit {
  validDate: any;
  startDate: any;
  public contentcategoryFormGroup: FormGroup;
  contentcategory: contentcategory;
  id: any;
  isSubmitted: boolean = false;
  ccId: any;
  createdby: any;
  status: Status;
  constructor(private adminservice: AdminService, private router: Router, public route: ActivatedRoute, private http: HttpClient, public fb: FormBuilder, public datepipe: DatePipe) {
    this.contentcategory = new contentcategory();
  }
  ngOnInit() {
    this.ContentCategoryform();
    this.route.params.subscribe(params => {
      this.ccId = params['ccId'];

    });
    this.onGetContentCategoryById();
    this.createdby = localStorage.getItem('MemberName');


  }



  onUpdateContentCategory(contentcategory) {
    let date = new Date();


    const content = {
      Id: this.contentcategory.Id,
      ContentCategoryName: this.contentcategoryFormGroup.controls["contentcategory.ContentCategoryName"].value,
      ContentCategoryType: this.contentcategoryFormGroup.controls["contentcategory.ContentCategoryType"].value,
      ContentCategoryStartedDate: this.contentcategoryFormGroup.controls["contentcategory.ContentCategoryStartedDate"].value,
      ContentCategoryValidTill: this.contentcategoryFormGroup.controls["contentcategory.ContentCategoryValidTill"].value,
      ContentCategoryStatus: this.contentcategoryFormGroup.controls["contentcategory.ContentCategoryStatus"].value,
      ContentCategoryCreatedBy: this.contentcategory.ContentCategoryCreatedBy,
      ContentCategoryCreatedDate: this.contentcategory.ContentCategoryCreatedDate,
      ContentCategoryUpdatedBy: this.createdby,
      ContentCategoryUpdatedDate: this.datepipe.transform(date, "yyyy-MM-dd")
    }

  

    if (this.contentcategoryFormGroup.valid) {
      //  alert(JSON.stringify(accountmanagement));
      this.isSubmitted = false;
     // alert(JSON.stringify(contentcategory));
      this.adminservice.onUpdateContentCategory(content).subscribe(res => {
        console.log(res);
        this.contentcategory = res
      });
      alert("Successfully Updated");
      this.router.navigate(['/contentcategory/']);
    }
    else {
      this.isSubmitted = true;
      //alert(JSON.stringify(license));
    }
    //alert(JSON.stringify(account));
  }


  ContentCategoryform() {
    this.contentcategoryFormGroup = this.fb.group({
      // AccountID: ['', [Validators.required]],
      'contentcategory.ContentCategoryName': ['', [Validators.required]],
      'contentcategory.ContentCategoryType': ['', [Validators.required]],
      'contentcategory.ContentCategoryStartedDate': ['', [Validators.required]],
      'contentcategory.ContentCategoryValidTill': ['', [Validators.required]],
     // 'contentcategory.ContentCategoryCreatedDate': ['', [Validators.required]],
     // 'contentcategory.ContentCategoryCreatedBy': ['', [Validators.required]],
     // 'contentcategory.ContentCategoryUpdatedDate': ['', [Validators.required]],
      //'contentcategory.ContentCategoryUpdatedBy': ['', [Validators.required]],
      'contentcategory.ContentCategoryStatus': ['', [Validators.required]],


    });
  }
 
  onGetContentCategoryById() {
    //alert(id);
    this.adminservice.onGetContentCategoryById(this.ccId).subscribe(result => {
      this.status = result;
      if (this.status.StatusCode == "1") {


        this.contentcategory = this.status.Data;
      }
      else {
        alert(this.status.Data);
      }
      console.log(this.contentcategory);

      if (this.contentcategory.ContentCategoryStartedDate != null && this.contentcategory.ContentCategoryStartedDate != "" && this.contentcategory.ContentCategoryStartedDate != undefined) {
        this.startDate = this.datepipe.transform(this.contentcategory.ContentCategoryStartedDate, 'yyyy-MM-dd');
        this.contentcategory.ContentCategoryStartedDate = this.startDate;
      }

      if (this.contentcategory.ContentCategoryValidTill != null && this.contentcategory.ContentCategoryValidTill != "" && this.contentcategory.ContentCategoryValidTill != undefined) {
        this.validDate = this.datepipe.transform(this.contentcategory.ContentCategoryValidTill, 'yyyy-MM-dd');
        this.contentcategory.ContentCategoryValidTill = this.validDate;
      }

    });
    return this.contentcategory;
  }


}
