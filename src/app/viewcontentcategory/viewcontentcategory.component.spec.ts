import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewcontentcategoryComponent } from './viewcontentcategory.component';

describe('ViewcontentcategoryComponent', () => {
  let component: ViewcontentcategoryComponent;
  let fixture: ComponentFixture<ViewcontentcategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewcontentcategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewcontentcategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
