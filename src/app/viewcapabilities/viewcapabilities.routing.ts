import { Routes } from '@angular/router';

import { ViewCapabilitiesComponent } from './viewcapabilities.component';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';
export const ViewCapabilitiesRoutes: Routes = [

  {
    path: '',
    component: ViewCapabilitiesComponent,
    canActivate: [AuthGuard],
    data: {
      heading: 'viewcapabilities'
    }
  },




];
