import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewCapabilitiesComponent } from './viewcapabilities.component';

describe('ViewCapabilitiesComponent', () => {
  let component: ViewCapabilitiesComponent;
  let fixture: ComponentFixture<ViewCapabilitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ViewCapabilitiesComponent]
    })
      .compileComponents();
  }));


  beforeEach(() => {
    fixture = TestBed.createComponent(ViewCapabilitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
