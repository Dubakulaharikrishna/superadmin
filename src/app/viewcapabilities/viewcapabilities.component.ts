
import { AdminService } from '../services/admin.service';
import { CustomValidators } from 'ng2-validation';
import 'rxjs/add/operator/debounceTime';
import { Subject } from 'rxjs/Subject';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/share';
import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl, } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { CustomFormsModule } from 'ng2-validation';
import { BrowserModule } from '@angular/platform-browser';
import { Capabilities } from '../Models/capabilities';
import { retry } from 'rxjs/Operator/retry';
import 'rxjs/Rx';
import { DatePipe } from '@angular/common'
import { Status } from '../Models/Status';
import { Programstructure } from '../Models/Programstructure';
import { programcategory } from '../Models/programcategory';





@Component({
  selector: 'app-viewcapabilities',
  templateUrl: './viewcapabilities.component.html',
  styleUrls: ['./viewcapabilities.component.css'],
  providers: [DatePipe]
})
export class ViewCapabilitiesComponent implements OnInit {
  media: any;
  dataLoaded: any;
  coverPic: any;
  createdby: any;
  public CapabilitiesUpdateFormGroup: FormGroup;
  //public form: FormGroup;
  capabilities: Capabilities;
  id: any;
  isSubmitted: boolean = false;
  ccId: any;
  status: Status;
  selectedcatagorys: any = [];
  expanded: boolean = false;
  secret: boolean = false;
  listofprogramstructure: Programstructure[];
  listofprogramcategory: programcategory[];
  name: any;
  constructor(private fb: FormBuilder,
    private adminservice: AdminService,
    private router: Router,
    public route: ActivatedRoute,
    private http: HttpClient,
    public ele: ElementRef,
    public datepipe: DatePipe) {


    this.capabilities = new Capabilities();


  }

  ngOnInit() {
    this.ViewCapabilitiesform();
    this.route.params.subscribe(params => {
      this.ccId = params['ccId'];

    });
    this.onGetCapabilitiestById();
    this.onGetAllProgramStructureList();
    this.onGetAllProgramCategoryList();
    this.createdby = localStorage.getItem('MemberName');
  }


  onGetAllProgramCategoryList() {
    let id = localStorage.getItem('AccountId');
   // alert(id);
    this.adminservice.onGetAllProgramCategory().subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofprogramcategory = this.status.Data;
        return this.listofprogramcategory;

      }
    });
  }

  onGetAllProgramStructureList() {
    let id = localStorage.getItem('AccountId');
    this.adminservice.onGetAllProgramStructureList(id).subscribe(data => {
      if (data.StatusCode == "1") {
        this.status = data;
        this.listofprogramstructure = this.status.Data;
        return this.listofprogramstructure;

      }
      console.log(this.listofprogramstructure);
    });

  }
  test(id) {
    //  alert("Hi");
    //console.log(id);
    //alert(id);
    console.log(this.name);
  //  alert(this.capabilities.ProgramStr_Id);
  }


  ProgramCategorytest() {
   
    this.secret = true;
    var checkboxes = document.getElementById("ProgramCategory");
    //  alert(this.expanded);
    if (!this.expanded) {
      checkboxes.style.display = "block";
      this.expanded = true;
    } else {
      checkboxes.style.display = "none";
      this.expanded = false;
    }


    //   alert("hi");
  }

  Programcategory(id) {
   // alert(id);
    var removedId;

    this.selectedcatagorys.forEach((existingId) => {

      if (existingId == id) {
        removedId = existingId;
        //  this.selectedcapabilities.pop(id);
      }

    });
    if (removedId != null) {
      var ob = this.selectedcatagorys.indexOf(removedId)
      this.selectedcatagorys.splice(ob, 1);
    }

    if (removedId != id) {
      this.selectedcatagorys.push(id);
    }
  //  alert(this.selectedcatagorys);

  }
  onUpdateCapabilities() {

    let files = this.ele.nativeElement.querySelector('#CapabilityCoverPicture').files;
    let filesMedia = this.ele.nativeElement.querySelector('#CapabilityCoverMediaFile').files;
    if (files.length > 0 && files.count != 0 && files != null) {

      let formData = new FormData();
      let file = files[0];
      this.coverPic = file.name;
      formData.append('CapabilityCoverPicture', file, file.name);
      console.log(formData);
      console.log(files);
      this.adminservice.onfileupload(formData).subscribe(res => this.dataLoaded = res);
    }
    if (filesMedia.length > 0 && filesMedia.count != 0 && filesMedia != null) {

      let formData = new FormData();
      let fileM = filesMedia[0];
      this.media = fileM.name;
      formData.append('CapabilityCoverMediaFile', fileM, fileM.name);
      console.log(formData);
      console.log(files);
      this.adminservice.onfileupload(formData).subscribe(res => this.dataLoaded = res);
    }


    //alert(capabilities.Id);


    let date = new Date();
    const capabilities = {
      Id: this.capabilities.Id,
      ProgramCategory_Id: this.selectedcatagorys,
      ProgramStr_Id: this.CapabilitiesUpdateFormGroup.controls["capabilities.ProgramStr_Id"].value,
      CapabilityName: this.CapabilitiesUpdateFormGroup.controls["capabilities.CapabilityName"].value,
      CapabilityDescription: this.CapabilitiesUpdateFormGroup.controls["capabilities.CapabilityDescription"].value,
      CapabilitySynopsis: this.CapabilitiesUpdateFormGroup.controls["capabilities.CapabilitySynopsis"].value,
      CapabilityCoverPicture: "Tcsimages/prgcoverimages/" + this.coverPic,
      CapabilityType: this.CapabilitiesUpdateFormGroup.controls["capabilities.CapabilityType"].value,
      CapabilityCategory: this.CapabilitiesUpdateFormGroup.controls["capabilities.CapabilityCategory"].value,
      CapabilityTags: this.CapabilitiesUpdateFormGroup.controls["capabilities.CapabilityTags"].value,
      CapabilityStartDate: this.CapabilitiesUpdateFormGroup.controls["capabilities.CapabilityStartDate"].value,
      CapabilityValidTill: this.CapabilitiesUpdateFormGroup.controls['capabilities.CapabilityValidTill'].value,
      CapabilityListedPrice: this.CapabilitiesUpdateFormGroup.controls['capabilities.CapabilityListedPrice'].value,
      CapabilityDiscountPrice: this.CapabilitiesUpdateFormGroup.controls['capabilities.CapabilityDiscountPrice'].value,
      CapabilityAccessType: this.CapabilitiesUpdateFormGroup.controls['capabilities.CapabilityAccessType'].value,
      CapabilityIsTracking: this.CapabilitiesUpdateFormGroup.controls['capabilities.CapabilityIsTracking'].value,
      CapabilityPlatforms: this.CapabilitiesUpdateFormGroup.controls['capabilities.CapabilityPlatforms'].value,
      CapabilityCoverMediaFile: "Tcsimages/prgcoverimages/" + this.media,
      CapabilityStatus: this.CapabilitiesUpdateFormGroup.controls['capabilities.CapabilityStatus'].value,
      CapabilityIsFree: this.CapabilitiesUpdateFormGroup.controls['capabilities.CapabilityIsFree'].value,
      CapabilityDependency: this.CapabilitiesUpdateFormGroup.controls['capabilities.CapabilityDependency'].value,
      MetaTitle: this.CapabilitiesUpdateFormGroup.controls['capabilities.CapabilitySocialElements.MetaTitle'].value,
      MetaKeywords: this.CapabilitiesUpdateFormGroup.controls['capabilities.CapabilitySocialElements.MetaKeywords'].value,
      MetaDescrition: this.CapabilitiesUpdateFormGroup.controls['capabilities.CapabilitySocialElements.MetaDescrition'].value,
      CapabilityCreatedBy: this.capabilities.CapabilityCreatedBy,
      CapabilityCreatedDate: this.capabilities.CapabilityCreatedDate,
      CapabilityUpdatedBy: this.createdby,
      CapabilityUpdatedDate: this.datepipe.transform(date, "yyyy-MM-dd")

    }



    if (this.CapabilitiesUpdateFormGroup.valid) {
      //  alert(JSON.stringify(accountmanagement));
      this.isSubmitted = false;
     // alert(JSON.stringify(capabilities));
      this.adminservice.onUpdateCapabilities(capabilities).subscribe(res => {
        console.log(res);
        //this.capabilities = res
      });
      alert("Successfully Updated");
      this.router.navigate(['/capabilities/']);
    }
    else {
      this.isSubmitted = true;
      //alert(JSON.stringify(license));
    }
    //alert(JSON.stringify(account));
  }



  ViewCapabilitiesform() {
    this.CapabilitiesUpdateFormGroup = this.fb.group({
      'capabilities.CapabilityName': ['', [Validators.required]],
      'capabilities.CapabilityDescription': ['', [Validators.required]],
      'capabilities.ProgramStr_Id': ['', [Validators.required]],

      'capabilities.CapabilitySynopsis': ['', [Validators.required]],

      // 'programmanagement.ProgramCoverPicture': ['', [Validators.required]],
      'capabilities.CapabilityType': ['', [Validators.required]],
      'capabilities.CapabilityCategory': ['', [Validators.required]],
      'capabilities.CapabilityTags': ['', [Validators.required]],
      'capabilities.CapabilityStartDate': ['', [Validators.required]],
      'capabilities.CapabilityValidTill': ['', [Validators.required]],
      'capabilities.CapabilityListedPrice': ['', [Validators.required]],
      'capabilities.CapabilityDiscountPrice': ['', [Validators.required]],
      'capabilities.CapabilityAccessType': ['', [Validators.required]],
      'capabilities.CapabilityIsTracking': ['', [Validators.required]],
      'capabilities.CapabilityIsFree': ['', [Validators.required]],
      'capabilities.CapabilityPlatforms': ['', [Validators.required]],
      'capabilities.CapabilityDependency': ['', [Validators.required]],
      // 'programmanagement.ProgramCoverMediaFile': ['', [Validators.required]],
      // 'programmanagement.ProgramCreatedDate': ['', [Validators.required]],
      // 'programmanagement.ProgramCreatedBy': ['', [Validators.required]],
      // 'programmanagement.ProgramUpdatedDate': ['', [Validators.required]],
      // 'programmanagement.ProgramUpdatedBy': ['', [Validators.required]],
      'capabilities.CapabilityStatus': ['', [Validators.required]],
      //'programmanagement.SocialElements': ['', [Validators.required]],
      'capabilities.CapabilitySocialElements.MetaTitle': ['', [Validators.required]],

      'capabilities.CapabilitySocialElements.MetaKeywords': ['', [Validators.required]],
      'capabilities.CapabilitySocialElements.MetaDescrition': ['', [Validators.required]],

      'capabilities.ProgramCategory_Id':[''],

    });
  }

  startDate: any;
  validDate: any;
  onGetCapabilitiestById() {

    this.adminservice.onGetCapabilitiesById(this.ccId).subscribe(result => {
   //   alert(JSON.stringify(result));
      //this.status = result;
      if (this.status.StatusCode == "1") {


        this.capabilities = this.status.Data;
      }
      else {
        alert(JSON.stringify (this.status.Data));
      }


      this.status.Data.ProgramCategory_Id.forEach((tes) => {
        this.selectedcatagorys.push(tes);
      });
      
      console.log(this.capabilities);

      if (this.capabilities.CapabilityStartDate != null && this.capabilities.CapabilityStartDate != "" && this.capabilities.CapabilityStartDate != undefined) {
        this.startDate = this.datepipe.transform(this.capabilities.CapabilityStartDate, 'yyyy-MM-dd');
        this.capabilities.CapabilityStartDate = this.startDate;
      }



      if (this.capabilities.CapabilityValidTill != null && this.capabilities.CapabilityValidTill != "" && this.capabilities.CapabilityValidTill != undefined) {
        this.validDate = this.datepipe.transform(this.capabilities.CapabilityValidTill, 'yyyy-MM-dd');
        this.capabilities.CapabilityValidTill = this.validDate;
      }

      this.capabilities.CapabilitySocialElements = JSON.parse(this.capabilities.CapabilitySocialElements);

    });
    return this.capabilities;
  }
}
