import { Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { NotAuthGuard } from '../guards/notAuth.guard';

import { EventtypeComponent } from './eventtype.component';

export const EventtypeRoutes: Routes = [

  {
    path: '',
    component: EventtypeComponent,
    canActivate: [AuthGuard],

    data: {
      heading: 'Login'
    }
  },


];

