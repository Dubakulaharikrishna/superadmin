import { Component, OnInit,ElementRef } from '@angular/core';
//changes
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { DatePipe } from '@angular/common'
import { Subject } from 'rxjs/Subject';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';
import { ExcelService } from '../services/excel.service';
import { AdminService } from '../services/admin.service';
import { Status } from '../Models/Status';
import { Ng2SmartTableModule, LocalDataSource } from 'ng2-smart-table';
import { Eventtype } from '../Models/Eventtype';
import { Session } from 'selenium-webdriver';
import { AccountManagement } from '../Models/AccountManagement';
@Component({
  selector: 'app-eventtype',
  templateUrl: './eventtype.component.html',
  styleUrls: ['./eventtype.component.css'],
  providers: [ExcelService, DatePipe]
})
export class EventtypeComponent implements OnInit {
  MasterAccount: any;
  dataLoaded: any;
  eveicon: any;
  MasterAccount_Id: any;
  source: LocalDataSource;
  selectedValue: any = 50;
  page: any;
  listofAccounts: AccountManagement[];
  isSubmitted: boolean = false;
  public data: Object;
  public temp_var: Object = false;
  Instanceform: FormGroup;
  public form: FormGroup;
  eventtype: Eventtype;
  accountId; any;
  success: boolean = false;
  message: any;
  dtTrigger: Subject<any> = new Subject();
  public _success = new Subject<string>();
  staticAlertClosed = false;
  listofeventtypes: Eventtype[];
  id: any;
  createdby: any;
  pagename: any;
  roleId: any;
  status: Status;
  constructor(public router: Router, public datepipe: DatePipe, public http: HttpClient, public ele: ElementRef, private excelService: ExcelService, public fb: FormBuilder, private adminservice: AdminService) {

    this.eventtype = new Eventtype();
  }

  ngOnInit() {

   // localStorage.removeItem('loginSessId');
   // localStorage.clear();
   // localStorage.setTimeout = 1;
   // sessionStorage.removeItem('loginSessId');
   // localStorage.removeItem('loginSessId');
    this.pagename = "Event Type";
    localStorage.setItem('loginSessId', this.pagename);
    this.accountId = localStorage.getItem('AccountId');
    this.onGetAllEventtypes(this.accountId);
    this.Eventtypeform();
    this.onGetAllAccountsList();
    this.roleId = localStorage.getItem('RoleId');
    setTimeout(() => this.staticAlertClosed = true, 20000);
    this._success.subscribe((message) => this.message = message);
    this._success.debounceTime(7000).subscribe(() => this.message = null);
    this.createdby = localStorage.getItem('MemberName');

  }
  onGetAllAccountsList() {
    this.adminservice.onGetAllAccountsList().subscribe(result => {
      if (result.success == 1) {
        
        this.listofAccounts = result.data;

        //alert(JSON.stringify(this.listofAccounts));
        return this.listofAccounts
      }

    });
  }
  test(id) {
    //  alert("Hi");
    //console.log(id);
    //alert(id);
    console.log(this.MasterAccount_Id);
   // alert(this.MasterAccount_Id);
  }
  reset() {
    this.onGetAllEventtypes(this.accountId);
    this.MasterAccount = undefined;
  }

  openCpopup() {

    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "block";
    cmask.style.display = "block";
  }

  closeCpopup() {
    let cpoper = document.getElementById("crpop");
    let cmask = document.getElementById("crmask");
    cpoper.style.display = "none";
    cmask.style.display = "none";
  }

  onChange(event) {
    console.log(event);
    this.onGetAllEventtypes(event.target.value);
  }
  onEdit(event: any) {
    this.router.navigate(['/vieweventtype/', { eveId: event.data._id }]);
  }
  onGetAllEventtypes(id) {
   // let id = sessionStorage.getItem('AccountId');
    // alert(id);
    if (id == "") {
    id = null;
  }
    this.adminservice.onGetAllEventtypes(id).subscribe(result => {

      if (result.success == 1) {
      
        this.listofeventtypes = result.data;
        this.source = new LocalDataSource(this.listofeventtypes);
        // alert(JSON.stringify(this.Programstructures));
        return this.listofeventtypes
      }

    });
  }
  memberImage: any;
  readURL(event) {
    let files = event.target.files;
    this.memberImage = files[0];
  }


  onCreateEventType() {
    let formData = new FormData();

    //if (files.length > 0 && files.count != 0 && files != null) {

    //  let file = files[0];
    //  this.imageurl = file.name;
    formData.append('eventFile', this.memberImage, this.memberImage.name);
    //  console.log(formData);
    //  console.log(files);
    //  //this.adminservice.onfileupload(formData).subscribe(res => this.dataLoaded = res);
    //}
    //let files = this.ele.nativeElement.querySelector('#EventIcon').files;
    //if (files.length > 0 && files.count != 0 && files != null) {

    //  let formData = new FormData();
    //  let file = files[0];
    //  this.eveicon = file.name;
    //  formData.append('EventIcon', file, file.name);
    //  console.log(formData);
    //  console.log(files);
    //  this.adminservice.onfileupload(formData).subscribe(res => this.dataLoaded = res);
    //}
    let date = new Date();

    if (this.form.valid) {
      this.isSubmitted = false;
      // alert(JSON.stringify(license));
      const eve = {
        MasterAccount_Id: this.form.get('MasterAccount_Id').value,
        EventName: this.form.get('EventName').value,
        EventIcon: this.memberImage.name,
        EventDescription: this.form.get('EventDescription').value,
        EventStatus: this.form.get('EventStatus').value,
        EventCreatedBy: this.createdby,
      //  EventCreatedDate: this.datepipe.transform(date, "yyyy-MM-dd")
      }
      // alert(JSON.stringify(lic));
      this.adminservice.onCreateEventType(eve, formData).subscribe(result => {
        console.log(result);
        if (result.success == 1) {
          this.success = true;
          this.message = result.Message;
          this._success.next(`Record Saved Successfully`);

          this.onGetAllEventtypes(this.accountId);
          this.closeCpopup();

        }
        else {
          this.success = false;
          this.message = "Failed to create account master";
        }
      });
    }
    else {
      this.isSubmitted = true;
      //alert(JSON.stringify(license));
    }

    // alert(JSON.stringify(lic));
  }


  Eventtypeform() {
    this.form = this.fb.group({
      MasterAccount_Id: ['', [Validators.required]],
      EventName: ['', [Validators.required]],
      EventDescription: ['', [Validators.required]],
      EventStatus: ['', [Validators.required]],
     

    });
  }



  /*Smart table*/
  settings = {

    columns: {
      EventName: {
        title: 'Event Name',
        filter: false
      },
      EventStatus: {
        title: 'Event Status',
        filter: false
      },
      
    },
    attr: { class: 'table table-bordered' },
    actions: {
      edit: false, //as an example
      delete: false, //as an example
      add: false,
      position: 'right',
      custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button" (click)="onEdit(rec._id)">Edit</button></span>` }]
    },
    pager: { display: true, perPage: 50 },
  };

  //route(event) {
  //  alert('hi');
  //}

  onSearch(query: string = '') {
    if (query != '') {
      this.source.setFilter([
        // fields we want to include in the search
        {
          field: 'InstanceName',
          search: query
        },
        {
          field: 'InstallAdmin',
          search: query
        }
      ], false);
    }
    else {
      this.source = new LocalDataSource(this.listofeventtypes);
    }

    // second parameter specifying whether to perform 'AND' or 'OR' search 
    // (meaning all columns should contain search query or at least one)
    // 'AND' by default, so changing to 'OR' by setting false here
  }

  onSelectedFilter(event) {
    this.page = event.target.value;
    this.source = new LocalDataSource(this.listofeventtypes);
    this.settings = {

      columns: {
        EventName: {
          title: 'Event Name',
          filter: false
        },
        EventStatus: {
          title: 'Event Status',
          filter: false
        },
       
      },
      attr: { class: 'table table-bordered' },
      actions: {
        edit: false, //as an example
        delete: false, //as an example
        add: false,
        position: 'right',
        custom: [{ name: 'routeToAPage', title: `<span><button class="btn btn-defualt mybtnedit" type="button">Edit</button></span>` }]
      },
      pager: { display: true, perPage: this.page },
    };

  }

  onExport() {
    this.excelService.exportAsExcelFile(this.listofeventtypes, 'EventType');
  }


}
